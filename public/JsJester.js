
	// This is a javascript version of
	// Jester (http://www.ludochess.com/) - a strong java chess engine
	// A real treasure for the Chess society!
	// The author is Stephane N.B. Nguyen.
	//
	// This chess engine is a state-machine. 
	// Look auto-samples at the end to see it working.
	// Use chess logic before calling requests.
	// 
	// Javascript work by http://chessforeva.blogspot.com
	

	function _BTREE()
	{
	  var b = new Object();
	  b.replay = 0;
	  b.f = 0;
	  b.t = 0;
	  b.flags = 0;
	  b.score = 0;
	  return b;
	}

	
	function _GAMESTATS()
	{
	  var g = new Object();
	  g.mate = false;
	  g.timeout = false;
	  g.recapture = false;
	  return g;
	}
	
	function _INT()
	{
	  var x = new Object();
	  x.i = 0;
	  return x;
	}
	
	function _MOVES()
	{
	  var m = new Object();
	  m.gamMv = 0;
	  m.score = 0;
	  m.piece = 0;
	  m.color = 0;
	  return m;
	}

	 Js_maxDepth = 7;			// Search Depth setting for javascript (no timeout option)

	 Js_searchTimeout = 9 * 1000;		// 9 seconds for search allowed

	 Js_startTime = 0;
	 Js_nMovesMade = 0;
	 Js_computer = 0;
	 Js_player = 0;
	 Js_enemy = 0;

	 Js_fUserWin_kc = false;
	 Js_fInGame = false;


	 Js_fGameOver = false;

	 Js_fCheck_kc = false;
	 Js_fMate_kc = false;
	 Js_bDraw = 0;
	 Js_fStalemate = false;
	 Js_fSoonMate_kc = false;		// Algo detects soon checkmate
	 Js_fAbandon = false;			// Algo resigns, if game lost
	
	 Js_working = 0;
	 Js_working2 = 0;
	 Js_advX_pawn = 10;
	 Js_isoX_pawn = 7;
	 Js_pawnPlus = 0;
	 Js_castle_pawn = 0;
	 Js_bishopPlus = 0;
	 Js_adv_knight = 0;
	 Js_far_knight = 0;
	 Js_far_bishop = 0;
	 Js_king_agress = 0;

	 Js_junk_pawn = -15;
	 Js_stopped_pawn = -4;
	 Js_doubled_pawn = -14;
	 Js_bad_pawn = -4;
	 Js_semiOpen_rook = 10;
	 Js_semiOpen_rookOther = 4;
	 Js_rookPlus = 0;
	 Js_crossArrow = 8;
	 Js_pinnedVal = 10;
	 Js_semiOpen_king = 0;
	 Js_semiOpen_kingOther = 0;
	 Js_castle_K = 0;
	 Js_moveAcross_K = 0;
	 Js_safe_King = 0;

	 Js_agress_across = -6;
	 Js_pinned_p = -8;
	 Js_pinned_other = -12;

	 Js_nGameMoves = [];	//int[]
	 Js_depth_Seek = 0;
	 Js_c1 = 0;
	 Js_c2 = 0;
	 Js_agress2 = [];	//int[]
	 Js_agress1 = [];	//int[]
	 Js_ptValue = 0;
	 Js_flip = false;
	 Js_fEat = false;
	 Js_myPiece = "";

	 Js_fiftyMoves = 0;
	 Js_indenSqr = 0;
	 Js_realBestDepth = 0;
	 Js_realBestScore = 0;
	 Js_realBestMove = 0;
	 Js_lastDepth = 0;
	 Js_lastScore = 0;
	 Js_fKO = false;


	 Js_fromMySquare = 0;
	 Js_toMySquare = 0;
	 Js_cNodes = 0;
	 Js_scoreDither = 0;
	 Js__alpha = 0;
	 Js__beta = 0;
	 Js_dxAlphaBeta = 0;
	 Js_maxDepthSeek = 0;
	 Js_specialScore = 0;
	 Js_hint = 0;

	 Js_currentScore = 0;

	 Js_proPiece = 0;
	 Js_pawc1 = [];
	 Js_pawc2 = [];
	 Js_origSquare = 0;
	 Js_destSquare = 0;

	 Js_cCompNodes = 0;
	 Js_dxDither = 0;
	 Js_scoreWin0 = 0;
	 Js_scoreWin1 = 0;
	 Js_scoreWin2 = 0;
	 Js_scoreWin3 = 0;
	 Js_scoreWin4 = 0;

	 Js_USER_TOPLAY = 0;
	 Js_JESTER_TOPLAY = 1;

	 Js_hollow = 2;
	 Js_empty = 0;
	 Js_pawn = 1;
	 Js_knight = 2;
	 Js_bishop = 3;
	 Js_rook = 4;
	 Js_queen = 5;
	 Js_king = 6;

	 Js_white = 0;
	 Js_black = 1;

	 Js_N9 = 90;

	 Js_szIdMvt = "ABCDEFGH" + "IJKLMNOP" + "QRSTUVWX" + "abcdefgh" + "ijklmnop" + "qrstuvwx" + "01234567" + "89YZyz*+";
	 Js_szAlgMvt = [ "a1", "b1", "c1", "d1", "e1", "f1", "g1", "h1", "a2", "b2", "c2", "d2", "e2", "f2", "g2", "h2", "a3", "b3", "c3", "d3", "e3", "f3", "g3", "h3", "a4", "b4", "c4", "d4", "e4", "f4", "g4", "h4", "a5", "b5", "c5", "d5", "e5", "f5", "g5", "h5", "a6", "b6", "c6", "d6", "e6", "f6", "g6", "h6", "a7", "b7", "c7", "d7", "e7", "f7", "g7", "h7", "a8", "b8", "c8", "d8", "e8", "f8", "g8", "h8" ];

	 Js_color_sq = [ 0, 1, 0, 1, 0, 1, 0, 1, 1, 0, 1, 0, 1, 0, 1, 0, 0, 1, 0, 1, 0, 1, 0, 1, 1, 0, 1, 0, 1, 0, 1, 0, 0, 1, 0, 1, 0, 1, 0, 1, 1, 0, 1, 0, 1, 0, 1, 0, 0, 1, 0, 1, 0, 1, 0, 1, 1, 0, 1, 0, 1, 0, 1, 0 ];

	 Js_bkPawn = 7;
	 Js_pawn_msk = 7;
	 Js_promote = 8;
	 Js_castle_msk = 16;
	 Js_enpassant_msk = 32;
	 Js__idem = 64;
	 Js_menace_pawn = 128;
	 Js_check = 256;
	 Js_capture = 512;
	 Js_draw = 1024;
	 Js_pawnVal = 100;
	 Js_knightVal = 350;
	 Js_bishopVal = 355;
	 Js_rookVal = 550;
	 Js_queenVal = 1050;
	 Js_kingVal = 1200;
	 Js_xltP = 16384;
	 Js_xltN = 10240;
	 Js_xltB = 6144;
	 Js_xltR = 1024;
	 Js_xltQ = 512;
	 Js_xltK = 256;
	 Js_xltBQ = 4608;
	 Js_xltBN = 2048;
	 Js_xltRQ = 1536;
	 Js_xltNN = 8192;

	 Js_movesList = [];			//new _MOVES[512];
	 Js_flag = new _GAMESTATS();		//new _GAMESTATS();
	 Js_Tree = [];				//new _BTREE[2000];
	 Js_root = new _BTREE();
	 Js_tmpTree = new _BTREE();		//new _BTREE();
	 Js_treePoint = [];			//new int[Js_maxDepth];
	 Js_board = [];				//new int[64];
	 Js_color = [];				//new int[64];
	 Js_pieceMap = [[]];			//new int[2][16];
	 Js_pawnMap = [[]];			//new int[2][8];
	 Js_roquer = [ 0, 0 ];
	 Js_nMvtOnBoard = [];			//new int[64];
	 Js_scoreOnBoard = [];			//new int[64];
	 Js_gainScore = new _INT();

	 Js_otherTroop = [ 1, 0, 2 ];
	 Js_variants = [];			//new int[Js_maxDepth];
	 Js_pieceIndex = [];			//new int[64];
	 Js_piecesCount = [ 0, 0 ];
	 Js_arrowData = [];			//new int[4200];
	 Js_crossData = [];			//new int[4200];
	 Js_agress = [[]];			//new int[2][64];
	 Js_matrl = [ 0, 0 ];
	 Js_pmatrl = [ 0, 0 ];
	 Js_ematrl = [ 0, 0 ];
	 Js_pinned = [ 0, 0 ];
	 Js_withPawn = [ 0, 0 ];
	 Js_withKnight = [ 0, 0 ];
	 Js_withBishop = [ 0, 0 ];
	 Js_withRook = [ 0, 0 ];
	 Js_withQueen = [ 0, 0 ];
	 Js_flagCheck = [];			//new int[Js_maxDepth];
	 Js_flagEat = [];			//new int[Js_maxDepth];
	 Js_menacePawn = [];			//new int[Js_maxDepth];
	 Js_scorePP = [];			//new int[Js_maxDepth];
	 Js_scoreTP = [];			//new int[Js_maxDepth];
	 Js_eliminate0 = [];			//new int[Js_maxDepth]; 
	 Js_eliminate1 = [];			//new int[Js_maxDepth];
	 Js_eliminate2 = [];			//new int[Js_maxDepth];
	 Js_eliminate3 = [];			//new int[Js_maxDepth];
	 Js_storage = [];			//new short[10000];
	 Js_wPawnMvt = [];			//new int[64];
	 Js_bPawnMvt = [];			//new int[64];
	 Js_knightMvt = [[]];			//new int[2][64];
	 Js_bishopMvt = [[]];			//new int[2][64];
	 Js_kingMvt = [[]];			//new int[2][64];
	 Js_killArea = [[]];			//new int[2][64];
	 Js_fDevl = [ 0, 0 ];
	 Js_nextCross = [];			//new char[40000];
	 Js_nextArrow = [];			//new char[40000];
	 Js_tmpCh = [];				//new char[20];
	 Js_movCh = [];				//new char[8];
	 Js_b_r = [];				//new int[64];

	 Js_upperNot = [ ' ', 'P', 'N', 'B', 'R', 'Q', 'K' ];
	 Js_lowerNot = [ ' ', 'p', 'n', 'b', 'r', 'q', 'k' ];
	 Js_rgszPiece = [ "", "", "N", "B", "R", "Q", "K" ];
	 Js_asciiMove = [ [ ' ', ' ', ' ', ' ', ' ', ' ' ], [ ' ', ' ', ' ', ' ', ' ', ' ' ], [ ' ', ' ', ' ', ' ', ' ', ' ' ], [ ' ', ' ', ' ', ' ', ' ', ' ' ] ];
	 Js_reguBoard = [ Js_rook, Js_knight, Js_bishop, Js_queen, Js_king, Js_bishop, Js_knight, Js_rook, Js_pawn, Js_pawn, Js_pawn, Js_pawn, Js_pawn, Js_pawn, Js_pawn, Js_pawn, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, Js_pawn, Js_pawn, Js_pawn, Js_pawn, Js_pawn, Js_pawn, Js_pawn, Js_pawn, Js_rook, Js_knight, Js_bishop, Js_queen, Js_king, Js_bishop, Js_knight, Js_rook ];
	 Js_reguColor = [ Js_white, Js_white, Js_white, Js_white, Js_white, Js_white, Js_white, Js_white, Js_white, Js_white, Js_white, Js_white, Js_white, Js_white, Js_white, Js_white, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, Js_black, Js_black, Js_black, Js_black, Js_black, Js_black, Js_black, Js_black, Js_black, Js_black, Js_black, Js_black, Js_black, Js_black, Js_black, Js_black ];
	 Js_pieceTyp = [ [ Js_empty, Js_pawn, Js_knight, Js_bishop, Js_rook, Js_queen, Js_king, Js_empty ], [ Js_empty, Js_bkPawn, Js_knight, Js_bishop, Js_rook, Js_queen, Js_king, Js_empty ] ];
	 Js_direction = [ [ 0, 0, 0, 0, 0, 0, 0, 0 ], [ 10, 9, 11, 0, 0, 0, 0, 0 ], [ 8, -8, 12, -12, 19, -19, 21, -21 ], [ 9, 11, -9, -11, 0, 0, 0, 0 ], [ 1, 10, -1, -10, 0, 0, 0, 0 ], [ 1, 10, -1, -10, 9, 11, -9, -11 ], [ 1, 10, -1, -10, 9, 11, -9, -11 ], [ -10, -9, -11, 0, 0, 0, 0, 0 ] ];
	 Js_maxJobs = [ 0, 2, 1, 7, 7, 7, 1, 2 ];
	 Js_virtualBoard = [ -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 0, 1, 2, 3, 4, 5, 6, 7, -1, -1, 8, 9, 10, 11, 12, 13, 14, 15, -1, -1, 16, 17, 18, 19, 20, 21, 22, 23, -1, -1, 24, 25, 26, 27, 28, 29, 30, 31, -1, -1, 32, 33, 34, 35, 36, 37, 38, 39, -1, -1, 40, 41, 42, 43, 44, 45, 46, 47, -1, -1, 48, 49, 50, 51, 52, 53, 54, 55, -1, -1, 56, 57, 58, 59, 60, 61, 62, 63, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 ];
	 Js_start_K = [ 0, 0, -4, -10, -10, -4, 0, 0, -4, -4, -8, -12, -12, -8, -4, -4, -12, -16, -20, -20, -20, -20, -16, -12, -16, -20, -24, -24, -24, -24, -20, -16, -16, -20, -24, -24, -24, -24, -20, -16, -12, -16, -20, -20, -20, -20, -16, -12, -4, -4, -8, -12, -12, -8, -4, -4, 0, 0, -4, -10, -10, -4, 0, 0 ];
	 Js_end_K = [ 0, 6, 12, 18, 18, 12, 6, 0, 6, 12, 18, 24, 24, 18, 12, 6, 12, 18, 24, 30, 30, 24, 18, 12, 18, 24, 30, 36, 36, 30, 24, 18, 18, 24, 30, 36, 36, 30, 24, 18, 12, 18, 24, 30, 30, 24, 18, 12, 6, 12, 18, 24, 24, 18, 12, 6, 0, 6, 12, 18, 18, 12, 6, 0 ];
	 Js_vanish_K = [ 0, 8, 16, 24, 24, 16, 8, 0, 8, 32, 40, 48, 48, 40, 32, 8, 16, 40, 56, 64, 64, 56, 40, 16, 24, 48, 64, 72, 72, 64, 48, 24, 24, 48, 64, 72, 72, 64, 48, 24, 16, 40, 56, 64, 64, 56, 40, 16, 8, 32, 40, 48, 48, 40, 32, 8, 0, 8, 16, 24, 24, 16, 8, 0 ];
	 Js_end_KBNK = [ 99, 90, 80, 70, 60, 50, 40, 40, 90, 80, 60, 50, 40, 30, 20, 40, 80, 60, 40, 30, 20, 10, 30, 50, 70, 50, 30, 10, 0, 20, 40, 60, 60, 40, 20, 0, 10, 30, 50, 70, 50, 30, 10, 20, 30, 40, 60, 80, 40, 20, 30, 40, 50, 60, 80, 90, 40, 40, 50, 60, 70, 80, 90, 99 ];
	 Js_knight_pos = [ 0, 4, 8, 10, 10, 8, 4, 0, 4, 8, 16, 20, 20, 16, 8, 4, 8, 16, 24, 28, 28, 24, 16, 8, 10, 20, 28, 32, 32, 28, 20, 10, 10, 20, 28, 32, 32, 28, 20, 10, 8, 16, 24, 28, 28, 24, 16, 8, 4, 8, 16, 20, 20, 16, 8, 4, 0, 4, 8, 10, 10, 8, 4, 0 ];
	 Js_bishop_pos = [ 14, 14, 14, 14, 14, 14, 14, 14, 14, 22, 18, 18, 18, 18, 22, 14, 14, 18, 22, 22, 22, 22, 18, 14, 14, 18, 22, 22, 22, 22, 18, 14, 14, 18, 22, 22, 22, 22, 18, 14, 14, 18, 22, 22, 22, 22, 18, 14, 14, 22, 18, 18, 18, 18, 22, 14, 14, 14, 14, 14, 14, 14, 14, 14 ];
	 Js_pawn_pos = [ 0, 0, 0, 0, 0, 0, 0, 0, 4, 4, 4, 0, 0, 4, 4, 4, 6, 8, 2, 10, 10, 2, 8, 6, 6, 8, 12, 16, 16, 12, 8, 6, 8, 12, 16, 24, 24, 16, 12, 8, 12, 16, 24, 32, 32, 24, 16, 12, 12, 16, 24, 32, 32, 24, 16, 12, 0, 0, 0, 0, 0, 0, 0, 0 ];
	 Js_valueMap = [ 0, Js_pawnVal, Js_knightVal, Js_bishopVal, Js_rookVal, Js_queenVal, Js_kingVal ];
	 Js_xlat = [ 0, Js_xltP, Js_xltN, Js_xltB, Js_xltR, Js_xltQ, Js_xltK ];
	 Js_pss_pawn0 = [ 0, 60, 80, 120, 200, 360, 600, 800 ];
	 Js_pss_pawn1 = [ 0, 30, 40, 60, 100, 180, 300, 800 ];
	 Js_pss_pawn2 = [ 0, 15, 25, 35, 50, 90, 140, 800 ];
	 Js_pss_pawn3 = [ 0, 5, 10, 15, 20, 30, 140, 800 ];
	 Js_isol_pawn = [ -12, -16, -20, -24, -24, -20, -16, -12 ];
	 Js_takeBack = [ -6, -10, -15, -21, -28, -28, -28, -28, -28, -28, -28, -28, -28, -28, -28, -28 ];
	 Js_mobBishop = [ -2, 0, 2, 4, 6, 8, 10, 12, 13, 14, 15, 16, 16, 16 ];
	 Js_mobRook = [ 0, 2, 4, 6, 8, 10, 11, 12, 13, 14, 14, 14, 14, 14, 14 ];
	 Js_menaceKing = [ 0, -8, -20, -36, -52, -68, -80, -80, -80, -80, -80, -80, -80, -80, -80, -80, -80, -80, -80, -80, -80, -80, -80, -80, -80, -80, -80, -80, -80, -80, -80, -80, -80, -80, -80, -80 ];
	 Js_queenRook = [ 0, 56, 0 ];
	 Js_kingRook = [ 7, 63, 0 ];
	 Js_kingPawn = [ 4, 60, 0 ];
	 Js_raw7 = [ 6, 1, 0 ];
	 Js_heavy = [ false, false, false, true, true, true, false, false ];

	 Js_AUTHOR = "Copyright � 1998-2002 - Stephane N.B. Nguyen - Vaureal, FRANCE";
	 Js_WEBSITE = "http://www.ludochess.com/";
	 Js_STR_COPY = "JESTER 1.10e by " + Js_AUTHOR + Js_WEBSITE;

	  
	function BoardCpy(a, b)
	{
	  var sq = 0;
	  do b[sq] = a[sq]; while (++sq < 64);
	}
	 
	 
	function WatchPosit()
	{
	  var PawnStorm = false;
	  var i=0; 
	  Agression(Js_white, Js_agress[Js_white]);
	  Agression(Js_black, Js_agress[Js_black]);
	 
	  ChangeForce();
	  Js_withKnight[Js_white] = 0;
	  Js_withKnight[Js_black] = 0;
	  Js_withBishop[Js_white] = 0;
	  Js_withBishop[Js_black] = 0;
	  Js_withRook[Js_white] = 0;
	  Js_withRook[Js_black] = 0;
	  Js_withQueen[Js_white] = 0;
	  Js_withQueen[Js_black] = 0;
	  Js_withPawn[Js_white] = 0;
	  Js_withPawn[Js_black] = 0;
	  for (var side = Js_white; side <= Js_black; ++side) {
	    for (i = Js_piecesCount[side]; i >= 0; --i)
	    {
	      var b = Js_board[Js_pieceMap[side][i]];
	      if (b == Js_knight)
	        Js_withKnight[side] += 1;
	      else if (b == Js_bishop)
	        Js_withBishop[side] += 1;
	      else if (b == Js_rook)
	        Js_withRook[side] += 1;
	      else if (b == Js_queen)
	        Js_withQueen[side] += 1;
	      else if (b == Js_pawn) {
	        Js_withPawn[side] += 1;
	      }
	    }
	  }
	 
	  if (Js_fDevl[Js_white] == 0)
	  {
	    Js_fDevl[Js_white] = (((Js_board[1] == Js_knight) || (Js_board[2] == Js_bishop) || (Js_board[5] == Js_bishop) || (Js_board[6] == Js_knight)) ? 0 : 1);
	  }
	 
	  if (Js_fDevl[Js_black] == 0)
	  {
	    Js_fDevl[Js_black] = (((Js_board[57] == Js_knight) || (Js_board[58] == Js_bishop) || (Js_board[61] == Js_bishop) || (Js_board[62] == Js_knight)) ? 0 : 1);
	  }
	 
	  if ((!(PawnStorm)) && (Js_working < 5))
	  {
	    PawnStorm = ((IColmn(Js_pieceMap[Js_white][0]) < 3) && (IColmn(Js_pieceMap[Js_black][0]) > 4)) || (
	      (IColmn(Js_pieceMap[Js_white][0]) > 4) && (IColmn(Js_pieceMap[Js_black][0]) < 3));
	  }
	 
	  //BoardCpy(Js_knight_pos, Js_knightMvt[Js_white]);
	  //BoardCpy(Js_knight_pos, Js_knightMvt[Js_black]);
	  //BoardCpy(Js_bishop_pos, Js_bishopMvt[Js_white]);
	  //BoardCpy(Js_bishop_pos, Js_bishopMvt[Js_black]);

	  //slice is faster
	  Js_knightMvt[Js_white] = Js_knight_pos.slice();
	  Js_knightMvt[Js_black] = Js_knight_pos.slice();
	  Js_bishopMvt[Js_white] = Js_bishop_pos.slice();
	  Js_bishopMvt[Js_black] = Js_bishop_pos.slice();

	  MixBoard(Js_start_K, Js_end_K, Js_kingMvt[Js_white]);
	  MixBoard(Js_start_K, Js_end_K, Js_kingMvt[Js_black]);
	 
	  var sq = 0;
	  do {
	    var fyle = IColmn(sq);
	    var rank = IRaw(sq);
	    var bstrong = 1;
	    var wstrong = 1;
	    for (i = sq; i < 64; i += 8) {
	      if (!(Pagress(Js_black, i)))
	        continue;
	      wstrong = 0;
	      break;
	    }
	    for (i = sq; i >= 0; i -= 8) {
	      if (!(Pagress(Js_white, i)))
	        continue;
	      bstrong = 0;
	      break;
	    }
	    var bpadv = Js_advX_pawn;
	    var wpadv = Js_advX_pawn;
	    if ((((fyle == 0) || (Js_pawnMap[Js_white][(fyle - 1)] == 0))) && ((
	      (fyle == 7) || (Js_pawnMap[Js_white][(fyle + 1)] == 0))))
	      wpadv = Js_isoX_pawn;
	    if ((((fyle == 0) || (Js_pawnMap[Js_black][(fyle - 1)] == 0))) && ((
	      (fyle == 7) || (Js_pawnMap[Js_black][(fyle + 1)] == 0))))
	      bpadv = Js_isoX_pawn;
	    Js_wPawnMvt[sq] = (wpadv * Js_pawn_pos[sq] / 10);
	    Js_bPawnMvt[sq] = (bpadv * Js_pawn_pos[(63 - sq)] / 10);
	    Js_wPawnMvt[sq] += Js_pawnPlus;
	    Js_bPawnMvt[sq] += Js_pawnPlus;
	    if (Js_nMvtOnBoard[Js_kingPawn[Js_white]] != 0)
	    {
	      if ((((fyle < 3) || (fyle > 4))) && (IArrow(sq, Js_pieceMap[Js_white][0]) < 3))
	        Js_wPawnMvt[sq] += Js_castle_pawn;
	    }
	    else if ((rank < 3) && (((fyle < 2) || (fyle > 5))))
	      Js_wPawnMvt[sq] += Js_castle_pawn / 2;
	    if (Js_nMvtOnBoard[Js_kingPawn[Js_black]] != 0)
	    {
	      if ((((fyle < 3) || (fyle > 4))) && (IArrow(sq, Js_pieceMap[Js_black][0]) < 3))
	        Js_bPawnMvt[sq] += Js_castle_pawn;
	    }
	    else if ((rank > 4) && (((fyle < 2) || (fyle > 5))))
	      Js_bPawnMvt[sq] += Js_castle_pawn / 2;
	    if (PawnStorm)
	    {
	      if (((IColmn(Js_pieceMap[Js_white][0]) < 4) && (fyle > 4)) || (
	        (IColmn(Js_pieceMap[Js_white][0]) > 3) && (fyle < 3)))
	        Js_wPawnMvt[sq] += 3 * rank - 21;
	      if (((IColmn(Js_pieceMap[Js_black][0]) < 4) && (fyle > 4)) || (
	        (IColmn(Js_pieceMap[Js_black][0]) > 3) && (fyle < 3)))
	        Js_bPawnMvt[sq] -= 3 * rank;
	    }
	    Js_knightMvt[Js_white][sq] += 5 - IArrow(sq, Js_pieceMap[Js_black][0]);
	    Js_knightMvt[Js_white][sq] += 5 - IArrow(sq, Js_pieceMap[Js_white][0]);
	    Js_knightMvt[Js_black][sq] += 5 - IArrow(sq, Js_pieceMap[Js_white][0]);
	    Js_knightMvt[Js_black][sq] += 5 - IArrow(sq, Js_pieceMap[Js_black][0]);
	    Js_bishopMvt[Js_white][sq] += Js_bishopPlus;
	    Js_bishopMvt[Js_black][sq] += Js_bishopPlus;
	
	    for (i = Js_piecesCount[Js_black]; i >= 0; --i)
	    {
	      var pMap2 = Js_pieceMap[Js_black][i];
	      if (IArrow(sq, pMap2) < 3)
	        Js_knightMvt[Js_white][sq] += Js_adv_knight;
	    }
	    for (i = Js_piecesCount[Js_white]; i >= 0; --i)
	    {
	      pMap2 = Js_pieceMap[Js_white][i];
	      if (IArrow(sq, pMap2) < 3) {
	        Js_knightMvt[Js_black][sq] += Js_adv_knight;
	      }
	    }
	 
	    if (wstrong != 0)
	      Js_knightMvt[Js_white][sq] += Js_far_knight;
	    if (bstrong != 0) {
	      Js_knightMvt[Js_black][sq] += Js_far_knight;
	    }
	    if (wstrong != 0)
	      Js_bishopMvt[Js_white][sq] += Js_far_bishop;
	    if (bstrong != 0) {
	      Js_bishopMvt[Js_black][sq] += Js_far_bishop;
	    }
	 
	    if (Js_withBishop[Js_white] == 2)
	      Js_bishopMvt[Js_white][sq] += 8;
	    if (Js_withBishop[Js_black] == 2)
	      Js_bishopMvt[Js_black][sq] += 8;
	    if (Js_withKnight[Js_white] == 2)
	      Js_knightMvt[Js_white][sq] += 5;
	    if (Js_withKnight[Js_black] == 2) {
	      Js_knightMvt[Js_black][sq] += 5;
	    }
	    Js_killArea[Js_white][sq] = 0;
	    Js_killArea[Js_black][sq] = 0;
	    if (IArrow(sq, Js_pieceMap[Js_white][0]) == 1)
	      Js_killArea[Js_black][sq] = Js_king_agress;
	    if (IArrow(sq, Js_pieceMap[Js_black][0]) == 1) {
	      Js_killArea[Js_white][sq] = Js_king_agress;
	    }
	    var Pd = 0;
	    var pp;
	    var z;
	    var j;
	    for (var k = 0; k <= Js_piecesCount[Js_white]; ++k)
	    {
	      i = Js_pieceMap[Js_white][k];
	      if (Js_board[i] != Js_pawn)
	        continue;
	      pp = 1;
	      if (IRaw(i) == 6)
	        z = i + 8;
	      else
	        z = i + 16;
	      for (j = i + 8; j < 64; j += 8) {
	        if ((!(Pagress(Js_black, j))) && (Js_board[j] != Js_pawn))
	          continue;
	        pp = 0;
	        break;
	      }
	      if (pp != 0)
	        Pd += 5 * Js_crossData[(sq * 64 + z)];
	      else {
	        Pd += Js_crossData[(sq * 64 + z)];
	      }
	    }
	    for (var k = 0; k <= Js_piecesCount[Js_black]; ++k)
	    {
	      i = Js_pieceMap[Js_black][k];
	      if (Js_board[i] != Js_pawn)
	        continue;
	      pp = 1;
	      if (IRaw(i) == 1)
	        z = i - 8;
	      else
	        z = i - 16;
	      for (j = i - 8; j >= 0; j -= 8) {
	        if ((!(Pagress(Js_white, j))) && (Js_board[j] != Js_pawn))
	          continue;
	        pp = 0;
	        break;
	      }
	      if (pp != 0)
	        Pd += 5 * Js_crossData[(sq * 64 + z)];
	      else {
	        Pd += Js_crossData[(sq * 64 + z)];
	      }
	    }
	    if (Pd == 0)
	      continue;
	    var val = Pd * Js_working2 / 10;
	    Js_kingMvt[Js_white][sq] -= val;
	    Js_kingMvt[Js_black][sq] -= val;
	  }
	  while (++sq < 64);
	}
	 
	 
	function CalcKBNK(winner, king1, king2)
	{
	  var end_KBNKsq = 0;
	 
	  var sq = 0;
	  do
	    if (Js_board[sq] == Js_bishop)
	      if (IRaw(sq) % 2 == IColmn(sq) % 2)
	        end_KBNKsq = 0;
	      else
	        end_KBNKsq = 7;
	  while (++sq < 64);
	 
	  var s = Js_ematrl[winner] - 300;
	  if (end_KBNKsq == 0)
	    s += Js_end_KBNK[king2];
	  else
	    s += Js_end_KBNK[Iwxy(IRaw(king2), 7 - IColmn(king2))];
	  s -= Js_crossData[(king1 * 64 + king2)];
	  s -= IArrow(Js_pieceMap[winner][1], king2);
	  s -= IArrow(Js_pieceMap[winner][2], king2);
	  return s;
	}
	 
	 
	function ChangeForce()
	{
	  Js_ematrl[Js_white] = (Js_matrl[Js_white] - Js_pmatrl[Js_white] - Js_kingVal);
	  Js_ematrl[Js_black] = (Js_matrl[Js_black] - Js_pmatrl[Js_black] - Js_kingVal);
	  var tmatrl = Js_ematrl[Js_white] + Js_ematrl[Js_black];
	  var s1 = (tmatrl < 1400) ? 10 : (tmatrl > 6600) ? 0 : (6600 - tmatrl) / 520;
	  if (s1 == Js_working)
	    return;
	  Js_working = s1;
	  Js_working2 = ((tmatrl < 1400) ? 10 : (tmatrl > 3600) ? 0 : (3600 - tmatrl) / 220);
	  
	  Js_castle_pawn = (10 - Js_working);

	  Js_pawnPlus = Js_working;
	 
	  Js_adv_knight = ((Js_working + 2) / 3);
	  Js_far_knight = ((Js_working + 6) / 2);
	 
	  Js_far_bishop = ((Js_working + 6) / 2);
	  Js_bishopPlus = (2 * Js_working);
	 
	  Js_rookPlus = (6 * Js_working);
	 
	  Js_semiOpen_king = ((3 * Js_working - 30) / 2);
	  Js_semiOpen_kingOther = (Js_semiOpen_king / 2);
	  Js_castle_K = (10 - Js_working);
	  Js_moveAcross_K = (-40 / (Js_working + 1));
	  Js_king_agress = ((10 - Js_working) / 2);
	  if (Js_working < 8)
	    Js_safe_King = (16 - (2 * Js_working));
	  else {
	    Js_safe_King = 0;
	  }

	}
	 
	 
	function Undo()
	{
	  var f = Js_movesList[Js_nGameMoves].gamMv >> 8;
	  var t = Js_movesList[Js_nGameMoves].gamMv & 0xFF;
	  if ((Js_board[t] == Js_king) && (IArrow(t, f) > 1))
	  {
	    DoCastle(Js_movesList[Js_nGameMoves].color, f, t, 2);
	  }
	  else
	  {
	    if (((Js_color[t] == Js_white) && (IRaw(f) == 6) && (IRaw(t) == 7)) || (
	      (Js_color[t] == Js_black) && (IRaw(f) == 1) && (IRaw(t) == 0)))
	    {
	      var from = f;
	      for (var g = Js_nGameMoves - 1; g > 0; --g)
	      {
	        if ((Js_movesList[g].gamMv & 0xFF) != from)
	          continue;
	        from = Js_movesList[g].gamMv >> 8;
	      }
	 
	      if (((Js_color[t] == Js_white) && (IRaw(from) == 1)) || ((Js_color[t] == Js_black) && (IRaw(from) == 6)))
	      {
	        Js_board[t] = Js_pawn;
	      }
	    }
	    Js_board[f] = Js_board[t];
	    Js_color[f] = Js_color[t];
	    Js_board[t] = Js_movesList[Js_nGameMoves].piece;
	    Js_color[t] = Js_movesList[Js_nGameMoves].color;
	    if (Js_color[t] != Js_hollow) Js_nMvtOnBoard[t] += -1;
	    Js_nMvtOnBoard[f] += -1;
	  }
	 
	  Js_nGameMoves += -1;
	  if( Js_fiftyMoves < Js_nGameMoves ) Js_fiftyMoves = Js_nGameMoves;

	  Js_computer = Js_otherTroop[Js_computer];
	  Js_enemy = Js_otherTroop[Js_enemy];
	  Js_flag.mate = false;
	  Js_depth_Seek = 0;

	  UpdateDisplay();

	  InitStatus();
			
	}
	 
	function ISqAgrs(sq,side)
	{
	  var xside = Js_otherTroop[side];
	 
	  var idir = Js_pieceTyp[xside][Js_pawn] * 64 * 64 + sq * 64;
	 
	  var u = Js_nextArrow[(idir + sq)];
	  if (u != sq)
	  {
	    if ((Js_board[u] == Js_pawn) && (Js_color[u] == side)) {
	      return 1;
	    }
	    u = Js_nextArrow[(idir + u)];
	    if ((u != sq) && (Js_board[u] == Js_pawn) && (Js_color[u] == side)) {
	      return 1;
	    }
	  }
	  if (IArrow(sq, Js_pieceMap[side][0]) == 1) {
	    return 1;
	  }
	 
	  var ipos = Js_bishop * 64 * 64 + sq * 64;
	  idir = ipos;
	 
	  u = Js_nextCross[(ipos + sq)];
	  do if (Js_color[u] == Js_hollow)
	    {
	      u = Js_nextCross[(ipos + u)];
	    }
	    else {
	      if ((Js_color[u] == side) && (((Js_board[u] == Js_queen) || (Js_board[u] == Js_bishop)))) {
	        return 1;
	      }
	      u = Js_nextArrow[(idir + u)];
	    }
	 
	  while (u != sq);
	 
	  ipos = Js_rook * 64 * 64 + sq * 64;
	  idir = ipos;
	 
	  u = Js_nextCross[(ipos + sq)];
	  do if (Js_color[u] == Js_hollow)
	    {
	      u = Js_nextCross[(ipos + u)];
	    }
	    else {
	      if ((Js_color[u] == side) && (((Js_board[u] == Js_queen) || (Js_board[u] == Js_rook)))) {
	        return 1;
	      }
	      u = Js_nextArrow[(idir + u)];
	    }
	 
	  while (u != sq);
	 
	  idir = Js_knight * 64 * 64 + sq * 64;
	 
	  u = Js_nextArrow[(idir + sq)];
	  do { if ((Js_color[u] == side) && (Js_board[u] == Js_knight))
	    {
	      return 1;
	    }
	    u = Js_nextArrow[(idir + u)];
	  }
	  while (u != sq);
	  return 0;
	}
	 
	function Iwxy(a,b)
	{
	  return (a << 3 | b);
	}
	 
	function XRayBR(sq, s, mob)
	{
	  var Kf = Js_killArea[Js_c1];
	  mob.i = 0;
	  var piece = Js_board[sq];
	 
	  var ipos = piece * 64 * 64 + sq * 64;
	  var idir = ipos;
	 
	  var u = Js_nextCross[(ipos + sq)];
	  var pin = -1;
	  do { s.i += Kf[u];
	 
	    if (Js_color[u] == Js_hollow)
	    {
	      mob.i += 1;
	 
	      if (Js_nextCross[(ipos + u)] == Js_nextArrow[(idir + u)])
	        pin = -1;
	      u = Js_nextCross[(ipos + u)];
	    }
	    else if (pin < 0)
	    {
	      if ((Js_board[u] == Js_pawn) || (Js_board[u] == Js_king)) {
	        u = Js_nextArrow[(idir + u)];
	      }
	      else {
	        if (Js_nextCross[(ipos + u)] != Js_nextArrow[(idir + u)])
	          pin = u;
	        u = Js_nextCross[(ipos + u)];
	      }
	    }
	    else
	    {
	      if ((Js_color[u] == Js_c2) && (((Js_board[u] > piece) || (Js_agress2[u] == 0))))
	      {
	        if (Js_color[pin] == Js_c2)
	        {
	          s.i += Js_pinnedVal;
	          if ((Js_agress2[pin] == 0) || (Js_agress1[pin] > Js_xlat[Js_board[pin]] + 1))
	            Js_pinned[Js_c2] += 1;
	        }
	        else {
	          s.i += Js_crossArrow; }
	      }
	      pin = -1;
	      u = Js_nextArrow[(idir + u)];
	    }
	  }
	  while (u != sq);
	}
	 
	function ComputerMvt()
	{
	  if (Js_flag.mate) {
	    return;
	  }
	  Js_startTime = (new Date()).getTime();

	  ChoiceMov(Js_computer, 1)
	  IfCheck();
	  if (!(Js_fUserWin_kc)) ShowMov(Js_asciiMove[0]);
	  if (!(CheckMatrl())) Js_bDraw = 1;
	  ShowStat();

	}
	 
	function InitMoves() {
	  var dest = [[]];
	  var steps = [];
	  var sorted = [];
	 
	  var ptyp = 0;
	  var po;
	  var p0;
	  do { po = 0;
	    do { p0 = 0;
	      do {
	        var i = ptyp * 64 * 64 + po * 64 + p0;
	        Js_nextCross[i] = po;	//(char)
	        Js_nextArrow[i] = po;	//(char)
	      }
	      while (++p0 < 64);
	    }
	    while (++po < 64);
	  }
	  while (++ptyp < 8);
	 
	  ptyp = 1;
	  do { po = 21;
	    do { if (Js_virtualBoard[po] < 0) {
	        continue;
	      }
	 
	      var ipos = ptyp * 64 * 64 + Js_virtualBoard[po] * 64;
	 
	      var idir = ipos;
	 
	      var d = 0;
		   var di = 0;
	        
	      var s;
	      do
	      {
	        dest[d] = [];				// creates object
	        dest[d][0] = Js_virtualBoard[po];
	        var delta = Js_direction[ptyp][d];
	        if (delta != 0)
	        {
	          p0 = po;
	          for (s = 0; s < Js_maxJobs[ptyp]; ++s)
	          {
	            p0 += delta;
	 
	            if ((Js_virtualBoard[p0] < 0) || (
	              (((ptyp == Js_pawn) || (ptyp == Js_bkPawn))) && (s > 0) && (((d > 0) || (Js_reguBoard[Js_virtualBoard[po]] != Js_pawn))))) {
	              break;
	            }
	            dest[d][s] = Js_virtualBoard[p0];
	          }
	        }
	        else {
	          s = 0;
	        }
	 
	        steps[d] = s;
		     for (di = d; (s > 0) && (di > 0); --di)
	          if (steps[sorted[(di - 1)]] == 0)
	            sorted[di] = sorted[(di - 1)];
	          else
	            break;
	        sorted[di] = d;
	      }
	      while (++d < 8);
	 
	      p0 = Js_virtualBoard[po];
	      if ((ptyp == Js_pawn) || (ptyp == Js_bkPawn))
	      {
	        for (s = 0; s < steps[0]; ++s)
	        {
	          Js_nextCross[(ipos + p0)] = dest[0][s];			//(char)
	          p0 = dest[0][s];
	        }
	        p0 = Js_virtualBoard[po];
	        d = 1;
	        do
	        {
	          Js_nextArrow[(idir + p0)] = dest[d][0];			//(char)
	          p0 = dest[d][0];
	        }
	        while (++d < 3);
	      }
	      else
	      {
	        Js_nextArrow[(idir + p0)] = dest[sorted[0]][0];			//(char)
	        d = 0;
	        do for (s = 0; s < steps[sorted[d]]; ++s)
	          {
	            Js_nextCross[(ipos + p0)] = dest[sorted[d]][s];		//(char)
	            p0 = dest[sorted[d]][s];
	            if (d >= 7)
	              continue;
	            Js_nextArrow[(idir + p0)] = dest[sorted[(d + 1)]][0];	//(char)
	          }
	        while (++d < 8);
	      }
	    }
	    while (++po < 99);
	  }
	  while (++ptyp < 8);
	}
	 
	function ShowMov(rgchMove)
	{
	  var fKcastle = false;
	  var fQcastle = false;
	 
	  var i = 0;
	  do Js_movCh[i] = ' '; while (++i < 8);
	 
	  var szM = "";
	  if (!(Js_flip))
	  {
	    Js_nMovesMade += 1;
	    if (Js_nMovesMade < 10) szM = " ";
	    szM = szM + Js_nMovesMade + ".";
	  }
	 
	  Js_movCh[0] = rgchMove[0];
	  Js_movCh[1] = rgchMove[1];
	  Js_movCh[2] = '-';
	
	  if ((Js_root.flags & Js_capture) != 0 || Js_fEat) Js_movCh[2] = 'x';
	  
	  Js_movCh[3] = rgchMove[2];
	  Js_movCh[4] = rgchMove[3];

	  var waspromo = (((Js_root.flags & Js_promote) != 0) ? Js_upperNot[Js_board[Js_root.t]] : "" );
	  if( rgchMove[4] == "=" ) waspromo = rgchMove[5];

	  i = 5;
	  if (waspromo.length>0 )
	  {
	    Js_movCh[(i++)] = '=';
	    Js_movCh[(i++)] = waspromo;
	  }
	  if (Js_bDraw != 0) Js_movCh[i] = '=';
	  if (Js_fCheck_kc) Js_movCh[i] = '+';
	  if (Js_fMate_kc) Js_movCh[i] = '#';

	  var mv2 = copyValueOf(Js_movCh);
	  if (Js_myPiece == 'K')
	  {
	    if ((mv2=="e1-g1") || (mv2=="e8-g8")) fKcastle = true;
	    if ((mv2=="e1-c1") || (mv2=="e8-c8")) fQcastle = true;
	  }
	 
	  if ((fKcastle) || (fQcastle))
	  {
	    if (fKcastle) szM += "O-O" + Js_movCh[i];
	    if (fQcastle) szM += "O-O-O" + Js_movCh[i];
	  }
	  else
	  {
	    szM += Js_myPiece + mv2;
	  }
	  szM += " ";

	  if (Js_fAbandon) szM = "resign";
	  Js_myPiece = "";
	  MessageOut(szM, Js_flip);

	  Js_flip = (!(Js_flip));
	}
	 
	function CheckMov(s, iop)
	{
	  var tempb = new _INT();
	  var tempc = new _INT();
	  var tempsf = new _INT();
	  var tempst = new _INT();
	  var xnode = new _BTREE();
	 
	  var cnt = 0;
	  var pnt = 0;
	 

	  if (iop == 2)
	  {
	    UnValidateMov(Js_enemy, xnode, tempb, tempc, tempsf, tempst);
	    return 0;
	  }
	  cnt = 0;
	  AvailMov(Js_enemy, 2);
	  pnt = Js_treePoint[2];
	  var s0 = copyValueOf(s);
	  while (pnt < Js_treePoint[3])
	  {
	    var node = Js_Tree[(pnt++)];	// _BTREE

	    Lalgb(node.f, node.t, node.flags);
	    var s1 = copyValueOf(Js_asciiMove[0]);
	    if ((((s[0] != Js_asciiMove[0][0]) || (s[1] != Js_asciiMove[0][1]) || (s[2] != Js_asciiMove[0][2]) || (s[3] != Js_asciiMove[0][3]))) && 
	      (((s[0] != Js_asciiMove[1][0]) || (s[1] != Js_asciiMove[1][1]) || (s[2] != Js_asciiMove[1][2]) || (s[3] != Js_asciiMove[1][3]))) && 
	      (((s[0] != Js_asciiMove[2][0]) || (s[1] != Js_asciiMove[2][1]) || (s[2] != Js_asciiMove[2][2]) || (s[3] != Js_asciiMove[2][3]))) && ((
	      (s[0] != Js_asciiMove[3][0]) || (s[1] != Js_asciiMove[3][1]) || (s[2] != Js_asciiMove[3][2]) || (s[3] != Js_asciiMove[3][3]))))
	      continue;
	    ++cnt;

	    xnode = node;

	    break;
	  }
	 
	  if (cnt == 1)
	  {
	    ValidateMov(Js_enemy, xnode, tempb, tempc, tempsf, tempst, Js_gainScore);
	    if (ISqAgrs(Js_pieceMap[Js_enemy][0], Js_computer) != 0)
	    {
	      UnValidateMov(Js_enemy, xnode, tempb, tempc, tempsf, tempst);
	 
	      return 0;
	    }
	 
	    if (iop == 1) return 1;
	 
	    UpdateDisplay();

	    Js_fEat = ((xnode.flags & Js_capture) != 0);
	    if ((Js_board[xnode.t] == Js_pawn) || ((xnode.flags & Js_capture) != 0) || ((xnode.flags & Js_castle_msk) != 0))
	    {
	      Js_fiftyMoves = Js_nGameMoves;
	    }
	 
	    Js_movesList[Js_nGameMoves].score = 0;
	 
	    Lalgb(xnode.f, xnode.t, 0);
	    return 1;
	  }
	 
	  return 0;
	}
	 	 
 
	function GetRnd(iVal)
	{
	  return Math.round( (Math.random() * 32000) % iVal);		// (int)	
	}
	 
	function UnValidateMov(side, node, tempb, tempc, tempsf, tempst)
	{
	  var xside = Js_otherTroop[side];
	  var f = node.f;
	  var t = node.t;
	  Js_indenSqr = -1;
	  Js_nGameMoves += -1;
	  if ((node.flags & Js_castle_msk) != 0) {
	    DoCastle(side, f, t, 2);
	  }
	  else {
	    Js_color[f] = Js_color[t];
	    Js_board[f] = Js_board[t];
	    Js_scoreOnBoard[f] = tempsf.i;
	    Js_pieceIndex[f] = Js_pieceIndex[t];
	    Js_pieceMap[side][Js_pieceIndex[f]] = f;
	    Js_color[t] = tempc.i;
	    Js_board[t] = tempb.i;
	    Js_scoreOnBoard[t] = tempst.i;
	    if ((node.flags & Js_promote) != 0)
	    {
	      Js_board[f] = Js_pawn;
	      Js_pawnMap[side][IColmn(t)] += 1;
	      Js_matrl[side] += (Js_pawnVal - Js_valueMap[(node.flags & Js_pawn_msk)]);
	      Js_pmatrl[side] += Js_pawnVal;
	    }
	 
	    if (tempc.i != Js_hollow)
	    {
	      UpdatePiecMap(tempc.i, t, 2);
	      if (tempb.i == Js_pawn)
	        Js_pawnMap[tempc.i][IColmn(t)] += 1;
	      if (Js_board[f] == Js_pawn)
	      {
	        Js_pawnMap[side][IColmn(t)] += -1;
	        Js_pawnMap[side][IColmn(f)] += 1;
	      }
	      Js_matrl[xside] += Js_valueMap[tempb.i];
	      if (tempb.i == Js_pawn) {
	        Js_pmatrl[xside] += Js_pawnVal;
	      }
	 
	      Js_nMvtOnBoard[t] += -1;
	    }
	    if ((node.flags & Js_enpassant_msk) != 0) {
	      PrisePassant(xside, f, t, 2);
	    }
	 
	    Js_nMvtOnBoard[f] += -1;

	  }
	}
	 
	function FJunk(sq)
	{
	  var piece = Js_board[sq];
	  var ipos = Js_pieceTyp[Js_c1][piece] * 64 * 64 + sq * 64;
	  var idir = ipos;
	  var u;
	  if (piece == Js_pawn)
	  {
	    u = Js_nextCross[(ipos + sq)];
	    if (Js_color[u] == Js_hollow)
	    {
	      if (Js_agress1[u] >= Js_agress2[u])
	        return false;
	      if (Js_agress2[u] < Js_xltP)
	      {
	        u = Js_nextCross[(ipos + u)];
	        if ((Js_color[u] == Js_hollow) && (Js_agress1[u] >= Js_agress2[u]))
	          return false;
	      }
	    }
	    u = Js_nextArrow[(idir + sq)];
	    if (Js_color[u] == Js_c2) return false;
	    u = Js_nextArrow[(idir + u)];
	    if (Js_color[u] == Js_c2) return false;
	  }
	  else
	  {
	    u = Js_nextCross[(ipos + sq)];
	    do { if ((Js_color[u] != Js_c1) && ((
	        (Js_agress2[u] == 0) || (Js_board[u] >= piece)))) {
	        return false;
	      }
	      if (Js_color[u] == Js_hollow)
	        u = Js_nextCross[(ipos + u)];
	      else
	        u = Js_nextArrow[(idir + u)];
	    }
	    while (u != sq);
	  }
	  return true;
	}
	 
	function ShowThink(score4, best)
	{
	  if (Js_depth_Seek > Js_realBestDepth) Js_realBestScore = -20000;
	  if ((Js_depth_Seek >= Js_realBestDepth) && (score4 >= Js_realBestScore))
	  {
	    Js_realBestDepth = Js_depth_Seek;
	    Js_realBestScore = score4;
	    Js_realBestMove = best[1];
	  }
	 
	  if ((Js_depth_Seek == Js_lastDepth) && (score4 == Js_lastScore)) return;
	  Js_lastDepth = Js_depth_Seek;
	  Js_lastScore = score4;
	 
	  var s = "";
	  for (var i = 0; best[(++i)] > 0; )
	  {
	    Lalgb(best[i] >> 8, best[i] & 0xFF, 0);
	 
	    Js_tmpCh[0] = Js_asciiMove[0][0];
	    Js_tmpCh[1] = Js_asciiMove[0][1];
	    Js_tmpCh[2] = '-';
	    Js_tmpCh[3] = Js_asciiMove[0][2];
	    Js_tmpCh[4] = Js_asciiMove[0][3];
	    Js_tmpCh[5] = 0;
	    s = s + copyValueOf(Js_tmpCh) + " ";
	  }
	  //MessageOut("Thinking: " + s , true);

	  //ShowScore(score4);
	}
	 
	 
	function ResetData()
	{
	  Js_movesList=[];
	  var i = 0;
	  do
	    Js_movesList[i] = new _MOVES();
	  while (++i < 512);
	 
	  Js_Tree=[];
	  i = 0;
	  do
	    Js_Tree[i] = new _BTREE();
	  while (++i < 2000);
	 

	  for (i = 0; i < Js_maxDepth; ++i)
	  {
	    Js_treePoint[i] = 0;
	    Js_variants[i] = 0;
	    Js_flagCheck[i] = 0;
	    Js_flagEat[i] = 0;
	    Js_menacePawn[i] = 0;
	    Js_scorePP[i] = 0;
	    Js_scoreTP[i] = 0;
	    Js_eliminate0[i] = 0;
	    Js_eliminate1[i] = 0;
	    Js_eliminate3[i] = 0; }
	 
	  i = 0;
	  var j;
	  do { j = 0;
	       Js_pieceMap[i] = [];	// creates object
	    do Js_pieceMap[i][j] = 0;
	    while (++j < 16);
	  }
	  while (++i < 2);
	 
	  i = 0;
	  do { j = 0;
	       Js_pawnMap[i] = [];		// creates object
	    do Js_pawnMap[i][j] = 0;
	    while (++j < 8);
	  }
	  while (++i < 2);
	 
	  i = 0;
	  do {
	    Js_nMvtOnBoard[i] = 0;
	    Js_scoreOnBoard[i] = 0;
	    Js_pieceIndex[i] = 0;
	  }
	  while (++i < 64);
	 
	  i = 0;
	  do {
	    Js_arrowData[i] = 0;
	    Js_crossData[i] = 0;
	  }
	  while (++i < 4200);
	 
	  i = 0;
	  do { j = 0;
	       Js_agress[i] = [];		// creates object
	    do Js_agress[i][j] = 0;
	    while (++j < 64);
	  }
	  while (++i < 2);
	 
	  i = 0;
	  do Js_storage[i] = 0; while (++i < 10000);
	 
	  i = 0;
	  do {
	    Js_wPawnMvt[i] = 0;
	    Js_bPawnMvt[i] = 0;
	  }
	  while (++i < 64);
	 
	  i = 0;
	  do { j = 0;
	      Js_knightMvt[i] = [];		// creates object
	      Js_bishopMvt[i] = [];		// creates object
	      Js_kingMvt[i] = [];		// creates object
	      Js_killArea[i] = [];		// creates object
	    do {
	      Js_knightMvt[i][j] = 0;
	      Js_bishopMvt[i][j] = 0;
	      Js_kingMvt[i][j] = 0;
	      Js_killArea[i][j] = 0;
	    }
	    while (++j < 64);
	  }
	  while (++i < 2);
	 
	  i = 0;
	  do {
	    Js_nextCross[i] = 0;
	    Js_nextArrow[i] = 0;
	  }
	  while (++i < 40000);
	}
	 	 
	function InChecking(side)
	{
	  var i = 0;
	  do
	    if ((Js_board[i] == Js_king) && 
	      (Js_color[i] == side) && 
	      (ISqAgrs(i, Js_otherTroop[side]) != 0)) return true;
	  while (++i < 64);
	 
	  return false;
	}
	 
	 
	function ShowScore(score5)
	{
	  var fMinus = score5 < 0;
	  if (fMinus) score5 = -score5;
	  if (score5 != 0) ++score5;

	  var sz;
	  if (score5 == 0)
	    sz = "";
	  else if (fMinus)
	    sz = "-";
	  else {
	    sz = "+";
	  }
	  var sc100 = Math.floor( score5 );
	  sz += (sc100/100).toString();
	  
	  MessageOut("(" + sz + ")",false);
	}

	 
	function MixBoard(a, b, c)
	{
	  var sq = 0;
	  do c[sq] = ((a[sq] * (10 - Js_working) + b[sq] * Js_working) / 10);
	  while (++sq < 64);
	}
	 
	function InitGame()
	{
	  ResetData();

	  Js_flip = false;

	  Js_fInGame = true;
	  Js_fGameOver = false;

	  Js_fCheck_kc = false;
	  Js_fMate_kc = false;
	  Js_fSoonMate_kc = false;
	 
	  Js_bDraw = 0;
	  Js_fStalemate = false;
	  Js_fAbandon = false;
	  Js_fUserWin_kc = false;
	 
	 
	  InitArrow();
	  InitMoves();
	 
	  Js_working = -1;
	  Js_working2 = -1;
	 
	  Js_flag.mate = false;
	  Js_flag.recapture = true;

	  Js_cNodes = 0;
	  Js_indenSqr = 0;
	  Js_scoreDither = 0;
	  Js__alpha = Js_N9;
	  Js__beta = Js_N9;
	  Js_dxAlphaBeta = Js_N9;
	  Js_maxDepthSeek = (Js_maxDepth - 1);

	  Js_nMovesMade = 0;	 
	  Js_specialScore = 0;
	  Js_nGameMoves = 0;
	  Js_fiftyMoves = 1;
	  Js_hint = 3092;

	  Js_fDevl[Js_white] = 0;
	  Js_fDevl[Js_black] = 0;
	  Js_roquer[Js_white] = 0;
	  Js_roquer[Js_black] = 0;
	  Js_menacePawn[0] = 0;
	  Js_flagEat[0] = 0;
	  Js_scorePP[0] = 12000;
	  Js_scoreTP[0] = 12000;
	 
	  i = 0;
	  do {
	    Js_board[i] = Js_reguBoard[i];
	    Js_color[i] = Js_reguColor[i];
	    Js_nMvtOnBoard[i] = 0;
	  }
	  while (++i < 64);
	 
	  if (Js_nMovesMade == 0)
	  {
	    Js_computer = Js_white;
	    Js_player = Js_black;
	    Js_enemy = Js_player;
	  }

	  Js_fUserWin_kc=false;

	  InitStatus();
		 
	}
	 
	 
	function IColmn(a)
	{
	  return (a & 0x7);
	}
	 

	function ShowStat()
	{
	  var sz = "";
	 
	  if ((Js_fMate_kc) && (!(Js_fCheck_kc)))
	  {
	    Js_fStalemate = true;
	  }
	 
	  if (Js_fCheck_kc)
	  {
	    sz = "Check+";
	  }
	 
	  if (Js_fMate_kc) sz = "Checkmate!";
	 
	  if (Js_bDraw != 0) sz = "Draw";
	  if (Js_fStalemate) sz = "Stalemate!";
	  if (Js_fAbandon) sz = "resign";
	  if (Js_bDraw == 3)
	  {
	    sz += "At least 3 times repeat-position !";
	  }
	  else if (Js_bDraw == 1)
	  {
	    sz += "Can't checkmate !";
	  }
	 
	  if ((!(Js_fMate_kc)) && (Js_bDraw == 0) && (!(Js_fStalemate)) && (!(Js_fAbandon)))
		 return;

		// when game is finished only, otherwise show status 
	  Js_fInGame = false;

	  if(sz.length>0) MessageOut(sz,true);
	}
	 
	function IRaw(a)
	{
	  return (a >> 3);
	} 
				 			 	
	 
	function CalcKPK(side, winner, loser, king1, king2, sq)
	{
	  var s;
	  if (Js_piecesCount[winner] == 1)
	    s = 50;
	  else
	    s = 120;
	  var r;
	  if (winner == Js_white)
	  {
	    if (side == loser)
	      r = IRaw(sq) - 1;
	    else
	      r = IRaw(sq);
	    if ((IRaw(king2) >= r) && (IArrow(sq, king2) < 8 - r))
	      s += 10 * IRaw(sq);
	    else
	      s = 500 + 50 * IRaw(sq);
	    if (IRaw(sq) < 6)
	      sq += 16;
	    else if (IRaw(sq) == 6)
	      sq += 8;
	  }
	  else
	  {
	    if (side == loser)
	      r = IRaw(sq) + 1;
	    else
	      r = IRaw(sq);
	    if ((IRaw(king2) <= r) && (IArrow(sq, king2) < r + 1))
	      s += 10 * (7 - IRaw(sq));
	    else
	      s = 500 + 50 * (7 - IRaw(sq));
	    if (IRaw(sq) > 1)
	      sq -= 16;
	    else if (IRaw(sq) == 1) {
	      sq -= 8;
	    }
	  }
	  s += 8 * Js_crossData[(king2 * 64 + sq)] - Js_crossData[(king1 * 64 + sq)];
	  return s;
	}
	 
	function CalcKg(side, score)
	{
	  ChangeForce();
	  var winner;
	  if (Js_matrl[Js_white] > Js_matrl[Js_black])
	    winner = Js_white;
	  else
	    winner = Js_black;
	  var loser = Js_otherTroop[winner];
	  var king1 = Js_pieceMap[winner][0];
	  var king2 = Js_pieceMap[loser][0];
	 
	  var s = 0;
	 
	  if (Js_pmatrl[winner] > 0)
	  {
	    for (var i = 1; i <= Js_piecesCount[winner]; ++i) {
	      s += CalcKPK(side, winner, loser, king1, king2, Js_pieceMap[winner][i]);
	    }
	  }
	  else if (Js_ematrl[winner] == Js_bishopVal + Js_knightVal)
	  {
	    s = CalcKBNK(winner, king1, king2);
	  }
	  else if (Js_ematrl[winner] > Js_bishopVal)
	  {
	    s = 500 + Js_ematrl[winner] - Js_vanish_K[king2] - (2 * IArrow(king1, king2));
	  }
	 
	  if (side == winner)
	    score.i = s;
	  else
	    score.i = (s * -1);
	}
	 
	function IArrow( a, b )
	{
	  return Js_arrowData[(a * 64 + b)];
	}
	 
	 
	function MoveTree(to, from)
	{
	  to.f = from.f;
	  to.t = from.t;
	  to.score = from.score;
	  to.replay = from.replay;
	  to.flags = from.flags;
	}
	 
	function PrisePassant( xside, f, t, iop)
	{
	  var l;
	  if (t > f)
	    l = t - 8;
	  else
	    l = t + 8;
	  if (iop == 1)
	  {
	    Js_board[l] = Js_empty;
	    Js_color[l] = Js_hollow;
	  }
	  else
	  {
	    Js_board[l] = Js_pawn;
	    Js_color[l] = xside;
	  }
	  InitStatus();
	}
	 
 
	function GetAlgMvt(ch)
	{
	  var i = 0;
	  do
	    if (ch == Js_szIdMvt.charAt(i))
	    {
	      return Js_szAlgMvt[i];
	    }
	  while (++i < 64);
	 
	  return "a1";
	}
	 

	function copyValueOf(a)
	{
	  var str="";
	  var i = 0;
	  while(a.length>i && a[i]!=0 )
	  {
	   str+=( typeof(a[i])=="string" ? a[i] : String.fromCharCode(a[i]) );
	   i++;
	  }
	  return str;
	}
	
	 
	function Agression(side, a)
	{
	  var i = 0;
	  do {
	    a[i] = 0;
	    Js_agress[side][i] = 0;
	  }
	  while (++i < 64);
	 
	  for (i = Js_piecesCount[side]; i >= 0; --i)
	  {
	    var sq = Js_pieceMap[side][i];
	    var piece = Js_board[sq];
	    var c = Js_xlat[piece];
	    var idir;
	    var u;
	    if (Js_heavy[piece] != false)
	    {
	      var ipos = piece * 64 * 64 + sq * 64;
	      idir = ipos;
	 
	      u = Js_nextCross[(ipos + sq)];
	      do {
		a[u] += 1;
		a[u] |= c;
	 
	        Js_agress[side][u] += 1;
	        Js_agress[side][u] |= c;
	 
	        if (Js_color[u] == Js_hollow)
	          u = Js_nextCross[(ipos + u)];
	        else
	          u = Js_nextArrow[(idir + u)];
	      }
	      while (u != sq);
	    }
	    else
	    {
	      idir = Js_pieceTyp[side][piece] * 64 * 64 + sq * 64;
	 
	      u = Js_nextArrow[(idir + sq)];
	      do {
		a[u] += 1;
		a[u] |= c;

	 
	        Js_agress[side][u] += 1;
	        Js_agress[side][u] |= c;
	 
	        u = Js_nextArrow[(idir + u)];
	      }
	      while (u != sq);
	    }
	  }
	}
	 	 
	function InitArrow()
	{
	  var a = 0;
	  do { var b = 0;
	    do {
	      var d = IColmn(a) - IColmn(b);
	      d = Math.abs(d);
	      var di = IRaw(a) - IRaw(b);
	      di = Math.abs(di);
	 
	      Js_crossData[(a * 64 + b)] = (d + di);
	      if (d > di)
	        Js_arrowData[(a * 64 + b)] = d;
	      else
	        Js_arrowData[(a * 64 + b)] = di;
	    }
	    while (++b < 64);
	  }
	  while (++a < 64);
	}
	 
	function IfCheck()
	{
	  var i = 0;
	  do {
	    if (Js_board[i] != Js_king)
	      continue;
	    if (Js_color[i] == Js_white)
	    {
	      if (ISqAgrs(i, Js_black) != 0)
	      {
	        Js_fCheck_kc = true;
	        return;
	      }
	    }
	    else
	    {
	      if (ISqAgrs(i, Js_white) == 0)
	        continue;
	      Js_fCheck_kc = true;
	      return;
	    }
	  }
	  while (++i < 64);
	 
	  Js_fCheck_kc = false;
	}
	 
	function Anyagress(c, u)
	{
	  if (Js_agress[c][u] > 0) {
	    return 1;
	  }
	  return 0;
	}
	 
	function KnightPts(sq, side)
	{
	  var s = Js_knightMvt[Js_c1][sq];
	  var a2 = Js_agress2[sq] & 0x4FFF;
	  if (a2 > 0)
	  {
	    var a1 = Js_agress1[sq] & 0x4FFF;
	    if ((a1 == 0) || (a2 > Js_xltBN + 1))
	    {
	      s += Js_pinned_p;
	      Js_pinned[Js_c1] += 1;
	      if (FJunk(sq))
	        Js_pinned[Js_c1] += 1;
	    }
	    else if ((a2 >= Js_xltBN) || (a1 < Js_xltP)) {
	      s += Js_agress_across; }
	  }
	  return s;
	}
	 
	function QueenPts(sq, side)
	{
	  var s = (IArrow(sq, Js_pieceMap[Js_c2][0]) < 3) ? 12 : 0;
	  if (Js_working > 2)
	    s += 14 - Js_crossData[(sq * 64 + Js_pieceMap[Js_c2][0])];
	  var a2 = Js_agress2[sq] & 0x4FFF;
	  if (a2 > 0)
	  {
	    var a1 = Js_agress1[sq] & 0x4FFF;
	    if ((a1 == 0) || (a2 > Js_xltQ + 1))
	    {
	      s += Js_pinned_p;
	      Js_pinned[Js_c1] += 1;
	      if (FJunk(sq)) Js_pinned[Js_c1] += 1;
	    }
	    else if ((a2 >= Js_xltQ) || (a1 < Js_xltP)) {
	      s += Js_agress_across; }
	  }
	  return s;
	}
	 
	function PositPts(side, score)
	{
	  var pscore = [ 0, 0 ];
	 
	  ChangeForce();
	  var xside = Js_otherTroop[side];
	  pscore[Js_black] = 0;
	  pscore[Js_white] = 0;
	 
	  for (Js_c1 = Js_white; Js_c1 <= Js_black; Js_c1 += 1)
	  {
	    Js_c2 = Js_otherTroop[Js_c1];
	    Js_agress1 = Js_agress[Js_c1];
	    Js_agress2 = Js_agress[Js_c2];
	    Js_pawc1 = Js_pawnMap[Js_c1];
	    Js_pawc2 = Js_pawnMap[Js_c2];
	    for (var i = Js_piecesCount[Js_c1]; i >= 0; --i)
	    {
	      var sq = Js_pieceMap[Js_c1][i];
	      var s;
	      if (Js_board[sq] == Js_pawn)
	        s = PawnPts(sq, side);
	      else if (Js_board[sq] == Js_knight)
	        s = KnightPts(sq, side);
	      else if (Js_board[sq] == Js_bishop)
	        s = BishopPts(sq, side);
	      else if (Js_board[sq] == Js_rook)
	        s = RookPts(sq, side);
	      else if (Js_board[sq] == Js_queen)
	        s = QueenPts(sq, side);
	      else if (Js_board[sq] == Js_king)
	        s = KingPts(sq, side);
	      else
	        s = 0;
	      pscore[Js_c1] += s;
	      Js_scoreOnBoard[sq] = s;
	    }
	  }
	  if (Js_pinned[side] > 1)
	    pscore[side] += Js_pinned_other;
	  if (Js_pinned[xside] > 1) {
	    pscore[xside] += Js_pinned_other;
	  }
	  score.i = (Js_matrl[side] - Js_matrl[xside] + pscore[side] - pscore[xside] + 10);
	 
	  if ((score.i > 0) && (Js_pmatrl[side] == 0))
	  {
	    if (Js_ematrl[side] < Js_rookVal)
	      score.i = 0;
	    else if (score.i < Js_rookVal)
	      score.i /= 2;
	  }
	  if ((score.i < 0) && (Js_pmatrl[xside] == 0))
	  {
	    if (Js_ematrl[xside] < Js_rookVal)
	      score.i = 0;
	    else if (-score.i < Js_rookVal) {
	      score.i /= 2;
	    }
	  }
	  if ((Js_matrl[xside] == Js_kingVal) && (Js_ematrl[side] > Js_bishopVal))
	    score.i += 200;
	  if ((Js_matrl[side] == Js_kingVal) && (Js_ematrl[xside] > Js_bishopVal))
	    score.i -= 200;
	}
	 
	function PlayMov()
	{
	  UpdateDisplay();
	 
	  Js_currentScore = Js_root.score;
	  Js_fSoonMate_kc = false;
	  if ((((Js_root.flags == 0) ? 0 : 1) & ((Js_draw == 0) ? 0 : 1)) != 0)
	  {
	    Js_fGameOver = true;
	  }
	  else if (Js_currentScore == -9999)
	  {
	    Js_fGameOver = true;
	    Js_fMate_kc = true;
	    Js_fUserWin_kc = true;
	  }
	  else if (Js_currentScore == 9998)
	  {
	    Js_fGameOver = true;
	    Js_fMate_kc = true;
	    Js_fUserWin_kc = false;
	  }
	  else if (Js_currentScore < -9000)
	  {
	    Js_fSoonMate_kc = true;
	  }
	  else if (Js_currentScore > 9000)
	  {
	    Js_fSoonMate_kc = true;
	  }
	  ShowScore(Js_currentScore);
	 
	}

	function IRepeat(cnt)
	{
	  var c = 0;
	  cnt = 0;
	  if (Js_nGameMoves > Js_fiftyMoves + 3)
	  {
	    var i = 0;
	    do Js_b_r[i] = 0; while (++i < 64);
	 
	    for (i = Js_nGameMoves; i > Js_fiftyMoves; --i)
	    {
	      var m = Js_movesList[i].gamMv;
	      var f = m >> 8;
	      var t = m & 0xFF;
	      Js_b_r[f] += 1;
	      if (Js_b_r[f] == 0) --c;
	      else ++c;
	      Js_b_r[t] += -1;
	      if (Js_b_r[t] == 0) --c;
	      else ++c;
	      if (c != 0) continue;
	      ++cnt;
	    }
	  }
	 
	  if (cnt == 3) Js_bDraw = 3;
	 
	  return cnt;
	}

	 
	function ChoiceMov(side, iop)
	{
	  var tempb = new _INT();
	  var tempc = new _INT();
	  var tempsf = new _INT();
	  var tempst = new _INT();
	  var rpt = new _INT();
	  var score = new _INT();
	 
	  var alpha = 0;
	  var beta = 0;
	 
	  Js_flag.timeout = false;
	  var xside = Js_otherTroop[side];
	  if (iop != 2)
	    Js_player = side;
	  WatchPosit();
	 
	  PositPts(side, score);
	  var i;
	  if (Js_depth_Seek == 0)
	  {
	    i = 0;
	    do Js_storage[i] = 0; while (++i < 10000);

	    Js_origSquare = -1;
	    Js_destSquare = -1;
	    Js_ptValue = 0;
	    if (iop != 2)
	      Js_hint = 0;
	    for (i = 0; i < Js_maxDepth; ++i)
	    {
	      Js_variants[i] = 0;
	      Js_eliminate0[i] = 0;
	      Js_eliminate1[i] = 0;
	      Js_eliminate2[i] = 0;
	      Js_eliminate3[i] = 0;
	    }
	    alpha = score.i - Js_N9;
	    beta = score.i + Js_N9;
	    rpt.i = 0;
	    Js_treePoint[1] = 0;
	    Js_root = Js_Tree[0];
	    AvailMov(side, 1);
	    for (i = Js_treePoint[1]; i < Js_treePoint[2]; ++i)
	    {
	      Peek(i, Js_treePoint[2] - 1);
	    }
	 
	    Js_cNodes = 0;
	    Js_cCompNodes = 0;
	
	    Js_scoreDither = 0;
	    Js_dxDither = 20;
	  }
	 
	  while ((!(Js_flag.timeout)) && (Js_depth_Seek < Js_maxDepthSeek))
	  {
	    Js_depth_Seek += 1;

	    score.i = Seek(side, 1, Js_depth_Seek, alpha, beta, Js_variants, rpt);
	    for (i = 1; i <= Js_depth_Seek; ++i)
	    {
	      Js_eliminate0[i] = Js_variants[i];
	    }
	    if (score.i < alpha)
	    {
	      score.i = Seek(side, 1, Js_depth_Seek, -9000, score.i, Js_variants, rpt);
	    }
	    if ((score.i > beta) && ((Js_root.flags & Js__idem) == 0))
	    {
	      score.i = Seek(side, 1, Js_depth_Seek, score.i, 9000, Js_variants, rpt);
	    }
	 
	    score.i = Js_root.score;

	    for (i = Js_treePoint[1] + 1; i < Js_treePoint[2]; ++i)
	    {
	      Peek(i, Js_treePoint[2] - 1);
	    }
	 

	 
	    for (i = 1; i <= Js_depth_Seek; ++i)
	    {
	      Js_eliminate0[i] = Js_variants[i];
	    }
	 
	    if ((Js_root.flags & Js__idem) != 0) {
	      Js_flag.timeout = true;
	    }
	    if (Js_Tree[1].score < -9000) {
	      Js_flag.timeout = true;
	    }

	    if (!(Js_flag.timeout))
	    {
	      Js_scoreTP[0] = score.i;
	      if (Js_scoreDither == 0)
	        Js_scoreDither = score.i;
	      else
	        Js_scoreDither = ((Js_scoreDither + score.i) / 2);
	    }
	    Js_dxDither = (20 + Math.abs(Js_scoreDither / 12));
	    beta = score.i + Js__beta;
	    if (Js_scoreDither < score.i)
	      alpha = Js_scoreDither - Js__alpha - Js_dxDither;
	    else {
	      alpha = score.i - Js__alpha - Js_dxDither;
	    }
	 
	  }
	 
 
	  score.i = Js_root.score;
	 
	  if (iop == 2) return;
	 
	  Js_hint = Js_variants[2];

	  if ((score.i == -9999) || (score.i == 9998))
	  {
	    Js_flag.mate = true;
	    Js_fMate_kc = true;
	  }
	  if ((score.i > -9999) && (rpt.i <= 2))
	  {
	    if (score.i < Js_realBestScore)
	    {
	      var m_f = Js_realBestMove >> 8;
	      var m_t = Js_realBestMove & 0xFF;
	      i = 0;
	      do {
	        if ((m_f != Js_Tree[i].f) || (m_t != Js_Tree[i].t) || (Js_realBestScore != Js_Tree[i].score))
	        {
	          continue;
	        }
	 
	        Js_root = Js_Tree[i];
	 
	        break;
	      }
	      while (++i < 2000);
	    }

	    Js_myPiece = Js_rgszPiece[Js_board[Js_root.f]];
	 
	    ValidateMov(side, Js_root, tempb, tempc, tempsf, tempst, Js_gainScore);
	    if (InChecking(Js_computer))
	    {
	      UnValidateMov(side, Js_root, tempb, tempc, tempsf, tempst);
	      Js_fAbandon = true;
	    }
	    else
	    {
	      Lalgb(Js_root.f, Js_root.t, Js_root.flags);
	      PlayMov();
	    }
	 
	  }
	  else if (Js_bDraw == 0)
	  {
	    Lalgb(0, 0, 0);
	    if (!(Js_flag.mate))
	    {
	      Js_fAbandon = true;

	    }
	    else
	    {
	      Js_fUserWin_kc = true;
	    }
	 
	  }
	 
	  if (Js_flag.mate)
	  {
	    Js_hint = 0;
	  }
	  if ((Js_board[Js_root.t] == Js_pawn) || ((Js_root.flags & Js_capture) != 0) || ((Js_root.flags & Js_castle_msk) != 0))
	  {
	    Js_fiftyMoves = Js_nGameMoves;
	  }
	  Js_movesList[Js_nGameMoves].score = score.i;
	 
	  if (Js_nGameMoves > 500)
	  {
	    Js_flag.mate = true;
	  }
	  Js_player = xside;
	  Js_depth_Seek = 0;
	}
	 
	function MultiMov(ply, sq, side, xside)
	{
	  var piece = Js_board[sq];
	 
	  var i = Js_pieceTyp[side][piece] * 64 * 64 + sq * 64;
	  var ipos = i;
	  var idir = i;
	  var u;
	  if (piece == Js_pawn)
	  {
	    u = Js_nextCross[(ipos + sq)];
	    if (Js_color[u] == Js_hollow)
	    {
	      AttachMov(ply, sq, u, 0, xside);
	 
	      u = Js_nextCross[(ipos + u)];
	      if (Js_color[u] == Js_hollow) {
	        AttachMov(ply, sq, u, 0, xside);
	      }
	    }
	    u = Js_nextArrow[(idir + sq)];
	    if (Js_color[u] == xside)
	      AttachMov(ply, sq, u, Js_capture, xside);
	    else if (u == Js_indenSqr) {
	      AttachMov(ply, sq, u, Js_capture | Js_enpassant_msk, xside);
	    }
	    u = Js_nextArrow[(idir + u)];
	    if (Js_color[u] == xside)
	      AttachMov(ply, sq, u, Js_capture, xside);
	    else if (u == Js_indenSqr) {
	      AttachMov(ply, sq, u, Js_capture | Js_enpassant_msk, xside);
	    }
	  }
	  else
	  {
	    u = Js_nextCross[(ipos + sq)];
	    do if (Js_color[u] == Js_hollow)
	      {
	        AttachMov(ply, sq, u, 0, xside);
	 
	        u = Js_nextCross[(ipos + u)];
	      }
	      else
	      {
	        if (Js_color[u] == xside) {
	          AttachMov(ply, sq, u, Js_capture, xside);
	        }
	        u = Js_nextArrow[(idir + u)];
	      }
	 
	    while (u != sq);
	  }
	}
	 	 
	function XRayKg(sq, s)
	{
	  var cnt = 0;
	  var u = 0;
	  var ipos;
	  var idir;
	  if ((Js_withBishop[Js_c2] != 0) || (Js_withQueen[Js_c2] != 0))
	  {
	    ipos = Js_bishop * 64 * 64 + sq * 64;
	    idir = ipos;
	 
	    u = Js_nextCross[(ipos + sq)];
	    do { if (((Js_agress2[u] & Js_xltBQ) != 0) && 
	        (Js_color[u] != Js_c2)) {
	        if ((Js_agress1[u] == 0) || ((Js_agress2[u] & 0xFF) > 1))
	          ++cnt;
	        else {
	          s.i -= 3;
	        }
	      }
	      if (Js_color[u] == Js_hollow)
	        u = Js_nextCross[(ipos + u)];
	      else
	        u = Js_nextArrow[(idir + u)];
	    }
	    while (u != sq);
	  }
	 
	  if ((Js_withRook[Js_c2] != 0) || (Js_withQueen[Js_c2] != 0))
	  {
	    ipos = Js_rook * 64 * 64 + sq * 64;
	    idir = ipos;
	 
	    u = Js_nextCross[(ipos + sq)];
	    do { if (((Js_agress2[u] & Js_xltRQ) != 0) && 
	        (Js_color[u] != Js_c2)) {
	        if ((Js_agress1[u] == 0) || ((Js_agress2[u] & 0xFF) > 1))
	          ++cnt;
	        else {
	          s.i -= 3;
	        }
	      }
	      if (Js_color[u] == Js_hollow)
	        u = Js_nextCross[(ipos + u)];
	      else
	        u = Js_nextArrow[(idir + u)];
	    }
	    while (u != sq);
	  }
	 
	  if (Js_withKnight[Js_c2] != 0)
	  {
	    idir = Js_knight * 64 * 64 + sq * 64;
	 
	    u = Js_nextArrow[(idir + sq)];
	    do { if (((Js_agress2[u] & Js_xltNN) != 0) && 
	        (Js_color[u] != Js_c2)) {
	        if ((Js_agress1[u] == 0) || ((Js_agress2[u] & 0xFF) > 1))
	          ++cnt;
	        else
	          s.i -= 3;
	      }
	      u = Js_nextArrow[(idir + u)];
	    }
	    while (u != sq);
	  }
	  s.i += Js_safe_King * Js_menaceKing[cnt] / 16;
	 
	  cnt = 0;
	  var ok = false;
	  idir = Js_king * 64 * 64 + sq * 64;
	 
	  u = Js_nextCross[(idir + sq)];
	  do { if (Js_board[u] == Js_pawn)
	    {
	      ok = true; }
	    if (Js_agress2[u] > Js_agress1[u])
	    {
	      ++cnt;
	      if (((Js_agress2[u] & Js_xltQ) != 0) && 
	        (Js_agress2[u] > Js_xltQ + 1) && (Js_agress1[u] < Js_xltQ)) {
	        s.i -= 4 * Js_safe_King;
	      }
	    }
	    u = Js_nextCross[(idir + u)];
	  }
	  while (u != sq);
	 
	  if (!(ok))
	    s.i -= Js_safe_King;
	  if (cnt > 1)
	    s.i -= Js_safe_King;
	}
	  
	function DoCastle( side, kf, kt, iop )
	{
	  var xside = Js_otherTroop[side];
	  var rf;
	  var rt;
	  if (kt > kf)
	  {
	    rf = kf + 3;
	    rt = kt - 1;
	  }
	  else
	  {
	    rf = kf - 4;
	    rt = kt + 1;
	  }
	  if (iop == 0)
	  {
	    if ((kf != Js_kingPawn[side]) || (Js_board[kf] != Js_king) || (Js_board[rf] != Js_rook) || (Js_nMvtOnBoard[kf] != 0) || (Js_nMvtOnBoard[rf] != 0) || (Js_color[kt] != Js_hollow) || (Js_color[rt] != Js_hollow) || (Js_color[(kt - 1)] != Js_hollow) || (ISqAgrs(kf, xside) != 0) || (ISqAgrs(kt, xside) != 0) || (ISqAgrs(rt, xside) != 0))
	    {
	      return 0;
	    }
	  }
	  else {
	    if (iop == 1)
	    {
	      Js_roquer[side] = 1;
	      Js_nMvtOnBoard[kf] += 1;
	      Js_nMvtOnBoard[rf] += 1;
	    }
	    else
	    {
	      Js_roquer[side] = 0;
	      Js_nMvtOnBoard[kf] += -1;
	      Js_nMvtOnBoard[rf] += -1;
	      var t0 = kt;
	      kt = kf;
	      kf = t0;
	      t0 = rt;
	      rt = rf;
	      rf = t0;
	    }
	    Js_board[kt] = Js_king;
	    Js_color[kt] = side;
	    Js_pieceIndex[kt] = 0;
	    Js_board[kf] = Js_empty;
	    Js_color[kf] = Js_hollow;
	    Js_board[rt] = Js_rook;
	    Js_color[rt] = side;
	    Js_pieceIndex[rt] = Js_pieceIndex[rf];
	    Js_board[rf] = Js_empty;
	    Js_color[rf] = Js_hollow;
	    Js_pieceMap[side][Js_pieceIndex[kt]] = kt;
	    Js_pieceMap[side][Js_pieceIndex[rt]] = rt;
	  }
	 
	  return 1;
	}
	 
	function DoCalc(side, ply, alpha, beta,gainScore, slk, InChk)
	{
	  var s = new _INT();
	 
	  var xside = Js_otherTroop[side];
	  s.i = (-Js_scorePP[(ply - 1)] + Js_matrl[side] - Js_matrl[xside] - gainScore);
	  Js_pinned[Js_black] = 0;
	  Js_pinned[Js_white] = 0;
	  if (((Js_matrl[Js_white] == Js_kingVal) && (((Js_pmatrl[Js_black] == 0) || (Js_ematrl[Js_black] == 0)))) || (
	    (Js_matrl[Js_black] == Js_kingVal) && (((Js_pmatrl[Js_white] == 0) || (Js_ematrl[Js_white] == 0)))))
	    slk.i = 1;
	  else
	    slk.i = 0;
	  var evflag;
	  if (slk.i != 0) {
	    evflag = false;
	  }
	  else
	  {
	    evflag = (ply == 1) || (ply < Js_depth_Seek) || (
	      (((ply == Js_depth_Seek + 1) || (ply == Js_depth_Seek + 2))) && ((
	      ((s.i > alpha - Js_dxAlphaBeta) && (s.i < beta + Js_dxAlphaBeta)) || (
	      (ply > Js_depth_Seek + 2) && (s.i >= alpha - 25) && (s.i <= beta + 25)))));
	  }
	  if (evflag)
	  {
	    Js_cCompNodes += 1;
	    Agression(side, Js_agress[side]);
	 
	    if (Anyagress(side, Js_pieceMap[xside][0]) == 1) return (10001 - ply);
	    Agression(xside, Js_agress[xside]);
	 
	    InChk.i = Anyagress(xside, Js_pieceMap[side][0]);
	    PositPts(side, s);
	  }
	  else
	  {
	    if (ISqAgrs(Js_pieceMap[xside][0], side) != 0)
	      return (10001 - ply);
	    InChk.i = ISqAgrs(Js_pieceMap[side][0], xside);
	 
	    if (slk.i != 0)
	    {
	      CalcKg(side, s);
	    }
	  }
	  Js_scorePP[ply] = (s.i - Js_matrl[side] + Js_matrl[xside]);
	  if (InChk.i != 0)
	  {
	    if (Js_destSquare == -1) Js_destSquare = Js_root.t;
	    Js_flagCheck[(ply - 1)] = Js_pieceIndex[Js_destSquare];
	  }
	  else {
	    Js_flagCheck[(ply - 1)] = 0; }
	  return s.i;
	}
	 
 
	function Lalgb( f, t, flag)
	{
	  var i;
	  var y;
	  if (f != t)
	  {
	    Js_asciiMove[0][0] = (97 + IColmn(f));		//(char)
	    Js_asciiMove[0][1] = (49 + IRaw(f));		//(char)
	    Js_asciiMove[0][2] = (97 + IColmn(t));		//(char)
	    Js_asciiMove[0][3] = (49 + IRaw(t));		//(char)
	    Js_asciiMove[0][4] = 0;
	    Js_asciiMove[3][0] = 0;
	    Js_asciiMove[1][0] = Js_upperNot[Js_board[f]];

	    if (Js_asciiMove[1][0] == 'P')
	    {
	      var m3p;
	      if (Js_asciiMove[0][0] == Js_asciiMove[0][2])
	      {
	        Js_asciiMove[1][0] = Js_asciiMove[0][2];
	        Js_asciiMove[2][0] = Js_asciiMove[1][0];
		Js_asciiMove[1][1] = Js_asciiMove[0][3];
	        Js_asciiMove[2][1] = Js_asciiMove[1][1];
	        m3p = 2;
	      }
	      else
	      {
		Js_asciiMove[1][0] = Js_asciiMove[0][0];
	        Js_asciiMove[2][0] = Js_asciiMove[1][0];
		Js_asciiMove[1][1] = Js_asciiMove[0][2];
	        Js_asciiMove[2][1] = Js_asciiMove[1][1];
	        Js_asciiMove[2][2] = Js_asciiMove[0][3];
	        m3p = 3;
	      }
	      Js_asciiMove[1][2] = 0;
	      Js_asciiMove[2][m3p] = 0;
	      if ((flag & Js_promote) != 0)
	      {
		Js_asciiMove[1][2] = Js_lowerNot[(flag & Js_pawn_msk)];
		Js_asciiMove[2][m3p] = Js_asciiMove[1][2];
	        Js_asciiMove[0][4] = Js_asciiMove[1][2];
		Js_asciiMove[0][5] = 0;
		Js_asciiMove[2][(m3p + 1)] = 0;
	        Js_asciiMove[1][3] = 0;
	      }
	 
	    }
	    else
	    {
	      Js_asciiMove[2][0] = Js_asciiMove[1][0];
	      Js_asciiMove[2][1] = Js_asciiMove[0][1];
	      Js_asciiMove[1][1] = Js_asciiMove[0][2];
	      Js_asciiMove[2][2] = Js_asciiMove[1][1];
	      Js_asciiMove[1][2] = Js_asciiMove[0][3];
	      Js_asciiMove[2][3] = Js_asciiMove[1][2];
	      Js_asciiMove[1][3] = 0;
	      Js_asciiMove[2][4] = 0;
	      i = 0;
	      do
	        Js_asciiMove[3][i] = Js_asciiMove[2][i];
	      while (++i < 6);
	 
	      Js_asciiMove[3][1] = Js_asciiMove[0][0];
	      if ((flag & Js_castle_msk) != 0)
	      {
	        if (t > f)
	        {
	          Js_asciiMove[1][0] = 111;
	          Js_asciiMove[1][1] = 45;
	          Js_asciiMove[1][2] = 111;
	          Js_asciiMove[1][3] = 0;
	 
	          Js_asciiMove[2][0] = 111;
	          Js_asciiMove[2][1] = 45;
	          Js_asciiMove[2][2] = 111;
	          Js_asciiMove[2][3] = 0;
	        }
	        else
	        {
	          Js_asciiMove[1][0] = 111;
	          Js_asciiMove[1][1] = 45;
	          Js_asciiMove[1][2] = 111;
	          Js_asciiMove[1][3] = 45;
	          Js_asciiMove[1][4] = 111;
	          Js_asciiMove[1][5] = 0;
	 
	          Js_asciiMove[2][0] = 111;
	          Js_asciiMove[2][1] = 45;
	          Js_asciiMove[2][2] = 111;
	          Js_asciiMove[2][3] = 45;
	          Js_asciiMove[2][4] = 111;
	          Js_asciiMove[2][5] = 0;
	        }
	      }
	    }
	  }
	  else
	  {
	    i = 0;
	    do Js_asciiMove[i][0] = 0; while (++i < 4);
	  }
	}
	 
	function UpdatePiecMap(side, sq, iop)
	{
	  if (iop == 1)
	  {
	    Js_piecesCount[side] += -1;
	    for (var i = Js_pieceIndex[sq]; i <= Js_piecesCount[side]; ++i)
	    {
	      Js_pieceMap[side][i] = Js_pieceMap[side][(i + 1)];
	      Js_pieceIndex[Js_pieceMap[side][i]] = i;
	    }
	  }
	  else
	  {
	    Js_piecesCount[side] += 1;
	    Js_pieceMap[side][Js_piecesCount[side]] = sq;
	    Js_pieceIndex[sq] = Js_piecesCount[side];
	  }
	}
	 




	function UpdateDisplay()
	{
				   
	  var BB = [[]];	// 8x8
	  var iCol = 0;
	  var iLine = 0;

	  var i = 0;
	  do { BB[i]=[]; } while (++i<8);		// create object

	  i = 0;
	  do {

	    iCol = i % 8;
	    iLine = (i-iCol) / 8;
	    BB[iLine][iCol] = (Js_color[i]==Js_black ? Js_lowerNot[ Js_board[i] ] : Js_upperNot[ Js_board[i] ] );
	
	  }
	  while (++i < 64);
	
	  var PP="<div><table>";
	  iLine = 7;
	  do {
	    PP+="<tr>";
	    iCol = 0;
	    do {
	        PP+="<td>."+BB[iLine][iCol]+".</td>";
	    }
	    while (++iCol < 8);
	    PP+="</tr>";
	  }
	  while (--iLine >= 0);
	  PP+="</table></div>";
	  document.getElementById("jst_posit").innerHTML=PP;
	
	 
	}
	  
	function AvailCaptur(side, ply)
	{
	  var xside = Js_otherTroop[side];
	  Js_treePoint[(ply + 1)] = Js_treePoint[ply];
	  var node = Js_Tree[Js_treePoint[ply]];		//_BTREE

	  var inext = Js_treePoint[ply] + 1;
	  var r7 = Js_raw7[side];
	 
	  var ipl = side;
	  for (var i = 0; i <= Js_piecesCount[side]; ++i)
	  {
	    var sq = Js_pieceMap[side][i];
	    var piece = Js_board[sq];
	    var ipos;
	    var idir;
	    var u;
	    if (Js_heavy[piece] != false)
	    {
	      ipos = piece * 64 * 64 + sq * 64;
	      idir = ipos;
	 
	      u = Js_nextCross[(ipos + sq)];
	      do if (Js_color[u] == Js_hollow)
	        {
	          u = Js_nextCross[(ipos + u)];
	        }
	        else {
	          if (Js_color[u] == xside)
	          {
	            node.f = sq;
	            node.t = u;
	            node.replay = 0;
	            node.flags = Js_capture;
	            node.score = (Js_valueMap[Js_board[u]] + Js_scoreOnBoard[Js_board[u]] - piece);
	            node = Js_Tree[(inext++)];
	            Js_treePoint[(ply + 1)] += 1;
	          }
	 
	          u = Js_nextArrow[(idir + u)];
	        }
	 
	      while (u != sq);
	    }
	    else
	    {
	      idir = Js_pieceTyp[side][piece] * 64 * 64 + sq * 64;
	      if ((piece == Js_pawn) && (IRaw(sq) == r7))
	      {
	        u = Js_nextArrow[(idir + sq)];
	        if (Js_color[u] == xside)
	        {
	          node.f = sq;
	          node.t = u;
	          node.replay = 0;
	          node.flags = (Js_capture | Js_promote | Js_queen);
	          node.score = Js_queenVal;
	          node = Js_Tree[(inext++)];
	          Js_treePoint[(ply + 1)] += 1;
	        }
	 
	        u = Js_nextArrow[(idir + u)];
	        if (Js_color[u] == xside)
	        {
	          node.f = sq;
	          node.t = u;
	          node.replay = 0;
	          node.flags = (Js_capture | Js_promote | Js_queen);
	          node.score = Js_queenVal;
	          node = Js_Tree[(inext++)];
	          Js_treePoint[(ply + 1)] += 1;
	        }
	 
	        ipos = Js_pieceTyp[side][piece] * 64 * 64 + sq * 64;
	 
	        u = Js_nextCross[(ipos + sq)];
	        if (Js_color[u] == Js_hollow)
	        {
	          node.f = sq;
	          node.t = u;
	          node.replay = 0;
	          node.flags = (Js_promote | Js_queen);
	          node.score = Js_queenVal;
	          node = Js_Tree[(inext++)];
	          Js_treePoint[(ply + 1)] += 1;
	        }
	 
	      }
	      else
	      {
	        u = Js_nextArrow[(idir + sq)];
	        do { if (Js_color[u] == xside)
	          {
	            node.f = sq;
	            node.t = u;
	            node.replay = 0;
	            node.flags = Js_capture;
	            node.score = (Js_valueMap[Js_board[u]] + Js_scoreOnBoard[Js_board[u]] - piece);
	            node = Js_Tree[(inext++)];
	            Js_treePoint[(ply + 1)] += 1;
	          }
	 
	          u = Js_nextArrow[(idir + u)];
	        }
	        while (u != sq);
	      }
	    }
	  }
	}
	 
	function InitStatus()
	{
	  Js_indenSqr = -1;
	  var i = 0;
	  do {
	    Js_pawnMap[Js_white][i] = 0;
	    Js_pawnMap[Js_black][i] = 0;
	  }
	  while (++i < 8);

	  Js_pmatrl[Js_black] = 0;
	  Js_pmatrl[Js_white] = 0;
	  Js_matrl[Js_black] = 0;
	  Js_matrl[Js_white] = 0;
	  Js_piecesCount[Js_black] = 0;
	  Js_piecesCount[Js_white] = 0;
	 
	 
	  var sq = 0;
	  do {
	    if (Js_color[sq] == Js_hollow)
	      continue;
	    Js_matrl[Js_color[sq]] += Js_valueMap[Js_board[sq]];
	    if (Js_board[sq] == Js_pawn)
	    {
	      Js_pmatrl[Js_color[sq]] += Js_pawnVal;
	      Js_pawnMap[Js_color[sq]][IColmn(sq)] += 1;
	    }
	    if (Js_board[sq] == Js_king)
	      Js_pieceIndex[sq] = 0;
	    else
	      {
	      Js_piecesCount[Js_color[sq]] += 1;
	      Js_pieceIndex[sq] = Js_piecesCount[Js_color[sq]];
	      }
	    Js_pieceMap[Js_color[sq]][Js_pieceIndex[sq]] = sq;
	 
	  }
	  while (++sq < 64);
	}
	 
	function MessageOut(msg, fNL)
	{
	  document.getElementById("jst_msg").innerHTML+=msg+((fNL) ? "<br>" : "");
	}
	 
	function Pagress(c, u)
	{
	  return (Js_agress[c][u] > Js_xltP);
	}
	 
	function CheckMatrl()
	{
	  var flag = true;
	  
	  var nP=0;
	  var nK=0;
	  var nB=0;
	  var nR=0;
	  var nQ=0;
	
	  
	  var nK1=0;
	  var nK2=0;
	  var nB1=0;
	  var nB2=0;
				   
	  var i = 0;
	  do
	    if (Js_board[i] == Js_pawn) {
	      ++nP;
	    } else if (Js_board[i] == Js_queen) {
	      ++nQ;
	    } else if (Js_board[i] == Js_rook) {
	      ++nR;
	    } else if (Js_board[i] == Js_bishop)
	    {
	      if (Js_color[i] == Js_white)
	        ++nB1;
	      else
	        ++nB2;
	    } else {
	      if (Js_board[i] != Js_knight)
	        continue;
	      if (Js_color[i] == Js_white)
	        ++nK1;
	      else
	        ++nK2;
	    }
	  while (++i < 64);
	 
	  if (nP != 0) return true;
	  if ((nQ != 0) || (nR != 0)) return true;
	 
	  nK = nK1 + nK2;
	  nB = nB1 + nB2;
	 
	  if ((nK == 0) && (nB == 0)) return false;
	  if ((nK == 1) && (nB == 0)) return false;
	  return ((nK != 0) || (nB != 1));
	}
	 
	function AttachMov(ply, f, t, flag, xside)
	{
	  var node = Js_Tree[Js_treePoint[(ply + 1)]];	//_BTREE

	  var inext = Js_treePoint[(ply + 1)] + 1;
	 
	  var mv = f << 8 | t;
	  var s = 0;
	  if (mv == Js_scoreWin0)
	    s = 2000;
	  else if (mv == Js_scoreWin1)
	    s = 60;
	  else if (mv == Js_scoreWin2)
	    s = 50;
	  else if (mv == Js_scoreWin3)
	    s = 40;
	  else if (mv == Js_scoreWin4)
	    s = 30;
	  var z = f << 6 | t;
	  if (xside == Js_white)
	    z |= 4096;

	  s += Js_storage[z];

	  if (Js_color[t] != Js_hollow)
	  {
	    if (t == Js_destSquare)
	      s += 500;
	    s += Js_valueMap[Js_board[t]] - Js_board[f];
	  }
	  if (Js_board[f] == Js_pawn)
	  {
	    if ((IRaw(t) == 0) || (IRaw(t) == 7))
	    {
	      flag |= Js_promote;
	      s += 800;
	 
	      node.f = f;
	      node.t = t;
	      node.replay = 0;
	      node.flags = (flag | Js_queen);
	      node.score = (s - 20000);
	      node = Js_Tree[(inext++)];
	      Js_treePoint[(ply + 1)] += 1;
	 
	      s -= 200;
	 
	      node.f = f;
	      node.t = t;
	      node.replay = 0;
	      node.flags = (flag | Js_knight);
	      node.score = (s - 20000);
	      node = Js_Tree[(inext++)];
	      Js_treePoint[(ply + 1)] += 1;
	 
	      s -= 50;
	 
	      node.f = f;
	      node.t = t;
	      node.replay = 0;
	      node.flags = (flag | Js_rook);
	      node.score = (s - 20000);
	      node = Js_Tree[(inext++)];
	      Js_treePoint[(ply + 1)] += 1;
	 
	      flag |= Js_bishop;
	      s -= 50;
	    }
	    else if ((IRaw(t) == 1) || (IRaw(t) == 6))
	    {
	      flag |= Js_menace_pawn;
	      s += 600;
	    }
	  }
	 
	  node.f = f;
	  node.t = t;
	  node.replay = 0;
	  node.flags = flag;
	  node.score = (s - 20000);
	  node = Js_Tree[(inext++)];
	  Js_treePoint[(ply + 1)] += 1;
	}
	 
	 
	function PawnPts(sq, side)
	{
	  var a1 = Js_agress1[sq] & 0x4FFF;
	  var a2 = Js_agress2[sq] & 0x4FFF;
	  var rank = IRaw(sq);
	  var fyle = IColmn(sq);
	  var s = 0;
	  var r;
	  var in_square;
	  var e;
	  var j;
	  if (Js_c1 == Js_white)
	  {
	    s = Js_wPawnMvt[sq];
	    if (((sq == 11) && (Js_color[19] != Js_hollow)) || (
	      (sq == 12) && (Js_color[20] != Js_hollow)))
	      s += Js_junk_pawn;
	    if ((((fyle == 0) || (Js_pawc1[(fyle - 1)] == 0))) && ((
	      (fyle == 7) || (Js_pawc1[(fyle + 1)] == 0))))
	      s += Js_isol_pawn[fyle];
	    else if (Js_pawc1[fyle] > 1)
	      s += Js_doubled_pawn;
	    if ((a1 < Js_xltP) && (Js_agress1[(sq + 8)] < Js_xltP))
	    {
	      s += Js_takeBack[(a2 & 0xFF)];
	      if (Js_pawc2[fyle] == 0)
	        s += Js_bad_pawn;
	      if (Js_color[(sq + 8)] != Js_hollow) {
	        s += Js_stopped_pawn;
	      }
	    }
	    if (Js_pawc2[fyle] == 0)
	    {
	      if (side == Js_black)
	        r = rank - 1;
	      else
	        r = rank;
	      in_square = (IRaw(Js_pieceMap[Js_black][0]) >= r) && (IArrow(sq, Js_pieceMap[Js_black][0]) < 8 - r);
	 
	      if ((a2 == 0) || (side == Js_white))
	        e = 0;
	      else
	        e = 1;
	      for (j = sq + 8; j < 64; j += 8) {
	        if (Js_agress2[j] >= Js_xltP)
	        {
	          e = 2;
	          break;
	        }
	        if ((Js_agress2[j] > 0) || (Js_color[j] != Js_hollow))
	          e = 1;
	      }
	      if (e == 2)
	        s += Js_working * Js_pss_pawn3[rank] / 10;
	      else if ((in_square) || (e == 1))
	        s += Js_working * Js_pss_pawn2[rank] / 10;
	      else if (Js_ematrl[Js_black] > 0)
	        s += Js_working * Js_pss_pawn1[rank] / 10;
	      else
	        s += Js_pss_pawn0[rank];
	    }
	  }
	  else if (Js_c1 == Js_black)
	  {
	    s = Js_bPawnMvt[sq];
	    if (((sq == 51) && (Js_color[43] != Js_hollow)) || (
	      (sq == 52) && (Js_color[44] != Js_hollow))) {
	      s += Js_junk_pawn;
	    }
	    if ((((fyle == 0) || (Js_pawc1[(fyle - 1)] == 0))) && ((
	      (fyle == 7) || (Js_pawc1[(fyle + 1)] == 0))))
	      s += Js_isol_pawn[fyle];
	    else if (Js_pawc1[fyle] > 1) {
	      s += Js_doubled_pawn;
	    }
	    if ((a1 < Js_xltP) && (Js_agress1[(sq - 8)] < Js_xltP))
	    {
	      s += Js_takeBack[(a2 & 0xFF)];
	      if (Js_pawc2[fyle] == 0)
	        s += Js_bad_pawn;
	      if (Js_color[(sq - 8)] != Js_hollow)
	        s += Js_stopped_pawn;
	    }
	    if (Js_pawc2[fyle] == 0)
	    {
	      if (side == Js_white)
	        r = rank + 1;
	      else
	        r = rank;
	      in_square = (IRaw(Js_pieceMap[Js_white][0]) <= r) && (IArrow(sq, Js_pieceMap[Js_white][0]) < r + 1);
	 
	      if ((a2 == 0) || (side == Js_black))
	        e = 0;
	      else
	        e = 1;
	      for (j = sq - 8; j >= 0; j -= 8) {
	        if (Js_agress2[j] >= Js_xltP)
	        {
	          e = 2;
	          break;
	        }
	        if ((Js_agress2[j] <= 0) && (Js_color[j] == Js_hollow))
	          continue;
	        e = 1;
	      }
	 
	      if (e == 2)
	        s += Js_working * Js_pss_pawn3[(7 - rank)] / 10;
	      else if ((in_square) || (e == 1))
	        s += Js_working * Js_pss_pawn2[(7 - rank)] / 10;
	      else if (Js_ematrl[Js_white] > 0)
	        s += Js_working * Js_pss_pawn1[(7 - rank)] / 10;
	      else
	        s += Js_pss_pawn0[(7 - rank)];
	    }
	  }
	  if (a2 > 0)
	  {
	    if ((a1 == 0) || (a2 > Js_xltP + 1))
	    {
	      s += Js_pinned_p;
	      Js_pinned[Js_c1] += 1;
	      if (FJunk(sq)) Js_pinned[Js_c1] += 1;
	    }
	    else if (a2 > a1) {
	      s += Js_agress_across; }
	  }
	  return s;
	}
	 
	function RookPts(sq, side)
	{
	  var s = new _INT();
	  var mob = new _INT();
	 
	  s.i = Js_rookPlus;
	  XRayBR(sq, s, mob);
	  s.i += Js_mobRook[mob.i];
	  var fyle = IColmn(sq);
	  if (Js_pawc1[fyle] == 0)
	    s.i += Js_semiOpen_rook;
	  if (Js_pawc2[fyle] == 0)
	    s.i += Js_semiOpen_rookOther;
	  if ((Js_pmatrl[Js_c2] > 100) && (IRaw(sq) == Js_raw7[Js_c1]))
	    s.i += 10;
	  if (Js_working > 2)
	    s.i += 14 - Js_crossData[(sq * 64 + Js_pieceMap[Js_c2][0])];
	  var a2 = Js_agress2[sq] & 0x4FFF;
	  if (a2 > 0)
	  {
	    var a1 = Js_agress1[sq] & 0x4FFF;
	    if ((a1 == 0) || (a2 > Js_xltR + 1))
	    {
	      s.i += Js_pinned_p;
	      Js_pinned[Js_c1] += 1;
	      if (FJunk(sq)) Js_pinned[Js_c1] += 1;
	    }
	    else if ((a2 >= Js_xltR) || (a1 < Js_xltP)) {
	      s.i += Js_agress_across; }
	  }
	  return s.i;
	}
	 
	function KingPts(sq, side)
	{
	  var s = new _INT();
	 
	  s.i = Js_kingMvt[Js_c1][sq];
	  if ((Js_safe_King > 0) && ((
	    (Js_fDevl[Js_c2] != 0) || (Js_working > 0))))
	  {
	    XRayKg(sq, s);
	  }
	  if (Js_roquer[Js_c1] != 0)
	    s.i += Js_castle_K;
	  else if (Js_nMvtOnBoard[Js_kingPawn[Js_c1]] != 0) {
	    s.i += Js_moveAcross_K;
	  }
	  var fyle = IColmn(sq);
	  if (Js_pawc1[fyle] == 0)
	    s.i += Js_semiOpen_king;
	  if (Js_pawc2[fyle] == 0) {
	    s.i += Js_semiOpen_kingOther;
	  }
	  switch (fyle)
	  {
	  case 5:
	    if (Js_pawc1[7] == 0)
	    {
	      s.i += Js_semiOpen_king; }
	    if (Js_pawc2[7] == 0)
	      s.i += Js_semiOpen_kingOther;
	  case 0:
	  case 4:
	  case 6:
	    if (Js_pawc1[(fyle + 1)] == 0)
	    {
	      s.i += Js_semiOpen_king; }
	    if (Js_pawc2[(fyle + 1)] == 0)
	      s.i += Js_semiOpen_kingOther;
	    break;
	  case 2:
	    if (Js_pawc1[0] == 0)
	    {
	      s.i += Js_semiOpen_king; }
	    if (Js_pawc2[0] == 0)
	      s.i += Js_semiOpen_kingOther;
	  case 1:
	  case 3:
	  case 7:
	    if (Js_pawc1[(fyle - 1)] == 0)
	    {
	      s.i += Js_semiOpen_king; }
	    if (Js_pawc2[(fyle - 1)] == 0)
	      s.i += Js_semiOpen_kingOther;
	    break;
	  }
	 
	  var a2 = Js_agress2[sq] & 0x4FFF;
	  if (a2 > 0)
	  {
	    var a1 = Js_agress1[sq] & 0x4FFF;
	    if ((a1 == 0) || (a2 > Js_xltK + 1))
	    {
	      s.i += Js_pinned_p;
	      Js_pinned[Js_c1] += 1;
	    }
	    else {
	      s.i += Js_agress_across; }
	  }
	  return s.i;
	}
	 
	function AvailMov(side, ply)
	{
	  var xside = Js_otherTroop[side];
	  Js_treePoint[(ply + 1)] = Js_treePoint[ply];
	  if (Js_ptValue == 0)
	    Js_scoreWin0 = Js_eliminate0[ply];
	  else
	    Js_scoreWin0 = Js_ptValue;
	  Js_scoreWin1 = Js_eliminate1[ply];
	  Js_scoreWin2 = Js_eliminate2[ply];
	  Js_scoreWin3 = Js_eliminate3[ply];
	  if (ply > 2)
	    Js_scoreWin4 = Js_eliminate1[(ply - 2)];
	  else
	    Js_scoreWin4 = 0;
	  for (var i = Js_piecesCount[side]; i >= 0; --i)
	  {
	    var square = Js_pieceMap[side][i];
	    MultiMov(ply, square, side, xside);
	  }
	  if (Js_roquer[side] != 0)
	    return;
	  var f = Js_pieceMap[side][0];
	  if (DoCastle(side, f, f + 2, 0) != 0)
	  {
	    AttachMov(ply, f, f + 2, Js_castle_msk, xside);
	  }
	  if (DoCastle(side, f, f - 2, 0) == 0)
	    return;
	  AttachMov(ply, f, f - 2, Js_castle_msk, xside);
	}
	 
	function BishopPts(sq, side)
	{
	  var s = new _INT();
	  var mob = new _INT();
	 
	  s.i = Js_bishopMvt[Js_c1][sq];
	  XRayBR(sq, s, mob);
	  s.i += Js_mobBishop[mob.i];
	  var a2 = Js_agress2[sq] & 0x4FFF;
	  if (a2 > 0)
	  {
	    var a1 = Js_agress1[sq] & 0x4FFF;
	    if ((a1 == 0) || (a2 > Js_xltBN + 1))
	    {
	      s.i += Js_pinned_p;
	      Js_pinned[Js_c1] += 1;
	      if (FJunk(sq)) Js_pinned[Js_c1] += 1;
	    }
	    else if ((a2 >= Js_xltBN) || (a1 < Js_xltP)) {
	      s.i += Js_agress_across; }
	  }
	  return s.i;
	}
	 
	function ValidateMov(side, node, tempb, tempc, tempsf, tempst, gainScore)
	{
	  var xside = Js_otherTroop[side];
	  Js_nGameMoves += 1;
	  var f = node.f;
	  var t = node.t;
	  Js_indenSqr = -1;
	  Js_origSquare = f;
	  Js_destSquare = t;
	  gainScore.i = 0;
	  Js_movesList[Js_nGameMoves].gamMv = (f << 8 | t);
	  if ((node.flags & Js_castle_msk) != 0)
	  {
	    Js_movesList[Js_nGameMoves].piece = Js_empty;
	    Js_movesList[Js_nGameMoves].color = side;
	    DoCastle(side, f, t, 1);
	  }
	  else
	  {

	    tempc.i = Js_color[t];
	    tempb.i = Js_board[t];
	    tempsf.i = Js_scoreOnBoard[f];
	    tempst.i = Js_scoreOnBoard[t];
	    Js_movesList[Js_nGameMoves].piece = tempb.i;
	    Js_movesList[Js_nGameMoves].color = tempc.i;
	    if (tempc.i != Js_hollow)
	    {
	      UpdatePiecMap(tempc.i, t, 1);
	      if (tempb.i == Js_pawn)
	        Js_pawnMap[tempc.i][IColmn(t)] += -1;
	      if (Js_board[f] == Js_pawn)
	      {
	        Js_pawnMap[side][IColmn(f)] += -1;
	        Js_pawnMap[side][IColmn(t)] += 1;
	        var cf = IColmn(f);
	        var ct = IColmn(t);
	        if (Js_pawnMap[side][ct] > 1 + Js_pawnMap[side][cf])
	          gainScore.i -= 15;
	        else if (Js_pawnMap[side][ct] < 1 + Js_pawnMap[side][cf])
	          gainScore.i += 15;
	        else if ((ct == 0) || (ct == 7) || (Js_pawnMap[side][(ct + ct - cf)] == 0))
	          gainScore.i -= 15;
	      }
	      Js_matrl[xside] -= Js_valueMap[tempb.i];
	      if (tempb.i == Js_pawn) {
	        Js_pmatrl[xside] -= Js_pawnVal;
	      }
	 
	      gainScore.i += tempst.i;
	      Js_nMvtOnBoard[t] += 1;
	    }
	    Js_color[t] = Js_color[f];
	    Js_board[t] = Js_board[f];
	    Js_scoreOnBoard[t] = Js_scoreOnBoard[f];
	    Js_pieceIndex[t] = Js_pieceIndex[f];
	    Js_pieceMap[side][Js_pieceIndex[t]] = t;
	    Js_color[f] = Js_hollow;
	    Js_board[f] = Js_empty;
	    if (Js_board[t] == Js_pawn)
	      if (t - f == 16)
	        Js_indenSqr = (f + 8);
	      else if (f - t == 16)
	        Js_indenSqr = (f - 8);
	    if ((node.flags & Js_promote) != 0)
	    {
	      if (Js_proPiece != 0)
	        Js_board[t] = Js_proPiece;
	      else
	        Js_board[t] = (node.flags & Js_pawn_msk);
	      if (Js_board[t] == Js_queen)
	        Js_withQueen[side] += 1;
	      else if (Js_board[t] == Js_rook)
	        Js_withRook[side] += 1;
	      else if (Js_board[t] == Js_bishop)
	        Js_withBishop[side] += 1;
	      else if (Js_board[t] == Js_knight)
	        Js_withKnight[side] += 1;
	      Js_pawnMap[side][IColmn(t)] += -1;
	      Js_matrl[side] += Js_valueMap[Js_board[t]] - Js_pawnVal;
	      Js_pmatrl[side] -= Js_pawnVal;
	 
	      gainScore.i -= tempsf.i;
	    }
	    if ((node.flags & Js_enpassant_msk) != 0) {
	      PrisePassant(xside, f, t, 1);
	    }
	 
	    Js_nMvtOnBoard[f] += 1;
	  }
	}
	 
	function Peek(p1, p2)
	{
	  var s0 = Js_Tree[p1].score;
	  var p0 = p1;
	  for (var p = p1 + 1; p <= p2; ++p)
	  {
	    var s = Js_Tree[p].score;
	    if (s <= s0)
	      continue;
	    s0 = s;
	    p0 = p;
	  }
	 
	  if (p0 == p1)
	  {
	    return;
	  }
	 
	  MoveTree(Js_tmpTree, Js_Tree[p1]);
	  MoveTree(Js_Tree[p1], Js_Tree[p0]);
	  MoveTree(Js_Tree[p0], Js_tmpTree);
	}
	 
	function Seek(side, ply, depth, alpha, beta, bstline, rpt)
	{
	  var tempb = new _INT();
	  var tempc = new _INT();
	  var tempsf = new _INT();
	  var tempst = new _INT();
	  var rcnt = new _INT();

	  var slk = new _INT();
	  var InChk = new _INT();
	  var nxtline = [];		// new int[Js_maxDepth];

	  var node = new _BTREE();
	 
	  Js_cNodes += 1;
	  var xside = Js_otherTroop[side];

	  if (ply <= Js_depth_Seek + 3)
		{
		rpt.i = IRepeat(rpt.i);
		}
	  else
		{
		rpt.i = 0;
		}
	 
	  if ((rpt.i == 1) && (ply > 1))
	  {
	    if (Js_nMovesMade <= 11) {
	      return 100;
	    }
	    return 0;
	  }
	 
	  var score3 = DoCalc(side, ply, alpha, beta, Js_gainScore.i, slk, InChk);
	  if (score3 > 9000)
	  {
	    bstline[ply] = 0;
	 
	    return score3;
	  }
	 
	  if (depth > 0)
	  {
	    if (InChk.i != 0)
	    {
	      depth = (depth < 2) ? 2 : depth;
	    }
	    else if ((Js_menacePawn[(ply - 1)] != 0) || (
	      (Js_flag.recapture) && (score3 > alpha) && (score3 < beta) && (ply > 2) && (Js_flagEat[(ply - 1)] != 0) && (Js_flagEat[(ply - 2)] != 0)))
	    {
	      ++depth;
	    }
	 
	  }
	  else if ((score3 >= alpha) && ((
	    (InChk.i != 0) || (Js_menacePawn[(ply - 1)] != 0) || (
	    (Js_pinned[side] > 1) && (ply == Js_depth_Seek + 1)))))
	  {
	    ++depth;
	  }
	  else if ((score3 <= beta) && 
	    (ply < Js_depth_Seek + 4) && (ply > 4) && (Js_flagCheck[(ply - 2)] != 0) && (Js_flagCheck[(ply - 4)] != 0) && (Js_flagCheck[(ply - 2)] != Js_flagCheck[(ply - 4)]))
	  {
	    ++depth;
	  }
	  var d;
	  if (Js_depth_Seek == 1)
	    d = 7;
	  else
	    d = 11;
	  if ((ply > Js_depth_Seek + d) || ((depth < 1) && (score3 > beta)))
	  {
	    return score3;
	  }
	 
	  if (ply > 1) {
	    if (depth > 0)
	      AvailMov(side, ply);
	    else
	      AvailCaptur(side, ply);
	  }
	  if (Js_treePoint[ply] == Js_treePoint[(ply + 1)])
	  {
	    return score3;
	  }
	  var cf;
	  if ((depth < 1) && (ply > Js_depth_Seek + 1) && (Js_flagCheck[(ply - 2)] == 0) && (slk.i == 0))
	  {
	    cf = 1;
	  }
	  else cf = 0;
	  var best;
	  if (depth > 0)
	    best = -12000;
	  else {
	    best = score3;
	  }
	  if (best > alpha)
	    alpha = best;
	  var pbst = Js_treePoint[ply];
	  var pnt = pbst;
	  var j;
	  var mv;
	  while ((pnt < Js_treePoint[(ply + 1)]) && (best <= beta))
	  {
	    if (ply > 1) Peek(pnt, Js_treePoint[(ply + 1)] - 1);
	 

	    node = Js_Tree[pnt];		//_BTREE

	    mv = node.f << 8 | node.t;
	    nxtline[(ply + 1)] = 0;
	 
	    if ((cf != 0) && (score3 + node.score < alpha))
	    {
	      break;
	    }
	 
	    if ((node.flags & Js__idem) == 0)
	    {
	      ValidateMov(side, node, tempb, tempc, tempsf, tempst, Js_gainScore);
	      Js_flagEat[ply] = (node.flags & Js_capture);
	      Js_menacePawn[ply] = (node.flags & Js_menace_pawn);
	      Js_scoreTP[ply] = node.score;
	      Js_ptValue = node.replay;
	 
	      node.score = (-Seek(xside, ply + 1, (depth > 0) ? depth - 1 : 0, -beta, -alpha, nxtline, rcnt));
	 
	      if (Math.abs(node.score) > 9000)
	        node.flags |= Js__idem;
	      else if (rcnt.i == 1) {
	        node.score /= 2;
	      }
	 
	      if ((rcnt.i >= 2) || (Js_nGameMoves - Js_fiftyMoves > 99) || (
	        (node.score == 9999 - ply) && (Js_flagCheck[ply] == 0)))
	      {
	        node.flags |= Js__idem;
	        if (side == Js_computer)
	          node.score = Js_specialScore;
	        else
	          node.score = (-Js_specialScore);
	      }
	      node.replay = nxtline[(ply + 1)];

	      UnValidateMov(side, node, tempb, tempc, tempsf, tempst);

	    }
	 
	    if ((node.score > best) && (!(Js_flag.timeout)))
	    {
	      if ((depth > 0) && 
	        (node.score > alpha) && ((node.flags & Js__idem) == 0)) {
	        node.score += depth;
	      }
	      best = node.score;
	      pbst = pnt;
	      if (best > alpha) alpha = best;
	      for (j = ply + 1; nxtline[(++j)] > 0; )
	      {
	        bstline[j] = nxtline[j]; 
	      }
	      bstline[j] = 0;
	      bstline[ply] = mv;
	      if (ply == 1)
	      {
	        if (best > Js_root.score)
	        {
	          MoveTree(Js_tmpTree, Js_Tree[pnt]);
	          for (j = pnt - 1; j >= 0; --j)
	          {
	            MoveTree(Js_Tree[(j + 1)], Js_Tree[j]);
	          }
	          MoveTree(Js_Tree[0], Js_tmpTree);
	          pbst = 0;
	        }
	 
	        if (Js_depth_Seek > 2) ShowThink(best, bstline);

	      }
	    }
	    if (Js_flag.timeout)
	    {
	      return (-Js_scoreTP[(ply - 1)]);
	    }
	    ++pnt;
	  }
	 

	  node = Js_Tree[pbst];		//_BTREE

	  mv = node.f << 8 | node.t;
	 
	  if (depth > 0)
	  {
	    j = node.f << 6 | node.t;
	    if (side == Js_black)
	      j |= 4096;

	    if (Js_storage[j] < 150)
	      Js_storage[j] = (Js_storage[j] + depth * 2);	//(short)

	    if (node.t != (Js_movesList[Js_nGameMoves].gamMv & 0xFF)) {
	      if (best <= beta) {
	        Js_eliminate3[ply] = mv;
	      } else if (mv != Js_eliminate1[ply])
	      {
	        Js_eliminate2[ply] = Js_eliminate1[ply];
	        Js_eliminate1[ply] = mv;
	      }
	    }
	    if (best > 9000)
	      Js_eliminate0[ply] = mv;
	    else {
	      Js_eliminate0[ply] = 0;
	    }
	  }

	  if ((new Date()).getTime() - Js_startTime > Js_searchTimeout) {
	      Js_flag.timeout = true;
	  }

	  return best;
	}
	
	// This sets active side
	function SwitchSides( oposit )
	{
	 var whitemove = (Js_nGameMoves % 2 == 0);
	 var whitecomp = (Js_computer == Js_white);
	 if( oposit == ( whitemove == whitecomp) )
	 {

	 Js_player = Js_otherTroop[Js_player];
	 Js_computer = Js_otherTroop[Js_computer];
	 Js_enemy = Js_otherTroop[Js_enemy];

	 Js_JESTER_TOPLAY = Js_otherTroop[Js_JESTER_TOPLAY];

	 }
	 Js_fUserWin_kc=false;
	}


	function GetFen()
	{
	  var fen = "";
	  var i = 64-8;
	  var pt = 0;
	  do
	    {

	    var piece = (Js_color[i]==Js_white ? Js_upperNot[Js_board[i]] : Js_lowerNot[Js_board[i]]);
	    if(piece==" ") pt+=1;
	    else
		{
		 if(pt>0) { fen += pt.toString(); pt = 0; }
		 fen += piece;
		}
	    i++;
	    if(i % 8 == 0) i -= 16;
	    if( (i>=0) && (i % 8 == 0))
		{
		 if(pt>0) { fen += pt.toString(); pt = 0; }
		 fen += "/";
		}
	    }
	  while (i>=0);
 	  if(pt>0) { fen += pt.toString(); pt = 0; }	

	  fen += " " + ( (Js_nGameMoves % 2 == 0) ? "w" : "b" ) + " ";


	  var wKm = ( (Js_roquer[ Js_white ]>0) || (Js_nMvtOnBoard[Js_kingPawn[Js_white]]>0) );
	  var wLRm = ( Js_nMvtOnBoard[Js_queenRook[Js_white]]>0 );
	  var wRRm = ( Js_nMvtOnBoard[Js_kingRook[Js_white]]>0 );

	  var bKm = ( (Js_roquer[ Js_black ]>0) || (Js_nMvtOnBoard[Js_kingPawn[Js_black]]>0) );
	  var bLRm = ( Js_nMvtOnBoard[Js_queenRook[Js_black]]>0 );
	  var bRRm = ( Js_nMvtOnBoard[Js_kingRook[Js_black]]>0 );

	  if( (wKm || wLRm || wRRm) && (bKm || bLRm || bRRm) )
		{
		 fen += "-";
		}
	  else
		{
		if( (!wKm) && (!wRRm) ) fen += "K";
		if( (!wKm) && (!wLRm) ) fen += "Q";
		if( (!bKm) && (!bRRm) ) fen += "k";
		if( (!bKm) && (!bLRm) ) fen += "q";
		}
	  fen += " ";

	  if ((Js_root.flags & Js_enpassant_msk) != 0)
		{
		fen += Js_szAlgMvt[ Js_root.t - (Js_color[Js_root.t]==Js_white ? 8 : -8 ) ] + " ";
	    	}
	  else fen += "- ";

	  var mv50 = Js_nGameMoves - Js_fiftyMoves;
	  fen += (mv50>0 ? mv50.toString() : "0" ) + " ";

	  fen += (Js_nMovesMade + ((Js_nGameMoves % 2 == 0) ? 1 : 0)).toString();

	  return fen;

	}

		// for Js_enemy move only
		// use SwitchSides before if oposit movement required
		// ignores checkmate status flag

	function EnterMove( from_sq, to_sq, promo )
	{
	  var mvt = 0;
	  var fsq_mvt = 0;
	  var tsq_mvt = 0;

	  SwitchSides( true );

	  var i = 0;
	  do {
	    if(Js_szAlgMvt[i]==from_sq) fsq_mvt = i;
	    if(Js_szAlgMvt[i]==to_sq) tsq_mvt = i;
	  }
	  while (++i < 64);

	  Js_proPiece = 0;
	  i = 2;
	  do {
	     if( Js_upperNot[i]==promo ) Js_proPiece = i;
	  }
	  while (++i < 6);

	  Js_root.f = 0;
	  Js_root.t = 0;
	  Js_root.flags = 0;
	 
	  var rgch = [];		//new char[8];
	 
	  Js_myPiece = Js_rgszPiece[Js_board[fsq_mvt]];
	 
	  if (Js_board[fsq_mvt] == Js_pawn)
	  {
	    var iflag = 0;
	    if ( (tsq_mvt < 8) || (tsq_mvt > 55) )
	    {
	      iflag = Js_promote | Js_proPiece;
	    }
	    Lalgb(fsq_mvt, tsq_mvt, iflag);
	  }
	 
	  i=0;
	  rgch[i++] = from_sq.charCodeAt(0);	//(char)
	  rgch[i++] = from_sq.charCodeAt(1);
	  rgch[i++] = to_sq.charCodeAt(0);	//(char)
	  rgch[i++] = to_sq.charCodeAt(1);
	  if( promo.length>0 )
		{
		rgch[i++] = "=";
		rgch[i++] = promo;
		}
	  rgch[i++] = 0;
	 
	  Js_flag.timeout = true;
	 
	  var iMvt = CheckMov(rgch, 0);
	 
	  if (iMvt != 0)
	  {
	    WatchPosit();
	    UpdateDisplay();
	    IfCheck();
	    if (!(CheckMatrl())) Js_bDraw = 1;
	    ShowStat();
	 
	    ShowMov(rgch);
	 
	    Js_depth_Seek = 0;
	  }
							
	}

	// ignores flags...
	// use after InitGame

	function SetFen(fen)
	{
	  var fen2 = "";
	  var i = 0;
	  do
	    {
	    var ch = fen.charAt(i);
	    var pt = parseInt(ch);
	    if(pt>0)
		{
	    	while((pt--)>0) fen2 += " ";
		}
	    else
		{
		if(ch == " ") break;
		if(!(ch=="/")) fen2 += ch;
		}
	    }
	  while ((i++)<fen.length);

	  var i = 64-8;
	  var i2 = 0;
	  do
	    {
	    Js_board[i] = Js_empty;
	    Js_color[i] = Js_hollow;
	    var piece = fen2.charAt(i2++);
	    var j = 1;
	    do {
		if( (Js_upperNot[j]==piece) || (Js_lowerNot[j]==piece) )
			{
			Js_board[i] = j;
			Js_color[i] = ( (Js_upperNot[j]==piece) ? Js_white : Js_black );
			}
	       }
	    while (++j <= 6);

	    Js_nMvtOnBoard[i] = 1;

	    i++;
	    if(i % 8 == 0) i -= 16;
	    }
	  while (i>=0);

	  Js_roquer[ Js_white ] = 1;
	  Js_roquer[ Js_black ] = 1;

	  Js_root.f = 0;
	  Js_root.t = 0;
	  Js_root.flags = 0;

	  var side = "";
	  var enp = "";
	  var mcnt = "";
	  var mv50s = "";

	  var st = 0;
	  i = 0;
	  do
	    {
	    ch = fen.charAt(i);
	    if(ch == " ") st++;
	    else if(st == 1)
		{
		side = ch;
		}
	    else if(st == 2)
		{		
		if(ch=="k" || ch=="q")
			{
			Js_roquer[ Js_black ] = 0;
			Js_nMvtOnBoard[Js_kingPawn[Js_black]] = 0;

			if(ch=="q") Js_nMvtOnBoard[Js_queenRook[Js_black]] = 0;
			if(ch=="k") Js_nMvtOnBoard[Js_kingRook[Js_black]] = 0;
			}
		if(ch=="K" || ch=="Q")
			{
			Js_roquer[ Js_white ] = 0;
			Js_nMvtOnBoard[Js_kingPawn[Js_white]] = 0;

			if(ch=="Q") Js_nMvtOnBoard[Js_queenRook[Js_white]] = 0;
			if(ch=="K") Js_nMvtOnBoard[Js_kingRook[Js_white]] = 0;
			}
		}
	    else if(st == 3)
		{
		enp += ch;
		}
	    else if(st == 4)
		{
		mv50s += ch;
		}
	    else if(st == 5)
		{
		mcnt += ch;
		}		
	    }
	  while ((i++)<fen.length);

	  if(enp.length>0)
	  {
	   i = 0;
	   do
	    {
	    if(Js_szAlgMvt[i]==enp)
		{
		Js_root.f = ((i<32) ? i-8: i+8 );
		Js_root.t = ((i<32) ? i+8: i-8 );
		Js_root.flags |= Js_enpassant_msk;
		}
	    }
	   while (++i < 64);
	  }

	  Js_nGameMoves = (parseInt(mcnt) * 2) - (side=="w" ? 2 : 1);
	  Js_nMovesMade = parseInt(mcnt) - ((Js_nGameMoves % 2==0) ? 1 : 0 );
	  Js_fiftyMoves = Js_nGameMoves - parseInt( mv50s );

	  Js_flip = (Js_nGameMoves % 2 > 0);

	  MessageOut("(FEN)", true);
	  UpdateDisplay();
	  InitStatus();

	  ResetFlags();

	  Js_flag.mate = false;
	  Js_flag.recapture = true;

	  IfCheck();
	  if (!(CheckMatrl())) Js_bDraw = 1;
	  ShowStat();

	}

	function ResetFlags()
	{
	  Js_fInGame = true;
	  Js_fCheck_kc = false;
	  Js_fMate_kc = false;
	  Js_fAbandon = false;
	  Js_bDraw = 0;
	  Js_fStalemate = false;
	  Js_fUserWin_kc = false;
	}

	function Jst_Play()
	{
	  SwitchSides( false );

	  Js_fEat = false;
	  ResetFlags();

	  Js_realBestScore = -20000;
	  Js_realBestDepth = 0;
	  Js_realBestMove = 0;
	 
	  ComputerMvt();

	  UpdateDisplay();
	 
	}


	function UndoMov()
	{
	if (Js_nGameMoves > 0)
	  {
	  SwitchSides( false );

	  Undo();

	  UpdateDisplay();

	  ResetFlags();

	  ShowStat();
	  MessageOut("(undo)", true);

	  Js_flip = false;
	  if(Js_nGameMoves % 2 == 0)
		{
		 Js_nMovesMade -= 1;
		}
	  else
		{
		Js_flip = true;
		}  
	  }
	}


	//-----------------------------------------
	// SAMPLES...
	//-----------------------------------------

	// moves entering
	function autosample1()
	{
	EnterMove("e2","e4","");
	EnterMove("c7","c5","");
	EnterMove("f1","e2","");
	EnterMove("c5","c4","");
	EnterMove("b2","b4","");
	EnterMove("c4","b3","");
	EnterMove("g1","f3","");
	EnterMove("b3","b2","");
	EnterMove("e1","g1","");
	EnterMove("b2","a1","R");		// promote rook
	MessageOut("FEN:"+GetFen(),true);
	}
	
	// automatic game
	function autosample2()
	{
	Jst_Play();
	setTimeout('autosample2()', 1000);	// next move
	}

	// undo cases
	function autosample3()
	{
	EnterMove("e2","e4","");
	UndoMov();
	EnterMove("a2","a4","");
	EnterMove("c7","c5","");
	UndoMov();
	Jst_Play();
	UndoMov()
	MessageOut(GetFen(),true);
	}

	// set FEN case
	function autosample4()
	{
	SetFen("7k/Q7/2P2K2/8/8/8/8/8 w - - 0 40");	// set given FEN
	Jst_Play();
	MessageOut(GetFen(),true);
	}


	InitGame();		// Also to start a new game again
	//autosample1();
	//autosample2();
	//autosample3();
	//autosample4();

	//setTimeout('autosample3();',1000);
