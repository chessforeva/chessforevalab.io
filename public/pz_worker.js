// worker

self.onmessage = function(event) {
	
	var s = event.data;
	if(s.substr(0,7)=="SETFEN:") {
		setStr(s.substr(7));
		_instance.exports._set_pos();
		postMessage("posset-ok");
	}
	if(s.substr(0,6)=="HcMAX:") {
		_instance.exports._set_HcMAX( parseInt( s.substr(6)) );
		postMessage("hcmax:");
	}
	if(s.substr(0,5)=="CALC:") {
		var found = _instance.exports._pz( parseInt( s.substr(5)) );
		postMessage("pz:"+found);
	}
	
	if(s.substr(0,4)=="GET:") {
		var S = getStr();
		postMessage("res:"+S);
	}
	
	return true;
	
}
	
// WebAssembly objects, will be accessible
var memSize = 0;
var _memory = {};
var _instance = {};
var _wasm_ready = 0;
var _done = 0;

function LoadWasm() {

memSize = 256;
_memory = new WebAssembly.Memory({
      initial: memSize,
      maximum: memSize
    });

(async () => {
  const response = await fetch('pz.wasm');
  const bytes = await response.arrayBuffer();
  const { instance } = await WebAssembly.instantiate(bytes, {
    env: { memory: _memory }
  });

  _instance = instance;
  _instance.exports._init();		// init chess in .wasm file
  _wasm_ready = 1;

})();

}


// makes string by looping returned chars "ab...\0"
function getStr() {
	var s="",i=0;
	for(;;) {
		var c=_instance.exports._get(i++);
		if(!c) break;
		s+=String.fromCharCode(c);
	}
	return s;
}

// writes string by looping chars "ab...\0"
function setStr(s) {
	var i=0;
	for(; i<s.length; i++) {
		var c=s.charCodeAt(i);
		_instance.exports._set_data(i,c);
	}
	_instance.exports._set_data(i,0);
}

function Calc() {
	return _instance.exports._pz(1);
}

LoadWasm();