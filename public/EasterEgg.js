// just include this in source

// definition
EasterEgg_ = {
 s:[],
 g:0,	// generated flag
 canv: "EasterEggCanvas",	// id of canvas
 left: 400,     // where to
 top:  80,     //   put Easter Egg
 vert: 280,	// vertical px
 horz: 300,   // horizontal px
 cos60: Math.cos(Math.PI/3),
 sin60: Math.sin(Math.PI/3), 
 nest1: 200,
 nest2: 10,
 
 greens: [/*"#C7A317",*/ "#009900", "#008800", "#007700", "#006600", "#005500", "#004400", "#003300", "#002200" ],
 pinks: ["#E238EC", "#F660AB", "#C2DFFF", "#E0FFFF", "#C3FDB8", "#FDD7E4", "#E6A9EC" ],
 reds: ["#800000","#8B0000", "#A52A2A", "#B22222","#DC143C","#FF0000", "#FF6347"," #FF7F50","#CD5C5C"],
 yellows: ["#FF8C00","#FFA500","#FFD700", "#B8860B", "#DAA520","#EEE8AA"],
 blues: ["#20B2AA","#2F4F4F","#008080","#008B8B","#00FFFF","#00CED1", "#40E0D0","#4682B4","#8A2BE2"],
 browns: ["#8B4513","#A0522D","#D2691E","#CD853F","#F4A460","#DEB887","#D2B48C"],
 grays: ["#000000","#696969","#808080","#A9A9A9","#C0C0C0","#D3D3D3","#DCDCDC","#F5F5F5"],
 cols:[],
 
 vect: function(x,y,z) { return {x:x,y:y,z:z} },
 vectAdd: function(o1,o2) { return {x:o1.x+o2.x,y:o1.y+o2.y,z:o1.z+o2.z} },
 pXdraw: function(o) { return (176+o.x-(this.cos60*o.y)) },
 pYdraw: function(o) { return (80-o.z+(this.sin60*o.y)) },
 
 ellipse: function(cx, x, y, w, h, c)
 {
  x-=w/2; y-=h/2;
 
  var k = .5522848,
      ox = (w / 2) * k, // control point offset horizontal
      oy = (h / 2) * k, // control point offset vertical
      xe = x + w,       // x-end
      ye = y + h,       // y-end
      xm = x + w / 2,   // x-middle
      ym = y + h / 2;   // y-middle

  cx.beginPath();
  cx.strokeStyle = c[ parseInt(c.length*Math.random()) ];
  cx.fillStyle = cx.strokeStyle;
  cx.lineWidth = Math.max(5,w/3);
  cx.moveTo(x, ym);
  cx.bezierCurveTo(x, ym - oy, xm - ox, y, xm, y);
  cx.bezierCurveTo(xm + ox, y, xe, ym - oy, xe, ym);
  cx.bezierCurveTo(xe, ym + oy, xm + ox, ye, xm, ye);
  cx.bezierCurveTo(xm - ox, ye, x, ym + oy, x, ym);
  cx.closePath();
  cx.stroke();
 },

 
 starter: function()
  {
  if(this.isEasterWeek()) setInterval('EasterEgg_.ticker()',50);
  },
  
 // is this the week before Easter date (years 2015-2099)
 isEasterWeek: function()
  {
	var s,dt,dd,mm,yy,d,y,i,j,p,c,m,r=false;
	s="5,27m,16,1,21,12,4,17,9,31m,20,5,28m,16,1,21,13,28m,17,9,25m,13,5,25,10,1,21,6,29m,17,9,25m,";
	s+="14,5,18,10,2,21,6,29m,18,2,22,14,30m,18,10,26m,15,6,29m,11,3,22,14,30m,19,10,26m,15,7,19,11,3,";
	s+="23,7,30m,19,4,26m,15,31m,20,11,3,16,8,30m,12,4,24,15,31m,20,12,";

	dt = (new Date);
	mm = dt.getMonth();	
	if(mm>1 && mm<4)
	{
	yy = dt.getFullYear();
	for(i=15,p=0; i<100; i++)
	{
	 y = 2000+i;
	 if(y>yy) break;
	 c = s.substr(p,5);
	 j = c.indexOf(',');
	 if(y==yy)
	  {
	   c = c.substr(0,j);
	   m = (c.indexOf("m")>0);
	   d = parseInt(c);
	   dd = dt.getDate();
	   for(j=0;(!r) && (j<14);j++)
	    	{
	    	r = ( ( (mm + (m?1:0)) == 3) && (d==dd) );
		dt.setDate(++dd);
		dd = dt.getDate();
		mm = dt.getMonth();
		}
	  }
	 p+=j+1;
	}
	}
  return r;
  },
  
 ticker: function()
  {
    if(this.g>6) { /* just do nothing */}
    else if(this.g==0)		// generate Easter eggs
     {
	var body = document.body, html = document.documentElement;
	var height = Math.max( body.scrollHeight, body.offsetHeight, body.clientHeight,
                       html.clientHeight, html.scrollHeight, html.offsetHeight ); 
	var width = Math.max( body.scrollWidth, body.offsetWidth, body.clientWidth,
                       html.clientWidth, html.scrollWidth, html.offsetWidth );
	if(width<800 || height<400)	// screen too small, can this.g=9
		{
		this.left = 120;
		setTimeout('EasterEgg_.HideEggs()',20000);
		}
	else
		{
		this.left = (width * 3)/5;
		}
	this.s = [];

	this.cols = [ this.pinks, this.reds, this.yellows, 
				this.blues, this.browns, this.grays ];
	this.genEggs();
	this.g++;
	 }
   else if(this.g==1)		// create object
     {
	 this.createCanvas();
	 this.g++;
	 }
   else if(this.g==2)		// wait while canvas is ready
     {
	 var o = document.getElementById(this.canv);
	 if(o!=null) this.g++;
	 }
   else if(this.g==3)		// prepare Easter eggs
     {
	 if(!this.prepEggs()) this.g++;
	 }
   else if(this.g==4)		// draw Nest in background
     {
	 this.drawNest(false);
	 this.g++;
	 }
   else if(this.g==5)		// draw Easter eggs
	 {
	 if(!this.drawEggsInCanvas()) this.g++;
	 }
   else if(this.g==6)		// draw Nest in front
     {
	 this.drawNest(true);
	 this.g++;
	 }
  },

 HideEggs: function()
 {
	var o = document.getElementById(this.canv);
	if(o!=null)
	{
	o.style.visibility = "hidden";
	}
 },

 createCanvas: function()
  {
	var o = document.getElementById(this.canv);
	if(o==null)
	{
	o = document.createElement("CANVAS",this.canv);
	o.id = this.canv;
	o.style.position="absolute";
	o.style.top = this.top + "px";
	o.style.left = this.left + "px";
	o.style.width = this.horz + "px";
	o.style.height = this.vert + "px";
	o.width = this.horz;
	o.height = this.vert;
	o.style.zIndex = 3000;
	document.body.appendChild(o);
	}
  },
  
 genEggs: function()
  {
    var c,i,l=0,j,u="",z;
	c = 4;
	for(i=0;i<c;i++)
	 {
	  for(j=0;j<20;j++)
	   {
	   l = parseInt( Math.random() * this.cols.length );
	   z="{" +l+ "}";
	   if(u.indexOf(z)<0) { u+=z; break; }
	   }
	  
	  this.s.push({ pos:this.vect(((i%2)*40)-20,i*(20+(i*5)),0), d:true, q:true, 
		W: 1+parseInt( Math.random() * 3 ), to:((Math.random()*2>1)?1:-1),
		c: this.cols[l], P:[] }); 
	 }
  },
  
 prepEggs: function()
  {
  var w=false, i,si,r,v,xi,p,d;
  for(i=0; (!w) && (i<this.s.length); i++)
  {
	si=this.s[i];
	if(si.d)			// if should draw
	{
	v = 6;
	xi = 0;
	p = si.pos;
	d = this.vectAdd(p,{x:(-si.to)*50,y:0,z:0});
		
	for(r=1; r>0; r=Math.max(0,r+v),v-=(v>0 ? 0.3 : 0.2))
	 {  
	  si.P.push( { x:this.pXdraw(d), y:this.pYdraw(d), rx:(r*si.W)/3, ry:r } );
	  d = this.vectAdd(d,{x:(si.to*r)/(si.W*20),y:si.W/4,z:0});
	 }
	
	si.d=false;
	w=true;
	}
  }
  return w;
  
  },
  
 drawNest: function(b)
  {
  var Pi2 = Math.PI*2;
  var Pi90 = Pi2/4, Pi = Math.PI, Pi270 = Pi+Pi90;
  
  var cs,cx,i,j,N,si,p,n,px,py,d,a,a1,R,v0=this.vect(0,0,0);
  cs = document.getElementById(this.canv);
  cx = cs.getContext('2d');
  
  var X = this.pXdraw(v0), Y = this.pYdraw(v0);
  var x0 = X, x9 = X, y0 = Y, y9 = Y;
	
  for(i=0; i<this.s.length; i++)
    {
   	si=this.s[i];
	for(j=0; j<si.P.length; j++)
	 {
	 p = si.P[j];
	 if(p.x-p.rx<x0) x0=p.x-p.rx;
	 if(p.x+p.rx>x9) x9=p.x+p.rx;
	 if(p.y-p.ry<y0) y0=p.y-p.ry;
	 if(p.y+p.ry>y9) y9=p.y+p.ry;
	 }
	}
  x0-=X; x9-=X; y0-=Y; y9-=Y;
  if(x0>0 || x9<0 || y0>0 || y9<0) { this.g=0; return; }	// if bad case
	
  x0=-x0; y0=-y0;
  N = (b ? this.nest2 : this.nest1);
    
  for(i=0;i<N;i++)
	{
	cx.beginPath();
	cx.strokeStyle = this.greens[ parseInt(this.greens.length*Math.random()) ];
	cx.fillStyle = cx.strokeStyle;
	cx.lineWidth = 10;

	for(n=0;n<2;n++)
	{
	for(;;)
		{
		if(n==0) a = Math.random() * Pi2;
		else a = a1 + ((Math.random()>0.5 ? 1 : -1) * 
				(Math.random() * (b ? Pi/4 : Pi)));
		if(a>0 && a<Pi2 && ((!b) || ((a>Pi)&&(a<Pi270)))) break;
		}

	if(n==0) a1=a;

	if(a<Pi90)		//I
	 {
	  d = (x9-y0)/Pi90;
	  R = x9-(d*a); R*=0.8;
	  px = X+R*Math.cos(a);
	  py = Y-R*Math.sin(a);
	 }
	else if(a<Pi)		//II
	 {
	  a -= Pi90;
	  d = (x0-y0)/Pi90;
	  R = x0-(d*a); R*=0.8;
	  px = X-R*Math.cos(a);
	  py = Y-R*Math.sin(a);
	 }
	else if(a<Pi270)	//III
	 {
	  a -= Pi;
	  d = (x0-y9)/Pi90;
	  R = x0-(d*a); R*=1.1;
	  px = X-R*Math.cos(a);
	  py = Y+R*Math.sin(a);
	 }
	else 				//IV
	 {
	  a -= Pi270;
	  d = (x9-y9)/Pi90;
	  R = x9-(d*a); R*=1.1;
	  px = X+R*Math.cos(a);
	  py = Y+R*Math.sin(a);
	 }
	
	if(n==0) cx.moveTo(px, py);
	else cx.lineTo(px, py);
	}
	
	cx.closePath();
	cx.stroke();
	}
  },
  
 drawEggsInCanvas: function()
  {
  var w=false, cs,cx,i,j,si,p;
  for(i=0; (!w) && (i<this.s.length); i++)
  {
	si=this.s[i];
	if(si.q)			// if should draw
	{
	cs = document.getElementById(this.canv);
	cx = cs.getContext('2d');
	for(j=0;j<si.P.length;j++)
		{
		p = si.P[j];
		this.ellipse(cx, p.x, p.y, p.rx, p.ry, si.c );
		}
	si.q=false;
	w=true;
	}
  }
  return w;
  
  },

  removeCanvas: function()
  {
	var o = document.getElementById(this.canv);
	if(o!=null) document.body.removeChild(o);
  },
 
  };

setTimeout('EasterEgg_.starter()',2000);		// start after 2 secs.

