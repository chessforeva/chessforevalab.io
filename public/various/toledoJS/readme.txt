Toledo Javascript Chess pack. Jun/11/2010
by Oscar Toledo G.  biyubi@gmail.com
(c) Copyright 2009-2010 Oscar Toledo G.

This pack contains the following files:

  toledo_javascript_chess_3.html
    Toledo Javascript Chess, using only Unicode
    graphics for displaying pieces. 2416 bytes.

  toledo_javascript_chess_1.html
    Toledo Javascript Chess, using only letters for
    displaying pieces. 2274 bytes.

  toledo_javascript_chess_2.html
    Toledo Javascript Chess, using graphics for
    displaying pieces. 3249 bytes.

  0.gif     Empty square
  1.gif     Black pawn
  2.gif     Black king
  3.gif     Black knight
  4.gif     Black bishop
  5.gif     Black rook
  6.gif     Black queen
  9.gif     White pawn
  10.gif    White king
  11.gif    White knight
  12.gif    White bishop
  13.gif    White rook
  14.gif    White queen

  readme.txt
    This file.

Play level is fixed at 3-ply depth, you can increase it
editing the source code (only if you have a very fast browser).

The GIF pieces can be anything up to 40x40 pixels (currently 8x8
for saving space)

Visit http://nanochess.110mb.com for more information and other
interesting bits.
