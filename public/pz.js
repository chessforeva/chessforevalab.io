

// to escape old browser errors
function is_Wasm_supported() {
    try {
        if (typeof WebAssembly === "object"
            && typeof WebAssembly.instantiate === "function") {
            const module = new WebAssembly.Module(Uint8Array.of(0x0, 0x61, 0x73, 0x6d, 0x01, 0x00, 0x00, 0x00));
            if (module instanceof WebAssembly.Module)
                {
				var a = new WebAssembly.Instance(module) instanceof WebAssembly.Instance;
				if(a) return true;
				}
        }
    } catch (e) {
    }
    return false;
}

var webwork = {};

function startWorker() {
	if(typeof(Worker) !== "undefined") {
		webwork = new Worker("pz_worker.js");
		webwork.onmessage = function(event) {
			
			// received from worker...
			var s = event.data;
			if(s=="posset-ok") {
					RESULT = "";
					PzStatus=1;
				}
			if(s.substr(0,3)=="pz:") {
				var FF = parseInt( s.charAt(3) );
				if(FF==0||FF==2) {
					RESULT += "{M"+(PzStatus|0)+"} " + (FF==2 ? "{solutions>1}" : "");
					webwork.postMessage("GET:");
					}
				else {
					PzStatus=99;	// bad search
					RESULT += '{-stopped}';
					}
			}
			if(s.substr(0,4)=="res:") {
					var rr = s.substr(4);
					RESULT += rr;
					if(rr.length) {
							RESULT += '{-ok}';
							PzStatus=100;		// found something, stop search
						}
					else PzStatus = (PzStatus|0)+1;
				}
			setCalcstatus(0);
			}

		return true;
		}
	return false;
}

	
// on loading
function StartPz() {

	return ( is_Wasm_supported() && startWorker() );
}
