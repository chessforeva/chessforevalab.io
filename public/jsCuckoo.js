//=================================================
//
// EMULATED 64bit chess engine
// Of course it plays chess of JAVASCRIPT's strength
// ported by chessforeva.blogspot.com
// for fun and chess lovers...


/*
    CuckooChess - A java chess program
    Copyright (C) 2011  Peter Osterlund

 */

CHROME = (navigator.userAgent.indexOf("Chrome") >= 0);

SHORT_BOOK = !CHROME;	// Chrome is much faster and book takes much CPU...

/* constants */ 

var o_x1 = i64v(1);
var o_x60 = i64v(0x60);
var o_xC0 = i64v(0xC0);
var o_x06 = i64v(0x06);
var o_x03 = i64v(0x03);
var o_x0E = i64v(0x0E);
var o_xFF = i64v(0xFF);
var o_xL6 = i64_ax(0x60000000,0);
var o_xLv6 =i64_ax(0x6000000,0);
var o_xLv3 =i64_ax(0x3000000,0);
var o_xLE = i64_ax(0xE000000,0);
var o_xLC = i64_ax(0xC0000000,0);
var o_xE7 = i64v(0xe7);
var o_x18 = i64v(0x18);
var o_x0303 = i64_ax(0x03030000,0);
var o_xC0C0 = i64_ax(0xC0C00000,0);
var o_xLv0303 = i64v(0x0303);
var o_xLvC0C0 = i64v(0xC0C0);
var o_xFFFF = i64v(0xFFFF);
var o_xKxZ1 = i64_ax(0x0F,0x1F1F1F1F);
var o_xKxZ2 = i64_ax(0,0x071F1F1F);
var o_xKxZ3 = i64_ax(0,0xE0F8F8F8);
var o_xKxZ4 = i64_ax(0xF0,0xF8F8F8F8);

var o_xFF1 = i64_ax(0x00ff0000,0);
var o_xFF2 = i64_ax(0xff000000,0);
var o_xFF3 = i64_ax(0,0xff00);
var o_xFF4 = i64_ax(0,0xff);

var o_x81x = i64_ax(0x00810000,0x8100);

var e0a = []; var e0a64 = [];
for (var i = 0; i <1000; i++) { e0a[i] = 0; e0a64[i]=i64(); }

Int_ = { MAX_VALUE: 2147483647, MIN_VALUE: -2147483648 };

o_lshift1 = [];       // bitwises 1<<64

/* clones objects */
/* simple and good clone */

function clone(o) {
   var th = [];
   for (var i in o) th[i] = (typeof(o[i]) == 'object' ? clone(o[i]) :  o[i] );
   return th;
}

bitcnt_ = [];
bitlsb_ = [];
bithsb_ = [];
function prpsbit()
{
 for(var i=0;i<0x10000;i++)
 {
  var c=0; var o=i;
  for(var k=0;k<32;k++) { c+=((o&1)>>>0); o>>>=1; };
  bitcnt_[i] = c;
 }
   
 for(var i=0;i<0x10000;i++)
 {
  var c=-1; var o=i;
  for(var k=0;c<0 && k<32;k++) { if(o&1) c=k; o>>>=1; };
  bitlsb_[i] = c;
 }
   
 for(var i=0;i<0x10000;i++)
 {
  var c=-1; var o=i; var m=(1<<31);
  for(var k=31;c<0 && k>=0;k--) { if(o&m) c=k; m>>>=1; };
  bithsb_[i] = c;
 }
}
prpsbit();

/*=====================================  Piece */

/* Constants for different piece types */

/*public*/ /*class*/ Piece = {
    /*public*/ /*int*/ EMPTY:0,

    /*public*/ /*int*/ WKING:1,
    /*public*/ /*int*/ WQUEEN:2,
    /*public*/ /*int*/ WROOK:3,
    /*public*/ /*int*/ WBISHOP:4,
    /*public*/ /*int*/ WKNIGHT:5,
    /*public*/ /*int*/ WPAWN:6,

    /*public*/ /*int*/ BKING:7,
    /*public*/ /*int*/ BQUEEN:8,
    /*public*/ /*int*/ BROOK:9,
    /*public*/ /*int*/ BBISHOP:10,
    /*public*/ /*int*/ BKNIGHT:11,
    /*public*/ /*int*/ BPAWN:12,

    /*public*/ /*int*/ nPieceTypes:13,

    /**
     * Return true if p is a white piece, false otherwise.
     * Note that if p is EMPTY, an unspecified value is returned.
     */
    /*public*/ /*boolean*/  isWhite:function(/*int*/ pType) {
        return pType < this.BKING;
    },
    /*public*/ /*int*/ makeWhite:function(/*int*/ pType) {
        return pType < this.BKING ? pType : pType - (this.BKING - this.WKING);
    },
    /*public*/ /*int*/ makeBlack:function(/*int*/ pType) {
        return ((pType > this.EMPTY) && (pType < this.BKING)) ? pType + (this.BKING - this.WKING) : pType;
    }
};
    
/*===================================== BitBoard */

/*public*/ /*class*/ BitBoard = {

 // creates new object 

    initwas: false,
    /** Squares attacked by a king on a given square. */
    /*public*/ /*long[]*/ kingAttacks:[],
    /*public*/ /*long[]*/ knightAttacks:[],
    /*public*/ /*long[]*/ wPawnAttacks:[], bPawnAttacks:[],

    /* Squares preventing a pawn from being a passed pawn, if occupied by enemy pawn */
    /*long[]*/ wPawnBlockerMask:[], bPawnBlockerMask:[],

    /*public*/ /*long*/ maskAToGFiles:i64s("0x7F7F7F7F7F7F7F7FL"),
    /*public*/ /*long*/ maskBToHFiles:i64s("0xFEFEFEFEFEFEFEFEL"),
    /*public*/ /*long*/ maskAToFFiles:i64s("0x3F3F3F3F3F3F3F3FL"),
    /*public*/ /*long*/ maskCToHFiles:i64s("0xFCFCFCFCFCFCFCFCL"),

    /*public*/ /*long[]*/ maskFile:[
        i64s("0x0101010101010101L"),
        i64s("0x0202020202020202L"),
        i64s("0x0404040404040404L"),
        i64s("0x0808080808080808L"),
        i64s("0x1010101010101010L"),
        i64s("0x2020202020202020L"),
        i64s("0x4040404040404040L"),
        i64s("0x8080808080808080L")
    ],

    /*public*/ /*long*/ maskRow1:i64s("0x00000000000000FFL"),
    /*public*/ /*long*/ maskRow2:i64s("0x000000000000FF00L"),
    /*public*/ /*long*/ maskRow3:i64s("0x0000000000FF0000L"),
    /*public*/ /*long*/ maskRow4:i64s("0x00000000FF000000L"),
    /*public*/ /*long*/ maskRow5:i64s("0x000000FF00000000L"),
    /*public*/ /*long*/ maskRow6:i64s("0x0000FF0000000000L"),
    /*public*/ /*long*/ maskRow7:i64s("0x00FF000000000000L"),
    /*public*/ /*long*/ maskRow8:i64s("0xFF00000000000000L"),
    /*public*/ /*long*/ maskRow1Row8:i64s("0xFF000000000000FFL"),

    /*public*/ /*long*/ maskDarkSq:i64s("0xAA55AA55AA55AA55L"),
    /*public*/ /*long*/ maskLightSq:i64s("0x55AA55AA55AA55AAL"),

    /*public*/ /*long*/ maskCorners:i64s("0x8100000000000081L"),

    /*private*/ /*long[]*/ rTables:[],
    /*private*/ /*long[]*/ rMasks:[],
    /*private*/ /*int[]*/ rBits: [ 12, 11, 11, 11, 11, 11, 11, 12,
                                         11, 10, 10, 10, 10, 10, 10, 11,
                                         11, 10, 10, 10, 10, 10, 10, 11,
                                         11, 10, 10, 10, 10, 10, 10, 11,
                                         11, 10, 10, 10, 10, 10, 10, 11,
                                         11, 10, 10, 10, 10, 10, 10, 11,
                                         10,  9,  9,  9,  9,  9, 10, 10,
                                         11, 10, 10, 10, 10, 11, 11, 11 ],
    /*private*/ /*long[]*/ rMagics: [
        i64s("0x0080011084624000L"), i64s("0x1440031000200141L"), i64s("0x2080082004801000L"), i64s("0x0100040900100020L"),
        i64s("0x0200020010200408L"), i64s("0x0300010008040002L"), i64s("0x040024081000a102L"), i64s("0x0080003100054680L"),
        i64s("0x1100800040008024L"), i64s("0x8440401000200040L"), i64s("0x0432001022008044L"), i64s("0x0402002200100840L"),
        i64s("0x4024808008000400L"), i64s("0x100a000410820008L"), i64s("0x8042001144020028L"), i64s("0x2451000041002082L"),
        i64s("0x1080004000200056L"), i64s("0xd41010c020004000L"), i64s("0x0004410020001104L"), i64s("0x0000818050000800L"),
        i64s("0x0000050008010010L"), i64s("0x0230808002000400L"), i64s("0x2000440090022108L"), i64s("0x0488020000811044L"),
        i64s("0x8000410100208006L"), i64s("0x2000a00240100140L"), i64s("0x2088802200401600L"), i64s("0x0a10100180080082L"),
        i64s("0x0000080100110004L"), i64s("0x0021002300080400L"), i64s("0x8400880400010230L"), i64s("0x2001008200004401L"),
        i64s("0x0000400022800480L"), i64s("0x00200040e2401000L"), i64s("0x4004100084802000L"), i64s("0x0218800800801002L"),
        i64s("0x0420800800800400L"), i64s("0x002a000402001008L"), i64s("0x0e0b000401008200L"), i64s("0x0815908072000401L"),
        i64s("0x1840008002498021L"), i64s("0x1070122002424000L"), i64s("0x1040200100410010L"), i64s("0x0600080010008080L"),
        i64s("0x0215001008010004L"), i64s("0x0000020004008080L"), i64s("0x1300021051040018L"), i64s("0x0004040040820001L"),
        i64s("0x48fffe99fecfaa00L"), i64s("0x48fffe99fecfaa00L"), i64s("0x497fffadff9c2e00L"), i64s("0x613fffddffce9200L"),
        i64s("0xffffffe9ffe7ce00L"), i64s("0xfffffff5fff3e600L"), i64s("0x2000080281100400L"), i64s("0x510ffff5f63c96a0L"),
        i64s("0xebffffb9ff9fc526L"), i64s("0x61fffeddfeedaeaeL"), i64s("0x53bfffedffdeb1a2L"), i64s("0x127fffb9ffdfb5f6L"),
        i64s("0x411fffddffdbf4d6L"), i64s("0x0005000208040001L"), i64s("0x264038060100d004L"), i64s("0x7645fffecbfea79eL"),
    ],

    /*private*/ /*long[]*/ bTables:[],
    /*private*/ /*long[]*/ bMasks:[],
    /*private*/ /*int[]*/ bBits: [ 5, 4, 5, 5, 5, 5, 4, 5,
                                         4, 4, 5, 5, 5, 5, 4, 4,
                                         4, 4, 7, 7, 7, 7, 4, 4,
                                         5, 5, 7, 9, 9, 7, 5, 5,
                                         5, 5, 7, 9, 9, 7, 5, 5,
                                         4, 4, 7, 7, 7, 7, 4, 4,
                                         4, 4, 5, 5, 5, 5, 4, 4,
                                         5, 4, 5, 5, 5, 5, 4, 5 ],
    /*private*/ /*long[]*/ bMagics: [
        i64s("0xffedf9fd7cfcffffL"), i64s("0xfc0962854a77f576L"), i64s("0x9010210041047000L"), i64s("0x52242420800c0000L"),
        i64s("0x884404220480004aL"), i64s("0x0002080248000802L"), i64s("0xfc0a66c64a7ef576L"), i64s("0x7ffdfdfcbd79ffffL"),
        i64s("0xfc0846a64a34fff6L"), i64s("0xfc087a874a3cf7f6L"), i64s("0x02000888010a2211L"), i64s("0x0040044040801808L"),
        i64s("0x0880040420000000L"), i64s("0x0000084110109000L"), i64s("0xfc0864ae59b4ff76L"), i64s("0x3c0860af4b35ff76L"),
        i64s("0x73c01af56cf4cffbL"), i64s("0x41a01cfad64aaffcL"), i64s("0x1010000200841104L"), i64s("0x802802142a006000L"),
        i64s("0x0a02000412020020L"), i64s("0x0000800040504030L"), i64s("0x7c0c028f5b34ff76L"), i64s("0xfc0a028e5ab4df76L"),
        i64s("0x0020082044905488L"), i64s("0xa572211102080220L"), i64s("0x0014020001280300L"), i64s("0x0220208058008042L"),
        i64s("0x0001010000104016L"), i64s("0x0005114028080800L"), i64s("0x0202640000848800L"), i64s("0x040040900a008421L"),
        i64s("0x400e094000600208L"), i64s("0x800a100400120890L"), i64s("0x0041229001480020L"), i64s("0x0000020080880082L"),
        i64s("0x0040002020060080L"), i64s("0x1819100100c02400L"), i64s("0x04112a4082c40400L"), i64s("0x0001240130210500L"),
        i64s("0xdcefd9b54bfcc09fL"), i64s("0xf95ffa765afd602bL"), i64s("0x008200222800a410L"), i64s("0x0100020102406400L"),
        i64s("0x80a8040094000200L"), i64s("0x002002006200a041L"), i64s("0x43ff9a5cf4ca0c01L"), i64s("0x4bffcd8e7c587601L"),
        i64s("0xfc0ff2865334f576L"), i64s("0xfc0bf6ce5924f576L"), i64s("0x0900420442088104L"), i64s("0x0062042084040010L"),
        i64s("0x01380810220a0240L"), i64s("0x0000101002082800L"), i64s("0xc3ffb7dc36ca8c89L"), i64s("0xc3ff8a54f4ca2c89L"),
        i64s("0xfffffcfcfd79edffL"), i64s("0xfc0863fccb147576L"), i64s("0x0050009040441000L"), i64s("0x00139a0000840400L"),
        i64s("0x9080000412220a00L"), i64s("0x0000002020010a42L"), i64s("0xfc087e8e4bb2f736L"), i64s("0x43ff9e4ef4ca2c89L"),
    ],

    /*private*/ /*byte*/ dirTable:
     [ -9,   0,   0,   0,   0,   0,   0,  -8,   0,   0,   0,   0,   0,   0,  -7,
        0,   0,  -9,   0,   0,   0,   0,   0,  -8,   0,   0,   0,   0,   0,  -7,   0,
        0,   0,   0,  -9,   0,   0,   0,   0,  -8,   0,   0,   0,   0,  -7,   0,   0,
        0,   0,   0,   0,  -9,   0,   0,   0,  -8,   0,   0,   0,  -7,   0,   0,   0,
        0,   0,   0,   0,   0,  -9,   0,   0,  -8,   0,   0,  -7,   0,   0,   0,   0,
        0,   0,   0,   0,   0,   0,  -9, -17,  -8, -15,  -7,   0,   0,   0,   0,   0,
        0,   0,   0,   0,   0,   0, -10,  -9,  -8,  -7,  -6,   0,   0,   0,   0,   0,
        0,  -1,  -1,  -1,  -1,  -1,  -1,  -1,   0,   1,   1,   1,   1,   1,   1,   1,
        0,   0,   0,   0,   0,   0,   6,   7,   8,   9,  10,   0,   0,   0,   0,   0,
        0,   0,   0,   0,   0,   0,   7,  15,   8,  17,   9,   0,   0,   0,   0,   0,
        0,   0,   0,   0,   0,   7,   0,   0,   8,   0,   0,   9,   0,   0,   0,   0,
        0,   0,   0,   0,   7,   0,   0,   0,   8,   0,   0,   0,   9,   0,   0,   0,
        0,   0,   0,   7,   0,   0,   0,   0,   8,   0,   0,   0,   0,   9,   0,   0,
        0,   0,   7,   0,   0,   0,   0,   0,   8,   0,   0,   0,   0,   0,   9,   0,
        0,   7,   0,   0,   0,   0,   0,   0,   8,   0,   0,   0,   0,   0,   0,   9  ],

 
    /*private*/ /*int*/ trailingZ:
      [ 63,  0, 58,  1, 59, 47, 53,  2,
        60, 39, 48, 27, 54, 33, 42,  3,
        61, 51, 37, 40, 49, 18, 28, 20,
        55, 30, 34, 11, 43, 14, 22,  4,
        62, 57, 46, 52, 38, 26, 32, 41,
        50, 36, 17, 19, 29, 10, 13, 21,
        56, 45, 25, 31, 35, 16,  9, 12,
        44, 24, 15,  8, 23,  7,  6,  5 ],

    /*long[]*/ squaresBetween:[],

    none: 0
    
}; 

// BitBoard functions

function /*int*/ numberOfTrailingZeros (/*long*/ mask) {

	return i64_bitlowestat(mask);

	/* original */
        return BitBoard.trailingZ[ i64_rshift(  i64_mul( i64_and(mask, i64_neg(mask)) ,
         i64_ax(0x07EDD5E5,0x9A4E28C2) ) , 58).l ];
    }


function /*int*/ getDirection (/*int*/ from, /*int*/ to) {
        var /*int*/ offs = to + (to|7) - from - (from|7) + 0x77;
        return BitBoard.dirTable[offs];
    }

function /*public*/ /*long*/ southFill (/*long*/ mask) {
        mask = i64_or(mask, i64_rshift(mask,8));
        mask = i64_or(mask, i64_rshift(mask,16));
        mask = i64_or(mask, i64_rshift(mask,32));
        return mask;
    }
    
function /*public*/ /*long*/ northFill (/*long*/ mask) {
        mask = i64_or(mask, i64_lshift(mask,8));
        mask = i64_or(mask, i64_lshift(mask,16));
        mask = i64_or(mask, i64_lshift(mask,32));
        return mask;
    }

function InitRookRays()
    {
     // Rook magics
       for (var /*int*/ sq = 0; sq < 64; sq++)  o_lshift1[sq] = i64_lshift(o_x1,sq);
       
       for (var /*int*/ sq = 0; sq < 64; sq++) {
            var /*int*/ x = Position.getX(sq);
            var /*int*/ y = Position.getY(sq);
            BitBoard.rMasks[sq] = addRookRays(x, y, i64(), true);
            var /*int*/ tableSize = 1 << BitBoard.rBits[sq];
            BitBoard.rTables[sq] = [];
            var /*long[]*/ table = BitBoard.rTables[sq];	/*new long[tableSize]*/
            for (var /*int*/ i = 0; i < tableSize; i++) table[i] = i64v(-1);
            var /*int*/ nPatterns = 1 << i64_bitcount(BitBoard.rMasks[sq]) /*Long.bitCount()*/;
            for (/*int*/ i = 0; i < nPatterns; i++) {
                var /*long*/ p = createPattern(i, BitBoard.rMasks[sq]);
                var /*int*/ entry = i64_rshift( i64_mul(p,BitBoard.rMagics[sq]), (64 - BitBoard.rBits[sq])).l;
                if (table[entry].l == -1) table[entry] = addRookRays(x, y, p, false);
            }
            for (i=0; i < tableSize; i++) if(table[i].l == -1) table[i] = cs64.MAX_VALUE;
        }
    }

function InitBishopRays()
    {
     // Bishop magics     
       for (var /*int*/ sq = 0; sq < 64; sq++) {
            var /*int*/ x = Position.getX(sq);
            var /*int*/ y = Position.getY(sq);
            BitBoard.bMasks[sq] = addBishopRays(x, y, i64(), true);
            var /*int*/ tableSize = 1 << BitBoard.bBits[sq];
            BitBoard.bTables[sq] = [];
            var /*long[]*/ table = BitBoard.bTables[sq];	/*new long[tableSize]*/
            for (var /*int*/ i = 0; i < tableSize; i++) table[i] = i64v(-1);
            var /*int*/ nPatterns = 1 << i64_bitcount(BitBoard.bMasks[sq]) /*Long.bitCount()*/;
            for (/*int*/ i = 0; i < nPatterns; i++) {
                var /*long*/ p = createPattern(i, BitBoard.bMasks[sq]);
                var /*int*/ entry = i64_rshift( i64_mul(p,BitBoard.bMagics[sq]), (64 - BitBoard.bBits[sq])).l;
                if (table[entry].l == -1) table[entry] = addBishopRays(x, y, p, false);
            }
            for (i=0; i < tableSize; i++) if(table[i].l == -1) table[i] = cs64.MAX_VALUE;
        }       
    }

function /*public*/ /*long*/ bishopAttacks(/*int*/ sq, /*long*/ occupied) {
        return BitBoard.bTables[sq][ i64_rshift(i64_mul( i64_and(occupied,BitBoard.bMasks[sq]) ,
             BitBoard.bMagics[sq]), (64 - BitBoard.bBits[sq])).l ];
    }


function /*public*/ /*long*/ rookAttacks (/*int*/ sq, /*long*/ occupied) {
        return BitBoard.rTables[sq][ i64_rshift(i64_mul( i64_and(occupied,BitBoard.rMasks[sq]) ,
             BitBoard.rMagics[sq]), (64 - BitBoard.rBits[sq])).l ];
    }

function InitKNpAttacks()
    {
     var m=i64(); var m1=i64(); var m2=i64(); var m3=i64(); var m4=i64();
     
        // Compute king attacks
        for (var /*int*/ sq = 0; sq < 64; sq++) {
            /*long*/ m = i64bitObj(sq);
            /*long*/ m1 = i64_or( i64_rshift(m,1), i64_lshift(m,7) );
            m1 = i64_and( i64_or( m1, i64_rshift(m,9) ), BitBoard.maskAToGFiles );
            /*long*/ m2 = i64_or( i64_lshift(m,1), i64_lshift(m,9) );
            m2 = i64_and( i64_or( m2, i64_rshift(m,7) ), BitBoard.maskBToHFiles );
            /*long*/ m3 = i64_or( i64_lshift(m,8), i64_rshift(m,8) );                 
            BitBoard.kingAttacks[sq] = i64_or( m1, i64_or( m2, m3 ) );
        }

        // Compute knight attacks
        for (/*int*/ sq = 0; sq < 64; sq++) {
            /*long*/ m = i64bitObj(sq);
            /*long*/ m1 = i64_and( i64_or( i64_lshift(m,6), i64_rshift(m,10) ), BitBoard.maskAToFFiles );
            /*long*/ m2 = i64_and( i64_or( i64_lshift(m,15), i64_rshift(m,17) ), BitBoard.maskAToGFiles );
            /*long*/ m3 = i64_and( i64_or( i64_lshift(m,17), i64_rshift(m,15) ), BitBoard.maskBToHFiles );
            /*long*/ m4 = i64_and( i64_or( i64_lshift(m,10), i64_rshift(m,6) ), BitBoard.maskCToHFiles );           
            BitBoard.knightAttacks[sq] = i64_or( m1, i64_or( m2, i64_or( m3, m4 ) ) );
        }

        // Compute pawn attacks       
        for (/*int*/ sq = 0; sq < 64; sq++) {
            /*long*/ m = i64bitObj(sq);
            /*long*/ m1 = i64_and( i64_lshift(m,7), BitBoard.maskAToGFiles );
            /*long*/ m2 = i64_and( i64_lshift(m,9), BitBoard.maskBToHFiles );
            BitBoard.wPawnAttacks[sq] = i64_or( m1, m2 );
            /*long*/ m1 = i64_and( i64_rshift(m,9), BitBoard.maskAToGFiles );
            /*long*/ m2 = i64_and( i64_rshift(m,7), BitBoard.maskBToHFiles );            
            BitBoard.bPawnAttacks[sq] =  i64_or( m1, m2 );
            
            var /*int*/ x = Position.getX(sq);
            var /*int*/ y = Position.getY(sq);
            m = i64();
            for (var /*int*/ y2 = y+1; y2 < 8; y2++) {
                if (x > 0) m = i64_or( m, i64bitObj( Position.getSquare(x-1, y2) ) );
                           m = i64_or( m, i64bitObj( Position.getSquare(x  , y2) ) );
                if (x < 7) m = i64_or( m, i64bitObj( Position.getSquare(x+1, y2) ) );
            }
            BitBoard.wPawnBlockerMask[sq] = i64c(m);
            m = i64();
            for (/*int*/ y2 = y-1; y2 >= 0; y2--) {
                if (x > 0) m = i64_or( m, i64bitObj( Position.getSquare(x-1, y2) ) );
                           m = i64_or( m, i64bitObj( Position.getSquare(x  , y2) ) );
                if (x < 7) m = i64_or( m, i64bitObj( Position.getSquare(x+1, y2) ) );
            }
            BitBoard.bPawnBlockerMask[sq] = i64c(m);
        }
    }

function Init_squaresBetween()
    {
        for (var /*int*/ sq1 = 0; sq1 < 64; sq1++) {
            BitBoard.squaresBetween[sq1] = []; /*new long[64]*/
            for (var /*int*/ j = 0; j < 64; j++)
                BitBoard.squaresBetween[sq1][j] = i64();
            for (var /*int*/ dx = -1; dx <= 1; dx++) {
                for (var /*int*/ dy = -1; dy <= 1; dy++) {
                    if ((dx != 0) || (dy != 0))
                    {
                        var /*long*/ m = i64();
                        var /*int*/ x = Position.getX(sq1);
                        var /*int*/ y = Position.getY(sq1);
                        while (true) {
                            x += dx; y += dy;
                            if ((x < 0) || (x > 7) || (y < 0) || (y > 7)) break;
                            var /*int*/ sq2 = Position.getSquare(x, y);
                            BitBoard.squaresBetween[sq1][sq2] = i64c(m);
                            m = i64_or( m, i64bitObj(sq2) );
                        }
                    }
                }
            }
        }
    }

function /*private*/ /*long*/ createPattern (/*int*/ i, /*long*/ mask) {
        var /*long*/ ret = i64()/*0L*/;
        for (var/*int*/ j = 0; ; j++) {
            // better is to use i64_bitclear( mask, bit );
            // but we do not know which of bits, so we clear bit by a tricky a&=a-1
            var /*long*/ nextMask = i64_and( mask , i64_sub(mask,o_x1));
            var /*long*/ bit = i64_xor( mask , nextMask);
            if ((i & (1 << j)) != 0)
                ret = i64_or( ret, bit );
            mask = nextMask;
            if (mask.l == 0 && mask.h == 0)
                break;
        }
        return ret;
    }
    
function /*private*/ /*long*/ addRookRays (/*int*/ x, /*int*/ y, /*long*/ occupied, /*boolean*/  inner) {
        var /*long*/ mask = i64();
        mask = addRay(mask, x, y,  1,  0, occupied, inner);
        mask = addRay(mask, x, y, -1,  0, occupied, inner);
        mask = addRay(mask, x, y,  0,  1, occupied, inner);
        mask = addRay(mask, x, y,  0, -1, occupied, inner);
        return mask;
    }

function /*private*/ /*long*/ addBishopRays (/*int*/ x, /*int*/ y, /*long*/ occupied, /*boolean*/  inner) {
        var /*long*/ mask = i64();
        mask = addRay(mask, x, y,  1,  1, occupied, inner);
        mask = addRay(mask, x, y, -1, -1, occupied, inner);
        mask = addRay(mask, x, y,  1, -1, occupied, inner);
        mask = addRay(mask, x, y, -1,  1, occupied, inner);
        return mask;
    }

function /*private*/ /*long*/ addRay (/*long*/ mask, /*int*/ x, /*int*/ y, /*int*/ dx, /*int*/ dy, 
                                     /*long*/ occupied, /*boolean*/  inner) {
        var /*int*/ lo = inner ? 1 : 0;
        var /*int*/ hi = inner ? 6 : 7;
        while (true) {
            if (dx != 0) {
                x += dx; if ((x < lo) || (x > hi)) break;
            }
            if (dy != 0) {
                y += dy; if ((y < lo) || (y > hi)) break;
            }
            var /*int*/ sq = Position.getSquare(x, y);
            var sq2 = o_lshift1[sq];
            mask = i64_or( mask, sq2 );
            var q = i64_and(occupied ,sq2);
            if (q.l!=0 || q.h!=0)
                break;
        }

        return mask;
    }

function InitBitDataAll_() {
    if( !BitBoard.initwas )
    {
     InitKNpAttacks();
     InitRookRays();
     InitBishopRays();
     Init_squaresBetween();
     BitBoard.initwas = true;
    }
}

/*=====================================  UndoInfo */

/**
 * Contains enough information to undo a previous move.
 * Set by makeMove(). Used by unMakeMove().
 */
function /*public*/ /*class*/ UndoInfo() {
    /*int*/ this.capturedPiece = 0;
    /*int*/ this.castleMask = i64();
    /*int*/ this.epSquare = 0;
    /*int*/ this.halfMoveClock = 0;
} 

/*=====================================  Move */

/*public*/ /*class*/

function /*public*/ Move(/*int*/ from, /*int*/ to, /*int*/ promoteTo, /*int*/ score) {
        this.from = from;
        this.to = to;
        this.promoteTo = promoteTo;
        this.score = score;
    }
  
    /*public*/ /*class*/ /*SortByScore implements Comparator<Move> */
function /*public*/ /*int*/ MoveCompare(/*Move*/ sm1, /*Move*/ sm2) {
            return sm2.score - sm1.score;
    }
    
    /** Note that score is not included in the comparison. */
    /*@Override*/
function /*public*/ /*boolean*/  equalsMove(/*Move*/ m, /*Object*/ other) {
        if (m==null || other==null) return false;
        if (m.from != other.from)
            return false;
        if (m.to != other.to)
            return false;
        if (m.promoteTo != other.promoteTo)
            return false;
        return true;
    }


/*=====================================  MoveGen */

/*public*/ /*class*/ MoveGen = {

    /*public*/ /*class*/ MoveList: function() {
        return {
        /*public*/ /*Move[]*/ m:[],
        /*public*/ /*int*/ size:0
        };
    },

    /**
     * Generate and return a list of pseudo-legal moves.
     * Pseudo-legal means that the moves doesn't necessarily defend from check threats.
     */
    /*public*/ /*MoveList*/ pseudoLegalMoves: function(/*Position*/ pos) {
        var sq,squares,m,k0,knights,pawns;
        var /*MoveList*/ moveList = this.getMoveListObj();
        var /*long*/ occupied = i64_or(pos.whiteBB, pos.blackBB);
        var Noccupied = i64_not(occupied);
        var NwhiteBB = i64_not(pos.whiteBB);
        var NblackBB = i64_not(pos.blackBB);
        var /*int*/ epSquare = pos.getEpSquare();
        var /*long*/ epMask = ((epSquare >= 0) ? i64bitObj(epSquare) : i64() );
           
        if (pos.whiteMove) {
            // Queen moves
            /*long*/ squares = i64c(pos.pieceTypeBB[Piece.WQUEEN]);
            while (squares.l||squares.h) {
                /*int*/ sq = numberOfTrailingZeros(squares);
                /*long*/ m = i64_and( i64_or(rookAttacks(sq, occupied),
                     bishopAttacks(sq, occupied)), NwhiteBB );
                if (this.addMovesByMask(moveList, pos, sq, m)) return moveList;
                i64_bitclear( squares, sq );
            }

            // Rook moves
            squares = i64c(pos.pieceTypeBB[Piece.WROOK]);
            while (squares.l||squares.h) {
                /*int*/ sq = numberOfTrailingZeros(squares);
                /*long*/ m = i64_and( rookAttacks(sq, occupied), NwhiteBB );
                if (this.addMovesByMask(moveList, pos, sq, m)) return moveList;
                i64_bitclear( squares, sq );
            }

            // Bishop moves
            squares = i64c(pos.pieceTypeBB[Piece.WBISHOP]);
            while (squares.l||squares.h) {
                /*int*/ sq = numberOfTrailingZeros(squares);
                /*long*/ m = i64_and( bishopAttacks(sq, occupied), NwhiteBB );
                if (this.addMovesByMask(moveList, pos, sq, m)) return moveList;
                i64_bitclear( squares, sq );
            }

            // King moves
            {
                /*int*/ sq = pos.getKingSq(true);
                /*long*/ m = i64_and( BitBoard.kingAttacks[sq], NwhiteBB );
                if (this.addMovesByMask(moveList, pos, sq, m)) return moveList;
                /*int*/ k0 = 4;
                if (sq == k0) {
                    var /*long*/ OO_SQ = o_x60;
                    var /*long*/ OOO_SQ = o_x0E;
                    if (((pos.getCastleMask() & (Position.H1_CASTLE)) != 0) &&
                        ( i64_is0( i64_and(OO_SQ,occupied) ) ) &&
                        (pos.getPiece(k0 + 3) == Piece.WROOK) &&
                        !this.sqAttacked(pos, k0) &&
                        !this.sqAttacked(pos, k0 + 1)) {
                        this.setMove(moveList, k0, k0 + 2, Piece.EMPTY);
                    }
                    if (((pos.getCastleMask() & (Position.A1_CASTLE)) != 0) &&
                        ( i64_is0( i64_and(OOO_SQ,occupied) ) ) &&
                        (pos.getPiece(k0 - 4) == Piece.WROOK) &&
                        !this.sqAttacked(pos, k0) &&
                        !this.sqAttacked(pos, k0 - 1)) {
                        this.setMove(moveList, k0, k0 - 2, Piece.EMPTY);
                    }
                }
            }

            // Knight moves
            /*long*/ knights = i64c(pos.pieceTypeBB[Piece.WKNIGHT]);
            while (knights.l||knights.h) {
                /*int*/ sq = numberOfTrailingZeros(knights);
                /*long*/ m = i64_and( BitBoard.knightAttacks[sq], NwhiteBB );
                if (this.addMovesByMask(moveList, pos, sq, m)) return moveList;
                i64_bitclear( knights, sq );
            }

            // Pawn moves
            /*long*/ pawns = i64c(pos.pieceTypeBB[Piece.WPAWN]);
            /*long*/ m = i64_and( i64_lshift(pawns, 8), Noccupied );
            if (this.addPawnMovesByMask(moveList, pos, m, -8, true)) return moveList;
            m = i64_and(  i64_lshift( i64_and(m,BitBoard.maskRow3), 8 ), Noccupied );
            this.addPawnDoubleMovesByMask(moveList, pos, m, -16);

            var epM2 = i64_or(pos.blackBB,epMask);
            m = i64_and( i64_and( i64_lshift(pawns,7), BitBoard.maskAToGFiles ), epM2 );
            if (this.addPawnMovesByMask(moveList, pos, m, -7, true)) return moveList;

            m = i64_and( i64_and( i64_lshift(pawns,9), BitBoard.maskBToHFiles ), epM2 );
            if (this.addPawnMovesByMask(moveList, pos, m, -9, true)) return moveList;
        } else {
            // Queen moves
            /*long*/ squares = i64c(pos.pieceTypeBB[Piece.BQUEEN]);
            while (squares.l||squares.h) {
                /*int*/ sq = numberOfTrailingZeros(squares);
                /*long*/ m = i64_and( i64_or(rookAttacks(sq, occupied),
                     bishopAttacks(sq, occupied)), NblackBB );
                if (this.addMovesByMask(moveList, pos, sq, m)) return moveList;
                i64_bitclear( squares, sq );
            }

            // Rook moves
            squares = i64c(pos.pieceTypeBB[Piece.BROOK]);
            while (squares.l||squares.h) {
                /*int*/ sq = numberOfTrailingZeros(squares);
                /*long*/ m = i64_and( rookAttacks(sq, occupied), NblackBB );
                if (this.addMovesByMask(moveList, pos, sq, m)) return moveList;
                i64_bitclear( squares, sq );
            }

            // Bishop moves
            squares = i64c(pos.pieceTypeBB[Piece.BBISHOP]);
            while (squares.l||squares.h) {
                /*int*/ sq = numberOfTrailingZeros(squares);
                /*long*/ m = i64_and( bishopAttacks(sq, occupied), NblackBB );
                if (this.addMovesByMask(moveList, pos, sq, m)) return moveList;
                i64_bitclear( squares, sq );
            }
            
            // King moves
            {
                /*int*/ sq = pos.getKingSq(false);
                /*long*/ m = i64_and( BitBoard.kingAttacks[sq], NblackBB );
                if (this.addMovesByMask(moveList, pos, sq, m)) return moveList;
                /*int*/ k0 = 60;
                if (sq == k0) {
                    var /*long*/ OO_SQ = o_xL6;
                    var /*long*/ OOO_SQ = o_xLE;
                    if (((pos.getCastleMask() & (Position.H8_CASTLE)) != 0) &&
                        ( i64_is0( i64_and(OO_SQ,occupied) ) ) &&
                        (pos.getPiece(k0 + 3) == Piece.BROOK) &&
                        !this.sqAttacked(pos, k0) &&
                        !this.sqAttacked(pos, k0 + 1)) {
                        this.setMove(moveList, k0, k0 + 2, Piece.EMPTY);
                    }
                    if (((pos.getCastleMask() & (Position.A8_CASTLE)) != 0) &&
                        ( i64_is0( i64_and(OOO_SQ,occupied) ) ) &&
                        (pos.getPiece(k0 - 4) == Piece.BROOK) &&
                        !this.sqAttacked(pos, k0) &&
                        !this.sqAttacked(pos, k0 - 1)) {
                        this.setMove(moveList, k0, k0 - 2, Piece.EMPTY);
                    }
                }
            }

            // Knight moves
            /*long*/ knights = i64c(pos.pieceTypeBB[Piece.BKNIGHT]);
            while (knights.l||knights.h) {
                /*int*/ sq = numberOfTrailingZeros(knights);
                /*long*/ m = i64_and( BitBoard.knightAttacks[sq], NblackBB );
                if (this.addMovesByMask(moveList, pos, sq, m)) return moveList;
                i64_bitclear( knights, sq );
            }

            // Pawn moves
            /*long*/ pawns = i64c(pos.pieceTypeBB[Piece.BPAWN]);
            /*long*/ m = i64_and( i64_rshift(pawns, 8), Noccupied );
            if (this.addPawnMovesByMask(moveList, pos, m, 8, true)) return moveList;
            m = i64_and(  i64_rshift( i64_and(m,BitBoard.maskRow6), 8 ), Noccupied );
            this.addPawnDoubleMovesByMask(moveList, pos, m, 16);

            var epM2 = i64_or(pos.whiteBB,epMask);
            m = i64_and( i64_and( i64_rshift(pawns,9), BitBoard.maskAToGFiles ), epM2 );
            if (this.addPawnMovesByMask(moveList, pos, m, 9, true)) return moveList;

            m = i64_and( i64_and( i64_rshift(pawns,7), BitBoard.maskBToHFiles ), epM2 );
            if (this.addPawnMovesByMask(moveList, pos, m, 7, true)) return moveList;
        }
        return moveList;
    },

    /**
     * Generate and return a list of pseudo-legal check evasion moves.
     * Pseudo-legal means that the moves doesn't necessarily defend from check threats.
     */
    /*public*/ /*MoveList*/ checkEvasions: function (/*Position*/ pos) {
        var sq,squares,m,knights,pawns;
        var threatSq, kingThreats, rookPieces, bishPieces, validTargets;
        
        var /*MoveList*/ moveList = this.getMoveListObj();
        var /*long*/ occupied = i64_or(pos.whiteBB, pos.blackBB);
        var Noccupied = i64_not(occupied);
        var NwhiteBB = i64_not(pos.whiteBB);
        var NblackBB = i64_not(pos.blackBB);
        var /*int*/ epSquare = pos.getEpSquare();
        var /*long*/ epMask = ((epSquare >= 0) ? i64bitObj(epSquare) : i64() );

        if (pos.whiteMove) {
            /*long*/ kingThreats = i64_and( pos.pieceTypeBB[Piece.BKNIGHT], BitBoard.knightAttacks[pos.wKingSq] );
            /*long*/ rookPieces = i64_or( pos.pieceTypeBB[Piece.BROOK], pos.pieceTypeBB[Piece.BQUEEN] );
            if (i64_not0(rookPieces))
                kingThreats = i64_or( kingThreats, i64_and( rookPieces, rookAttacks(pos.wKingSq, occupied) ) );
            /*long*/ bishPieces = i64_or( pos.pieceTypeBB[Piece.BBISHOP], pos.pieceTypeBB[Piece.BQUEEN] );
            if (i64_not0(bishPieces))
                kingThreats = i64_or( kingThreats, i64_and( bishPieces, bishopAttacks(pos.wKingSq, occupied) ) );
            kingThreats = i64_or( kingThreats, i64_and( pos.pieceTypeBB[Piece.BPAWN], BitBoard.wPawnAttacks[pos.wKingSq] ) );
            /*long*/ validTargets = i64();
            if (i64_not0(kingThreats) &&
                 i64_is0( i64_and(kingThreats, i64_sub(kingThreats,o_x1))) ) { // Exactly one attacking piece
                /*int*/ threatSq = numberOfTrailingZeros(kingThreats);
                validTargets = i64_or( kingThreats, BitBoard.squaresBetween[pos.wKingSq][threatSq] );
            }
            validTargets = i64_or( validTargets, pos.pieceTypeBB[Piece.BKING] );

            // Queen moves
            /*long*/ squares = i64c(pos.pieceTypeBB[Piece.WQUEEN]);
            while (squares.l||squares.h) {
                /*int*/ sq = numberOfTrailingZeros(squares);
                /*long*/ m = i64_and( i64_and( i64_or(rookAttacks(sq, occupied),
                     bishopAttacks(sq, occupied)), NwhiteBB ), validTargets);
                if (this.addMovesByMask(moveList, pos, sq, m)) return moveList;
                i64_bitclear( squares, sq );
            }

            // Rook moves
            squares = i64c(pos.pieceTypeBB[Piece.WROOK]);
            while (squares.l||squares.h) {
                /*int*/ sq = numberOfTrailingZeros(squares);
                /*long*/ m = i64_and( i64_and( rookAttacks(sq, occupied), NwhiteBB ), validTargets);
                if (this.addMovesByMask(moveList, pos, sq, m)) return moveList;
                i64_bitclear( squares, sq );
            }

            // Bishop moves
            squares = i64c(pos.pieceTypeBB[Piece.WBISHOP]);
            while (squares.l||squares.h) {
                /*int*/ sq = numberOfTrailingZeros(squares);
                /*long*/ m = i64_and( i64_and( bishopAttacks(sq, occupied), NwhiteBB ), validTargets);
                if (this.addMovesByMask(moveList, pos, sq, m)) return moveList;
                i64_bitclear( squares, sq );
            }

            // King moves
            {
                /*int*/ sq = pos.getKingSq(true);
                /*long*/ m = i64_and( BitBoard.kingAttacks[sq], NwhiteBB );
                if (this.addMovesByMask(moveList, pos, sq, m)) return moveList;
            }

            // Knight moves
            /*long*/ knights = i64c(pos.pieceTypeBB[Piece.WKNIGHT]);
            while (knights.l||knights.h) {
                /*int*/ sq = numberOfTrailingZeros(knights);
                /*long*/ m = i64_and( i64_and( BitBoard.knightAttacks[sq], NwhiteBB ), validTargets);
                if (this.addMovesByMask(moveList, pos, sq, m)) return moveList;
                i64_bitclear( knights, sq );
            }

            // Pawn moves
            /*long*/ pawns = i64c(pos.pieceTypeBB[Piece.WPAWN]);
            /*long*/ m = i64_and( i64_lshift(pawns, 8), Noccupied );
            if (this.addPawnMovesByMask(moveList, pos, i64_and( m, validTargets), -8, true)) return moveList;
            m = i64_and(  i64_lshift( i64_and(m,BitBoard.maskRow3), 8 ), Noccupied );
            this.addPawnDoubleMovesByMask(moveList, pos, i64_and( m, validTargets), -16);

            var epM2 = i64_or( i64_and( pos.blackBB, validTargets ),epMask);
            m = i64_and( i64_and( i64_lshift(pawns,7), BitBoard.maskAToGFiles ), epM2 );
            if (this.addPawnMovesByMask(moveList, pos, m, -7, true)) return moveList;

            m = i64_and( i64_and( i64_lshift(pawns,9), BitBoard.maskBToHFiles ), epM2 );
            if (this.addPawnMovesByMask(moveList, pos, m, -9, true)) return moveList;

        } else {
            /*long*/ kingThreats = i64_and( pos.pieceTypeBB[Piece.WKNIGHT], BitBoard.knightAttacks[pos.bKingSq] );
            /*long*/ rookPieces = i64_or( pos.pieceTypeBB[Piece.WROOK], pos.pieceTypeBB[Piece.WQUEEN] );
            if (i64_not0(rookPieces))
                kingThreats = i64_or( kingThreats, i64_and( rookPieces, rookAttacks(pos.bKingSq, occupied) ) );
            /*long*/ bishPieces = i64_or( pos.pieceTypeBB[Piece.WBISHOP], pos.pieceTypeBB[Piece.WQUEEN] );
            if (i64_not0(bishPieces))
                kingThreats = i64_or( kingThreats, i64_and( bishPieces, bishopAttacks(pos.bKingSq, occupied) ) );
            kingThreats = i64_or( kingThreats, i64_and( pos.pieceTypeBB[Piece.BPAWN], BitBoard.wPawnAttacks[pos.bKingSq] ) );
            /*long*/ validTargets = i64();
            if (i64_not0(kingThreats) &&
                 i64_is0( i64_and(kingThreats, i64_sub(kingThreats,o_x1))) ) { // Exactly one attacking piece
                /*int*/ threatSq = numberOfTrailingZeros(kingThreats);
                validTargets = i64_or( kingThreats, BitBoard.squaresBetween[pos.bKingSq][threatSq] );
            }
            validTargets = i64_or( validTargets, pos.pieceTypeBB[Piece.WKING] );

            // Queen moves
            /*long*/ squares = i64c(pos.pieceTypeBB[Piece.BQUEEN]);
            while (squares.l||squares.h) {
                /*int*/ sq = numberOfTrailingZeros(squares);
                /*long*/ m = i64_and( i64_and( i64_or(rookAttacks(sq, occupied),
                     bishopAttacks(sq, occupied)), NblackBB ), validTargets);
                if (this.addMovesByMask(moveList, pos, sq, m)) return moveList;
                i64_bitclear( squares, sq );
            }

            // Rook moves
            squares = i64c(pos.pieceTypeBB[Piece.BROOK]);
            while (squares.l||squares.h) {
                /*int*/ sq = numberOfTrailingZeros(squares);
                /*long*/ m = i64_and( i64_and( rookAttacks(sq, occupied), NblackBB ), validTargets);
                if (this.addMovesByMask(moveList, pos, sq, m)) return moveList;
                i64_bitclear( squares, sq );
            }

            // Bishop moves
            squares = i64c(pos.pieceTypeBB[Piece.BBISHOP]);
            while (squares.l||squares.h) {
                /*int*/ sq = numberOfTrailingZeros(squares);
                /*long*/ m = i64_and( i64_and( bishopAttacks(sq, occupied), NblackBB ), validTargets);
                if (this.addMovesByMask(moveList, pos, sq, m)) return moveList;
                i64_bitclear( squares, sq );
            }

            // King moves
            {
                /*int*/ sq = pos.getKingSq(false);
                /*long*/ m = i64_and( BitBoard.kingAttacks[sq], NblackBB );
                if (this.addMovesByMask(moveList, pos, sq, m)) return moveList;
            }

            // Knight moves
            /*long*/ knights = i64c(pos.pieceTypeBB[Piece.BKNIGHT]);
            while (knights.l||knights.h) {
                /*int*/ sq = numberOfTrailingZeros(knights);
                /*long*/ m = i64_and( i64_and( BitBoard.knightAttacks[sq], NblackBB ), validTargets);
                if (this.addMovesByMask(moveList, pos, sq, m)) return moveList;
                i64_bitclear( knights, sq );
            }

            // Pawn moves
            /*long*/ pawns = i64c(pos.pieceTypeBB[Piece.BPAWN]);
            /*long*/ m = i64_and( i64_rshift(pawns, 8), Noccupied );
            if (this.addPawnMovesByMask(moveList, pos, i64_and( m, validTargets), 8, true)) return moveList;
            m = i64_and(  i64_rshift( i64_and(m,BitBoard.maskRow6), 8 ), Noccupied );
            this.addPawnDoubleMovesByMask(moveList, pos, i64_and( m, validTargets), 16);

            var epM2 = i64_and( i64_or(pos.whiteBB,epMask), validTargets);
            m = i64_and( i64_and( i64_rshift(pawns,9), BitBoard.maskAToGFiles ), epM2 );
            if (this.addPawnMovesByMask(moveList, pos, m, 9, true)) return moveList;

            m = i64_and( i64_and( i64_rshift(pawns,7), BitBoard.maskBToHFiles ), epM2 );
            if (this.addPawnMovesByMask(moveList, pos, m, 7, true)) return moveList;

        }

        return moveList;
    },

    /** Generate captures, checks, and possibly some other moves that are too hard to filter out. */
    /*public*/ /*MoveList*/ pseudoLegalCapturesAndChecks:function (/*Position*/ pos) {
    
        var sq,squares,m,k0,knights,pawns;
        var discovered,  kRookAtk,NkRookAtk, kBishAtk, NkBishAtk, kKnightAtk;
        var OrRookBishAtk, pawnAll, NpawnAll;
        
        var /*MoveList*/ moveList = this.getMoveListObj();
        var /*long*/ occupied = i64_or(pos.whiteBB, pos.blackBB);
        var Noccupied = i64_not(occupied);
        var NwhiteBB = i64_not(pos.whiteBB);
        var NblackBB = i64_not(pos.blackBB);
        var /*int*/ epSquare = pos.getEpSquare();
        var /*long*/ epMask = ((epSquare >= 0) ? i64bitObj(epSquare) : i64() );
        
        if (pos.whiteMove) {
            var /*int*/ bKingSq = pos.getKingSq(false);
            /*long*/ discovered = i64(); // Squares that could generate discovered checks
            /*long*/ kRookAtk = rookAttacks(bKingSq, occupied);
            NkRookAtk = i64_not(kRookAtk);
            if (  i64_not0( i64_and(rookAttacks(bKingSq, i64_or(occupied, NkRookAtk)) ,
                    i64_or(pos.pieceTypeBB[Piece.WQUEEN] , pos.pieceTypeBB[Piece.WROOK])) ) )
                discovered = i64_or( discovered, kRookAtk );
            /*long*/ kBishAtk = bishopAttacks(bKingSq, occupied);
            NkBishAtk = i64_not(kBishAtk);
            if (  i64_not0( i64_and(bishopAttacks(bKingSq, i64_and(occupied, NkBishAtk)) ,
                    i64_or(pos.pieceTypeBB[Piece.WQUEEN] , pos.pieceTypeBB[Piece.WBISHOP])) ) )
                discovered = i64_or( discovered, kBishAtk );
            OrRookBishAtk = i64_or(kRookAtk , kBishAtk);    

            // Queen moves
            /*long*/ squares = i64c(pos.pieceTypeBB[Piece.WQUEEN]);
            while (squares.l||squares.h) {
                /*int*/ sq = numberOfTrailingZeros(squares);
                /*long*/ m = i64_or(rookAttacks(sq, occupied) , bishopAttacks(sq, occupied));
                if ( i64_is0( i64_and(discovered, i64bitObj(sq)) ) )
                    m = i64_and( m, i64_or(pos.blackBB , OrRookBishAtk ) );
                m = i64_and( m, NwhiteBB );
                if (this.addMovesByMask(moveList, pos, sq, m)) return moveList;
                i64_bitclear( squares, sq );
            }

            // Rook moves
            squares = i64c(pos.pieceTypeBB[Piece.WROOK]);
            while (squares.l||squares.h) {
                /*int*/ sq = numberOfTrailingZeros(squares);
                /*long*/ m = rookAttacks(sq, occupied);
                if ( i64_is0( i64_and(discovered, i64bitObj(sq)) ) )
                    m = i64_and( m, i64_or(pos.blackBB , kRookAtk ) );
                m = i64_and( m, NwhiteBB );
                if (this.addMovesByMask(moveList, pos, sq, m)) return moveList;
                i64_bitclear( squares, sq );
            }

            // Bishop moves
            squares = i64c(pos.pieceTypeBB[Piece.WBISHOP]);
            while (squares.l||squares.h) {
                /*int*/ sq = numberOfTrailingZeros(squares);
                /*long*/ m = bishopAttacks(sq, occupied);
                if ( i64_is0( i64_and(discovered, i64bitObj(sq)) ) )
                    m = i64_and( m, i64_or(pos.blackBB , kBishAtk ) );
                m = i64_and( m, NwhiteBB );
                if (this.addMovesByMask(moveList, pos, sq, m)) return moveList;
                i64_bitclear( squares, sq );
            }

            // King moves
            {
                /*int*/ sq = pos.getKingSq(true);
                /*long*/ m = BitBoard.kingAttacks[sq];
                m = i64_and( m, ( i64_is0( i64_and(discovered, i64bitObj(sq)) ) )  ? pos.blackBB : NwhiteBB );

                if (this.addMovesByMask(moveList, pos, sq, m)) return moveList;
                /*int*/ k0 = 4;
                if (sq == k0) {
                    var /*long*/ OO_SQ = o_x60;
                    var /*long*/ OOO_SQ = o_x0E;
                    if (((pos.getCastleMask() & (Position.H1_CASTLE)) != 0) &&
                        ( i64_is0( i64_and(OO_SQ,occupied) ) ) &&
                        (pos.getPiece(k0 + 3) == Piece.WROOK) &&
                        !this.sqAttacked(pos, k0) &&
                        !this.sqAttacked(pos, k0 + 1)) {
                        this.setMove(moveList, k0, k0 + 2, Piece.EMPTY);
                    }
                    if (((pos.getCastleMask() & (Position.A1_CASTLE)) != 0) &&
                        ( i64_is0( i64_and(OOO_SQ,occupied) ) ) &&
                        (pos.getPiece(k0 - 4) == Piece.WROOK) &&
                        !this.sqAttacked(pos, k0) &&
                        !this.sqAttacked(pos, k0 - 1)) {
                        this.setMove(moveList, k0, k0 - 2, Piece.EMPTY);
                    }
                }
            }

            // Knight moves
            /*long*/ knights = i64c(pos.pieceTypeBB[Piece.WKNIGHT]);
            /*long*/ kKnightAtk = BitBoard.knightAttacks[bKingSq];
            while (knights.l||knights.h) {
                /*int*/ sq = numberOfTrailingZeros(knights);
                /*long*/ m = i64_and(BitBoard.knightAttacks[sq], NwhiteBB);
                if ( i64_is0( i64_and(discovered, i64bitObj(sq)) ) )
                    m = i64_and( m, i64_or(pos.blackBB , kKnightAtk ) );
                m = i64_and( m, NwhiteBB );
                if (this.addMovesByMask(moveList, pos, sq, m)) return moveList;
                i64_bitclear( knights, sq );
            }

            // Pawn moves
            // Captures
            /*long*/ pawns = i64c(pos.pieceTypeBB[Piece.WPAWN]);

            var epM2 = i64_or(pos.blackBB,epMask);
            m = i64_and( i64_and( i64_lshift(pawns,7), BitBoard.maskAToGFiles ), epM2 );
            if (this.addPawnMovesByMask(moveList, pos, m, -7, false)) return moveList;

            m = i64_and( i64_and( i64_lshift(pawns,9), BitBoard.maskBToHFiles ), epM2 );
            if (this.addPawnMovesByMask(moveList, pos, m, -9, false)) return moveList;


            // Discovered checks and promotions
            /*long*/ pawnAll = i64_or( discovered , BitBoard.maskRow7 );
            NpawnAll = i64_not(pawnAll);

            /*long*/ m = i64_and( i64_lshift( i64_and(pawns,pawnAll), 8), Noccupied );
            if (this.addPawnMovesByMask(moveList, pos, m, -8, false)) return moveList;
            m = i64_and(  i64_lshift( i64_and(m,BitBoard.maskRow3), 8 ), Noccupied );
            this.addPawnDoubleMovesByMask(moveList, pos, m, -16);

            // Normal checks
            /*long*/ m = i64_and( i64_lshift( i64_and(pawns,NpawnAll), 8), Noccupied );
            if (this.addPawnMovesByMask(moveList, pos,
                i64_and( m, BitBoard.bPawnAttacks[bKingSq] ), -8, false)) return moveList;
            m = i64_and(  i64_lshift( i64_and(m,BitBoard.maskRow3), 8 ), Noccupied );
            this.addPawnDoubleMovesByMask(moveList, pos,
                i64_and( m, BitBoard.bPawnAttacks[bKingSq]), -16);
        } else {
            var /*int*/ wKingSq = pos.getKingSq(true);
            /*long*/ discovered = i64(); // Squares that could generate discovered checks
            /*long*/ kRookAtk = rookAttacks(wKingSq, occupied);
            NkRookAtk = i64_not(kRookAtk);
            if (  i64_not0( i64_and(rookAttacks(wKingSq, i64_or(occupied, NkRookAtk)) ,
                    i64_or(pos.pieceTypeBB[Piece.BQUEEN] , pos.pieceTypeBB[Piece.BROOK])) ) )
                discovered = i64_or( discovered, kRookAtk );
            /*long*/ kBishAtk = bishopAttacks(wKingSq, occupied);
            NkBishAtk = i64_not(kBishAtk);
            if (  i64_not0( i64_and(bishopAttacks(wKingSq, i64_and(occupied, NkBishAtk)) ,
                    i64_or(pos.pieceTypeBB[Piece.BQUEEN] , pos.pieceTypeBB[Piece.BBISHOP])) ) )
                discovered = i64_or( discovered, kBishAtk );
            OrRookBishAtk = i64_or(kRookAtk , kBishAtk);    

            // Queen moves
            /*long*/ squares = i64c(pos.pieceTypeBB[Piece.BQUEEN]);
            while (squares.l||squares.h) {
                /*int*/ sq = numberOfTrailingZeros(squares);
                /*long*/ m = i64_or(rookAttacks(sq, occupied) , bishopAttacks(sq, occupied));
                if ( i64_is0( i64_and(discovered, i64bitObj(sq)) ) )
                    m = i64_and( m, i64_or(pos.whiteBB , OrRookBishAtk ) );
                m = i64_and( m, NblackBB );
                if (this.addMovesByMask(moveList, pos, sq, m)) return moveList;
                i64_bitclear( squares, sq );
            }

            // Rook moves
            squares = i64c(pos.pieceTypeBB[Piece.BROOK]);
            while (squares.l||squares.h) {
                /*int*/ sq = numberOfTrailingZeros(squares);
                /*long*/ m = rookAttacks(sq, occupied);
                if ( i64_is0( i64_and(discovered, i64bitObj(sq)) ) )
                    m = i64_and( m, i64_or(pos.whiteBB , kRookAtk ) );
                m = i64_and( m, NblackBB );
                if (this.addMovesByMask(moveList, pos, sq, m)) return moveList;
                i64_bitclear( squares, sq );
            }

            // Bishop moves
            squares = i64c(pos.pieceTypeBB[Piece.BBISHOP]);
            while (squares.l||squares.h) {
                /*int*/ sq = numberOfTrailingZeros(squares);
                /*long*/ m = bishopAttacks(sq, occupied);
                if ( i64_is0( i64_and(discovered, i64bitObj(sq)) ) )
                    m = i64_and( m, i64_or(pos.whiteBB , kBishAtk ) );
                m = i64_and( m, NblackBB );
                if (this.addMovesByMask(moveList, pos, sq, m)) return moveList;
                i64_bitclear( squares, sq );
            }
            
            // King moves
            {
                /*int*/ sq = pos.getKingSq(false);
                /*long*/ m = BitBoard.kingAttacks[sq];
                m = i64_and( m, ( i64_is0( i64_and(discovered, i64bitObj(sq)) ) )  ? pos.whiteBB : NblackBB );
                if (this.addMovesByMask(moveList, pos, sq, m)) return moveList;
                /*int*/ k0 = 60;
                if (sq == k0) {
                    var /*long*/ OO_SQ = o_xL6;
                    var /*long*/ OOO_SQ = o_xLE;
                    if (((pos.getCastleMask() & (Position.H8_CASTLE)) != 0) &&
                        ( i64_is0( i64_and(OO_SQ,occupied) ) ) &&
                        (pos.getPiece(k0 + 3) == Piece.BROOK) &&
                        !this.sqAttacked(pos, k0) &&
                        !this.sqAttacked(pos, k0 + 1)) {
                        this.setMove(moveList, k0, k0 + 2, Piece.EMPTY);
                    }
                    if (((pos.getCastleMask() & (Position.A8_CASTLE)) != 0) &&
                        ( i64_is0( i64_and(OOO_SQ,occupied) ) ) &&
                        (pos.getPiece(k0 - 4) == Piece.BROOK) &&
                        !this.sqAttacked(pos, k0) &&
                        !this.sqAttacked(pos, k0 - 1)) {
                        this.setMove(moveList, k0, k0 - 2, Piece.EMPTY);
                    }
                }
            }

            // Knight moves
            /*long*/ knights = i64c(pos.pieceTypeBB[Piece.BKNIGHT]);
            /*long*/ kKnightAtk = BitBoard.knightAttacks[wKingSq];
            while (knights.l||knights.h) {
                /*int*/ sq = numberOfTrailingZeros(knights);
                /*long*/ m = i64_and(BitBoard.knightAttacks[sq], NblackBB);
                if ( i64_is0( i64_and(discovered, i64bitObj(sq)) ) )
                    m = i64_and( m, i64_or(pos.whiteBB , kKnightAtk ) );
                m = i64_and( m, NblackBB );
                if (this.addMovesByMask(moveList, pos, sq, m)) return moveList;
                i64_bitclear( knights, sq );
            }

            // Pawn moves
            // Captures
            /*long*/ pawns = i64c(pos.pieceTypeBB[Piece.BPAWN]);

            var epM2 = i64_or(pos.whiteBB,epMask);
            m = i64_and( i64_and( i64_rshift(pawns,9), BitBoard.maskAToGFiles ), epM2 );
            if (this.addPawnMovesByMask(moveList, pos, m, 9, false)) return moveList;

            m = i64_and( i64_and( i64_rshift(pawns,7), BitBoard.maskBToHFiles ), epM2 );
            if (this.addPawnMovesByMask(moveList, pos, m, 7, false)) return moveList;

            // Discovered checks and promotions
            /*long*/ pawnAll = i64_or( discovered , BitBoard.maskRow2 );
            NpawnAll = i64_not(pawnAll);

            /*long*/ m = i64_and( i64_rshift( i64_and(pawns,pawnAll), 8), Noccupied );
            if (this.addPawnMovesByMask(moveList, pos, m, 8, false)) return moveList;
            m = i64_and(  i64_rshift( i64_and(m,BitBoard.maskRow6), 8 ), Noccupied );
            this.addPawnDoubleMovesByMask(moveList, pos, m, 16);

            // Normal checks
            /*long*/ m = i64_and( i64_rshift( i64_and(pawns,NpawnAll), 8), Noccupied );
            if (this.addPawnMovesByMask(moveList, pos,
                i64_and( m, BitBoard.wPawnAttacks[wKingSq] ), 8, false)) return moveList;
            m = i64_and(  i64_rshift( i64_and(m,BitBoard.maskRow6), 8 ), Noccupied );
            this.addPawnDoubleMovesByMask(moveList, pos,
                i64_and( m, BitBoard.wPawnAttacks[wKingSq]), 16);
        }

        return moveList;
    },

    /*public*/ /*MoveList*/ pseudoLegalCaptures: function (/*Position*/ pos) {
    
        var sq,squares,m,knights,pawns;       
        var /*MoveList*/ moveList = this.getMoveListObj();
        var /*long*/ occupied = i64_or(pos.whiteBB, pos.blackBB);
        var Noccupied = i64_not(occupied);
        var NwhiteBB = i64_not(pos.whiteBB);
        var NblackBB = i64_not(pos.blackBB);
        var /*int*/ epSquare = pos.getEpSquare();
        var /*long*/ epMask = ((epSquare >= 0) ? i64bitObj(epSquare) : i64() );

        if (pos.whiteMove) {

            // Queen moves
            /*long*/ squares = i64c(pos.pieceTypeBB[Piece.WQUEEN]);
            while (squares.l||squares.h) {
                /*int*/ sq = numberOfTrailingZeros(squares);
                /*long*/ m = i64_and( i64_or(rookAttacks(sq, occupied),
                     bishopAttacks(sq, occupied)), pos.blackBB );
                if (this.addMovesByMask(moveList, pos, sq, m)) return moveList;
                i64_bitclear( squares, sq );
            }

            // Rook moves
            squares = i64c(pos.pieceTypeBB[Piece.WROOK]);
            while (squares.l||squares.h) {
                /*int*/ sq = numberOfTrailingZeros(squares);
                /*long*/ m = i64_and( rookAttacks(sq, occupied), pos.blackBB );
                if (this.addMovesByMask(moveList, pos, sq, m)) return moveList;
                i64_bitclear( squares, sq );
            }

            // Bishop moves
            squares = i64c(pos.pieceTypeBB[Piece.WBISHOP]);
            while (squares.l||squares.h) {
                /*int*/ sq = numberOfTrailingZeros(squares);
                /*long*/ m = i64_and( bishopAttacks(sq, occupied), pos.blackBB );
                if (this.addMovesByMask(moveList, pos, sq, m)) return moveList;
                i64_bitclear( squares, sq );
            }

            // Knight moves
            /*long*/ knights = i64c(pos.pieceTypeBB[Piece.WKNIGHT]);
            while (knights.l||knights.h) {
                /*int*/ sq = numberOfTrailingZeros(knights);
                /*long*/ m = i64_and( BitBoard.knightAttacks[sq], pos.blackBB );
                if (this.addMovesByMask(moveList, pos, sq, m)) return moveList;
                i64_bitclear( knights, sq );
            }

            // King moves
            /*int*/ sq = pos.getKingSq(true);
            /*long*/ m = i64_and( BitBoard.kingAttacks[sq], pos.blackBB );
            if (this.addMovesByMask(moveList, pos, sq, m)) return moveList;


            // Pawn moves
            /*long*/ pawns = i64c(pos.pieceTypeBB[Piece.WPAWN]);
            /*long*/ m = i64_and( i64_and( i64_lshift(pawns, 8), Noccupied ), BitBoard.maskRow8 );
            if (this.addPawnMovesByMask(moveList, pos, m, -8, false)) return moveList;


            var epM2 = i64_or(pos.blackBB,epMask);
            m = i64_and( i64_and( i64_lshift(pawns,7), BitBoard.maskAToGFiles ), epM2 );
            if (this.addPawnMovesByMask(moveList, pos, m, -7, false)) return moveList;

            m = i64_and( i64_and( i64_lshift(pawns,9), BitBoard.maskBToHFiles ), epM2 );
            if (this.addPawnMovesByMask(moveList, pos, m, -9, false)) return moveList;

        } else {

            // Queen moves
            /*long*/ squares = i64c(pos.pieceTypeBB[Piece.BQUEEN]);
            while (squares.l||squares.h) {
                /*int*/ sq = numberOfTrailingZeros(squares);
                /*long*/ m = i64_and( i64_or(rookAttacks(sq, occupied),
                     bishopAttacks(sq, occupied)), pos.whiteBB );
                if (this.addMovesByMask(moveList, pos, sq, m)) return moveList;
                i64_bitclear( squares, sq );
            }

            // Rook moves
            squares = i64c(pos.pieceTypeBB[Piece.BROOK]);
            while (squares.l||squares.h) {
                /*int*/ sq = numberOfTrailingZeros(squares);
                /*long*/ m = i64_and( rookAttacks(sq, occupied), pos.whiteBB );
                if (this.addMovesByMask(moveList, pos, sq, m)) return moveList;
                i64_bitclear( squares, sq );
            }

            // Bishop moves
            squares = i64c(pos.pieceTypeBB[Piece.BBISHOP]);
            while (squares.l||squares.h) {
                /*int*/ sq = numberOfTrailingZeros(squares);
                /*long*/ m = i64_and( bishopAttacks(sq, occupied), pos.whiteBB );
                if (this.addMovesByMask(moveList, pos, sq, m)) return moveList;
                i64_bitclear( squares, sq );
            }

            // Knight moves
            /*long*/ knights = i64c(pos.pieceTypeBB[Piece.BKNIGHT]);
            while (knights.l||knights.h) {
                /*int*/ sq = numberOfTrailingZeros(knights);
                /*long*/ m = i64_and( BitBoard.knightAttacks[sq], pos.whiteBB );
                if (this.addMovesByMask(moveList, pos, sq, m)) return moveList;
                i64_bitclear( knights, sq );
            }

            // King moves
            /*int*/ sq = pos.getKingSq(false);
            /*long*/ m = i64_and( BitBoard.kingAttacks[sq], pos.whiteBB );
            if (this.addMovesByMask(moveList, pos, sq, m)) return moveList;


            // Pawn moves
            /*long*/ pawns = i64c(pos.pieceTypeBB[Piece.BPAWN]);
            /*long*/ m = i64_and( i64_and( i64_rshift(pawns, 8), Noccupied ), BitBoard.maskRow1 );
            if (this.addPawnMovesByMask(moveList, pos, m, 8, false)) return moveList;

            var epM2 = i64_or(pos.whiteBB,epMask);

            m = i64_and( i64_and( i64_rshift(pawns,9), BitBoard.maskBToHFiles ), epM2 );
            if (this.addPawnMovesByMask(moveList, pos, m, 9, false)) return moveList;

            m = i64_and( i64_and( i64_rshift(pawns,7), BitBoard.maskAToGFiles ), epM2 );
            if (this.addPawnMovesByMask(moveList, pos, m, 7, false)) return moveList;
        }
        return moveList;
    },

    /**
     * Return true if the side to move is in check.
     */
    /*public*/ /*boolean*/  inCheck: function (/*Position*/ pos) {
        var /*int*/ kingSq = pos.getKingSq(pos.whiteMove);
        return this.sqAttacked(pos, kingSq);
    },

    /**
     * Return the next piece in a given direction, starting from sq.
     */
    /*private*/ /*int*/ nextPiece: function (/*Position*/ pos, /*int*/ sq, /*int*/ delta) {
        while (delta!=0) {
            sq += delta;
            var /*int*/ p = pos.getPiece(sq);
            if (p != Piece.EMPTY)
                return p;
        }
        return Piece.EMPTY;
    },

    /** Like nextPiece(), but handles board edges. */
    /*private*/ /*int*/ nextPieceSafe: function (/*Position*/ pos, /*int*/ sq, /*int*/ delta) {
        var /*int*/ dx = 0, dy = 0;
        switch (delta) {
        case 1: dx=1; dy=0; break;
        case 9: dx=1; dy=1; break;
        case 8: dx=0; dy=1; break;
        case 7: dx=-1; dy=1; break;
        case -1: dx=-1; dy=0; break;
        case -9: dx=-1; dy=-1; break;
        case -8: dx=0; dy=-1; break;
        case -7: dx=1; dy=-1; break;
        default: return Piece.EMPTY;
        }
        var /*int*/ x = Position.getX(sq);
        var /*int*/ y = Position.getY(sq);
        while (true) {
            x += dx; y += dy;
            if ((x < 0) || (x > 7) || (y < 0) || (y > 7)) return Piece.EMPTY;
            var /*int*/ p = pos.getPiece(Position.getSquare(x, y));
            if (p != Piece.EMPTY) return p;
        }
    },
    
    /**
     * Return true if making a move delivers check to the opponent
     */
    /*public*/ /*boolean*/  givesCheck: function(/*Position*/ pos, /*Move*/ m) {
        var /*boolean*/  wtm = pos.whiteMove;
        var /*int*/ oKingSq = pos.getKingSq(!wtm);
        var /*int*/ oKing = (wtm ? Piece.BKING : Piece.WKING);
        var /*int*/ p = Piece.makeWhite(m.promoteTo == Piece.EMPTY ? pos.getPiece(m.from) : m.promoteTo);
        var /*int*/ d1 = getDirection(m.to, oKingSq);
        var d2, p2, dx, d3;
        switch (d1) {
        case 8: case -8: case 1: case -1: // Rook direction
            if ((p == Piece.WQUEEN) || (p == Piece.WROOK))
                if ((d1 != 0) && (MoveGen.nextPiece(pos, m.to, d1) == oKing))
                    return true;
            break;
        case 9: case 7: case -9: case -7: // Bishop direction
            if ((p == Piece.WQUEEN) || (p == Piece.WBISHOP)) {
                if ((d1 != 0) && (MoveGen.nextPiece(pos, m.to, d1) == oKing))
                    return true;
            } else if (p == Piece.WPAWN) {
                if (((d1 > 0) == wtm) && (pos.getPiece(m.to + d1) == oKing))
                    return true;
            }
            break;
        default:
            if (d1 != 0) { // Knight direction
                if (p == Piece.WKNIGHT)
                    return true;
            }
        }
        /*int*/ d2 = getDirection(m.from, oKingSq);
        if ((d2 != 0) && (d2 != d1) && (MoveGen.nextPiece(pos, m.from, d2) == oKing)) {
            /*int*/ p2 = MoveGen.nextPieceSafe(pos, m.from, -d2);
            switch (d2) {
            case 8: case -8: case 1: case -1: // Rook direction
                if ((p2 == (wtm ? Piece.WQUEEN : Piece.BQUEEN)) ||
                    (p2 == (wtm ? Piece.WROOK : Piece.BROOK)))
                    return true;
                break;
            case 9: case 7: case -9: case -7: // Bishop direction
                if ((p2 == (wtm ? Piece.WQUEEN : Piece.BQUEEN)) ||
                    (p2 == (wtm ? Piece.WBISHOP : Piece.BBISHOP)))
                    return true;
                break;
            }
        }
        if ((m.promoteTo != Piece.EMPTY) && (d1 != 0) && (d1 == d2)) {
            switch (d1) {
            case 8: case -8: case 1: case -1: // Rook direction
                if ((p == Piece.WQUEEN) || (p == Piece.WROOK))
                    if ((d1 != 0) && (MoveGen.nextPiece(pos, m.from, d1) == oKing))
                        return true;
                break;
            case 9: case 7: case -9: case -7: // Bishop direction
                if ((p == Piece.WQUEEN) || (p == Piece.WBISHOP)) {
                    if ((d1 != 0) && (MoveGen.nextPiece(pos, m.from, d1) == oKing))
                        return true;
                }
                break;
            }
        }
        if (p == Piece.WKING) {
            if (m.to - m.from == 2) { // O-O
                if (MoveGen.nextPieceSafe(pos, m.from, -1) == oKing)
                    return true;
                if (MoveGen.nextPieceSafe(pos, m.from + 1, wtm ? 8 : -8) == oKing)
                    return true;
            } else if (m.to - m.from == -2) { // O-O-O
                if (MoveGen.nextPieceSafe(pos, m.from, 1) == oKing)
                    return true;
                if (MoveGen.nextPieceSafe(pos, m.from - 1, wtm ? 8 : -8) == oKing)
                    return true;
            }
        } else if (p == Piece.WPAWN) {
            if (pos.getPiece(m.to) == Piece.EMPTY) {
                /*int*/ dx = Position.getX(m.to) - Position.getX(m.from);
                if (dx != 0) { // en passant
                    var /*int*/ epSq = m.from + dx;
                    /*int*/ d3 = getDirection(epSq, oKingSq);
                    switch (d3) {
                    case 9: case 7: case -9: case -7:
                        if (MoveGen.nextPiece(pos, epSq, d3) == oKing) {
                            /*int*/ p2 = MoveGen.nextPieceSafe(pos, epSq, -d3);
                            if ((p2 == (wtm ? Piece.WQUEEN : Piece.BQUEEN)) ||
                                (p2 == (wtm ? Piece.WBISHOP : Piece.BBISHOP)))
                                return true;
                        }
                        break;
                    case 1:
                        if (MoveGen.nextPiece(pos, Math.max(epSq, m.from), d3) == oKing) {
                            /*int*/ p2 = MoveGen.nextPieceSafe(pos, Math.min(epSq, m.from), -d3);
                            if ((p2 == (wtm ? Piece.WQUEEN : Piece.BQUEEN)) ||
                                (p2 == (wtm ? Piece.WROOK : Piece.BROOK)))
                                return true;
                        }
                        break;
                    case -1:
                        if (MoveGen.nextPiece(pos, Math.min(epSq, m.from), d3) == oKing) {
                            /*int*/ p2 = MoveGen.nextPieceSafe(pos, Math.max(epSq, m.from), -d3);
                            if ((p2 == (wtm ? Piece.WQUEEN : Piece.BQUEEN)) ||
                                (p2 == (wtm ? Piece.WROOK : Piece.BROOK)))
                                return true;
                        }
                        break;
                    }
                }
            }
        }
        return false;
    },

    /**
     * Return true if the side to move can take the opponents king.
     */
    /*public*/ /*boolean*/  canTakeKing: function(/*Position*/ pos) {
        pos.setWhiteMove(!pos.whiteMove);
        var /*boolean*/  ret = MoveGen.inCheck(pos);
        pos.setWhiteMove(!pos.whiteMove);
        return ret;
    },

    /**
     * Return true if a square is attacked by the opposite side.
     */
    /*public*/ /*boolean*/  sqAttacked: function(/*Position*/ pos, /*int*/ sq) {
        if (pos.whiteMove) {
            if (i64_not0( i64_and(BitBoard.knightAttacks[sq], pos.pieceTypeBB[Piece.BKNIGHT]) ))
                return true;
            if (i64_not0( i64_and(BitBoard.kingAttacks[sq], pos.pieceTypeBB[Piece.BKING]) ))
                return true;
            if (i64_not0( i64_and(BitBoard.wPawnAttacks[sq], pos.pieceTypeBB[Piece.BPAWN]) ))
                return true;
            var /*long*/ occupied = i64_or( pos.whiteBB, pos.blackBB );
            var /*long*/ bbQueen = pos.pieceTypeBB[Piece.BQUEEN];
            if ( i64_not0( i64_and(bishopAttacks(sq, occupied),
                 i64_or(pos.pieceTypeBB[Piece.BBISHOP], bbQueen)) ))
                return true;
            if ( i64_not0( i64_and(rookAttacks(sq, occupied),
                 i64_or(pos.pieceTypeBB[Piece.BROOK], bbQueen)) ))
                return true;
        } else {
            if (i64_not0( i64_and(BitBoard.knightAttacks[sq], pos.pieceTypeBB[Piece.WKNIGHT]) ))
                return true;
            if (i64_not0( i64_and(BitBoard.kingAttacks[sq], pos.pieceTypeBB[Piece.WKING]) ))
                return true;
            if (i64_not0( i64_and(BitBoard.bPawnAttacks[sq], pos.pieceTypeBB[Piece.WPAWN]) ))
                return true;
            var /*long*/ occupied = i64_or( pos.whiteBB, pos.blackBB );
            var /*long*/ bbQueen = pos.pieceTypeBB[Piece.WQUEEN];
            if ( i64_not0( i64_and(bishopAttacks(sq, occupied),
                 i64_or(pos.pieceTypeBB[Piece.WBISHOP], bbQueen)) ))
                return true;
            if ( i64_not0( i64_and(rookAttacks(sq, occupied),
                 i64_or(pos.pieceTypeBB[Piece.WROOK], bbQueen)) ))
                return true;
        }
        return false;
    },

    /**
     * Remove all illegal moves from moveList.
     * "moveList" is assumed to be a list of pseudo-legal moves.
     * This function removes the moves that don't defend from check threats.
     */
    /*public*/ removeIllegal: function(/*Position*/ pos, /*MoveList*/ moveList) {
        var /*int*/ l = 0;
        var /*UndoInfo*/ ui = new UndoInfo();
        for (var /*int*/ mi = 0; mi < moveList.size; mi++) {
            var /*Move*/ m = moveList.m[mi];
            pos.makeMove(m, ui);
            pos.setWhiteMove(!pos.whiteMove);
            if (!MoveGen.inCheck(pos))
                moveList.m[ l++ ] = new Move( m.from, m.to, m.promoteTo, 0);
            pos.setWhiteMove(!pos.whiteMove);
            pos.unMakeMove(m, ui);
        }
        moveList.size = l;
    },

    /*private*/ /*boolean*/  addPawnMovesByMask: function(/*MoveList*/ moveList, /*Position*/ pos, /*long*/ mask,
                                                    /*int*/ delta, /*boolean*/  allPromotions) {
        if(i64_is0(mask))
            return false;
        var sq,sq0;
        var /*long*/ oKingMask = i64c( pos.pieceTypeBB[pos.whiteMove ? Piece.BKING : Piece.WKING] );
        var kingmask = i64_and(mask,oKingMask);
        if (  i64_not0( kingmask ) ) {
            /*int*/ sq = numberOfTrailingZeros(kingmask);
            moveList.size = 0;
            this.setMove(moveList, sq + delta, sq, Piece.EMPTY);
            return true;
        }
        var /*long*/ promMask = i64_and( mask, BitBoard.maskRow1Row8 );
        mask = i64_and( mask, i64_not(promMask) );
        while (promMask.l||promMask.h) {
            /*int*/ sq = numberOfTrailingZeros(promMask);
            /*int*/ sq0 = sq + delta;
            if (sq >= 56) { // White promotion
                this.setMove(moveList, sq0, sq, Piece.WQUEEN);
                this.setMove(moveList, sq0, sq, Piece.WKNIGHT);
                if (allPromotions) {
                    this.setMove(moveList, sq0, sq, Piece.WROOK);
                    this.setMove(moveList, sq0, sq, Piece.WBISHOP);
                }
            } else { // Black promotion
                this.setMove(moveList, sq0, sq, Piece.BQUEEN);
                this.setMove(moveList, sq0, sq, Piece.BKNIGHT);
                if (allPromotions) {
                    this.setMove(moveList, sq0, sq, Piece.BROOK);
                    this.setMove(moveList, sq0, sq, Piece.BBISHOP);
                }
            }
            i64_bitclear( promMask, sq );
        }
        while (mask.l||mask.h) {
            /*int*/ sq = numberOfTrailingZeros(mask);
            this.setMove(moveList, sq + delta, sq, Piece.EMPTY);
            i64_bitclear( mask, sq )
        }
        return false;
    },

    /*private*/ addPawnDoubleMovesByMask: function(/*MoveList*/ moveList, /*Position*/ pos,
                                                       /*long*/ mask, /*int*/ delta) {
        while (mask.l||mask.h) {
            var /*int*/ sq = numberOfTrailingZeros(mask);
            this.setMove(moveList, sq + delta, sq, Piece.EMPTY);
            i64_bitclear( mask, sq )
        }
    },
    
    /*private*/ /*boolean*/  addMovesByMask: function(/*MoveList*/ moveList, /*Position*/ pos, /*int*/ sq0, /*long*/ mask) {
        /*long*/ oKingMask = i64c( pos.pieceTypeBB[pos.whiteMove ? Piece.BKING : Piece.WKING] );
        var kingmask = i64_and(mask,oKingMask);
	var sq;
        if (  i64_not0( kingmask ) ) {
            /*int*/ sq = numberOfTrailingZeros(kingmask);
            moveList.size = 0;
            this.setMove(moveList, sq0, sq, Piece.EMPTY);
            return true;
        }
        while (mask.l||mask.h) {
            /*int*/ sq = numberOfTrailingZeros(mask);
            this.setMove(moveList, sq0, sq, Piece.EMPTY);
            i64_bitclear( mask, sq )
        }
        return false;
    },

    /*private*/ setMove: function(/*MoveList*/ moveList, /*int*/ from, /*int*/ to, /*int*/ promoteTo) {
        moveList.m[moveList.size++] = new Move( from, to, promoteTo, 0);
    },

    // Code to handle the Move cache.
    /*private*/ /*Object[]*/ moveListCache: [], /* new Object[200] */
    /*private*/ /*int*/ moveListsInCache: 0,
    
    /*private*/ /*int*/ /*MAX_MOVES = 256*/

    /*private*/ /*MoveList*/ getMoveListObj: function() {
        var /*MoveList*/ ml;
        if (this.moveListsInCache > 0) {
            this.moveListCache.length = --this.moveListsInCache;
            ml = /*(MoveList)*/ this.moveListCache[this.moveListsInCache];
            ml.m = []; ml.size = 0;
        } else {
            ml = new this.MoveList();
        }
        return ml;
    },

    /** Return all move objects in moveList to the move cache. */
    /*public*/ returnMoveList: function(/*MoveList*/ moveList) {
        if (this.moveListsInCache < this.moveListCache.length) {
            this.moveListCache[this.moveListsInCache++] = moveList;
        }
    }
};

  
/*===================================== book */

/*public*/ /*class*/ Book  = {

    Book_data: null,       // for book datas
   
    /*public*/ BookEntry: function (/*Move*/ move, /*String*/ moveStr) {
            this.move = move;
            this.moveStr = moveStr;
            this.count = 1;
    },
    
    /*public*/ ListBookEntries: function () {
            this.entries = [];
    },   
    
    /*private*/  bookMap:[],    /*Map<Long, List<BookEntry>>*/
    /*private*/ /*int*/ numBookMoves: -1,
    /*private*/ /*int*/ badmoves: 0,
    /*private*/ /*int*/ same_entries: 0,

    /*private*/ initBook: function (/*boolean*/  verbose) {       
        var /*Position*/ startPos = TextIO.readFEN(TextIO.startPosFEN);
        var /*Position*/ pos = startPos.clone();
        var /*UndoInfo*/ ui = new UndoInfo();
        this.numBookMoves = 0;
        {
            for (var /*int*/ i = 0; i < this.Book_data.length; i += 2) {
                var /*int*/ b0 = this.Book_data[i];
                var /*int*/ b1 = this.Book_data[i+1];
                var /*int*/ move = (b0 << 8) + b1;
                if (move == 0) {
                    pos = startPos.clone();
                    if(SHORT_BOOK)
                    {
                     // skip some
                     for(var skp=Math.floor( Math.random(1)*8);skp>0;skp--)
                     for(;;)
                      {
			i += 2;
			if(i >= this.Book_data.length) break;
			b0 = this.Book_data[i];
			b1 = this.Book_data[i+1];
			move = (b0 << 8) + b1;
			if(move == 0) break;
                      }
                    }		
                } else {
                    var /*boolean*/  bad = (((move >>> 15) & 1) != 0);
                    var /*int*/ prom = (move >>> 12) & 7;
                    var /*Move*/ m = new Move(move & 63, (move >>> 6) & 63,
                                      this.promToPiece(prom, pos.whiteMove),0);
                    if (!bad) this.addToBook(pos, m);
                    pos.makeMove(m, ui);
                }
            }
        }
        if (verbose) {
            // Book contains only some variants for e4
            printf("Book moves: %d \n", this.numBookMoves );            
        }
    },

    /* Get list of book moves for given position */
    /*public*/ GetMvList: function( /*Position*/ pos )
    {
        var idx = pos.zobristHash() & 0xFFFFFF;
        if(typeof(this.bookMap[idx]) == "undefined") return null;
        return this.bookMap[idx];
    },

    /** Add a move to a position in the opening book. */
    /*private*/ addToBook: function (/*Position*/ pos, /*Move*/ moveToAdd) {

        var idx = pos.zobristHash() & 0xFFFFFF;        
        if(typeof(this.bookMap[idx]) == "undefined")
            {
            this.bookMap[idx] = new this.ListBookEntries();
            }
        var bookMoves = this.bookMap[idx];
        
        for (var /*int*/ i in bookMoves.entries) {
            var ent = bookMoves.entries[i];
            if (equalsMove(ent.move, moveToAdd)) {
                ent.count++;
                return;
            }
        }
        
        bookMoves.entries.push( new this.BookEntry(moveToAdd, TextIO.moveToString(pos, moveToAdd, false) ) );
        this.numBookMoves++;
    },
    
    /** Return a random book move for a position, or null if out of book. */
    /*public*/ /*Move*/ getBookMove: function (/*Position*/ pos) {

        var bookMoves = this.GetMvList(pos);
        if(bookMoves == null) return null;
        
        var /*MoveGen.MoveList*/ moves =  MoveGen.pseudoLegalMoves(pos);
        MoveGen.removeIllegal(pos, moves);      

        var /*int*/ sum = 0;
        for (var /*int*/ i in bookMoves.entries) {
            var /*BookEntry*/ be = bookMoves.entries[i];
            var /*boolean*/  containz = false;
            for (var /*int*/ mi = 0; mi < moves.size; mi++)
                if (equalsMove(moves.m[mi], be.move)) {
                    containz = true;
                    break;
                }
            if  (!containz) {
                // If an illegal move was found, it means there was a hash collision.
                return null;
            }
            sum += this.getWeight(be.count);
        }
        if (sum <= 0) return null;
        var /*int*/ rnd = Math.floor(Math.random(1)*sum);
        sum = 0;
        for ( i in bookMoves.entries ) {
            be = bookMoves.entries[i];
            sum += this.getWeight(be.count);
            if (rnd < sum) {
                return be.move;
            }
        }
        // Should never get here
        /*throw new*/ RuntimeException("BookMove exception!");
    },

    /*private*/ /*int*/ getWeight: function(/*int*/ count) {
        var /*double*/ tmp = Math.sqrt(count);
        return /*(int)*/((tmp * Math.sqrt(tmp) * 100) + 1);
    },

    /** Return a string describing all book moves. */
    /*public*/ /*String*/ getAllBookMoves: function (/*Position*/ pos) {
        var bookMoves = this.GetMvList(pos);
        if(bookMoves == null) return "";

        var ret = "";
        for (var i in bookMoves.entries) {
            var /*BookEntry*/ be = bookMoves.entries[i];
            ret+= be.moveStr + "(" + be.count.toString() + ") ";
        }
        return ret;
    },

    /*private*/ /*int*/ pieceToProm: function(/*int*/ p) {
        var i = ( p>6? p-6 : p );
        if( i>1 && i<6 ) return i-1;
        return 0;
    },
    
    /*private*/ /*int*/ promToPiece: function(/*int*/ p, /*boolean*/  whiteMove) {
        if(p>0 && p<5) return (p + (whiteMove?1:7));
        return 0;
    }
};

/*===================================== Evaluate */

/* Position evaluation routines. */
 
/*public*/ /*class*/ Evaluate = {
    /*int*/ pV: 100,
    /*int*/ nV: 400,
    /*int*/ bV: 400,
    /*int*/ rV: 600,
    /*int*/ qV: 1200,
    /*int*/ kV: 9900, // Used by SEE algorithm, but not included in board material sums

    /*int[]*/ pieceValue:[],

    /** Piece/square table for king during middle game. */
    /*int[]*/ kt1b: [ -22,-35,-40,-40,-40,-40,-35,-22,
                                -22,-35,-40,-40,-40,-40,-35,-22,
                                -25,-35,-40,-45,-45,-40,-35,-25,
                                -15,-30,-35,-40,-40,-35,-30,-15,
                                -10,-15,-20,-25,-25,-20,-15,-10,
                                  4, -2, -5,-15,-15, -5, -2,  4,
                                 16, 14,  7, -3, -3,  7, 14, 16,
                                 24, 24,  9,  0,  0,  9, 24, 24 ],

    /** Piece/square table for king during end game. */
    /*int[]*/ kt2b: [  0,  8, 16, 24, 24, 16,  8,  0,
                                 8, 16, 24, 32, 32, 24, 16,  8,
                                16, 24, 32, 40, 40, 32, 24, 16,
                                24, 32, 40, 48, 48, 40, 32, 24,
                                24, 32, 40, 48, 48, 40, 32, 24,
                                16, 24, 32, 40, 40, 32, 24, 16,
                                 8, 16, 24, 32, 32, 24, 16,  8,
                                 0,  8, 16, 24, 24, 16,  8,  0 ],

    /** Piece/square table for pawns during middle game. */
    /*int[]*/ pt1b: [  0,  0,  0,  0,  0,  0,  0,  0,
                                 8, 16, 24, 32, 32, 24, 16,  8,
                                 3, 12, 20, 28, 28, 20, 12,  3,
                                -5,  4, 10, 20, 20, 10,  4, -5,
                                -6,  4,  5, 16, 16,  5,  4, -6,
                                -6,  4,  2,  5,  5,  2,  4, -6,
                                -6,  4,  4,-15,-15,  4,  4, -6,
                                 0,  0,  0,  0,  0,  0,  0,  0 ],

    /** Piece/square table for pawns during end game. */
    /*int[]*/ pt2b: [  0,  0,  0,  0,  0,  0,  0,  0,
                                 25, 40, 45, 45, 45, 45, 40, 25,
                                 17, 32, 35, 35, 35, 35, 32, 17,
                                  5, 24, 24, 24, 24, 24, 24,  5,
                                 -9, 11, 11, 11, 11, 11, 11, -9,
                                -17,  3,  3,  3,  3,  3,  3,-17,
                                -20,  0,  0,  0,  0,  0,  0,-20,
                                  0,  0,  0,  0,  0,  0,  0,  0 ],

    /** Piece/square table for knights during middle game. */
    /*int[]*/ nt1b: [ -53,-42,-32,-21,-21,-32,-42,-53,
                                -42,-32,-10,  0,  0,-10,-32,-42,
                                -21,  5, 10, 16, 16, 10,  5,-21,
                                -18,  0, 10, 21, 21, 10,  0,-18,
                                -18,  0,  3, 21, 21,  3,  0,-18,
                                -21,-10,  0,  0,  0,  0,-10,-21,
                                -42,-32,-10,  0,  0,-10,-32,-42,
                                -53,-42,-32,-21,-21,-32,-42,-53 ],

    /** Piece/square table for knights during end game. */
    /*int[]*/ nt2b: [ -56,-44,-34,-22,-22,-34,-44,-56,
                                -44,-34,-10,  0,  0,-10,-34,-44,
                                -22,  5, 10, 17, 17, 10,  5,-22,
                                -19,  0, 10, 22, 22, 10,  0,-19,
                                -19,  0,  3, 22, 22,  3,  0,-19,
                                -22,-10,  0,  0,  0,  0,-10,-22,
                                -44,-34,-10,  0,  0,-10,-34,-44,
                                -56,-44,-34,-22,-22,-34,-44,-56 ],

    /** Piece/square table for bishops during middle game. */
    /*int[]*/ bt1b: [  0,  0,  0,  0,  0,  0,  0,  0,
                                 0,  4,  2,  2,  2,  2,  4,  0,
                                 0,  2,  4,  4,  4,  4,  2,  0,
                                 0,  2,  4,  4,  4,  4,  2,  0,
                                 0,  2,  4,  4,  4,  4,  2,  0,
                                 0,  3,  4,  4,  4,  4,  3,  0,
                                 0,  4,  2,  2,  2,  2,  4,  0,
                                 0,  0, -2,  0,  0, -2,  0,  0 ],

    /** Piece/square table for queens during middle game. */
    /*int[]*/ qt1b: [ -10, -5,  0,  0,  0,  0, -5,-10,
                                 -5,  0,  5,  5,  5,  5,  0, -5,
                                  0,  5,  5,  6,  6,  5,  5,  0,
                                  0,  5,  6,  6,  6,  6,  5,  0,
                                  0,  5,  6,  6,  6,  6,  5,  0,
                                  0,  5,  5,  6,  6,  5,  5,  0,
                                 -5,  0,  5,  5,  5,  5,  0, -5,
                                -10, -5,  0,  0,  0,  0, -5,-10 ],

    /** Piece/square table for rooks during middle game. */
    /*int[]*/ rt1b: [  0,  3,  5,  5,  5,  5,  3,  0,
                                15, 20, 20, 20, 20, 20, 20, 15,
                                 0,  0,  0,  0,  0,  0,  0,  0,
                                 0,  0,  0,  0,  0,  0,  0,  0,
                                -2,  0,  0,  0,  0,  0,  0, -2,
                                -2,  0,  0,  2,  2,  0,  0, -2,
                                -3,  2,  5,  5,  5,  5,  2, -3,
                                 0,  3,  5,  5,  5,  5,  3,  0 ],

    /*int[]*/ kt1w:[], qt1w:[], rt1w:[], bt1w:[], nt1w:[], pt1w:[], kt2w:[], nt2w:[], pt2w:[],

    initAll: function()
        {
        this.pieceValue = [ 0, this.kV, this.qV, this.rV, this.bV, this.nV, this.pV,
                 this.kV, this.qV, this.rV, this.bV, this.nV, this.pV ];
     
        this.kt1w = this.kt1b.slice().reverse();
        this.qt1w = this.qt1b.slice().reverse();
        this.rt1w = this.rt1b.slice().reverse();
        this.bt1w = this.bt1b.slice().reverse();
        this.nt1w = this.nt1b.slice().reverse();
        this.pt1w = this.pt1b.slice().reverse();
        this.kt2w = this.kt2b.slice().reverse();
        this.nt2w = this.nt2b.slice().reverse();
        this.pt2w = this.pt2b.slice().reverse();

        this.psTab1 = [ this.e0, this.kt1w, this.qt1w, this.rt1w, this.bt1w, this.nt1w, this.pt1w,
                            this.kt1b, this.qt1b, this.rt1b, this.bt1b, this.nt1b, this.pt1b ];
        this.psTab2 = [ this.e0, this.kt2w, this.qt1w, this.rt1w, this.bt1w, this.nt2w, this.pt2w,
                            this.kt2b, this.qt1b, this.rt1b, this.bt1b, this.nt2b, this.pt2b ];
        this.castleFactor = this.GenCastleFactor(256);
        },

    /*private*/ /*int[]*/ e0: e0a.slice(0,64),
    
    /*int[]*/ psTab1:[],
    /*int[]*/ psTab2:[],

    /*int[]*/ distToH1A8:
      [ [ 0, 1, 2, 3, 4, 5, 6, 7 ],
        [ 1, 2, 3, 4, 5, 6, 7, 6 ],
        [ 2, 3, 4, 5, 6, 7, 6, 5 ],
        [ 3, 4, 5, 6, 7, 6, 5, 4 ],
        [ 4, 5, 6, 7, 6, 5, 4, 3 ],
        [ 5, 6, 7, 6, 5, 4, 3, 2 ],
        [ 6, 7, 6, 5, 4, 3, 2, 1 ],
        [ 7, 6, 5, 4, 3, 2, 1, 0 ] ],

    /*int[]*/ rookMobScore: [-10,-7,-4,-1,2,5,7,9,11,12,13,14,14,14,14],
    /*int[]*/ bishMobScore: [-15,-10,-6,-2,2,6,10,13,16,18,20,22,23,24],
    /*int[]*/ queenMobScore: [-5,-4,-3,-2,-1,0,1,2,3,4,5,6,7,8,9,9,10,10,10,10,10,10,10,10,10,10,10,10],

    /*private*/ /*class*/ PawnHashData: function(key,score) {
        /*long*/ this.key = key;
        /*int*/ this.score = score;         // Positive score means good for white
        /*short*/ this.passedBonusW = 0;
        /*short*/ this.passedBonusB = 0;
        /*long*/ this.passedPawnsW = 0;     // The most advanced passed pawns for each file
        /*long*/ this.passedPawnsB = 0;
    },
    
    /*PawnHashData[]*/ pawnHash:[],

    ppBonus: [-1,24,26,30,36,47,64,-1],


    /*byte[]*/ kpkTable: null,

    // King safety variables
    /*private*/ /*long*/ wKingZone:i64(), bKingZone:i64(),     // Squares close to king that are worth attacking
    /*private*/ /*int*/ wKingAttacks:0, bKingAttacks:0, // Number of attacks close to white/black king
    /*private*/ /*long*/ wAttacksBB:i64(), bAttacksBB:i64(),
    
    /**
     * evaluation of a position.
     * @param pos The position to evaluate.
     * @return The evaluation score, measured in centipawns.
     *         Positive values are good for the side to make the next move.
     */
    /*public*/ /*int*/ evalPos: function(/*Position*/ pos) {
        var /*int*/ score = pos.wMtrl - pos.bMtrl;

        this.wKingAttacks = 0;
        this.bKingAttacks = 0;
        this.wKingZone = BitBoard.kingAttacks[pos.getKingSq(true)];
        this.wKingZone = i64_or( this.wKingZone, i64_lshift(this.wKingZone,8) );
        this.bKingZone = BitBoard.kingAttacks[pos.getKingSq(false)];
        this.bKingZone = i64_or( this.bKingZone, i64_rshift(this.bKingZone,8) );
        this.wAttacksBB = i64();
        this.bAttacksBB = i64();

        score += this.pieceSquareEval(pos);
        score += this.pawnBonus(pos);
        score += this.tradeBonus(pos);
        score += this.castleBonus(pos);

        score += this.rookBonus(pos);
        score += this.bishopEval(pos, score);
        score += this.threatBonus(pos);
        score += this.kingSafety(pos);
        score = this.endGameEval(pos, score);

        if (!pos.whiteMove) score = -score;
        return score;
    },

    /** Compute white_material - black_material. */
    /*int*/ material: function (/*Position*/ pos) {
        return pos.wMtrl - pos.bMtrl;
    },
    
    /** Compute score based on piece square tables. Positive values are good for white. */
    /*private*/ /*int*/ pieceSquareEval: function(/*Position*/ pos) {
        var /*int*/ score = 0;
        var /*int*/ wMtrl = pos.wMtrl;
        var /*int*/ bMtrl = pos.bMtrl;
        var /*int*/ wMtrlPawns = pos.wMtrlPawns;
        var /*int*/ bMtrlPawns = pos.bMtrlPawns;
        var k1,k2,t1,t2,t,tw,tb;
        
        // Kings
        {
            /*int*/ t1 = this.qV + (this.rV<<1) + (this.bV<<1);
            /*int*/ t2 = this.rV;
            {
                /*int*/ k1 = pos.psScore1[Piece.WKING];
                /*int*/ k2 = pos.psScore2[Piece.WKING];
                /*int*/ t = bMtrl - bMtrlPawns;
                score += this.interpolate(t, t2, k2, t1, k1);
            }
            {
                /*int*/ k1 = pos.psScore1[Piece.BKING];
                /*int*/ k2 = pos.psScore2[Piece.BKING];
                /*int*/ t = wMtrl - wMtrlPawns;
                score -= this.interpolate(t, t2, k2, t1, k1);
            }
        }

        // Pawns
        {
            /*int*/ t1 = this.qV + (this.rV<<1) + (this.bV<<1);
            /*int*/ t2 = this.rV;
            /*int*/ wp1 = pos.psScore1[Piece.WPAWN];
            /*int*/ wp2 = pos.psScore2[Piece.WPAWN];
            if ((wp1 != 0) || (wp2 != 0)) {
                /*int*/ tw = bMtrl - bMtrlPawns;
                score += this.interpolate(tw, t2, wp2, t1, wp1);
            }
            /*int*/ bp1 = pos.psScore1[Piece.BPAWN];
            /*int*/ bp2 = pos.psScore2[Piece.BPAWN];
            if ((bp1 != 0) || (bp2 != 0)) {
                /*int*/ tb = wMtrl - wMtrlPawns;
                score -= this.interpolate(tb, t2, bp2, t1, bp1);
            }
        }

        // Knights
        {
            /*int*/ t1 = this.qV + (this.rV<<1) + this.bV + this.nV + (6*this.pV);
            /*int*/ t2 = this.nV + (this.pV<<3);
            /*int*/ n1 = pos.psScore1[Piece.WKNIGHT];
            /*int*/ n2 = pos.psScore2[Piece.WKNIGHT];
            if ((n1 != 0) || (n2 != 0)) {
                score += this.interpolate(bMtrl, t2, n2, t1, n1);
            }
            n1 = pos.psScore1[Piece.BKNIGHT];
            n2 = pos.psScore2[Piece.BKNIGHT];
            if ((n1 != 0) || (n2 != 0)) {
                score -= this.interpolate(wMtrl, t2, n2, t1, n1);
            }
        }

        // Bishops
        {
            score += pos.psScore1[Piece.WBISHOP];
            score -= pos.psScore1[Piece.BBISHOP];
        }

        // Queens
        {
            var m, sq, atk;
            var /*long*/ occupied = i64_or( pos.whiteBB, pos.blackBB );
            var NwhiteBB = i64_not( pos.whiteBB );
            var NblackBB = i64_not( pos.blackBB );
            
            score += pos.psScore1[Piece.WQUEEN];
            /*long*/ m = i64c(pos.pieceTypeBB[Piece.WQUEEN]);
            while (m.l||m.h) {
                /*int*/ sq = numberOfTrailingZeros(m);
                /*long*/ atk = i64_or( rookAttacks(sq, occupied), bishopAttacks(sq, occupied) );
                this.wAttacksBB = i64_or( this.wAttacksBB, atk );
                score += this.queenMobScore[ i64_bitcount( i64_and( atk, NwhiteBB ) ) ];
                this.bKingAttacks += (i64_bitcount( i64_and( atk, this.bKingZone) ) << 1);
                i64_bitclear( m, sq );
            }
            score -= pos.psScore1[Piece.BQUEEN];
            m = i64c(pos.pieceTypeBB[Piece.BQUEEN]);
            while (m.l||m.h) {
                /*int*/ sq = numberOfTrailingZeros(m);
                /*long*/ atk = i64_or( rookAttacks(sq, occupied), bishopAttacks(sq, occupied) );
                this.bAttacksBB = i64_or( this.bAttacksBB, atk );
                score -= this.queenMobScore[ i64_bitcount( i64_and( atk, NblackBB ) ) ];
                this.wKingAttacks += (i64_bitcount( i64_and( atk, this.wKingZone) ) << 1);
                i64_bitclear( m, sq );
            }
        }

        // Rooks
        {
            var /*int*/ r1,nP,s;
            r1 = pos.psScore1[Piece.WROOK];
            if (r1 != 0) {
                /*int*/ nP = bMtrlPawns / this.pV;
                /*int*/ s = r1 * Math.min(nP, 6) / 6;
                score += s;
            }
            r1 = pos.psScore1[Piece.BROOK];
            if (r1 != 0) {
                /*int*/ nP = wMtrlPawns / this.pV;
                /*int*/ s = r1 * Math.min(nP, 6) / 6;
                score -= s;
            }
        }

        return score;
    },

    /** Implement the "when ahead trade pieces, when behind trade pawns" rule. */
    /*private*/ /*int*/ tradeBonus: function(/*Position*/ pos) {
        var /*int*/ wM = pos.wMtrl;
        var /*int*/ bM = pos.bMtrl;
        var /*int*/ wPawn = pos.wMtrlPawns;
        var /*int*/ bPawn = pos.bMtrlPawns;
        var /*int*/ deltaScore = wM - bM;

        var /*int*/ pBonus = 0;
        pBonus += this.interpolate((deltaScore > 0) ? wPawn : bPawn,
                 0, (-30) * deltaScore / 100, 6 * this.pV, 0);
        pBonus += this.interpolate((deltaScore > 0) ? bM : wM,
                 0, 30 * deltaScore / 100, this.qV + (this.rV<<1) + (this.bV<<1) + (this.nV<<1), 0);

        return pBonus;
    },

    /*private*/ /*int[]*/ GenCastleFactor: function(sz) {
        var castleFactor = [];  /*new int[256]*/

        for (var /*int*/ i = 0; i < sz; i++) {
            var /*int*/ h1Dist = 100;
            var /*boolean*/ h1Castle = ( (i & (1<<7)) != 0 );
            if (h1Castle)
                h1Dist = 2 + i64_bitcount( i64_and( i64v(i), o_x60 ) ); // f1,g1
            var /*int*/ a1Dist = 100;
            var /*boolean*/ a1Castle = ( (i & 1) != 0 );
            if (a1Castle)
                a1Dist = 2 + i64_bitcount( i64_and( i64v(i), o_x0E ) ); // b1,c1,d1
            castleFactor[i] = 1024 / Math.min(a1Dist, h1Dist);
        }
        return castleFactor;
    },
    
    castleFactor:[],

    /** Score castling ability. */
    /*private*/ /*int*/ castleBonus: function(/*Position*/ pos) {
        if (pos.getCastleMask() == 0) return 0;
        var q = (7<<3);
        var /*int*/ k1 = this.kt1b[q+6] - this.kt1b[q+4];
        var /*int*/ k2 = this.kt2b[q+6] - this.kt2b[q+4];
        var /*int*/ t1 = this.qV + (this.rV<<1) + (this.bV<<1);
        var /*int*/ t2 = this.rV;
        var /*int*/ t = pos.bMtrl - pos.bMtrlPawns;
        var /*int*/ ks = this.interpolate(t, t2, k2, t1, k1);

        var /*int*/ castleValue = ks + this.rt1b[q+5] - this.rt1b[q+7];
        if (castleValue <= 0) return 0;

        var /*long*/ occupied = i64_or( pos.whiteBB, pos.blackBB );
        var /*int*/ tw = /*(int)*/ (occupied.l) & 0x6E;
        if (pos.a1Castle()) tw |= 1;
        if (pos.h1Castle()) tw |= (1 << 7);
        var /*int*/ wBonus = (castleValue * this.castleFactor[tw]) >> 10;

        var /*int*/ tb = /*(int)*/ ( i64_rshift(occupied, 56).l ) & 0x6E;
        if (pos.a8Castle()) tb |= 1;
        if (pos.h8Castle()) tb |= (1 << 7);
        var /*int*/ bBonus = (castleValue * this.castleFactor[tb]) >> 10;

        return wBonus - bBonus;
    },


    /*private*/ /*int*/ pawnBonus: function(/*Position*/ pos) {
        var /*long*/ key = pos.pawnZobristHash();
        var phk = key & 0xFFFFFF;
        if(typeof(Evaluate.pawnHash[phk]) == "undefined") Evaluate.pawnHash[phk] = new this.PawnHashData(-1,0);
        var phd = Evaluate.pawnHash[phk];
        if (phd.key != key) this.computePawnHashData(pos, phd);
        var /*int*/ score = phd.score;

        var /*int*/ hiMtrl = this.qV + this.rV;
        score += this.interpolate(pos.bMtrl - pos.bMtrlPawns, 0, phd.passedBonusW, hiMtrl, phd.passedBonusW << 1);
        score -= this.interpolate(pos.wMtrl - pos.wMtrlPawns, 0, phd.passedBonusB, hiMtrl, phd.passedBonusB << 1);

        // Passed pawns are more dangerous if enemy king is far away
        var mtrlNoPawns, kingPos, kingX, kingY, sq, x, y, pawnDist, kingDistX, kingDistY, kingDist, kScore;
        var /*int*/ highMtrl = this.qV + this.rV;
        var /*long*/ m = i64c(phd.passedPawnsW);
        if ( m.l||m.h ) {
            mtrlNoPawns = pos.bMtrl - pos.bMtrlPawns;
            if (mtrlNoPawns < highMtrl) {
                /*int*/ kingPos = pos.getKingSq(false);
                /*int*/ kingX = Position.getX(kingPos);
                /*int*/ kingY = Position.getY(kingPos);
                while ( m.l||m.h ) {
                    /*int*/ sq = numberOfTrailingZeros(m);
                    /*int*/ x = Position.getX(sq);
                    /*int*/ y = Position.getY(sq);
                    /*int*/ pawnDist = Math.min(5, 7 - y);
                    /*int*/ kingDistX = Math.abs(kingX - x);
                    /*int*/ kingDistY = Math.abs(kingY - 7);
                    /*int*/ kingDist = Math.max(kingDistX, kingDistY);
                    /*int*/ kScore = kingDist << 2;
                    if (kingDist > pawnDist) kScore += (kingDist - pawnDist) * (kingDist - pawnDist);
                    score += this.interpolate(mtrlNoPawns, 0, kScore, highMtrl, 0);
                    if (!pos.whiteMove)
                        kingDist--;
                    if ((pawnDist < kingDist) && (mtrlNoPawns == 0))
                        score += 500; // King can't stop pawn
                    i64_bitclear( m, sq );
                }
            }
        }
        m = i64c(phd.passedPawnsB);
        if ( m.l||m.h ) {
            mtrlNoPawns = pos.wMtrl - pos.wMtrlPawns;
            if (mtrlNoPawns < highMtrl) {
                /*int*/ kingPos = pos.getKingSq(true);
                /*int*/ kingX = Position.getX(kingPos);
                /*int*/ kingY = Position.getY(kingPos);
                while ( m.l||m.h ) {
                    /*int*/ sq = numberOfTrailingZeros(m);
                    /*int*/ x = Position.getX(sq);
                    /*int*/ y = Position.getY(sq);
                    /*int*/ pawnDist = Math.min(5, y);
                    /*int*/ kingDistX = Math.abs(kingX - x);
                    /*int*/ kingDistY = Math.abs(kingY - 0);
                    /*int*/ kingDist = Math.max(kingDistX, kingDistY);
                    /*int*/ kScore = kingDist << 2;
                    if (kingDist > pawnDist) kScore += (kingDist - pawnDist) * (kingDist - pawnDist);
                    score -= this.interpolate(mtrlNoPawns, 0, kScore, highMtrl, 0);
                    if (pos.whiteMove)
                        kingDist--;
                    if ((pawnDist < kingDist) && (mtrlNoPawns == 0))
                        score -= 500; // King can't stop pawn
                    i64_bitclear( m, sq );
                }
            }
        }

        return score;
    },

    /** Compute pawn hash data for pos. */
    /*private*/ computePawnHashData: function (/*Position*/ pos, /*PawnHashData*/ ph) {
        var /*int*/ score = 0;

        // Evaluate double pawns and pawn islands
        var /*long*/ wPawns = pos.pieceTypeBB[Piece.WPAWN];
        var /*long*/ wPawnFiles = i64_and( southFill(wPawns) , o_xFF );
        var NwPawnFiles = i64_not(wPawnFiles);
        var NrwPawnFiles = i64_rshift(wPawnFiles,1);
        var N1wPawnFiles = i64_not( i64_lshift(wPawnFiles,1) );
        var N2wPawnFiles = i64_not( NrwPawnFiles );
        var /*int*/ wDouble = i64_bitcount(wPawns) - i64_bitcount(wPawnFiles);
        var /*int*/ wIslands = i64_bitcount( i64_and( NrwPawnFiles , wPawnFiles) );
        var /*int*/ wIsolated = i64_bitcount( i64_and(N1wPawnFiles , i64_and(wPawnFiles,N2wPawnFiles) ) );
       
        var /*long*/ bPawns = pos.pieceTypeBB[Piece.BPAWN];
        var /*long*/ bPawnFiles = i64_and( southFill(bPawns) , o_xFF );
        var NbPawnFiles = i64_not(bPawnFiles);
        var NrbPawnFiles = i64_rshift(bPawnFiles,1);
        var N1bPawnFiles = i64_not( i64_lshift(bPawnFiles,1) );
        var N2bPawnFiles = i64_not( NrbPawnFiles );
        var /*int*/ bDouble = i64_bitcount(bPawns) - i64_bitcount(bPawnFiles);
        var /*int*/ bIslands = i64_bitcount( i64_and( NrbPawnFiles , bPawnFiles) );
        var /*int*/ bIsolated = i64_bitcount( i64_and(N1bPawnFiles , i64_and(bPawnFiles,N2bPawnFiles) ) );

        score -= (wDouble - bDouble) * 25;
        score -= (wIslands - bIslands) * 15;
        score -= (wIsolated - bIsolated) * 15;


        // Evaluate backward pawns, defined as a pawn that guards a friendly pawn,
        // can't be guarded by friendly pawns, can advance, but can't advance without 
        // being captured by an enemy pawn.
        var wP_AG = i64_and(wPawns, BitBoard.maskAToGFiles);
        var wP_BH = i64_and(wPawns, BitBoard.maskBToHFiles);

        var wP_r7_AG = i64_rshift(wP_AG,7);
        var wP_l9_AG = i64_lshift(wP_AG,9);
        var wP_r9_BH = i64_rshift(wP_BH,9);
        var wP_l7_BH = i64_lshift(wP_BH,7);


        var bP_AG = i64_and(bPawns, BitBoard.maskAToGFiles);
        var bP_BH = i64_and(bPawns, BitBoard.maskBToHFiles);

        var bP_r7_AG = i64_rshift(bP_AG,7);
        var bP_l9_AG = i64_lshift(bP_AG,9);
        var bP_r9_BH = i64_rshift(bP_BH,9);
        var bP_l7_BH = i64_lshift(bP_BH,7);

        var wbPor = i64_or(wPawns, bPawns);
        var wb_l8 = i64_lshift(wbPor,8);
        var wb_r8 = i64_rshift(wbPor,8);
        
        var /*long*/ wPawnAttacks = i64_or(wP_l7_BH, wP_l9_AG);
                             
        var /*long*/ bPawnAttacks = i64_or(bP_r9_BH, bP_r7_AG);

        var /*long*/ wBackward = i64_and( i64_and( wPawns, i64_not(wb_r8) ), i64_rshift(bPawnAttacks,8) );
        wBackward = i64_and( wBackward, i64_not( northFill(wPawnAttacks) ) );
        wBackward = i64_and( wBackward, i64_or( wP_r9_BH, wP_r7_AG ) );
        wBackward = i64_and( wBackward, i64_not( northFill(bPawnFiles) ) );
        
        var /*long*/ bBackward = i64_and( i64_and( bPawns, i64_not(wb_l8) ), i64_lshift(wPawnAttacks,8) );
        bBackward = i64_and( bBackward, i64_not( southFill(bPawnAttacks) ) );
        bBackward = i64_and( bBackward, i64_or( bP_l7_BH, bP_l9_AG ) );
        bBackward = i64_and( bBackward, i64_not( northFill(wPawnFiles) ) );

        score -= (i64_bitcount(wBackward) - i64_bitcount(bBackward)) * 15;

        // Evaluate passed pawn bonus, white
        
        var ppW = i64_or( bPawns, i64_or( bPawnAttacks, i64_rshift(wPawns,8) ) ); 
        var /*long*/ passedPawnsW = i64_and( wPawns, i64_not( southFill(ppW) ) );
        var /*int*/ passedBonusW = 0;
        if (i64_not0(passedPawnsW)) {
            var /*long*/ guardedPassedW = i64_and( passedPawnsW, i64_or( wP_l7_BH,  wP_l9_AG ) );
            passedBonusW += 15 * i64_bitcount(guardedPassedW);
            var /*long*/ m = i64c(passedPawnsW);
            while ( m.l||m.h ) {
                var /*int*/ sq = numberOfTrailingZeros(m);
                var /*int*/ y = Position.getY(sq);
                passedBonusW += this.ppBonus[y];
                i64_bitclear( m, sq );
            }
        }

        // Evaluate passed pawn bonus, black
        var ppB = i64_or( wPawns, i64_or( wPawnAttacks, i64_lshift(bPawns,8) ) ); 
        var /*long*/ passedPawnsB = i64_and( bPawns, i64_not( northFill(ppB) ) );
        var /*int*/ passedBonusB = 0;
        if (i64_not0(passedPawnsB)) {
            var /*long*/ guardedPassedB = i64_and( passedPawnsB, i64_or( bP_r9_BH,  bP_r7_AG ) );
            passedBonusB += 15 * i64_bitcount(guardedPassedB);
            var /*long*/ m = i64c(passedPawnsB);
            while ( m.l||m.h ) {
                var /*int*/ sq = numberOfTrailingZeros(m);
                var /*int*/ y = Position.getY(sq);
                passedBonusB += this.ppBonus[7-y];
                i64_bitclear( m, sq );
            }
        }

        ph.key = pos.pawnZobristHash();
        ph.score = score;
        ph.passedBonusW = passedBonusW;
        ph.passedBonusB = passedBonusB;
        ph.passedPawnsW = passedPawnsW;
        ph.passedPawnsB = passedPawnsB;
    },

    /** Compute rook bonus. Rook on open/half-open file. */
    /*private*/ /*int*/ rookBonus: function (/*Position*/ pos) {
        var /*int*/ score = 0;
        var /*long*/ wPawns = pos.pieceTypeBB[Piece.WPAWN];
        var /*long*/ bPawns = pos.pieceTypeBB[Piece.BPAWN];
        var sq, x, atk, r7, m;       
        var /*long*/ occupied = i64_or(pos.whiteBB, pos.blackBB);
        var Noccupied = i64_not(occupied);
        var NwhiteBB = i64_not(pos.whiteBB);
        var NblackBB = i64_not(pos.blackBB);
        /*long*/ m = i64c(pos.pieceTypeBB[Piece.WROOK]);
        while ( m.l||m.h ) {
            /*int*/ sq = numberOfTrailingZeros(m);
            /*int*/ x = Position.getX(sq);
            if ( i64_is0( i64_and(wPawns,BitBoard.maskFile[x]) ) ) { // At least half-open file
                score += ( i64_is0( i64_and(bPawns, BitBoard.maskFile[x]) ) ? 25 : 12 );
            }
            /*long*/ atk = rookAttacks(sq, occupied);
            this.wAttacksBB = i64_or( this.wAttacksBB, atk );
            score += this.rookMobScore[ i64_bitcount( i64_and(atk, NwhiteBB) ) ];
            if ( i64_not0( i64_and(atk, this.bKingZone) ) )
                this.bKingAttacks += i64_bitcount( i64_and(atk, this.bKingZone) );
            i64_bitclear( m, sq );
        }
        /*long*/ r7 = i64_and( pos.pieceTypeBB[Piece.WROOK], o_xFF1 );
        if (  i64_not0( i64_and(r7, i64_sub(r7,o_x1)) ) &&
                i64_not0( i64_and(pos.pieceTypeBB[Piece.BKING], o_xFF2) )  )
            score += 20; // Two rooks on 7:th row
        m = i64c(pos.pieceTypeBB[Piece.BROOK]);
        while ( m.l||m.h ) {
            /*int*/ sq = numberOfTrailingZeros(m);
            /*int*/ x = Position.getX(sq);
            if ( i64_is0( i64_and(bPawns, BitBoard.maskFile[x]) ) ) {
                score -= ( i64_is0( i64_and(wPawns, BitBoard.maskFile[x]) ) ? 25 : 12 );
            }
            /*long*/ atk = rookAttacks(sq, occupied);
            this.bAttacksBB = i64_or( this.bAttacksBB, atk );
            score -= this.rookMobScore[ i64_bitcount( i64_and(atk, NblackBB) ) ];
            if ( i64_not0( i64_and(atk, this.wKingZone) ) )
                this.wKingAttacks += i64_bitcount( i64_and(atk, this.wKingZone) );
            i64_bitclear( m, sq );
        }
        r7 = i64_and( pos.pieceTypeBB[Piece.BROOK], o_xFF3 );
        if (  i64_not0( i64_and(r7, i64_sub(r7,o_x1)) ) &&
                i64_not0( i64_and(pos.pieceTypeBB[Piece.WKING], o_xFF4) )  )
          score -= 20; // Two rooks on 2:nd row
        return score;
    },

    /** Compute bishop evaluation. */
    /*private*/ /*int*/ bishopEval: function (/*Position*/ pos, /*int*/ oldScore) {
        var /*int*/ score = 0;
        var /*long*/ occupied = i64_or(pos.whiteBB, pos.blackBB);
        var Noccupied = i64_not(occupied);
        var NwhiteBB = i64_not(pos.whiteBB);
        var NblackBB = i64_not(pos.blackBB);
        var /*long*/ wBishops = pos.pieceTypeBB[Piece.WBISHOP];
        var /*long*/ bBishops = pos.pieceTypeBB[Piece.BBISHOP];
        if ( i64_is0( i64_or(wBishops, bBishops) ) ) return 0;
        var sq, atk, m;       
        /*long*/ m = i64c(wBishops);
        while ( m.l||m.h ) {
            /*int*/ sq = numberOfTrailingZeros(m);
            /*long*/ atk = bishopAttacks(sq, occupied);
            this.wAttacksBB = i64_or( this.wAttacksBB, atk );
            score += this.bishMobScore[ i64_bitcount( i64_and(atk, NwhiteBB) ) ];
            if ( i64_not0( i64_and(atk, this.bKingZone) ) )
                this.bKingAttacks += i64_bitcount( i64_and(atk, this.bKingZone) );
            i64_bitclear( m, sq );
        }
        m = i64c(bBishops);
        while ( m.l||m.h ) {
            /*int*/ sq = numberOfTrailingZeros(m);
            /*long*/ atk = bishopAttacks(sq, occupied);
            this.bAttacksBB = i64_or( this.bAttacksBB, atk );
            score -= this.bishMobScore[ i64_bitcount( i64_and(atk, NblackBB) ) ];
            if ( i64_not0( i64_and(atk, this.wKingZone) ) )
                this.wKingAttacks += i64_bitcount( i64_and(atk, this.wKingZone) );
            i64_bitclear( m, sq );
        }

        var /*boolean*/  whiteDark  = i64_not0( i64_and(pos.pieceTypeBB[Piece.WBISHOP], BitBoard.maskDarkSq ));
        var /*boolean*/  whiteLight = i64_not0( i64_and(pos.pieceTypeBB[Piece.WBISHOP], BitBoard.maskLightSq));
        var /*boolean*/  blackDark  = i64_not0( i64_and(pos.pieceTypeBB[Piece.BBISHOP], BitBoard.maskDarkSq ));
        var /*boolean*/  blackLight = i64_not0( i64_and(pos.pieceTypeBB[Piece.BBISHOP], BitBoard.maskLightSq));
        var /*int*/ numWhite = (whiteDark ? 1 : 0) + (whiteLight ? 1 : 0);
        var /*int*/ numBlack = (blackDark ? 1 : 0) + (blackLight ? 1 : 0);
        var /*int*/ numPawns;
    
        // Bishop pair bonus
        if (numWhite == 2) {
            /*int*/ numPawns = pos.wMtrlPawns / this.pV;
            score += 20 + (8 - numPawns) * 3;
        }
        if (numBlack == 2) {
            /*int*/ numPawns = pos.bMtrlPawns / this.pV;
            score -= 20 + (8 - numPawns) * 3;
        }

        // bad bishop       
        if ((numWhite == 1) && (numBlack == 1) && (whiteDark != blackDark) &&
            (pos.wMtrl - pos.wMtrlPawns == pos.bMtrl - pos.bMtrlPawns)) {
            var /*int*/ penalty = (oldScore + score) / 2;
            var /*int*/ loMtrl = this.bV << 1;
            var /*int*/ hiMtrl = (this.qV + this.rV + this.bV) << 1;
            var /*int*/ mtrl = pos.wMtrl + pos.bMtrl - pos.wMtrlPawns - pos.bMtrlPawns;
            score -= this.interpolate(mtrl, loMtrl, penalty, hiMtrl, 0);
        }

        // Penalty for bishop trapped behind pawn at a2/h2/a7/h7
        if ( i64_not0( i64_and( i64_or(wBishops, bBishops) , o_x81x ) ) ) {
            if ((pos.squares[48] == Piece.WBISHOP) && // a7
                (pos.squares[41] == Piece.BPAWN) &&
                (pos.squares[50] == Piece.BPAWN))
                score -= ((this.pV * 3)>>>1);
            if ((pos.squares[55] == Piece.WBISHOP) && // h7
                (pos.squares[46] == Piece.BPAWN) &&
                (pos.squares[53] == Piece.BPAWN))
                score -= i64_not0(pos.pieceTypeBB[Piece.WQUEEN]) ? this.pV : ((this.pV * 3)>>>1) ;
            if ((pos.squares[8] == Piece.BBISHOP) &&  // a2
                (pos.squares[17] == Piece.WPAWN) &&
                (pos.squares[10] == Piece.WPAWN))
                score += this.pV * 3 / 2;
            if ((pos.squares[15] == Piece.BBISHOP) && // h2
                (pos.squares[22] == Piece.WPAWN) &&
                (pos.squares[13] == Piece.WPAWN))
                score += i64_not0(pos.pieceTypeBB[Piece.BQUEEN]) ? this.pV : ((this.pV * 3)>>>1);
        }

        return score;
    },

    /*private*/ /*int*/ threatBonus: function (/*Position*/ pos) {
        var /*int*/ score = 0;
        var m,sq, pawns, tmp;
        // Sum values for all black pieces under attack
        /*long*/ m = i64c(pos.pieceTypeBB[Piece.WKNIGHT]);
        while ( m.l||m.h ) {
            /*int*/ sq = numberOfTrailingZeros(m);
            this.wAttacksBB = i64_or( this.wAttacksBB, BitBoard.knightAttacks[sq] );
            i64_bitclear( m, sq );
        }
        var bOr1 = i64_or( pos.pieceTypeBB[Piece.BKNIGHT], pos.pieceTypeBB[Piece.BBISHOP] );
        bOr1 = i64_or( bOr1, i64_or( pos.pieceTypeBB[Piece.BROOK], pos.pieceTypeBB[Piece.BQUEEN] ) );
        this.wAttacksBB = i64_and( this.wAttacksBB, bOr1 );
        /*long*/ pawns = pos.pieceTypeBB[Piece.WPAWN];
        this.wAttacksBB = i64_or( this.wAttacksBB, i64_lshift( i64_and(pawns, BitBoard.maskBToHFiles), 7 ) );
        this.wAttacksBB = i64_or( this.wAttacksBB, i64_lshift( i64_and(pawns, BitBoard.maskAToGFiles), 9 ) );
        m = i64_and( this.wAttacksBB, i64_and( pos.blackBB, i64_not(pos.pieceTypeBB[Piece.BKING]) ) );
        /*int*/ tmp = 0;
        while ( m.l||m.h ) {
            /*int*/ sq = numberOfTrailingZeros(m);
            tmp += this.pieceValue[pos.squares[sq]];
            i64_bitclear( m, sq );
        }
        score += tmp + (tmp * tmp / this.qV);

        // Sum values for all white pieces under attack
        m = i64c(pos.pieceTypeBB[Piece.BKNIGHT]);
        while ( m.l||m.h ) {
            /*int*/ sq = numberOfTrailingZeros(m);
            this.bAttacksBB = i64_or( this.bAttacksBB, BitBoard.knightAttacks[sq] );
            i64_bitclear( m, sq );
        }
        var wOr1 = i64_or( pos.pieceTypeBB[Piece.WKNIGHT], pos.pieceTypeBB[Piece.WBISHOP] );
        wOr1 = i64_or( wOr1, i64_or( pos.pieceTypeBB[Piece.WROOK], pos.pieceTypeBB[Piece.WQUEEN] ) );
        this.bAttacksBB = i64_and( this.bAttacksBB, wOr1 );
        pawns = pos.pieceTypeBB[Piece.BPAWN];
        this.bAttacksBB = i64_or( this.bAttacksBB, i64_rshift( i64_and(pawns, BitBoard.maskBToHFiles), 9 ) );
        this.bAttacksBB = i64_or( this.bAttacksBB, i64_rshift( i64_and(pawns, BitBoard.maskAToGFiles), 7 ) );
        m = i64_and( this.bAttacksBB, i64_and( pos.whiteBB, i64_not(pos.pieceTypeBB[Piece.WKING]) ) );
        tmp = 0;
        while ( m.l||m.h ) {
            /*int*/ sq = numberOfTrailingZeros(m);
            tmp += this.pieceValue[pos.squares[sq]];
            i64_bitclear( m, sq );
        }
        score -= tmp + (tmp * tmp / this.qV);
        return (score / 64);
    },


    /** Compute king safety for both kings. */
    /*private*/ /*int*/ kingSafety: function(/*Position*/ pos) {
        var /*int*/ minM = this.rV + this.bV;
        var /*int*/ m = (pos.wMtrl - pos.wMtrlPawns + pos.bMtrl - pos.bMtrlPawns) / 2;
        if (m <= minM) return 0;
        var /*int*/ maxM = this.qV + (this.rV<<1) + (this.bV<<1) + (this.nV<<1);
        var /*int*/ score = this.kingSafetyKPPart(pos);
        if (Position.getY(pos.wKingSq) == 0) {
            if ( i64_not0( i64_and(pos.pieceTypeBB[Piece.WKING], o_x60) ) && // King on f1 or g1
                i64_not0( i64_and(pos.pieceTypeBB[Piece.WROOK], o_xC0) ) && // Rook on g1 or h1
                i64_not0( i64_and(pos.pieceTypeBB[Piece.WPAWN], BitBoard.maskFile[6]) ) &&
                i64_not0( i64_and(pos.pieceTypeBB[Piece.WPAWN], BitBoard.maskFile[7]) ) ) {
                score -= 90;
            } else
            if ( i64_not0( i64_and(pos.pieceTypeBB[Piece.WKING], o_x06) ) && // King on b1 or c1
                i64_not0( i64_and(pos.pieceTypeBB[Piece.WROOK], o_x03) ) && // Rook on a1 or b1
                i64_not0( i64_and(pos.pieceTypeBB[Piece.WPAWN], BitBoard.maskFile[0]) ) &&
                i64_not0( i64_and(pos.pieceTypeBB[Piece.WPAWN], BitBoard.maskFile[1]) ) ) {
                score -= 90;
            }
        }
        if (Position.getY(pos.bKingSq) == 7) {
            if ( i64_not0( i64_and(pos.pieceTypeBB[Piece.BKING], o_xL6)) && // King on f8 or g8
                i64_not0( i64_and(pos.pieceTypeBB[Piece.BROOK], o_xLC)) && // Rook on g8 or h8
                i64_not0( i64_and(pos.pieceTypeBB[Piece.BPAWN], BitBoard.maskFile[6])) &&
                i64_not0( i64_and(pos.pieceTypeBB[Piece.BPAWN], BitBoard.maskFile[7]))) {
                score += 90;
            } else
            if ( i64_not0( i64_and(pos.pieceTypeBB[Piece.BKING], o_xLv6)) && // King on b8 or c8
                i64_not0( i64_and(pos.pieceTypeBB[Piece.BROOK], o_xLv3)) && // Rook on a8 or b8
                i64_not0( i64_and(pos.pieceTypeBB[Piece.BPAWN], BitBoard.maskFile[0])) &&
                i64_not0( i64_and(pos.pieceTypeBB[Piece.BPAWN], BitBoard.maskFile[1]))) {
                score += 90;
            }
        }
        score += ((this.bKingAttacks - this.wKingAttacks)<<2);
        return /*int*/ this.interpolate(m, minM, 0, maxM, score);
    },

    /*private*/ /*class*/ KingSafetyHashData: function(key,score) {
        /*long*/ this.key = key;
        /*int*/ this.score = score;
    },
       
    kingSafetyHash:[],

    /*private*/ /*int*/ kingSafetyKPPart: function(/*Position*/ pos) {
        var /*long*/ key = pos.pawnZobristHash() ^ pos.kingZobristHash();
        var phk = key & 0xFFFFFF;
        if(typeof(Evaluate.kingSafetyHash[phk]) == "undefined")
             Evaluate.kingSafetyHash[phk] = new this.KingSafetyHashData(-1,0);
        var ksh = Evaluate.kingSafetyHash[phk];
        if (ksh.key != key) {
            var /*long*/ wOpen, bOpen, kSafety, safety, halfOpenFiles, shelter;
            var /*int*/ score = 0;
            var /*long*/ wPawns = pos.pieceTypeBB[Piece.WPAWN];
            var /*long*/ bPawns = pos.pieceTypeBB[Piece.BPAWN];
            {
                /*int*/ safety = 0;
                /*int*/ halfOpenFiles = 0;
                if (Position.getY(pos.wKingSq) < 2) {
                    /*long*/ shelter = i64bitObj( Position.getX(pos.wKingSq) );
                    var sh1 = i64_or( i64_rshift( i64_and(shelter, BitBoard.maskBToHFiles), 1) ,
                               i64_lshift( i64_and(shelter, BitBoard.maskAToGFiles), 1) );
                    shelter = i64_lshift( i64_or( shelter, sh1 ), 8 );
                    safety += 3 * i64_bitcount( i64_and( wPawns, shelter ) );
                    safety -= ( i64_bitcount( i64_and(bPawns, i64_or(shelter,i64_lshift(shelter,8)))) << 1);
                    shelter = i64_lshift( shelter, 8 );
                    safety += ( i64_bitcount( i64_and( wPawns, shelter ) ) << 1);
                    shelter = i64_lshift( shelter, 8 );
                    safety -= i64_bitcount( i64_and( bPawns, shelter ) );
                    
                    /*long*/ wOpen = i64_and( southFill(shelter),
                        i64_and( i64_not(southFill(wPawns)), o_xFF ) );
                    if ( i64_not0(wOpen) ) {
                        halfOpenFiles += 25 * i64_bitcount( i64_and(wOpen, o_xE7) );
                        halfOpenFiles += 10 * i64_bitcount( i64_and(wOpen, o_x18) );
                    }
                    /*long*/ bOpen = i64_and( southFill(shelter),
                        i64_and( i64_not(southFill(bPawns)), o_xFF ) );
                    if ( i64_not0(bOpen) ) {
                        halfOpenFiles += 25 * i64_bitcount( i64_and(bOpen, o_xE7) );
                        halfOpenFiles += 10 * i64_bitcount( i64_and(bOpen, o_x18) );
                    }
                    safety = Math.min(safety, 8);
                }
                /*int*/ kSafety = ((safety - 9) * 15) - halfOpenFiles;
                score += kSafety;
            }
            {
                /*int*/ safety = 0;
                /*int*/ halfOpenFiles = 0;
                if (Position.getY(pos.bKingSq) >= 6) {
                    /*long*/ shelter = i64bitObj( 56 + Position.getX(pos.bKingSq) );
                    var sh2 = i64_or( i64_rshift( i64_and(shelter, BitBoard.maskBToHFiles), 1) ,
                               i64_lshift( i64_and(shelter, BitBoard.maskAToGFiles), 1) );
                    shelter = i64_rshift( i64_or( shelter, sh2 ), 8 );
                    safety += 3 * i64_bitcount( i64_and(bPawns, shelter) );
                    safety -= ( i64_bitcount( i64_and(wPawns, i64_or(shelter,i64_rshift(shelter,8)))) << 1);
                    shelter = i64_rshift( shelter, 8 );
                    safety += 2 * i64_bitcount( i64_and(bPawns, shelter) );
                    shelter = i64_rshift( shelter, 8 );
                    safety -= i64_bitcount( i64_and(wPawns, shelter) );

                    /*long*/ wOpen = i64_and( southFill(shelter),
                        i64_and( i64_not(southFill(wPawns)), o_xFF ) );
                    if ( i64_not0(wOpen) ) {
                        halfOpenFiles += 25 * i64_bitcount( i64_and(wOpen, o_xE7) );
                        halfOpenFiles += 10 * i64_bitcount( i64_and(wOpen, o_x18) );
                    }
                    /*long*/ bOpen = i64_and( southFill(shelter),
                        i64_and( i64_not(southFill(bPawns)), o_xFF ) );
                    if ( i64_not0(bOpen) ) {
                        halfOpenFiles += 25 * i64_bitcount( i64_and(bOpen, o_xE7) );
                        halfOpenFiles += 10 * i64_bitcount( i64_and(bOpen, o_x18) );
                    }
                    safety = Math.min(safety, 8);
                }
                /*int*/ kSafety = ((safety - 9) * 15) - halfOpenFiles;
                score -= kSafety;
            }
            ksh.key = key;
            ksh.score = score;
        }
        return ksh.score;
    },

    /** Implements special knowledge for some endgame situations. */
    /*private*/ /*int*/ endGameEval:function( /*Position*/ pos, /*int*/ oldScore) {
        var /*int*/ score = oldScore;
        if (pos.wMtrl + pos.bMtrl > 6 * this.rV) return score;
        var /*int*/ wMtrlPawns = pos.wMtrlPawns;
        var /*int*/ bMtrlPawns = pos.bMtrlPawns;
        var /*int*/ wMtrlNoPawns = pos.wMtrl - wMtrlPawns;
        var /*int*/ bMtrlNoPawns = pos.bMtrl - bMtrlPawns;

        var /*boolean*/  handled = false;
        if ((wMtrlPawns + bMtrlPawns == 0) && (wMtrlNoPawns < this.rV) && (bMtrlNoPawns < this.rV)) {
            // King + minor piece vs king + minor piece is a draw
            return 0;
        }
        if (!handled && (pos.wMtrl == this.qV) && (pos.bMtrl == this.pV) &&
            (i64_bitcount(pos.pieceTypeBB[Piece.WQUEEN]) == 1)) {
            var /*int*/ wk = numberOfTrailingZeros(pos.pieceTypeBB[Piece.WKING]);
            var /*int*/ wq = numberOfTrailingZeros(pos.pieceTypeBB[Piece.WQUEEN]);
            var /*int*/ bk = numberOfTrailingZeros(pos.pieceTypeBB[Piece.BKING]);
            var /*int*/ bp = numberOfTrailingZeros(pos.pieceTypeBB[Piece.BPAWN]);
            score = this.evalKQKP(wk, wq, bk, bp);
            handled = true;
        }
        if (!handled && (pos.bMtrl == this.qV) && (pos.wMtrl == this.pV) && 
            (i64_bitcount(pos.pieceTypeBB[Piece.BQUEEN]) == 1)) {
            var /*int*/ bk = numberOfTrailingZeros(pos.pieceTypeBB[Piece.BKING]);
            var /*int*/ bq = numberOfTrailingZeros(pos.pieceTypeBB[Piece.BQUEEN]);
            var /*int*/ wk = numberOfTrailingZeros(pos.pieceTypeBB[Piece.WKING]);
            var /*int*/ wp = numberOfTrailingZeros(pos.pieceTypeBB[Piece.WPAWN]);
            score = -this.evalKQKP(63-bk, 63-bq, 63-wk, 63-wp);
            handled = true;
        }
        if (!handled && (score > 0)) {
            if ((wMtrlPawns == 0) && (wMtrlNoPawns <= bMtrlNoPawns + this.bV)) {
                if (wMtrlNoPawns < this.rV) {
                    return -pos.bMtrl / 50;
                } else {
                    score /= 8;         // Too little excess material, probably draw
                    handled = true;
                }
            } else if ( i64_not0( i64_or(pos.pieceTypeBB[Piece.WROOK], i64_or( pos.pieceTypeBB[Piece.WKNIGHT],
                        pos.pieceTypeBB[Piece.WQUEEN])) ) ) {
                // Check for rook pawn + wrong color bishop
                if ( i64_is0( i64_and(pos.pieceTypeBB[Piece.WPAWN], BitBoard.maskBToHFiles)) &&
                    i64_is0( i64_and(pos.pieceTypeBB[Piece.WBISHOP], BitBoard.maskLightSq)) &&
                    i64_not0( i64_and(pos.pieceTypeBB[Piece.BKING], o_x0303 )) ) {
                    return 0;
                } else
                if ( i64_is0( i64_and(pos.pieceTypeBB[Piece.WPAWN], BitBoard.maskAToGFiles)) &&
                    i64_is0( i64_and(pos.pieceTypeBB[Piece.WBISHOP], BitBoard.maskDarkSq)) &&
                    i64_not0( i64_and(pos.pieceTypeBB[Piece.BKING], o_xC0C0 )) ) {
                    return 0;
                }
            }
        }
        if (!handled) {
            if (bMtrlPawns == 0) {
                if (wMtrlNoPawns - bMtrlNoPawns > this.bV) {
                    var /*int*/ wKnights = i64_bitcount(pos.pieceTypeBB[Piece.WKNIGHT]);
                    var /*int*/ wBishops = i64_bitcount(pos.pieceTypeBB[Piece.WBISHOP]);
                    if ((wKnights == 2) && (wMtrlNoPawns == 2 * this.nV) && (bMtrlNoPawns == 0)) {
                        score /= 50;    // KNNK is a draw
                    } else if ((wKnights == 1) && (wBishops == 1) &&
                             (wMtrlNoPawns == this.nV + this.bV) && (bMtrlNoPawns == 0)) {
                        score /= 10;
                        score += this.nV + this.bV + 300;
                        var /*int*/ kSq = pos.getKingSq(false);
                        var /*int*/ x = Position.getX(kSq);
                        var /*int*/ y = Position.getY(kSq);
                        if ( i64_not0( i64_and(pos.pieceTypeBB[Piece.WBISHOP], BitBoard.maskDarkSq))) {
                            score += (7 - this.distToH1A8[7-y][7-x]) * 10;
                        } else {
                            score += (7 - this.distToH1A8[7-y][x]) * 10;
                        }
                    } else {
                        score += 300;       // Enough excess material, should win
                    }
                    handled = true;
                } else if ((wMtrlNoPawns + bMtrlNoPawns == 0) && (wMtrlPawns == this.pV)) { // KPK
                    var /*int*/ wp = numberOfTrailingZeros(pos.pieceTypeBB[Piece.WPAWN]);
                    score = this.kpkEval(pos.getKingSq(true), pos.getKingSq(false),
                                    wp, pos.whiteMove);
                    handled = true;
                }
            }
        }
        if (!handled && (score < 0)) {
            if ((bMtrlPawns == 0) && (bMtrlNoPawns <= wMtrlNoPawns + this.bV)) {
                if (bMtrlNoPawns < this.rV) {
                    return pos.wMtrl / 50;
                } else {
                    score /= 8;         // Too little excess material, probably draw
                    handled = true;
                }
            } else if ( i64_not0( i64_or(pos.pieceTypeBB[Piece.BROOK], i64_or( pos.pieceTypeBB[Piece.BKNIGHT],
                        pos.pieceTypeBB[Piece.BQUEEN])) ) ) {     
                // Check for rook pawn + wrong color bishop
                if ( i64_is0( i64_and(pos.pieceTypeBB[Piece.BPAWN], BitBoard.maskBToHFiles)) &&
                    i64_is0( i64_and(pos.pieceTypeBB[Piece.BBISHOP], BitBoard.maskDarkSq)) &&
                    i64_not0( i64_and(pos.pieceTypeBB[Piece.WKING], o_xLv0303 )) ) {
                    return 0;
                } else
                if ( i64_is0( i64_and(pos.pieceTypeBB[Piece.BPAWN], BitBoard.maskAToGFiles)) &&
                    i64_is0( i64_and(pos.pieceTypeBB[Piece.BBISHOP], BitBoard.maskLightSq)) &&
                    i64_not0( i64_and(pos.pieceTypeBB[Piece.WKING], o_xLvC0C0 )) ) {
                    return 0;
                }
            }
        }
        if (!handled) {
            if (wMtrlPawns == 0) {
                if (bMtrlNoPawns - wMtrlNoPawns > this.bV) {
                    var /*int*/ bKnights = i64_bitcount(pos.pieceTypeBB[Piece.BKNIGHT]);
                    var /*int*/ bBishops = i64_bitcount(pos.pieceTypeBB[Piece.BBISHOP]);
                    if ((bKnights == 2) && (bMtrlNoPawns == 2 * this.nV) && (wMtrlNoPawns == 0)) {
                        score /= 50;    // KNNK is a draw
                    } else if ((bKnights == 1) && (bBishops == 1) &&
                             (bMtrlNoPawns == this.nV + this.bV) && (wMtrlNoPawns == 0)) {
                        score /= 10;
                        score -= this.nV + this.bV + 300;
                        var /*int*/ kSq = pos.getKingSq(true);
                        var /*int*/ x = Position.getX(kSq);
                        var /*int*/ y = Position.getY(kSq);
                        if ( i64_not0( i64_and(pos.pieceTypeBB[Piece.BBISHOP], BitBoard.maskDarkSq))) {
                            score -= (7 - this.distToH1A8[7-y][7-x]) * 10;
                        } else {
                            score -= (7 - this.distToH1A8[7-y][x]) * 10;
                        }
                    } else {
                        score -= 300;       // Enough excess material, should win
                    }
                    handled = true;
                } else if ((wMtrlNoPawns + bMtrlNoPawns == 0) && (bMtrlPawns == this.pV)) { // KPK
                    var /*int*/ bp = numberOfTrailingZeros(pos.pieceTypeBB[Piece.BPAWN]);
                    score = -this.kpkEval(63-pos.getKingSq(false), 63-pos.getKingSq(true),
                                     63-bp, !pos.whiteMove);
                    handled = true;
                }
            }
        }
        return score;
    },

    /*private*/ /*int*/ evalKQKP: function(/*int*/ wKing, /*int*/ wQueen, /*int*/ bKing, /*int*/ bPawn) {
        var /*boolean*/  canWin = false;
        
        if ( i64_is0( i64_and( i64bitObj(bKing), o_xFFFF) ) ) {
            canWin = true; // King doesn't support pawn
        } else if (Math.abs(Position.getX(bPawn) - Position.getX(bKing)) > 2) {
            canWin = true; // King doesn't support pawn
        } else {
            var bi1 = i64bitObj(wKing);
            switch (bPawn) {
            case 8:  // a2
                canWin = i64_not0( i64_and(bi1,o_xKxZ1) );
                break;
            case 10: // c2
                canWin = i64_not0( i64_and(bi1,o_xKxZ2) );
                break;
            case 13: // f2
                canWin = i64_not0( i64_and(bi1,o_xKxZ3) );
                break;
            case 15: // h2
                canWin = i64_not0( i64_and(bi1,o_xKxZ4) );
                break;
            default:
                canWin = true;
                break;
            }
        }

        var /*int*/ dist = Math.max(Math.abs(Position.getX(wKing)-Position.getX(bPawn)),
                                  Math.abs(Position.getY(wKing)-Position.getY(bPawn)));
        var /*int*/ score = this.qV - this.pV - (20 * dist);
        if (!canWin)
            score /= 50;
        return score;
    },

    /*private*/ /*int*/ kpkEval: function(/*int*/ wKing, /*int*/ bKing, /*int*/ wPawn, /*boolean*/  whiteMove) {
        if (Position.getX(wKing) >= 4) { // Mirror X
            wKing ^= 7;
            bKing ^= 7;
            wPawn ^= 7;
        }
        var /*int*/ ix = (whiteMove ? 0 : 1);
        ix = (ix<<5) + (Position.getY(wKing)<<2)+Position.getX(wKing);
        ix = (ix<<6) + bKing;
        ix = (ix*48) + wPawn - 8;

        var /*int*/ bytePos = ix>>>3;
        var /*int*/ bitPos = ix % 8;
        var /*boolean*/  draw = false;
	if(bytePos<Evaluate.kpkTable.length)
		{
		 draw = ( ((/*(int)*/Evaluate.kpkTable[bytePos]) & (1 << bitPos)) == 0 );
		}
        if (draw) return 0;
        return (this.qV - ((this.pV>>2) * (7-Position.getY(wPawn))));
    },

    /**
     * Interpolate between (x1,y1) and (x2,y2).
     * If x < x1, return y1, if x > x2 return y2. Otherwise, use linear interpolation.
     */
    /*int*/ interpolate: function(/*int*/ x, /*int*/ x1, /*int*/ y1, /*int*/ x2, /*int*/ y2) {
        if (x > x2) return y2;
        else if (x < x1) return y1;
        else return (x - x1) * (y2 - y1) / (x2 - x1) + y1;
    }
};

Evaluate.initAll();


/*===================================== History */

/**
 * Implements the relative history heuristic.
 */
/*public*/ /*class*/ History = {
    /*private*/ /*int*/ countSuccess:[],
    /*private*/ /*int*/ countFail:[],
    /*private*/ /*int*/ score:[],

    /*public*/ clone: function () { return new clone(this) },

    /*public*/ creaHistory: function() {
        for (var p = 0; p < Piece.nPieceTypes; p++) {
            this.countSuccess[p] = [];
            this.countFail[p] = [];
            this.score[p] = [];
            for (var sq = 0; sq < 64; sq++) {
                this.countSuccess[p][sq] = 0;
                this.countFail[p][sq] = 0;
                this.score[p][sq] = -1;
            }
        }
    },

    /** Record move as a success. */
    /*public*/ addSuccess: function(/*Position*/ pos, /*Move*/ m, /*int*/ depth) {
        var /*int*/ p = pos.getPiece(m.from);
        var /*int*/ cnt = depth;
        var /*int*/ val = this.countSuccess[p][m.to] + cnt;
        if (val > 1000) { val /= 2; this.countFail[p][m.to] /= 2; }
        this.countSuccess[p][m.to] = val;
        this.score[p][m.to] = -1;
    },

    /** Record move as a failure. */
    /*public*/ addFail: function(/*Position*/ pos, /*Move*/ m, /*int*/ depth) {
        var /*int*/ p = pos.getPiece(m.from);
        var /*int*/ cnt = depth;
        this.countFail[p][m.to] += cnt;
        this.score[p][m.to] = -1;
    },

    /** Get a score between 0 and 49, depending of the success/fail ratio of the move. */
    /*public*/ /*int*/ getHistScore: function(/*Position*/ pos, /*Move*/ m) {
        var /*int*/ p = pos.getPiece(m.from);
        var /*int*/ ret = this.score[p][m.to];
        if (ret >= 0) return ret;
        var /*int*/ succ = this.countSuccess[p][m.to];
        var /*int*/ fail = this.countFail[p][m.to];
        if (succ + fail > 0) {
            ret = succ * 49 / (succ + fail);
        } else {
            ret = 0;
        }
        this.score[p][m.to] = ret;
        return ret;
    }
};

History.creaHistory();

/*===================================== Game */


/*public*/ /*enum*/ GameState = {
        ALIVE:0,
        WHITE_MATE:1,         // White mates
        BLACK_MATE:2,         // Black mates
        WHITE_STALEMATE:3,    // White is stalemated
        BLACK_STALEMATE:4,    // Black is stalemated
        DRAW_REP:5,           // Draw by 3-fold repetition
        DRAW_50:6,            // Draw by 50 move rule
        DRAW_NO_MATE:7,       // Draw by impossibility of check mate
        DRAW_AGREE:8,         // Draw by agreement
        RESIGN_WHITE:9,       // White resigns
        RESIGN_BLACK:10       // Black resigns
};


/*public*/ /*class*/ Game = {
    /*protected*/ /*List<Move>*/ moveList:[],
    /*protected*/ /*List<UndoInfo>*/ uiInfoList:[],
    hist:[],		// history of positions for computer search 
    /*List<bool>*/ drawOfferList:[],
    /*protected*/ /*int*/ currentMove:0,
    /*bool*/ pendingDrawOffer:false,
    /*GameState*/ drawState:0,
    /*String*/ drawStateMoveStr: "",     // Move required to claim DRAW_REP or DRAW_50
    /*GameState*/ resignState:0,
    /*public*/ /*Position*/ pos: null,
    /*protected*/ /*Player*/ whitePlayer: null,
    /*protected*/ /*Player*/ blackPlayer: null,
    
    /*public*/ CreaGame: function(/*Player*/ whitePlayer, /*Player*/ blackPlayer) {
        this.whitePlayer = whitePlayer;
        this.blackPlayer = blackPlayer;
        this.handleCommand("new");
    },

    /**
     * Update the game state according to move/command string from a player.
     * @param str The move or command to process.
     * @return True if str was understood, false otherwise.
     */
    /*public*/ /*bool*/ processString: function(/*String*/ str) {
		
		// jan.2024 bugfix, try reduce memory usage
		this.whitePlayer.clearTT();
		this.blackPlayer.clearTT()
				
        if (this.handleCommand(str)) return true;
        if (this.getGameState() != GameState.ALIVE) return false;

        var /*Move*/ m = TextIO.stringToMove(this.pos, str);
        if (m == null) return false;

        var /*UndoInfo*/ ui = new UndoInfo();
	var pos_pre = this.pos.clone();
        this.pos.makeMove(m, ui);
        TextIO.fixupEPSquare(this.pos);
        while (this.currentMove < this.moveList.length) {
            this.moveList.pop();
	    this.hist.pop();
            this.uiInfoList.pop();
            this.drawOfferList.pop();
        }
        this.moveList.push(m);
	this.hist.push(pos_pre);
        this.uiInfoList.push(ui);
        this.drawOfferList.push(this.pendingDrawOffer);
        this.pendingDrawOffer = false;
        this.currentMove++;
        return true;
    },

    /*public*/ /*String*/ getGameStateString: function() {
        var dst = this.drawStateMoveStr;
        switch (this.getGameState()) {
            case GameState.ALIVE:
                return "";
            case GameState.WHITE_MATE:
                return "Game over, white mates!";
            case GameState.BLACK_MATE:
                return "Game over, black mates!";
            case GameState.WHITE_STALEMATE:
            case GameState.BLACK_STALEMATE:
                return "Game over, draw by stalemate!";
            case GameState.DRAW_REP:
            {
                var /*String*/ ret = "Game over, draw by repetition!";
                if ((dst != null) && (dst.length > 0)) {
                    ret = ret + " [" + dst + "]";
                }
                return ret;
            }
            case GameState.DRAW_50:
            {
                var /*String*/ ret = "Game over, draw by 50 move rule!";
                if ((dst != null) && (dst.length > 0)) {
                    ret = ret + " [" + dst + "]";  
                }
                return ret;
            }
            case GameState.DRAW_NO_MATE:
                return "Game over, draw by impossibility of mate!";
            case GameState.DRAW_AGREE:
                return "Game over, draw by agreement!";
            case GameState.RESIGN_WHITE:
                return "Game over, white resigns!";
            case GameState.RESIGN_BLACK:
                return "Game over, black resigns!";
            default:
                /*throw new*/ RuntimeException("Gamestate exception");
        }
    },

    /**
     * Get the last played move, or null if no moves played yet.
     */
    /*public*/ /*Move*/ getLastMove: function() {
        var /*Move*/ m = null;
        if (this.currentMove > 0) {
            m = this.moveList[ this.currentMove - 1 ];
        }
        return m;
    },

    /**
     * Get the current state of the game.
     */
    /*public*/ /*GameState*/ getGameState: function() {
        var /*MoveGen.MoveList*/ moves = MoveGen.pseudoLegalMoves(this.pos);
        MoveGen.removeIllegal(this.pos, moves);
        if (moves.size == 0) {
            if (MoveGen.inCheck(this.pos)) {
                return this.pos.whiteMove ? GameState.BLACK_MATE : GameState.WHITE_MATE;
            } else {
                return this.pos.whiteMove ? GameState.WHITE_STALEMATE : GameState.BLACK_STALEMATE;
            }
        }
        if (this.insufficientMaterial()) {
            return GameState.DRAW_NO_MATE;
        }
        if (this.resignState != GameState.ALIVE) {
            return this.resignState;
        }
        return this.drawState;
    },

    /**
     * Check if a draw offer is available.
     * @return True if the current player has the option to accept a draw offer.
     */
    /*public*/ /*bool*/ haveDrawOffer: function() {
        if (this.currentMove > 0) {
            return this.drawOfferList[this.currentMove - 1];
        } else {
            return false;
        }
    },
    
    /**
     * Handle a special command.
     * @param moveStr  The command to handle
     * @return  True if command handled, false otherwise.
     */
    /*protected*/ /*bool*/ handleCommand: function(/*String*/ moveStr) {
        if (moveStr == "help") {
         printf("Valid commands:\n");
         printf("new, undo, redo, swap, go, list, setpos, getpos, resign, time n, book on/off, perft n \n");       
         return true;
        } else if (moveStr == "new") {
            this.moveList = [];
            this.hist = [];
            this.uiInfoList = [];
            this.drawOfferList = [];
            this.currentMove = 0;
            this.pendingDrawOffer = false;
            this.drawState = GameState.ALIVE;
            this.resignState = GameState.ALIVE;
            this.pos = TextIO.readFEN(TextIO.startPosFEN);
            this.whitePlayer.clearTT();
            this.blackPlayer.clearTT();
            this.activateHumanPlayer();
            
            // clear cache
            Evaluate.pawnHash = [];
            Evaluate.kingSafetyHash = [];
            return true;
        } else if (moveStr=="undo") {
            if (this.currentMove > 0) {
                this.pos.unMakeMove(this.moveList[this.currentMove - 1], this.uiInfoList[this.currentMove - 1]);
                this.currentMove--;
                this.pendingDrawOffer = false;
                this.drawState = GameState.ALIVE;
                this.resignState = GameState.ALIVE;
                return this.handleCommand("swap");
            } else {
                printf("Nothing to undo\n");
            }
            return true;
        } else if (moveStr=="redo") {
            if (this.currentMove < this.moveList.length) {
                this.pos.makeMove(this.moveList[this.currentMove], this.uiInfoList[this.currentMove]);
                this.currentMove++;
                this.pendingDrawOffer = false;
                return this.handleCommand("swap");
            } else {
                printf("Nothing to undo\n");
            }
            return true;
        } else if (moveStr=="swap" || moveStr=="go") {
            var /*Player*/ tmp = this.whitePlayer;
            this.whitePlayer = this.blackPlayer;
            this.blackPlayer = tmp;
            return true;
        } else if (moveStr=="list") {
            this.listMoves();
            return true;
        } else if ( startsWith(moveStr, "setpos ")) {
            var /*String*/ fen = moveStr.substr(moveStr.indexOf(" ") + 1);
            var /*Position*/ newPos = null;
            try {
                newPos = TextIO.readFEN(fen);
            } catch (ex) {
                printf("Invalid FEN: " + fen + "\n");
            }
            if (newPos != null) {
                this.handleCommand("new");
                this.pos = newPos;
                this.activateHumanPlayer();
            }
            return true;
        } else if (moveStr=="getpos") {
            var /*String*/ fen = TextIO.toFEN(this.pos);
            printf( fen + "\n");
            return true;
        } else if (startsWith(moveStr,"draw ")) {
            if (this.getGameState() == GameState.ALIVE) {
                var /*String*/ drawCmd = moveStr.substr(moveStr.indexOf(" ") + 1);
                return this.handleDrawCmd(drawCmd);
            } else {
                return true;
            }
        } else if (moveStr=="resign") {
            if (this.getGameState()== GameState.ALIVE) {
                this.resignState = this.pos.whiteMove ? GameState.RESIGN_WHITE : GameState.RESIGN_BLACK;
                return true;
            } else {
                return true;
            }
        } else if (startsWith(moveStr,"book")) {
            var /*String*/ bookCmd = moveStr.substr(moveStr.indexOf(" ") + 1);
            return this.handleBookCmd(bookCmd);
        } else if (startsWith(moveStr,"time")) {
            try {
                var /*String*/ timeStr = moveStr.substr(moveStr.indexOf(" ") + 1);
                var /*int*/ timeLimit = parseInt(timeStr);
                this.whitePlayer.timeLimit(timeLimit, timeLimit, false);
                this.blackPlayer.timeLimit(timeLimit, timeLimit, false);
                return true;
            }
            catch (nfe) {
                printf("Number format exception\n");
                return false;
            }
        } else if (startsWith(moveStr, "perft ")) {
            try {;		
                var /*String*/ depthStr = moveStr.substr(moveStr.indexOf(" ") + 1);
                var /*int*/ depth = parseInt(depthStr);
                var /*long*/ t0 = rtime();
                var /*long*/ nodes = this.perfT(this.pos, depth);
                var /*long*/ t1 = rtime();
                printf("perft(%d)=", depth);
                printf("%d nodes, ", nodes );
                printf("time %d sec\n", (t1 - t0)/1000 );
            }
            catch (nfe) {
                printf("Number format exception\n");
                return false;
            }
            return true;
        } else {
            return false;
        }
    },

    /** Swap players around if needed to make the human player in control of the next move. */
    /*protected*/ activateHumanPlayer: function () {
        if (!(this.pos.whiteMove ? this.whitePlayer : this.blackPlayer).isHumanPlayer()) {
            var /*Player*/ tmp = this.whitePlayer;
            this.whitePlayer = this.blackPlayer;
            this.blackPlayer = tmp;
        }
    },

    /**
     * Print a list of all moves.
     */
    /*private*/ listMoves: function() {
        var /*String*/ movesStr = this.getMoveListString();
        printf( movesStr + "\n");
    },

    /*public*/ /*String*/ getMoveListString: function() {
        var ret = "";

        // Undo all moves in move history.
        var /*Position*/ pos = this.pos.clone();
        for (var i = this.currentMove; i > 0; i--) {
            pos.unMakeMove(this.moveList[i - 1], this.uiInfoList[i - 1]);
        }

        // Print all moves
        var /*String*/ whiteMove = "";
        var /*String*/ blackMove = "";
        for (i = 0; i < this.currentMove; i++) {
            var /*Move*/ move = this.moveList[i];
            var /*String*/ strMove = TextIO.moveToString(pos, move, false);
            if (this.drawOfferList[i]) {
                strMove += " (d)";
            }
            if (pos.whiteMove) {
                whiteMove = strMove;
            } else {
                blackMove = strMove;
                if (whiteMove.length == 0) {
                    whiteMove = "...";
                }
                ret+=(pos.fullMoveCounter.toString() + ". " + whiteMove + " " + blackMove + " ");
                whiteMove = "";
                blackMove = "";
            }
            var /*UndoInfo*/ ui = new UndoInfo();
            pos.makeMove(move, ui);
        }
        if ((whiteMove.length > 0) || (blackMove.length > 0)) {
            if (whiteMove.length == 0) {
                whiteMove = "...";
            }
            ret+=(pos.fullMoveCounter.toString() + ". " + whiteMove + " " + blackMove + " ");
        }
        var /*String*/ gameResult = this.getPGNResultString();
        if (!(gameResult=="*")) ret+=gameResult;
        return ret;
    },
    
    /*public*/ /*String*/ getPGNResultString: function() {
        var /*String*/ gameResult = "*";
        switch (this.getGameState()) {
            case GameState.ALIVE:
                break;
            case GameState.WHITE_MATE:
            case GameState.RESIGN_BLACK:
                gameResult = "1-0";
                break;
            case GameState.BLACK_MATE:
            case GameState.RESIGN_WHITE:
                gameResult = "0-1";
                break;
            case GameState.WHITE_STALEMATE:
            case GameState.BLACK_STALEMATE:
            case GameState.DRAW_REP:
            case GameState.DRAW_50:
            case GameState.DRAW_NO_MATE:
            case GameState.DRAW_AGREE:
                gameResult = "1/2-1/2";
                break;
        }
        return gameResult;
    },

    /** Return a list of previous positions in this game, back to the last "zeroing" move. */
    /*public*/ /*ArrayList<Position>*/ getHistory: function() {
        var /*ArrayList<Position>*/ posList = [];
        var /*Position*/ pos = this.pos.clone();
        for (var i = this.currentMove; i > 0; i--) {
            if (pos.halfMoveClock == 0) break;
            pos.unMakeMove(this.moveList[i- 1], this.uiInfoList[i- 1]);
            posList.push(pos);
        }
        return posList.reverse();
    },

    /*private*/ /*bool*/ handleDrawCmd: function(/*String*/ drawCmd) {
        if ( startsWith(drawCmd,"rep") || startsWith(drawCmd, "50")) {
            var /*bool*/ rep = startsWith(drawCmd, "rep");
            var /*Move*/ m = null;
            var /*String*/ ms = drawCmd.substr(drawCmd.indexOf(" ") + 1);
	    if (startsWith(ms, "50 ")) ms = ms.substr(3);
		
            if (ms.length > 0) {
                m = TextIO.stringToMove(this.pos, ms);
            }
            var /*bool*/ valid = false;
            if (rep) {
                var /*List<Position>*/ oldPositions = [];
                if (m != null) {
                    var /*UndoInfo*/ ui = new UndoInfo();
                    var /*Position*/ tmpPos = this.pos.clone();
                    tmpPos.makeMove(m, ui);
                    oldPositions.push(tmpPos);
                }
                oldPositions.push(this.pos);
                var /*Position*/ tmpPos = this.pos;
                for (var i = this.currentMove - 1; i >= 0; i--) {
                    tmpPos = tmpPos.clone();
                    tmpPos.unMakeMove(this.moveList[i], this.uiInfoList[i]);
                    oldPositions.push(tmpPos);
                }
                var /*int*/ repetitions = 0;
                var /*Position*/ firstPos = oldPositions[0];
                for (var j in oldPositions) {
                    if (oldPositions[j].drawRuleEquals(firstPos))
                        repetitions++;
                }
                if (repetitions >= 3) {
                    valid = true;
                }
            } else {
                var /*Position*/ tmpPos = this.pos.clone();
                if (m != null) {
                    var /*UndoInfo*/ ui = new UndoInfo();
                    tmpPos.makeMove(m, ui);
                }
                valid = ( tmpPos.halfMoveClock >= 100 );
            }
            if (valid) {
                this.drawState = ( rep ? GameState.DRAW_REP : GameState.DRAW_50 );
                this.drawStateMoveStr = null;
                if (m != null) {
                    this.drawStateMoveStr = TextIO.moveToString(this.pos, m, false);
                }
            } else {
                this.pendingDrawOffer = true;
                if (m != null) {
                    this.processString(ms);
                }
            }
            return true;
        } else if (startsWith(drawCmd,"offer ")) {
            this.pendingDrawOffer = true;
            var /*String*/ ms = drawCmd.substr(drawCmd.indexOf(" ") + 1);
            if (TextIO.stringToMove(this.pos, ms) != null) {
                processString(ms);
            }
            return true;
        } else if (drawCmd == "accept") {
            if (this.haveDrawOffer()) {
                this.drawState = GameState.DRAW_AGREE;
            }
            return true;
        } else {
            return false;
        }
    },

    /*private*/ /*bool*/ handleBookCmd: function(/*String*/ bookCmd) {
        if (bookCmd == "off") {
            this.whitePlayer.useBook(false);
            this.blackPlayer.useBook(false);
            return true;
        } else if (bookCmd == "on") {
            this.whitePlayer.useBook(true);
            this.whitePlayer.useBook(true);
            return true;
        }
        return false;
    },

    /*private*/ /*bool*/ insufficientMaterial: function() {
        if (this.pos.nPieces(Piece.WQUEEN) > 0) return false;
        if (this.pos.nPieces(Piece.WROOK)  > 0) return false;
        if (this.pos.nPieces(Piece.WPAWN)  > 0) return false;
        if (this.pos.nPieces(Piece.BQUEEN) > 0) return false;
        if (this.pos.nPieces(Piece.BROOK)  > 0) return false;
        if (this.pos.nPieces(Piece.BPAWN)  > 0) return false;
        var /*int*/ wb = this.pos.nPieces(Piece.WBISHOP);
        var /*int*/ wn = this.pos.nPieces(Piece.WKNIGHT);
        var /*int*/ bb = this.pos.nPieces(Piece.BBISHOP);
        var /*int*/ bn = this.pos.nPieces(Piece.BKNIGHT);
        if (wb + wn + bb + bn <= 1) {
            return true;    // King + bishop/knight vs king is draw
        }
        if (wn + bn == 0) {
            // Only bishops. If they are all on the same color, the position is a draw.
            var /*bool*/ bSquare = false;
            var /*bool*/ wSquare = false;
            for (var /*int*/ x = 0; x < 8; x++) {
                for (var /*int*/ y = 0; y < 8; y++) {
                    var /*int*/ p = this.pos.getPiece(Position.getSquare(x, y));
                    if ((p == Piece.BBISHOP) || (p == Piece.WBISHOP)) {
                        if (Position.darkSquare(x, y)) {
                            bSquare = true;
                        } else {
                            wSquare = true;
                        }
                    }
                }
            }
            if (!bSquare || !wSquare) {
                return true;
            }
        }

        return false;
    },

    /*long*/ perfT: function(/*Position*/ pos, /*int*/ depth) {
        if (depth == 0) return 1;
        var /*long*/ nodes = 0;
        var /*MoveGen.MoveList*/ moves = MoveGen.pseudoLegalMoves(pos);
        MoveGen.removeIllegal(pos, moves);
        if (depth == 1) {
            var /*int*/ ret = moves.size;
            MoveGen.returnMoveList(moves);
            return ret;
        }
        var /*UndoInfo*/ ui = new UndoInfo();
        for (var /*int*/ mi = 0; mi < moves.size; mi++) {
            var /*Move*/ m = moves.m[mi];
            pos.makeMove(m, ui);
            nodes += this.perfT( pos, depth - 1 );
            pos.unMakeMove(m, ui);
        }
        MoveGen.returnMoveList(moves);
        return nodes;
    }
};


/*=====================================  HumanPlayer */

/**
 * A player that reads input from the keyboard.
 */
/*public*/ /*class*/ HumanPlayer /*implements Player*/ = {

    /*public*/ clone: function () { return new clone(this) },

    /*@Override*/
    /*public*/ /*String*/ getCommand: function(/*Position*/ pos, /*boolean*/  drawOffer )
      { return ""; /* just do nothing */ },
    
    /*@Override*/
    /*public*/ /*boolean*/  isHumanPlayer: function() { return true; },
    
    /*@Override*/
    /*public*/ useBook: function(/*boolean*/  bookOn) { },

    /*@Override*/
    /*public*/ timeLimit: function(/*int*/ minTimeLimit, /*int*/ maxTimeLimit, /*boolean*/  randomMode) { },

    /*@Override*/
    /*public*/ clearTT: function() { }
};


/*=====================================  ComputerPlayer */

/**
 * A computer algorithm player.
 */
/*public*/ /*class*/ ComputerPlayer /*implements Player*/ = {

    /*public*/ clone: function () { return new clone(this) },

    /*public*/ /*String*/ engineName: "CuckooChess 1.11 Javascript port",

    /*int*/ minTimeMillis: 8 * 1000,	/* 8sec */
    /*int*/ maxTimeMillis: 10 * 1000,	/* 10sec */
    /*int*/ maxDepth: (CHROME ? 3 : 2),
    
    /*int*/ maxNodes: 100000,
    /*public*/ /*boolean*/  verbose:true,
    /*TranspositionTable*/ tt: null,
    /*boolean*/  bookEnabled:true,
    /*boolean*/  randomMode:false,
    /*Search*/ currentSearch: null,
   
    Init: function() {
        this.tt = TranspositionTable.clone();
    },
   
    /*@Override*/
    /*public*/ /*String*/ getCommand: function(/*Position*/ pos, /*boolean*/  drawOffer ) {
        // Create a search object
        var /*long[]*/ posHashList = [];    /*long[200 + histo.length]*/
        var /*int*/ posHashListSize = 0;
        for( var j in Game.hist ) {
            posHashList[posHashListSize++] = Game.hist[j].zobristHash();
        }
        this.tt.nextGeneration();
        var /*Search*/ sc = Search.clone();
        sc.Search(pos, posHashList, posHashListSize, this.tt);

        // Determine all legal moves
        var /*MoveGen.MoveList*/ moves = MoveGen.pseudoLegalMoves(pos);
        MoveGen.removeIllegal(pos, moves);
        sc.scoreMoveList(moves, 0);

        // Test for "game over"
        if (moves.size == 0) {
            // Switch sides so that the human can decide what to do next.
            return "swap";
        }

        if (this.bookEnabled) {
            var /*Move*/ bookMove = Book.getBookMove(pos);
            if (bookMove != null) {
                printf("Book moves: " + Book.getAllBookMoves(pos) + "\n");
                return TextIO.moveToUCIString(bookMove);
            }
        }
        
        // Find best move using iterative deepening
        this.currentSearch = sc;
        var /*Move*/ bestM;
        if ((moves.size == 1) && (this.canClaimDraw(pos, posHashList, posHashListSize, moves.m[0]).length == 0)) {
            bestM = moves.m[0];
            bestM.score = 0;
        } else if (this.randomMode) {
            bestM = this.findSemiRandomMove(sc, moves);
        } else {
            sc.timeLimit(this.minTimeMillis, this.maxTimeMillis);
            bestM = sc.iterativeDeepening(moves, this.maxDepth, this.maxNodes, this.verbose);
        }
        this.currentSearch = null;
        this.tt.printStats();
        var /*String*/ strMove = TextIO.moveToUCIString(bestM);
	printf(strMove + "\n");

        // Claim draw if appropriate
        if (bestM.score <= 0) {
            var /*String*/ drawClaim = this.canClaimDraw(pos, posHashList, posHashListSize, bestM);
            if (drawClaim.length>0)
                {
                 strMove = drawClaim;
                 printf(strMove + "\n");
                 DrawByEngine = true;
                }
        }
        return strMove;
    },
    
    /** Check if a draw claim is allowed, possibly after playing "move".
     * @param move The move that may have to be made before claiming draw.
     * @return The draw string that claims the draw, or empty string if draw claim not valid.
     */
    /*private*/ /*String*/ canClaimDraw: function(/*Position*/ pos, /*long[]*/ posHashList,
                 /*int*/ posHashListSize, /*Move*/ move) {
        var /*String*/ drawStr = "";
        if (Search.canClaimDraw50(pos)) {
            drawStr = "draw 50";
        } else if (Search.canClaimDrawRep(pos, posHashList, posHashListSize, posHashListSize)) {
            drawStr = "draw rep";
        } else {
            var /*String*/ strMove = TextIO.moveToString(pos, move, false);
            posHashList[posHashListSize++] = pos.zobristHash();
            var /*UndoInfo*/ ui = new UndoInfo();
            pos.makeMove(move, ui);
            if (Search.canClaimDraw50(pos)) {
                drawStr = "draw 50 " + strMove;
            } else if (Search.canClaimDrawRep(pos, posHashList, posHashListSize, posHashListSize)) {
                drawStr = "draw rep " + strMove;
            }
            pos.unMakeMove(move, ui);
        }
        return drawStr;
    },

    /*@Override*/
    /*public*/ /*boolean*/  isHumanPlayer: function() {
        return false;
    },

    /*@Override*/
    /*public*/ useBook: function (/*boolean*/  bookOn) {
        this.bookEnabled = bookOn;
    },

    /*@Override*/
    /*public*/ timeLimit: function(/*int*/ minTimeLimit, /*int*/ maxTimeLimit, /*boolean*/  randomMode) {
        if (randomMode) {
            minTimeLimit = 0;
            maxTimeLimit = 0;
        }
        this.minTimeMillis = minTimeLimit;
        this.maxTimeMillis = maxTimeLimit;
        this.randomMode = randomMode;
        if (this.currentSearch != null) {
            this.currentSearch.timeLimit(minTimeLimit, maxTimeLimit);
        }
    },

    /*@Override*/
    /*public*/  clearTT: function() {
        this.tt.clearTable();
    },

    /*private*/ /*Move*/ findSemiRandomMove:function(/*Search*/ sc, /*MoveGen.MoveList*/ moves) {
        sc.timeLimit(this.minTimeMillis, this.maxTimeMillis);
        var /*Move*/ bestM = sc.iterativeDeepening(moves, 1, this.maxNodes, this.verbose);
        var /*int*/ bestScore = bestM.score;

        var /*int*/ sum = 0;
        for (var /*int*/ mi = 0; mi < moves.size; mi++) {
            sum += this.moveProbWeight(moves.m[mi].score, bestScore);
        }
        var /*int*/ rnd = Math.floor(Math.random(1)*sum);
        for (var /*int*/ mi = 0; mi < moves.size; mi++) {
            var /*int*/ weight = this.moveProbWeight(moves.m[mi].score, bestScore);
            if (rnd < weight) {
                return moves.m[mi];
            }
            rnd -= weight;
        }
        printf("Fail on findSemiRandomMove\n");
        return null;
    },

    /*private*/ /*int*/ moveProbWeight: function(/*int*/ moveScore, /*int*/ bestScore) {
        var /*double*/ d = (bestScore - moveScore) / 100.0;
        var /*double*/ w = 100 * Math.exp(-d*d/2);
        return /*(int)*/ Math.ceil(w);
    }

};


/*=====================================  KillerTable */

/**
 * Implement a table of killer moves for the killer heuristic.
 */
 
/*public*/ /*class*/ KillerTable = {

    /*public*/ clone: function () { return new clone(this) },

    /** There is one KTEntry for each ply in the search tree. */
    /*class*/ KTEntry: function() {
        this.move0 = 0;
        this.move1 = 0;
    },

    /*KTEntry[]*/ ktList:[],

    /** Create an empty killer table. */
    /*public*/ GenKillerTable: function( ln ) {
        var ktL = [];    
        for (var i = 0; i < ln; i++) ktL[i] = new this.KTEntry();
        return ktL;
    },
    
    /*public*/ InitAll: function() {
        this.ktList = this.GenKillerTable( 200 );
    },
        
    /** Add a killer move to the table. Moves are replaced on an LRU basis. */
    /*public*/ addKiller: function(/*int*/ ply, /*Move*/ m) {
        if (ply >= this.ktList.length) return;
        var /*int*/ move = (m.from + (m.to << 6) + (m.promoteTo << 12));
        var /*KTEntry*/ ent = this.ktList[ply];
        if (move != ent.move0) {
            ent.move1 = ent.move0;
            ent.move0 = move;
        }
    },

    /**
     * Get a score for move m based on hits in the killer table.
     * The score is 4 for primary   hit at ply.
     * The score is 3 for secondary hit at ply.
     * The score is 2 for primary   hit at ply - 2.
     * The score is 1 for secondary hit at ply - 2.
     * The score is 0 otherwise.
     */
    /*public*/ /*int*/ getKillerScore: function(/*int*/ ply, /*Move*/ m) {
        var /*int*/ move = (m.from + (m.to << 6) + (m.promoteTo << 12));
        var /*KTEntry*/ ent;
        if (ply < this.ktList.length) {
            ent = this.ktList[ply];
            if (move == ent.move0) return 4;
            else if (move == ent.move1) return 3;
        }
        if ((ply - 2 >= 0) && (ply - 2 < this.ktList.length)) {
            ent = this.ktList[ply - 2];
            if (move == ent.move0) return 2;
            else if (move == ent.move1) return 1;
        }
        return 0;
    }
};

KillerTable.InitAll();

/*===================================== Hash */

Hash = {

    /*long[]*/ psHashKeys:[],    // [piece][square]
    /*private*/ /*long*/ whiteHashKey:0,
    /*private*/ /*long[]*/ castleHashKeys:[],  // [castleMask]
    /*private*/ /*long[]*/ epHashKeys:[],      // [epFile + 1] (epFile==-1 for no ep)
    /*private*/ /*long[]*/ moveCntKeys:[],     // [min(halfMoveClock, 100)]
    
    initHash: function()
    {
        var /*int*/ rndNo = 0;
        for (var /*int*/ p = 0; p < Piece.nPieceTypes; p++) {
            this.psHashKeys[p] = [];
            for (var /*int*/ sq = 0; sq < 64; sq++) {
                this.psHashKeys[p][sq] = this.getRandomHashVal(rndNo++);
            }
        }
        this.whiteHashKey = this.getRandomHashVal(rndNo++);
        for (var /*int*/ cm = 0; cm < 16; cm++)
            this.castleHashKeys[cm] = this.getRandomHashVal(rndNo++);
        for (var /*int*/ f = 0; f < 9; f++)
            this.epHashKeys[f] = this.getRandomHashVal(rndNo++);
        for (var /*int*/ mc = 0; mc < 101; mc++)
            this.moveCntKeys[mc] = this.getRandomHashVal(rndNo++);
    },

    /*private*/ /*long*/ getRandomHashVal: function(/*int*/ rndNo) {
        var rr = 0;
        for(var i=0;i<6;i++)
            rr = (rr << 8) | (( Math.floor(Math.random()*256)+rndNo) % 256 );
        return rr;
    }
};

Hash.initHash();    // and initialize it

/*===================================== Position */

/**
 * Stores the state of a chess position.
 * All required state is stored, except for all previous positions
 * since the last capture or pawn move. That state is only needed
 * for three-fold repetition draw detection, and is better stored
 * in a separate hash table.
 * @author petero
 */

/*public*/ /*class*/ Position = {

    /** Bit definitions for the castleMask bit mask. */
    /*public*/ /*int*/ A1_CASTLE: ( 1 << 0 ), /** White e1-c1 castle. */
    /*public*/ /*int*/ H1_CASTLE: ( 1 << 1 ), /** White e1-g1 castle. */
    /*public*/ /*int*/ A8_CASTLE: ( 1 << 2 ), /** Black e8-c8 castle. */
    /*public*/ /*int*/ H8_CASTLE: ( 1 << 3 ), /** Black e8-g8 castle. */
       
    /*public*/ /*int[]*/ squares:e0a.slice(0,64),

    // Bitboards
    /*public*/ /*long[]*/ pieceTypeBB:e0a64.slice(0,Piece.nPieceTypes),
    /*public*/ /*long*/ whiteBB:i64(), blackBB:i64(),
    
    // Piece square table scores
    /*public*/ /*short[]*/ psScore1:e0a.slice(0,Piece.nPieceTypes), psScore2:e0a.slice(0,Piece.nPieceTypes),

    /*public*/ /*boolean*/  whiteMove:true,
    
    /*private*/ /*int*/ castleMask:0,

    /*private*/ /*int*/ epSquare:-1,
    
    /** Number of half-moves since last 50-move reset. */
    /*int*/ halfMoveClock:0,
    
    /** Game move number, starting from 1. */
    /*public*/ /*int*/ fullMoveCounter:1,
    /*private*/ /*long*/ hashKey:0,          // Cached Zobrist hash key, init has been removed
    /*private*/ /*long*/ pHashKey:0,
    /*public*/ /*int*/ wKingSq:-1, bKingSq:-1,   // Cached king positions
    /*public*/ /*int*/ wMtrl:-Evaluate.kV,      // Total value of all white pieces and pawns
    /*public*/ /*int*/ bMtrl:-Evaluate.kV,      // Total value of all black pieces and pawns
    /*public*/ /*int*/ wMtrlPawns:0, // Total value of all white pawns
    /*public*/ /*int*/ bMtrlPawns:0, // Total value of all black pawns

    /*public*/ clone: function () { return new clone(this) },

    /*public*/ copy: function (/*Position*/ other) {
        this.squares = other.squares.slice();
        this.pieceTypeBB = other.pieceTypeBB.slice();
        this.psScore1 = other.psScore1.slice();
        this.psScore2 = other.psScore2.slice();
        this.whiteBB = other.whiteBB;
        this.blackBB = other.blackBB;
        this.whiteMove = other.whiteMove;
        this.castleMask = other.castleMask;
        this.epSquare = other.epSquare;
        this.halfMoveClock = other.halfMoveClock;
        this.fullMoveCounter = other.fullMoveCounter;
        this.hashKey = other.hashKey;
        this.pHashKey = other.pHashKey;
        this.wKingSq = other.wKingSq;
        this.bKingSq = other.bKingSq;
        this.wMtrl = other.wMtrl;
        this.bMtrl = other.bMtrl;
        this.wMtrlPawns = other.wMtrlPawns;
        this.bMtrlPawns = other.bMtrlPawns;
    },
    
    /*@Override*/       
    /*public*/ /*boolean*/  equals: function(/*Object*/ other) {
        //if ((o == null) || (o.get/*class*/() != this.get/*class*/()))
        //    return false;
        if (!this.drawRuleEquals(other))
            return false;
        if (this.halfMoveClock != other.halfMoveClock)
            return false;
        if (this.fullMoveCounter != other.fullMoveCounter)
            return false;
        if (this.hashKey != other.hashKey)
            return false;
        if (this.pHashKey != other.pHashKey)
            return false;
        return true;
    },
    
    /**
     * Return Zobrist hash value for the current position.
     * Everything except the move counters are included in the hash value.
     */
    /*public*/ /*long*/ zobristHash: function() {
        return this.hashKey;
    },
    /*public*/ /*long*/ pawnZobristHash: function() {
        return this.pHashKey;
    },
    /*public*/ /*long*/ kingZobristHash: function() {
        return ( Hash.psHashKeys[Piece.WKING][this.wKingSq] ^ Hash.psHashKeys[Piece.BKING][this.bKingSq] );
    },

    /*public*/ /*long*/ historyHash: function() {
        return (this.halfMoveClock < 80 ?  this.hashKey :
         ( this.hashKey ^ Hash.moveCntKeys[Math.min(this.halfMoveClock, 100)] ) );
    },
    
    /**
     * Decide if two positions are equal in the sense of the draw by repetition rule.
     * @return True if positions are equal, false otherwise.
     */
    /*public*/ /*boolean*/  drawRuleEquals: function(/*Position*/ other) {
        if(this.squares.valueOf().toString() != other.squares.valueOf().toString())
            return false;
        if (this.whiteMove != other.whiteMove)
            return false;
        if (this.castleMask != other.castleMask)
            return false;
        if (this.epSquare != other.epSquare)
            return false;
        return true;
    },

    /*public*/ setWhiteMove: function(/*boolean*/  whiteMove) {
        if (whiteMove != this.whiteMove) {
            this.hashKey ^= Hash.whiteHashKey;
            this.whiteMove = whiteMove;
        }
    },
    
    /** Return index in squares[] vector corresponding to (x,y). */
    /*public*/ /*int*/ getSquare: function(/*int*/ x, /*int*/ y) {
        return (y * 8) + x;
    },
    /** Return x position (file) corresponding to a square. */
    /*public*/ /*int*/ getX: function(/*int*/ square) {
        return square & 7;
    },
    /** Return y position (rank) corresponding to a square. */
    /*public*/ /*int*/ getY: function(/*int*/ square) {
        return square >> 3;
    },
    /** Return true if (x,y) is a dark square. */
    /*public*/ /*boolean*/  darkSquare: function(/*int*/ x, /*int*/ y) {
        return (x & 1) == (y & 1);
    },

    /** Return piece occupying a square. */
    /*public*/ /*int*/ getPiece: function(/*int*/ square) {
        return this.squares[square];
    },

    /** Move a non-pawn piece to an empty square. */
    /*private*/ movePieceNotPawn: function(/*int*/ from, /*int*/ to) {
        var /*int*/ piece = this.squares[from];
        this.hashKey ^= Hash.psHashKeys[piece][from];
        this.hashKey ^= Hash.psHashKeys[piece][to];
        this.hashKey ^= Hash.psHashKeys[Piece.EMPTY][from];
        this.hashKey ^= Hash.psHashKeys[Piece.EMPTY][to];
        
        this.squares[from] = Piece.EMPTY;
        this.squares[to] = piece;

        var /*long*/ sqMaskF = i64bitObj( from );
        var /*long*/ sqMaskT = i64bitObj( to );
        var NsqMaskF = i64_not(sqMaskF);
        var NsqMaskT = i64_not(sqMaskT);
        
        this.pieceTypeBB[piece] = i64_or( i64_and( this.pieceTypeBB[piece], NsqMaskF ), sqMaskT );
         if (Piece.isWhite(piece)) {
            this.whiteBB = i64_or( i64_and( this.whiteBB, NsqMaskF ), sqMaskT );
            if (piece == Piece.WKING)
                this.wKingSq = to;
        } else {
            this.blackBB = i64_or( i64_and( this.blackBB, NsqMaskF ), sqMaskT );
            if (piece == Piece.BKING)
                this.bKingSq = to;
        }

        this.psScore1[piece] += Evaluate.psTab1[piece][to] - Evaluate.psTab1[piece][from];
        this.psScore2[piece] += Evaluate.psTab2[piece][to] - Evaluate.psTab2[piece][from];
    },

    /** Set a square to a piece value. */
    /*public*/ setPiece: function(/*int*/ square, /*int*/ piece) {
        // Update hash key
        var /*int*/ removedPiece = this.squares[square];
        this.hashKey ^= Hash.psHashKeys[removedPiece][square];
        this.hashKey ^= Hash.psHashKeys[piece][square];
        if ((removedPiece == Piece.WPAWN) || (removedPiece == Piece.BPAWN))
            this.pHashKey ^= Hash.psHashKeys[removedPiece][square];
        if ((piece == Piece.WPAWN) || (piece == Piece.BPAWN))
            this.pHashKey ^= Hash.psHashKeys[piece][square];
        
        // Update material balance
        var /*int*/ pVal = Evaluate.pieceValue[removedPiece];
        if (Piece.isWhite(removedPiece)) {
            this.wMtrl -= pVal;
            if (removedPiece == Piece.WPAWN)
                this.wMtrlPawns -= pVal;
        } else {
            this.bMtrl -= pVal;
            if (removedPiece == Piece.BPAWN)
                this.bMtrlPawns -= pVal;
        }
        pVal = Evaluate.pieceValue[piece];
        if (Piece.isWhite(piece)) {
            this.wMtrl += pVal;
            if (piece == Piece.WPAWN)
                this.wMtrlPawns += pVal;
        } else {
            this.bMtrl += pVal;
            if (piece == Piece.BPAWN)
                this.bMtrlPawns += pVal;
        }

        // Update board
        this.squares[square] = piece;

        // Update bitboards
        var /*long*/ sqMask = i64bitObj( square );
        var NsqMask = i64_not(sqMask);
       
        this.pieceTypeBB[removedPiece] = i64_and( this.pieceTypeBB[removedPiece], NsqMask );
        this.pieceTypeBB[piece] = i64_or( this.pieceTypeBB[piece], sqMask );
        
        if (removedPiece != Piece.EMPTY) {
            if (Piece.isWhite(removedPiece))
                this.whiteBB = i64_and( this.whiteBB, NsqMask );
            else
                this.blackBB = i64_and( this.blackBB, NsqMask );
        }
        if (piece != Piece.EMPTY) {
            if (Piece.isWhite(piece))
                this.whiteBB = i64_or( this.whiteBB, sqMask );
            else
                this.blackBB = i64_or( this.blackBB, sqMask );
        }

        // Update king position 
        if (piece == Piece.WKING) {
            this.wKingSq = square;
        } else if (piece == Piece.BKING) {
            this.bKingSq = square;
        }

        // Update piece/square table scores
        this.psScore1[removedPiece] -= Evaluate.psTab1[removedPiece][square];
        this.psScore2[removedPiece] -= Evaluate.psTab2[removedPiece][square];
        this.psScore1[piece]        += Evaluate.psTab1[piece][square];
        this.psScore2[piece]        += Evaluate.psTab2[piece][square];
    },

    /**
     * Set a square to a piece value.
     * Special version that only updates enough of the state for the SEE function to be happy.
     */
    /*public*/ setSEEPiece: function(/*int*/ square, /*int*/ piece) {
        var /*int*/ removedPiece = this.squares[square];

        // Update board
        this.squares[square] = piece;

        // Update bitboards
        var /*long*/ sqMask = i64bitObj( square );
        var NsqMask = i64_not(sqMask);

        this.pieceTypeBB[removedPiece] = i64_and( this.pieceTypeBB[removedPiece], NsqMask );
        this.pieceTypeBB[piece] = i64_or( this.pieceTypeBB[piece], sqMask );

        if (removedPiece != Piece.EMPTY) {
            if (Piece.isWhite(removedPiece))
                this.whiteBB = i64_and( this.whiteBB, NsqMask );
            else
                this.blackBB = i64_and( this.blackBB, NsqMask );
        }
        if (piece != Piece.EMPTY) {
            if (Piece.isWhite(piece))
                this.whiteBB = i64_or( this.whiteBB, sqMask );
            else
                this.blackBB = i64_or( this.blackBB, sqMask );
        }
    },

    /** Return true if white e1c1 castling right has not been lost. */
    /*public*/ /*boolean*/  a1Castle: function() {
        return (this.castleMask & Position.A1_CASTLE) != 0;
    },
    /** Return true if white e1g1 castling right has not been lost. */
    /*public*/ /*boolean*/  h1Castle: function() {
        return (this.castleMask & Position.H1_CASTLE) != 0;
    },
    /** Return true if black e8c8 castling right has not been lost. */
    /*public*/ /*boolean*/  a8Castle: function() {
        return (this.castleMask & Position.A8_CASTLE) != 0;
    },
    /** Return true if black e8g8 castling right has not been lost. */
    /*public*/ /*boolean*/  h8Castle: function() {
        return (this.castleMask & Position.H8_CASTLE) != 0;
    },
    
    /** Bitmask describing castling rights. */
    /*public*/ /*int*/ getCastleMask: function() {
        return this.castleMask;
    },
    /*public*/ setCastleMask: function(/*int*/ castleMask) {
        this.hashKey ^= Hash.castleHashKeys[this.castleMask];
        this.hashKey ^= Hash.castleHashKeys[castleMask];
        this.castleMask = castleMask;
    },

    /** En passant square, or -1 if no ep possible. */
    /*public*/ /*int*/ getEpSquare: function() {
        return this.epSquare;
    },
    /*public*/ setEpSquare: function(/*int*/ epSquare) {
        if (this.epSquare != epSquare) {
            this.hashKey ^= Hash.epHashKeys[(this.epSquare >= 0) ? Position.getX(this.epSquare) + 1 : 0];
            this.hashKey ^= Hash.epHashKeys[(epSquare >= 0) ? Position.getX(epSquare) + 1 : 0];
            this.epSquare = epSquare;
        }
    },

    /*public*/ /*int*/ getKingSq: function(/*boolean*/  whiteMove) {
        return (whiteMove ? this.wKingSq : this.bKingSq);
    },

    /**
     * Count number of pieces of a certain type.
     */
    /*public*/ /*int*/ nPieces: function(/*int*/ pType) {
        return ( this.squares.valueOf().toString().split(pType.toString()).length - 1);
        /*
        var ret = 0;
        for (var sq = 0; sq < 64; sq++) {
            if (this.squares[sq] == pType)
                ret++;
        }
        return ret;
        */
    },

    /** Apply a move to the current position. */
    /*public*/ makeMove: function(/*Move*/ move, /*UndoInfo*/ ui) {
        ui.capturedPiece = this.squares[move.to];
        ui.castleMask = this.castleMask;
        ui.epSquare = this.epSquare;
        ui.halfMoveClock = this.halfMoveClock;
        
        var /*boolean*/  wtm = this.whiteMove;
        
        var /*int*/ p = this.squares[move.from];
        var /*int*/ capP = this.squares[move.to];
        var /*long*/ fromMask = i64bitObj( move.from );

        var /*int*/ prevEpSquare = this.epSquare;
        this.setEpSquare(-1);

        if ((capP != Piece.EMPTY) ||
             (  i64_not0( i64_and( i64_or(this.pieceTypeBB[Piece.WPAWN],
                    this.pieceTypeBB[Piece.BPAWN]), fromMask) ) ) ) {
            this.halfMoveClock = 0;

            // Handle en passant and epSquare
            if (p == Piece.WPAWN) {
                if (move.to - move.from == 2 * 8) {
                    var /*int*/ x = Position.getX(move.to);
                    if (((x > 0) && (this.squares[move.to - 1] == Piece.BPAWN)) ||
                            ((x < 7) && (this.squares[move.to + 1] == Piece.BPAWN))) {
                        this.setEpSquare(move.from + 8);
                    }
                } else if (move.to == prevEpSquare) {
                    this.setPiece(move.to - 8, Piece.EMPTY);
                }
            } else if (p == Piece.BPAWN) {
                if (move.to - move.from == -2 * 8) {
                    var /*int*/ x = Position.getX(move.to);
                    if (((x > 0) && (this.squares[move.to - 1] == Piece.WPAWN)) ||
                            ((x < 7) && (this.squares[move.to + 1] == Piece.WPAWN))) {
                        this.setEpSquare(move.from - 8);
                    }
                } else if (move.to == prevEpSquare) {
                    this.setPiece(move.to + 8, Piece.EMPTY);
                }
            }

            if (  i64_not0( i64_and( i64_or(this.pieceTypeBB[Piece.WKING],
                    this.pieceTypeBB[Piece.BKING]), fromMask) ) ) {
                if (wtm) {
                    this.setCastleMask(this.castleMask & (~Position.A1_CASTLE));
                    this.setCastleMask(this.castleMask & (~Position.H1_CASTLE));
                } else {
                    this.setCastleMask(this.castleMask & (~Position.A8_CASTLE));
                    this.setCastleMask(this.castleMask & (~Position.H8_CASTLE));
                }
            }

            // Perform move
            this.setPiece(move.from, Piece.EMPTY);
            // Handle promotion
            if (move.promoteTo != Piece.EMPTY) {
                this.setPiece(move.to, move.promoteTo);
            } else {
                this.setPiece(move.to, p);
            }
        } else {
            this.halfMoveClock++;

            // Handle castling
            if (  i64_not0( i64_and( i64_or(this.pieceTypeBB[Piece.WKING],
                    this.pieceTypeBB[Piece.BKING]), fromMask) ) ) {
                var /*int*/ k0 = move.from;
                if (move.to == k0 + 2) { // O-O
                    this.movePieceNotPawn(k0 + 3, k0 + 1);
                } else if (move.to == k0 - 2) { // O-O-O
                    this.movePieceNotPawn(k0 - 4, k0 - 1);
                }
                if (wtm) {
                    this.setCastleMask(this.castleMask & (~Position.A1_CASTLE));
                    this.setCastleMask(this.castleMask & (~Position.H1_CASTLE));
                } else {
                    this.setCastleMask(this.castleMask & (~Position.A8_CASTLE));
                    this.setCastleMask(this.castleMask & (~Position.H8_CASTLE));
                }
            }

            // Perform move
            this.movePieceNotPawn(move.from, move.to);
        }
        if (!wtm) {
            this.fullMoveCounter++;
        }

        // Update castling rights when rook moves
        if ( i64_not0( i64_and( BitBoard.maskCorners, fromMask ) ) ) {
            var /*int*/ rook = wtm ? Piece.WROOK : Piece.BROOK;
            if (p == rook)
                this.removeCastleRights(move.from);
        }
        if ( i64_not0( i64_and(BitBoard.maskCorners, i64bitObj(move.to)) ) ) {
            var /*int*/ oRook = wtm ? Piece.BROOK : Piece.WROOK;
            if (capP == oRook)
                this.removeCastleRights(move.to);
        }

        this.hashKey ^= Hash.whiteHashKey;
        this.whiteMove = !wtm;
    },

    /*public*/ unMakeMove: function(/*Move*/ move, /*UndoInfo*/ ui) {
        this.hashKey ^= Hash.whiteHashKey;
        this.whiteMove = !this.whiteMove;
        var /*int*/ p = this.squares[move.to];
        this.setPiece(move.from, p);
        this.setPiece(move.to, ui.capturedPiece);
        this.setCastleMask(ui.castleMask);
        this.setEpSquare(ui.epSquare);
        this.halfMoveClock = ui.halfMoveClock;
        var /*boolean*/  wtm = this.whiteMove;
        if (move.promoteTo != Piece.EMPTY) {
            p = wtm ? Piece.WPAWN : Piece.BPAWN;
            this.setPiece(move.from, p);
        }
        if (!wtm) {
            this.fullMoveCounter--;
        }
        
        // Handle castling
        var /*int*/ king = wtm ? Piece.WKING : Piece.BKING;
        if (p == king) {
            var /*int*/ k0 = move.from;
            if (move.to == k0 + 2) { // O-O
                this.movePieceNotPawn(k0 + 1, k0 + 3);
            } else if (move.to == k0 - 2) { // O-O-O
                this.movePieceNotPawn(k0 - 1, k0 - 4);
            }
        }

        // Handle en passant
        if (move.to == this.epSquare) {
            if (p == Piece.WPAWN) {
                this.setPiece(move.to - 8, Piece.BPAWN);
            } else if (p == Piece.BPAWN) {
                this.setPiece(move.to + 8, Piece.WPAWN);
            }
        }
    },

    /**
     * Apply a move to the current position.
     * Special version that only updates enough of the state for the SEE function to be happy.
     */
    /*public*/ makeSEEMove: function(/*Move*/ move, /*UndoInfo*/ ui) {
        ui.capturedPiece = this.squares[move.to];
        /*boolean*/  wtm = this.whiteMove;
        
        var /*int*/ p = this.squares[move.from];
        var /*long*/ fromMask = i64bitObj( move.from );

        // Handle castling
        if (  i64_not0( i64_and( i64_or(this.pieceTypeBB[Piece.WKING], this.pieceTypeBB[Piece.BKING]), fromMask) ) ) {
            var /*int*/ k0 = move.from;
            if (move.to == k0 + 2) { // O-O
                this.setSEEPiece(k0 + 1, this.squares[k0 + 3]);
                this.setSEEPiece(k0 + 3, Piece.EMPTY);
            } else if (move.to == k0 - 2) { // O-O-O
                this.setSEEPiece(k0 - 1, this.squares[k0 - 4]);
                this.setSEEPiece(k0 - 4, Piece.EMPTY);
            }
        }

        // Handle en passant
        if (move.to == this.epSquare) {
            if (p == Piece.WPAWN) {
                this.setSEEPiece(move.to - 8, Piece.EMPTY);
            } else if (p == Piece.BPAWN) {
                this.setSEEPiece(move.to + 8, Piece.EMPTY);
            }
        }

        // Perform move
        this.setSEEPiece(move.from, Piece.EMPTY);
        this.setSEEPiece(move.to, p);
        this.whiteMove = !wtm;
    },

    /*public*/ unMakeSEEMove: function(/*Move*/ move, /*UndoInfo*/ ui) {
        this.whiteMove = !this.whiteMove;
        var /*int*/ p = this.squares[move.to];
        this.setSEEPiece(move.from, p);
        this.setSEEPiece(move.to, ui.capturedPiece);
        var /*boolean*/  wtm = this.whiteMove;

        // Handle castling
        var /*int*/ king = wtm ? Piece.WKING : Piece.BKING;
        if (p == king) {
            var /*int*/ k0 = move.from;
            if (move.to == k0 + 2) { // O-O
                this.setSEEPiece(k0 + 3, this.squares[k0 + 1]);
                this.setSEEPiece(k0 + 1, Piece.EMPTY);
            } else if (move.to == k0 - 2) { // O-O-O
                this.setSEEPiece(k0 - 4, this.squares[k0 - 1]);
                this.setSEEPiece(k0 - 1, Piece.EMPTY);
            }
        }

        // Handle en passant
        if (move.to == this.epSquare) {
            if (p == Piece.WPAWN) {
                this.setSEEPiece(move.to - 8, Piece.BPAWN);
            } else if (p == Piece.BPAWN) {
                this.setSEEPiece(move.to + 8, Piece.WPAWN);
            }
        }
    },

    /*private*/ removeCastleRights: function(/*int*/ square) {
        if (square == Position.getSquare(0, 0)) {
            this.setCastleMask(this.castleMask & (~Position.A1_CASTLE));
        } else if (square == Position.getSquare(7, 0)) {
            this.setCastleMask(this.castleMask & (~Position.H1_CASTLE));
        } else if (square == Position.getSquare(0, 7)) {
            this.setCastleMask(this.castleMask & (~Position.A8_CASTLE));
        } else if (square == Position.getSquare(7, 7)) {
            this.setCastleMask(this.castleMask & (~Position.H8_CASTLE));
        }
    }

};

/*=====================================  Search */

/*private*/ /*class*/ SearchTreeInfo = {

        /*public*/ clone: function () { return new clone(this) },

        undoInfo: new UndoInfo(),
        hashMove: new Move(0, 0, 0, 0),     // Temporary storage for local hashMove variable
        allowNullMove: true,                // Don't allow two null-moves in a row
        bestMove: new Move(0, 0, 0, 0),     // Copy of the best found move at this ply
        currentMove: new Move(0, 0, 0, 0),  // Move currently being searched
        /*int*/ lmr:0,                      // LMR reduction amount
        /*long*/ nodeIdx:0        
 };


/*public*/ /*class*/ Search = {
    /*int*/ plyScale: 2 /*8*/, // Fractional ply resolution

    /*public*/ clone: function () { return new clone(this) },

    /*Position*/ pos: null,
    /*KillerTable*/ kt: KillerTable.clone(),  
    /*History*/ ht: History.clone(),
    /*long[]*/ posHashList:[],      // List of hashes for previous positions up to the last "zeroing" move.
    /*int*/ posHashListSize:0,        // Number of used entries in posHashList
    /*int*/ posHashFirstNew:0,        // First entry in posHashList that has not been played OTB.
    /*TranspositionTable*/ tt: null,

    /*SearchTreeInfo[]*/ searchTreeInfo:[],

    SetAtt2AllowNullMove: function() {
        for (var i in this.searchTreeInfo) {
            this.searchTreeInfo[i].allowNullMove = true;
        }
    },

    // Time management
    /*long*/ tStart:0,          // Time when search started
    /*long*/ minTimeMillis:-1,   // Minimum recommended thinking time
    /*long*/ maxTimeMillis:-1,   // Maximum allowed thinking time
    /*boolean*/  searchNeedMoreTime: false, // True if negaScout should use up to maxTimeMillis time.
    /*int*/ maxNodes:-1,         // Maximum number of nodes to search (approximately)
    /*int*/ nodesToGo:0,        // Number of nodes until next time check
    /*int*/ nodesBetweenTimeCheck: 5000, // How often to check remaining time

    // Search statistics stuff
    /*int*/ nodes:0,
    /*int*/ qNodes:0,
    /*int[]*/ nodesPlyVec:[],
    /*int[]*/ nodesDepthVec:[],
    /*int*/ totalNodes:0,
    /*long*/ tLastStats:0,        // Time when notifyStats was last called
    /*boolean*/  verbose: false,
    /*boolean*/  StopSearch: false,
    
    /*public*/ /*int*/ MATE0: 32000,

    /*private*/ /*int[]*/ captures: e0a.slice(0,64),  /* new int[64] */   // Value of captured pieces
    /*private*/ /*UndoInfo*/ seeUi: new UndoInfo(),


    /*public*/ /*int*/ UNKNOWN_SCORE: -32767, // Represents unknown eval score
    /*int*/ q0Eval:0,        // eval score at first level of quiescence search 

    /*private*/ initNodeStats: function() {
        this.nodes = 0;
        this.qNodes = 0;
        this.nodesPlyVec = e0a.slice(0,20);
        this.nodesDepthVec = e0a.slice(0,20);
    },
    
    /*public*/ Search:function(/*Position*/ pos, /*long[]*/ posHashList, /*int*/ posHashListSize,
                            /*TranspositionTable*/ tt) {
        this.pos = pos.clone();
        this.posHashList = posHashList;
        this.posHashListSize = posHashListSize;
        this.tt = tt;
        this.posHashFirstNew = posHashListSize;
        this.initNodeStats();
        for (var i = 0; i<200; i++) this.searchTreeInfo[i] = SearchTreeInfo.clone();
        this.StopSearch = false;
    },

    /*public*/ /*class*/ MoveInfo: function (/*Move*/ m, /*int*/ n)
         {
          this.move = m;
          this.nodes = n;
         },
                  
        /*public*/ /*class*/ /* implements Comparator<MoveInfo> */
        SortByScore: function (/*MoveInfo*/ mi1, /*MoveInfo*/ mi2) {
                if ((mi1 == null) && (mi2 == null))
                    return 0;
                if (mi1 == null)
                    return 1;
                if (mi2 == null)
                    return -1;
                return mi2.move.score - mi1.move.score;
        },
        /*public*/ /*class*/ /* implements Comparator<MoveInfo> */
        SortByNodes: function(/*MoveInfo*/ mi1, /*MoveInfo*/ mi2) {
                if ((mi1 == null) && (mi2 == null))
                    return 0;
                if (mi1 == null)
                    return 1;
                if (mi2 == null)
                    return -1;
                return mi2.nodes - mi1.nodes;
        },

    /*public*/ timeLimit: function (/*int*/ minTimeLimit, /*int*/ maxTimeLimit) {
        this.minTimeMillis = minTimeLimit;
        this.maxTimeMillis = maxTimeLimit;
    },

    /*public*/ /*Move*/ iterativeDeepening: function( /*MoveGen.MoveList*/ scMovesIn,
                        /*int*/ maxDepth, /*int*/ initialMaxNodes, /*boolean*/  verbose) {
        this.tStart = rtime();
        this.tLastStats = rtime();
        this.totalNodes = 0;
        var /*MoveInfo[]*/ scMoves = [];    /*new MoveInfo[scMovesIn.size]*/
        var len = 0;
        for (mi = 0; mi < scMovesIn.size; mi++) {
            var /*Move*/ m = scMovesIn.m[mi];
            scMoves[len++] = new this.MoveInfo(m, 0);
        }
        this.maxNodes = initialMaxNodes;
        this.nodesToGo = 0;
        var /*Position*/ origPos = this.pos.clone();
        var /*int*/ aspirationDelta = 20;
        var /*int*/ bestScoreLastIter = 0;
        var /*Move*/ bestMove = scMoves[0].move;
        this.verbose = verbose;
        this.SetAtt2AllowNullMove();

        for (var depth = 1; !this.StopSearch; depth++) {
            this.initNodeStats();
            
            printf("Depth:%d\n",depth);
            
            var /*int*/ alpha = ( depth > 1 ? Math.max(bestScoreLastIter - aspirationDelta,
                             -Search.MATE0) : -Search.MATE0 );
            var /*int*/ bestScore = -Search.MATE0;
            var /*UndoInfo*/ ui = new UndoInfo();
            var /*boolean*/  needMoreTime = false;
            for (var /*int*/ mi in scMoves ) {
                this.searchNeedMoreTime = (mi > 0);
                var /*Move*/ m = scMoves[mi].move;
                      // printf "Current move" TextIO.moveToString(origPos,m,true)
                this.nodes = 0;
                this.qNodes = 0;
                this.posHashList[this.posHashListSize++] = this.pos.zobristHash();
                var /*boolean*/ givesCheck = MoveGen.givesCheck(this.pos, m);
                var /*int*/ beta;
                if (depth > 1) {
                    beta = (mi == 0) ? Math.min(bestScoreLastIter + aspirationDelta, Search.MATE0) : alpha + 1;
                } else {
                    beta = Search.MATE0;
                }

                var /*int*/ lmr = 0;
                var /*boolean*/  isCapture = (this.pos.getPiece(m.to) != Piece.EMPTY);
                var /*boolean*/  isPromotion = (m.promoteTo != Piece.EMPTY);
                if ((depth >= 3) && !isCapture && !isPromotion) {
                    if (!givesCheck && !this.passedPawnPush(this.pos, m)) {
                        if (mi >= 3) lmr = 1;
                    }
                }
                this.pos.makeMove(m, ui);
                var /*SearchTreeInfo*/ sti = this.searchTreeInfo[0];
                sti.currentMove = m;
                sti.lmr = lmr * this.plyScale;
                sti.nodeIdx = -1;
                var /*int*/ score = -this.negaScout(-beta, -alpha, 1,
                         (depth - lmr - 1) * this.plyScale, -1, givesCheck);
                if ((lmr > 0) && (score > alpha)) {
                    sti.lmr = 0;
                    score = -this.negaScout(-beta, -alpha, 1, (depth - 1) * this.plyScale, -1, givesCheck);
                }
                var /*int*/ nodesThisMove = this.nodes + this.qNodes;
                this.posHashListSize--;
                this.pos.unMakeMove(m, ui);
                {
                    var /*int*/ Stype = TTEntry.T_EXACT;
                    if (score <= alpha) {
                        Stype = TTEntry.T_LE;
                    } else if (score >= beta) {
                        Stype = TTEntry.T_GE;
                    }
                    m.score = score;
                    this.tt.insert(this.pos.historyHash(), m, Stype, 0, depth, Search.UNKNOWN_SCORE);
                }
                if (score >= beta) {
                    var /*int*/ retryDelta = (aspirationDelta<<1);
                    while ((score >= beta) && (!this.StopSearch)) {
                        beta = Math.min(score + retryDelta, Search.MATE0);
                        retryDelta = (Search.MATE0<<1);
                        if (mi != 0) needMoreTime = true;
                        bestMove = m;
                        if (verbose)
                        {
                            //printf TextIO.moveToString(this.pos, m, false),
                            //   score, this.nodes, this.qNodes;
                            this.notifyPV(depth, score, false, true, m);
                        }
                        this.nodes = 0;
                        this.qNodes = 0;
                        this.posHashList[this.posHashListSize++] = this.pos.zobristHash();
                        this.pos.makeMove(m, ui);
                        score = -this.negaScout(-beta, -score, 1, (depth - 1) * this.plyScale, -1, givesCheck);
                        nodesThisMove += this.nodes + this.qNodes;
                        this.posHashListSize--;
                        this.pos.unMakeMove(m, ui);
                    }
                } else if ((mi == 0) && (score <= alpha)) {
                    var /*int*/ retryDelta = (Search.MATE0<<1);
                    while ((score <= alpha) && (!this.StopSearch)) {
                        alpha = Math.max(score - retryDelta, -Search.MATE0);
                        retryDelta = (Search.MATE0<<1);
                        needMoreTime = true;
                        this.searchNeedMoreTime = true;
                        if (verbose)
                        {
                            //printf TextIO.moveToString(this.pos, m, false),
                            //   score, this.nodes, this.qNodes;
                            this.notifyPV(depth, score, true, false, m);
                        }    
                        this.nodes = 0;
                        this.qNodes = 0;
                        this.posHashList[this.posHashListSize++] = this.pos.zobristHash();
                        this.pos.makeMove(m, ui);
                        score = -this.negaScout(-score, -alpha, 1, (depth - 1) * this.plyScale, -1, givesCheck);
                        nodesThisMove += this.nodes + this.qNodes;
                        this.posHashListSize--;
                        this.pos.unMakeMove(m, ui);
                    }
                }
                if (verbose) {
                    var /*boolean*/  havePV = false;
                    var /*String*/ PV = "";
                    if ((score > alpha) || (mi == 0)) {
                        havePV = true;
                        if (verbose) {
                            PV = TextIO.moveToString(this.pos, m, false) + " ";
                            this.pos.makeMove(m, ui);
                            PV += this.tt.extractPV(this.pos);
                            this.pos.unMakeMove(m, ui);
                        }
                    }
                    //if (verbose) {
                        // printf     TextIO.moveToString(this.pos, m, false), score,
                        //    this.nodes, this.qNodes, (score > alpha ? " *" : ""), PV
                    //}
                    if (verbose && havePV && (depth > 1)) {
                        this.notifyPV(depth, score, false, false, m);
                    }
                }
                scMoves[mi].move.score = score;
                scMoves[mi].nodes = nodesThisMove;
                bestScore = Math.max(bestScore, score);
                if (depth > 1) {
                    if ((score > alpha) || (mi == 0)) {
                        alpha = score;
                        var /*MoveInfo*/ tmp = scMoves[mi];
                        for (var /*int*/ i = mi - 1; i >= 0;  i--) {
                            scMoves[i + 1] = scMoves[i];
                        }
                        scMoves[0] = tmp;
                        bestMove = scMoves[0].move;
                    }
                }


            if (depth == 1) {
                scMoves.sort(this.SortByScore);
                bestMove = scMoves[0].move;
                this.notifyPV(depth, bestMove.score, false, false, bestMove);
            }
            
            if (this.maxTimeMillis >= 0) {
                var /*long*/ tNow = rtime();
                if (tNow - this.tStart >= this.maxTimeMillis)
                    {
                    if( ComputerPlayer.maxDepth > 3 ) ComputerPlayer.maxDepth--;    // self adjusting depth
                    printf('<font color="yellow"><b>timeout</b></font>\n');
                    this.StopSearch = true;
                    break;
                    }
		if(depth > 1 )
		{
		var /*long*/ timeLimit = (needMoreTime ? this.maxTimeMillis : this.minTimeMillis);
		if (tNow - this.tStart >= timeLimit)
		    {
		    this.StopSearch = true;
		    break;
		    }
		}
            }
            
            if (this.maxNodes >= 0)
             {
             if (this.totalNodes >= this.maxNodes) this.StopSearch = true;
             }

            if (depth > maxDepth)
                {
			//Limited
		 if(ComputerPlayer.maxDepth < (CHROME?6:3))
		  {
                   ComputerPlayer.maxDepth++;     // self adjusting depth
		  }
                 this.StopSearch = true;
                } 
                    
            var /*int*/ plyToMate = Search.MATE0 - Math.abs(bestScore);
            if (depth >= plyToMate) break;
            bestScoreLastIter = bestScore;

            if (depth > 1) {
                // Moves that were hard to search should be searched early in the next iteration
                // Originally java .sort(1,len,sortfunc) from the position 1
                scMoves = scMoves.slice(0,1).concat( scMoves.slice(1).sort(this.SortByNodes) );
            }
            
        }
        
        }
        this.notifyStats();
        
        return bestMove;
    },

    /*private*/ notifyPV: function(/*int*/ depth, /*int*/ score,
                     /*boolean*/  uBound, /*boolean*/  lBound, /*Move*/ m) {
            var /*boolean*/  isMate = false;
            if (score > Search.MATE0 / 2) {
                isMate = true;
                score = (Search.MATE0 - score) / 2;
            } else if (score < -Search.MATE0 / 2) {
                isMate = true;
                score = -((Search.MATE0 + score - 1) / 2);
            }
            var /*long*/ tNow = rtime();
            var /*int*/ time = /*(int)*/ (tNow - this.tStart);
            var /*int*/ nps = (time > 0) ? /*(int)*/(this.totalNodes / (time / 1000)) : 0;
            var /*ArrayList<Move>*/ pv = this.tt.extractPVMoves(this.pos, m);
            var pvS = "";
            for(var j in pv)
             {
              pvS+= TextIO.moveToString(this.pos, pv[j], false) + "(" + parseInt(pv[j].score).toString() + ") ";
             }
            
            var s = "depth:" + depth.toString();
            s+= " score:" + parseInt(score).toString();
            s+= " time:" + parseInt(time / 1000).toString();
            s+= " nodes:" + this.totalNodes.toString();
            s+= " nps:" + parseInt(nps).toString();
            s+= " mate:" + (isMate?"1":"0");
            //s+= " ub:" + (uBound?"1":"0");
            //s+= " lb:" + (lBound?"1":"0");
            s+= " " + pvS;
            printf("%s\n", s);
    },

    /*private*/ notifyStats: function() {
        var /*long*/ tNow = rtime();
        var /*int*/ time = /*(int)*/ (tNow - this.tStart);
        var /*int*/ nps = (time > 0) ? /*(int)*/(this.totalNodes / (time / 1000)) : 0;
        var s = "nodes:" + this.totalNodes.toString();
        s+= " nps:" + parseInt(nps).toString();
        s+= " time:" + parseInt(time / 1000).toString();
        printf("%s\n", s);
        this.tLastStats = tNow;
    },

    /*private*/ /*Move*/ emptyMove: new Move(0, 0, Piece.EMPTY, 0),

    /** 
     * Main recursive search algorithm.
     * @return Score for the side to make a move, in position given by "pos".
     */
    /*public*/ /*int*/ negaScout: function(/*int*/ alpha, /*int*/ beta, /*int*/ ply,
                     /*int*/ depth, /*int*/ recaptureSquare, /*boolean*/  inCheck) {
        var pos = this.pos;
        if (--this.nodesToGo <= 0) {
            this.nodesToGo = this.nodesBetweenTimeCheck;
            var /*long*/ tNow = rtime();
            var /*long*/ timeLimit = this.searchNeedMoreTime ? this.maxTimeMillis : this.minTimeMillis;
            if (    ((timeLimit >= 0) && (tNow - this.tStart >= timeLimit)) ||
                    ((this.maxNodes >= 0) && (this.totalNodes >= this.maxNodes))) {
                this.StopSearch = true;
            }
            if (tNow - this.tLastStats >= 5000) {
                this.notifyStats();
            }
        }
        
        // Collect statistics
        if (this.verbose) {
            if (ply < 20) this.nodesPlyVec[ply]++;
            if (depth < 20*this.plyScale) this.nodesDepthVec[depth/this.plyScale]++;
        }
        this.nodes++;
        this.totalNodes++;
        var /*long*/ hKey = pos.historyHash();

        // Draw tests
        if (this.canClaimDraw50(pos)) {
            if (MoveGen.canTakeKing(pos)) {
                var /*int*/ score = Search.MATE0 - ply;
                return score;
            }
            if (inCheck) {
            
                var /*MoveGen.MoveList*/ moves = MoveGen.pseudoLegalMoves(pos);
                MoveGen.removeIllegal(pos, moves);
            
                if (moves.size == 0) {            // Can't claim draw if already check mated.
                    /*int*/ score = -(Search.MATE0-(ply+1));
                    MoveGen.returnMoveList(moves);
                    return score;
                }
                MoveGen.returnMoveList(moves);
            }
          return 0;
        }
        if (this.canClaimDrawRep(pos, this.posHashList, this.posHashListSize, this.posHashFirstNew)) {
            return 0;            // No need to test for mate here, since it would have been
                                 // discovered the first time the position came up.
        }

        var /*int*/ evalScore = Search.UNKNOWN_SCORE;
        // Check transposition table
        var /*TTEntry*/ ent = this.tt.probe(hKey);
        var /*Move*/ hashMove = null;
        var /*SearchTreeInfo*/ sti = this.searchTreeInfo[ply];
        if (ent.Stype != TTEntry.T_EMPTY) {
            var /*int*/ score = ent.getScore(ply);
            evalScore = ent.evalScore;
            var /*int*/ plyToMate = Search.MATE0 - Math.abs(score);
            var /*int*/ eDepth = ent.getDepth();
            if ((beta == alpha + 1) && ((eDepth >= depth) || (eDepth >= plyToMate*this.plyScale))) {
                if (    (ent.Stype == TTEntry.T_EXACT) ||
                        (ent.Stype == TTEntry.T_GE) && (score >= beta) ||
                        (ent.Stype == TTEntry.T_LE) && (score <= alpha)) {
                 return score;
                }
            }
            hashMove = sti.hashMove;
            ent.getMove(hashMove);
        }
        
        var /*int*/ posExtend = (inCheck ? this.plyScale : 0); // Check extension

        // If out of depth, perform quiescence search
        if (depth + posExtend <= 0) {
            this.qNodes--;
            this.totalNodes--;
            this.q0Eval = evalScore;
            var /*int*/ score = this.quiesce(alpha, beta, ply, 0, inCheck);
            var /*int*/ Ztype = TTEntry.T_EXACT;
            if (score <= alpha) {
                Ztype = TTEntry.T_LE;
            } else if (score >= beta) {
                Ztype = TTEntry.T_GE;
            }
            this.emptyMove.score = score;
            this.tt.insert(hKey, this.emptyMove, Ztype, ply, depth, this.q0Eval);
            return score;
        }

        // Try null-move pruning
        sti.currentMove = this.emptyMove;
        if (    (depth >= 3*this.plyScale) && !inCheck && sti.allowNullMove &&
                (Math.abs(beta) <= Search.MATE0 / 2)) {
            if (MoveGen.canTakeKing(pos)) {
                var /*int*/ score = Search.MATE0 - ply;
              return score;
            }
            var /*boolean*/  nullOk;
            if (pos.whiteMove) {
                nullOk = (pos.wMtrl > pos.wMtrlPawns) && (pos.wMtrlPawns > 0);
            } else {
                nullOk = (pos.bMtrl > pos.bMtrlPawns) && (pos.bMtrlPawns > 0);
            }
            if (nullOk) {
                var /*int*/ R = (depth > 6*this.plyScale) ? (this.plyScale<<2) : 3*this.plyScale;
                pos.setWhiteMove(!pos.whiteMove);
                var /*int*/ epSquare = pos.getEpSquare();
                pos.setEpSquare(-1);
                this.searchTreeInfo[ply+1].allowNullMove = false;
                var /*int*/ score = -this.negaScout(-beta, -(beta - 1), ply + 1, depth - R, -1, false);
                this.searchTreeInfo[ply+1].allowNullMove = true;
                pos.setEpSquare(epSquare);
                pos.setWhiteMove(!pos.whiteMove);
                if (score >= beta) {
                    if (score > Search.MATE0 / 2)
                        score = beta;
                    this.emptyMove.score = score;
                    this.tt.insert(hKey, this.emptyMove, TTEntry.T_GE, ply, depth, evalScore);
                    return score;
                } else {
                    if ((this.searchTreeInfo[ply-1].lmr > 0) && (depth < 5*this.plyScale)) {
                        var /*Move*/ m1 = this.searchTreeInfo[ply-1].currentMove;
                        var /*Move*/ m2 = this.searchTreeInfo[ply+1].bestMove; // threat move
                        if (m1.from != m1.to) {
                            if ((m1.to == m2.from) || (m1.from == m2.to) ||
                                i64_not0( i64_and(BitBoard.squaresBetween[m2.from][m2.to], i64bitObj(m1.from))) ) {
                                // if the threat move was made possible by a reduced
                                // move on the previous ply, the reduction was unsafe.
                                // Return alpha to trigger a non-reduced re-search.
                              return alpha;
                            }
                        }
                    }
                }
            }
        }

        // Razoring
        if ((Math.abs(alpha) <= Search.MATE0 / 2) && (depth < (this.plyScale<<2)) && (beta == alpha + 1)) {
            if (evalScore == Search.UNKNOWN_SCORE) {
                evalScore = Evaluate.evalPos(pos);
            }
            var /*int*/ razorMargin = 250;
            if (evalScore < beta - razorMargin) {
                this.qNodes--;
                this.totalNodes--;
                this.q0Eval = evalScore;
                var /*int*/ score = this.quiesce(alpha-razorMargin, beta-razorMargin, ply, 0, inCheck);
                if (score <= alpha-razorMargin) {
                  return score;
                }
            }
        }

        var /*boolean*/  futilityPrune = false;
        var /*int*/ futilityScore = alpha;
        if (!inCheck && (depth < 5*this.plyScale) && (posExtend == 0)) {
            if ((Math.abs(alpha) <= Search.MATE0 / 2) && (Math.abs(beta) <= Search.MATE0 / 2)) {
                var /*int*/ margn;
                if (depth <= this.plyScale) {
                    margn = 125;
                } else if (depth <= (this.plyScale<<1)) {
                    margn = 250;
                } else if (depth <= 3*this.plyScale) {
                    margn = 375;
                } else {
                    margn = 500;
                }
                if (evalScore == Search.UNKNOWN_SCORE) {
                    evalScore = Evaluate.evalPos(pos);
                }
                futilityScore = evalScore + margn;
                if (futilityScore <= alpha) {
                    futilityPrune = true;
                }
            }
        }

        if ((depth > (this.plyScale<<2)) && ((hashMove == null) || (hashMove.from == hashMove.to))) {
            var /*boolean*/  isPv = beta > alpha + 1;
            if (isPv || (depth > (this.plyScale<<3))) {
                // No hash move. Try internal iterative deepening.
                var /*long*/ savedNodeIdx = sti.nodeIdx;
                var /*int*/ newDepth = ( isPv ? depth  - (this.plyScale<<1) : (depth * 3 )>>>3 );
                this.negaScout(alpha, beta, ply, newDepth, -1, inCheck);
                sti.nodeIdx = savedNodeIdx;
                ent = this.tt.probe(hKey);
                if (ent.Stype != TTEntry.T_EMPTY) {
                    hashMove = sti.hashMove;
                    ent.getMove(hashMove);
                }
            }
        }

        // Start searching move alternatives
        
        var /*MoveGen.MoveList*/ moves;
        if (inCheck)
            moves = MoveGen.checkEvasions(pos);
        else 
            moves = MoveGen.pseudoLegalMoves(pos);
        var /*boolean*/  seeDone = false;
        var /*boolean*/  hashMoveSelected = true;
        if (!this.selectHashMove(moves, hashMove)) {
            this.scoreMoveList(moves, ply);
            seeDone = true;
            hashMoveSelected = false;
        }

        var /*UndoInfo*/ ui = sti.undoInfo;
        var /*boolean*/  haveLegalMoves = false;
        var /*int*/ illegalScore = -(Search.MATE0-(ply+1));
        var /*int*/ b = beta;
        var /*int*/ bestScore = illegalScore;
        var /*int*/ bestMove = -1;
        var /*int*/ lmrCount = 0;
        for (var  mi = 0; mi < moves.size; mi++) {
            if ((mi == 1) && !seeDone) {
                this.scoreMoveList(moves, ply, 1);
                seeDone = true;
            }
            if ((mi > 0) || !hashMoveSelected) {
                this.selectBest(moves, mi);
            }
            var /*Move*/ m = moves.m[mi];
            if (pos.getPiece(m.to) == (pos.whiteMove ? Piece.BKING : Piece.WKING)) {
                MoveGen.returnMoveList(moves);
                var /*int*/ score = Search.MATE0-ply;
                return score;       // King capture
            }
            var /*int*/ newCaptureSquare = -1;
            var /*boolean*/  isCapture = false;
            var /*boolean*/  isPromotion = (m.promoteTo != Piece.EMPTY);
            var /*int*/ sVal = Int_.MIN_VALUE;
            if (pos.getPiece(m.to) != Piece.EMPTY) {
                isCapture = true;
                var /*int*/ fVal = Evaluate.pieceValue[pos.getPiece(m.from)];
                var /*int*/ tVal = Evaluate.pieceValue[pos.getPiece(m.to)];
                var /*int*/ pV = Evaluate.pV;
                if (Math.abs(tVal - fVal) < (pV>>>1)) {    // "Equal" capture
                    sVal = this.SEE(m);
                    if (Math.abs(sVal) < (pV>>>1))
                        newCaptureSquare = m.to;
                }
            }
            var /*int*/ moveExtend = 0;
            if (posExtend == 0) {
                var /*int*/ pV = Evaluate.pV;
                if ((m.to == recaptureSquare)) {
                    if (sVal == Int_.MIN_VALUE) sVal = this.SEE(m);
                    var /*int*/ tVal = Evaluate.pieceValue[pos.getPiece(m.to)];
                    if (sVal > tVal - (pV>>>1))
                        moveExtend = this.plyScale;
                }
                if ((moveExtend == 0) && isCapture && (pos.wMtrlPawns + pos.bMtrlPawns > pV)) {
                    // Extend if going into pawn endgame
                    var /*int*/ capVal = Evaluate.pieceValue[pos.getPiece(m.to)];
                    if (pos.whiteMove) {
                        if ((pos.wMtrl == pos.wMtrlPawns) && (pos.bMtrl - pos.bMtrlPawns == capVal))
                            moveExtend = this.plyScale;
                    } else {
                        if ((pos.bMtrl == pos.bMtrlPawns) && (pos.wMtrl - pos.wMtrlPawns == capVal))
                            moveExtend = this.plyScale;
                    }
                }
            }
            var /*boolean*/  mayReduce = (m.score < 53) && (!isCapture || m.score < 0) && (!isPromotion);
            
            var /*boolean*/  givesCheck = MoveGen.givesCheck(pos, m); 
            var /*boolean*/  doFutility = false;
            if (futilityPrune && mayReduce && haveLegalMoves) {
                if ((!givesCheck) && (!this.passedPawnPush(pos, m)))
                    doFutility = true;
            }
            var /*int*/ score;
            if (doFutility) {
                score = futilityScore;
            } else {
                var /*int*/ extend1 = Math.max(posExtend, moveExtend);
                var /*int*/ lmr = 0;
                if ((depth >= 3*this.plyScale) && mayReduce && (extend1 == 0)) {
                    if (!givesCheck && !this.passedPawnPush(pos, m)) {
                        lmrCount++;
                        if ((lmrCount > 3) && (depth > 3*this.plyScale)) {
                            lmr = (this.plyScale<<1);
                        } else {
                            lmr = this.plyScale;
                        }
                    }
                }
                this.posHashList[this.posHashListSize++] = pos.zobristHash();
                pos.makeMove(m, ui);
                sti.currentMove = m;
                var /*int*/ newDepth = depth - this.plyScale + extend1 - lmr;
                sti.lmr = lmr;
                score = -this.negaScout(-b, -alpha, ply + 1, newDepth, newCaptureSquare, givesCheck);
                if (((lmr > 0) && (score > alpha)) ||
                    ((score > alpha) && (score < beta) && (b != beta) && (score != illegalScore))) {
                    sti.lmr = 0;
                    newDepth += lmr;
                    score = -this.negaScout(-beta, -alpha, ply + 1, newDepth, newCaptureSquare, givesCheck);
                }
                this.posHashListSize--;
                pos.unMakeMove(m, ui);
            }
            m.score = score;

            if (score != illegalScore) {
                haveLegalMoves = true;
            }
            bestScore = Math.max(bestScore, score);
            if (score > alpha) {
                alpha = score;
                bestMove = mi;
                sti.bestMove.from      = m.from;
                sti.bestMove.to        = m.to;
                sti.bestMove.promoteTo = m.promoteTo;
            }
            if (alpha >= beta) {
                if (pos.getPiece(m.to) == Piece.EMPTY) {
                    this.kt.addKiller(ply, m);
                    this.ht.addSuccess(pos, m, depth/this.plyScale);
                    for (var mi2 = mi - 1; mi2 >= 0; mi2--) {
                        var /*Move*/ m2 = moves.m[mi2];
                        if (pos.getPiece(m2.to) == Piece.EMPTY)
                            this.ht.addFail(pos, m2, depth/this.plyScale);
                    }
                }
                this.tt.insert(hKey, m, TTEntry.T_GE, ply, depth, evalScore);
                MoveGen.returnMoveList(moves);
              return alpha;
            }
            b = alpha + 1;
        }
        if (!haveLegalMoves && !inCheck) {
            MoveGen.returnMoveList(moves);
           return 0;       // Stale-mate
        }
        if (bestMove >= 0) {
            this.tt.insert(hKey, moves.m[bestMove], TTEntry.T_EXACT, ply, depth, evalScore);
      } else {
            this.emptyMove.score = bestScore;
            this.tt.insert(hKey, this.emptyMove, TTEntry.T_LE, ply, depth, evalScore);
      }
        MoveGen.returnMoveList(moves);
        return bestScore;
    },

    /*private*/ /*boolean*/  passedPawnPush: function(/*Position*/ pos, /*Move*/ m) {
        var /*int*/ p = pos.getPiece(m.from);
        if (pos.whiteMove) {
            if (p != Piece.WPAWN)
                return false;
            if ( i64_not0( i64_and(BitBoard.wPawnBlockerMask[m.to], pos.pieceTypeBB[Piece.BPAWN]) ) )
                return false;
            return (m.to >= 40);
        } else {
            if (p != Piece.BPAWN)
                return false;
            if ( i64_not0( i64_and(BitBoard.bPawnBlockerMask[m.to], pos.pieceTypeBB[Piece.WPAWN]) ) )
                return false;
            return (m.to <= 23);
        }
    },

    /**
     * Quiescence search. Only non-losing captures are searched.
     */
    /*private*/ /*int*/ quiesce: function(/*int*/ alpha, /*int*/ beta, /*int*/ ply,
                 /*int*/ depth, /*boolean*/  inCheck) {
        var pos = this.pos;
        this.qNodes++;
        this.totalNodes++;
        var /*int*/ score;
        if (inCheck) {
            score = -(Search.MATE0 - (ply+1));
        } else {
            if ((depth == 0) && (this.q0Eval != Search.UNKNOWN_SCORE)) {
                score = this.q0Eval;
            } else {
                score = Evaluate.evalPos(pos);
                if (depth == 0)
                    this.q0Eval = score;
            }
        }
        if (score >= beta) {
            if ((depth == 0) && (score < Search.MATE0 - ply)) {
                if (MoveGen.canTakeKing(pos)) {
                    // To make stale-mate detection work
                    score = Search.MATE0 - ply;
                }
            }
            return score;
        }
        var /*int*/ evalScore = score;
        if (score > alpha)
            alpha = score;
        var /*int*/ bestScore = score;
        var /*boolean*/  tryChecks = (depth > -3);
        var /*MoveGen.MoveList*/ moves;
        if (inCheck) {
            moves = MoveGen.checkEvasions(pos);
        } else if (tryChecks) {
            moves = MoveGen.pseudoLegalCapturesAndChecks(pos);
        } else {
            moves = MoveGen.pseudoLegalCaptures(pos);
        }
        this.scoreMoveListMvvLva(moves);
        var /*UndoInfo*/ ui = this.searchTreeInfo[ply].undoInfo;
        for (var mi = 0; mi < moves.size; mi++) {
            if (mi < 8) {
                // If the first 8 moves didn't fail high, this is probably an ALL-node,
                // so spending more effort on move ordering is probably wasted time.
                this.selectBest(moves, mi);
            }
            var /*Move*/ m = moves.m[mi];
            if (pos.getPiece(m.to) == (pos.whiteMove ? Piece.BKING : Piece.WKING)) {
                MoveGen.returnMoveList(moves);
                return Search.MATE0-ply;       // King capture
            }
            var /*boolean*/  givesCheck = false;
            var /*boolean*/  givesCheckComputed = false;
            if (inCheck) {
                // Allow all moves
            } else {
                if ((pos.getPiece(m.to) == Piece.EMPTY) && (m.promoteTo == Piece.EMPTY)) {
                    // Non-capture
                    if (!tryChecks)
                        continue;
                    givesCheck = MoveGen.givesCheck(pos, m);
                    givesCheckComputed = true;
                    if (!givesCheck)
                        continue;
                    if (this.negSEE(m)) // Needed because m.score is not computed for non-captures
                        continue;
                } else {
                    if (this.negSEE(m))
                        continue;
                    var /*int*/ capt = Evaluate.pieceValue[pos.getPiece(m.to)];
                    var /*int*/ prom = Evaluate.pieceValue[m.promoteTo];
                    var /*int*/ optimisticScore = evalScore + capt + prom + 200;
                    if (optimisticScore < alpha) { // Delta pruning
                        if ((pos.wMtrlPawns > 0) && (pos.wMtrl > capt + pos.wMtrlPawns) &&
                            (pos.bMtrlPawns > 0) && (pos.bMtrl > capt + pos.bMtrlPawns)) {
                            if (depth -1 > -4) {
                                givesCheck = MoveGen.givesCheck(pos, m);
                                givesCheckComputed = true;
                            }
                            if (!givesCheck) {
                                if (optimisticScore > bestScore)
                                    bestScore = optimisticScore;
                                continue;
                            }
                        }
                    }
                }
            }

            if (!givesCheckComputed) {
                if (depth - 1 > -4) {
                    givesCheck = MoveGen.givesCheck(pos, m);
                }
            }
            var /*boolean*/ nextInCheck = ((depth - 1) > -4 ? givesCheck : false );

            pos.makeMove(m, ui);
            score = -this.quiesce(-beta, -alpha, ply + 1, depth - 1, nextInCheck);
            pos.unMakeMove(m, ui);
            if (score > bestScore) {
                bestScore = score;
                if (score > alpha) {
                    alpha = score;
                    if (alpha >= beta) {
                        MoveGen.returnMoveList(moves);
                        return alpha;
                    }
                }
            }
        }
        MoveGen.returnMoveList(moves);
        return bestScore;
    },

    /*private*/ /*boolean*/  negSEE: function(/*Move*/ m) {
        var /*int*/ p0 = Evaluate.pieceValue[this.pos.getPiece(m.from)];
        var /*int*/ p1 = Evaluate.pieceValue[this.pos.getPiece(m.to)];
        if (p1 >= p0) return false;
        return (this.SEE(m) < 0);
    },

    /**
     * exchange evaluation function.
     * @return SEE score for m. Positive value is good for the side that makes the first move.
     */
    /*public*/ /*int*/ SEE: function(/*Move*/ m) {
        var /*int*/ kV = Evaluate.kV;
        var pos = this.pos;
        var /*int*/ square = m.to;
        if (square == pos.getEpSquare()) {
            this.captures[0] = Evaluate.pV;
        } else {
            this.captures[0] = Evaluate.pieceValue[pos.getPiece(square)];
            if (this.captures[0] == kV) return kV;
        }
        var /*int*/ nCapt = 1;                  // Number of entries in captures[]

        pos.makeSEEMove(m, this.seeUi);
        var /*boolean*/  white = pos.whiteMove;
        var /*int*/ valOnSquare = Evaluate.pieceValue[pos.getPiece(square)];
        var /*long*/ occupied = i64_or( pos.whiteBB, pos.blackBB );
        while (true) {
            var /*int*/ bestValue = Int_.MAX_VALUE;
            var /*long*/ atk;
            if (white) {
                atk = i64_and( BitBoard.bPawnAttacks[square], i64_and( pos.pieceTypeBB[Piece.WPAWN], occupied ));
                if ( i64_not0(atk) ) {
                    bestValue = Evaluate.pV;
                } else {
                    atk = i64_and( BitBoard.knightAttacks[square],
                         i64_and( pos.pieceTypeBB[Piece.WKNIGHT], occupied ));
                    if ( i64_not0(atk) ) {
                        bestValue = Evaluate.nV;
                    } else {
                        var /*long*/ bAtk = i64_and( bishopAttacks(square, occupied), occupied );
                        atk = i64_and( bAtk, pos.pieceTypeBB[Piece.WBISHOP] );
                        if ( i64_not0(atk) ) {
                            bestValue = Evaluate.bV;
                        } else {
                            var /*long*/ rAtk = i64_and( rookAttacks(square, occupied), occupied );
                            atk = i64_and( rAtk, pos.pieceTypeBB[Piece.WROOK] );
                            if ( i64_not0(atk) ) {
                                bestValue = Evaluate.rV;
                            } else {
                                atk = i64_and( i64_or(bAtk,rAtk), pos.pieceTypeBB[Piece.WQUEEN] );
                                if ( i64_not0(atk) ) {
                                    bestValue = Evaluate.qV;
                                } else {
                                    atk = i64_and( BitBoard.kingAttacks[square],
                                        i64_and(  pos.pieceTypeBB[Piece.WKING], occupied ));
                                    if ( i64_not0(atk) ) {
                                        bestValue = kV;
                                    } else {
                                        break;
                                    }
                                }
                            }
                        }
                    }
                }
            } else {
                atk = i64_and( BitBoard.wPawnAttacks[square],
                        i64_and( pos.pieceTypeBB[Piece.BPAWN], occupied ));
                if ( i64_not0(atk) ) {
                    bestValue = Evaluate.pV;
                } else {
                    atk = i64_and( BitBoard.knightAttacks[square],
                        i64_and( pos.pieceTypeBB[Piece.BKNIGHT], occupied ));
                    if ( i64_not0(atk) ) {
                        bestValue = Evaluate.nV;
                    } else {
                        var /*long*/ bAtk = i64_and( bishopAttacks(square, occupied), occupied );
                        atk = i64_and( bAtk, pos.pieceTypeBB[Piece.BBISHOP] );
                        if ( i64_not0(atk) ) {
                            bestValue = Evaluate.bV;
                        } else {
                            var /*long*/ rAtk = i64_and( rookAttacks(square, occupied), occupied );
                            atk = i64_and( rAtk, pos.pieceTypeBB[Piece.BROOK] );
                            if ( i64_not0(atk) ) {
                                bestValue = Evaluate.rV;
                            } else {
                                atk = i64_and( i64_or(bAtk,rAtk), pos.pieceTypeBB[Piece.BQUEEN] );
                                if ( i64_not0(atk) ) {
                                    bestValue = Evaluate.qV;
                                } else {
                                    atk = i64_and( BitBoard.kingAttacks[square],
                                        i64_and( pos.pieceTypeBB[Piece.BKING], occupied ));
                                    if ( i64_not0(atk) ) {
                                        bestValue = kV;
                                    } else {
                                        break;
                                    }
                                }
                            }
                        }
                    }
                }
            }
            this.captures[nCapt++] = valOnSquare;
            if (valOnSquare == kV)
                break;
            valOnSquare = bestValue;
            occupied = i64_and( occupied,  i64_not( i64_and( atk, i64_neg(atk) ) ) );
            white = !white;
        }
        pos.unMakeSEEMove(m, this.seeUi);
        
        var /*int*/ score = 0;
        for (var i = nCapt - 1; i > 0; i--) {
            score = Math.max(0, this.captures[i] - score);
        }
        return (this.captures[0] - score);
    },

    /**
     * Compute scores for each move in a move list, using SEE, killer and history information.
     * @param moves  List of moves to score.
     */
    scoreMoveList: function (/*MoveGen.MoveList*/ moves, /*int*/ ply) {
        this.scoreMoveList(moves, ply, 0);
    },
    
    scoreMoveList: function (/*MoveGen.MoveList*/ moves, /*int*/ ply, /*int*/ startIdx) {
        for (var /*int*/ i = startIdx; i < moves.size; i++) {
            var /*Move*/ m = moves.m[i];
            var /*boolean*/  isCapture = (this.pos.getPiece(m.to) != Piece.EMPTY) || (m.promoteTo != Piece.EMPTY);
            var /*int*/ score = (isCapture ? this.SEE(m) : 0);
            var /*int*/ ks = this.kt.getKillerScore(ply, m);
            if (ks > 0) {
                score += ks + 50;
            } else {
                var /*int*/ hs = this.ht.getHistScore(this.pos, m);
                score += hs;
            }
            m.score = score;
        }
    },
    
    /*private*/ scoreMoveListMvvLva: function (/*MoveGen.MoveList*/ moves) {
        for (var /*int*/ i = 0; i < moves.size; i++) {
            var /*Move*/ m = moves.m[i];
            var /*int*/ v = this.pos.getPiece(m.to);
            var /*int*/ a = this.pos.getPiece(m.from);
            m.score = ((Evaluate.pieceValue[v]<<4) * 625) - Evaluate.pieceValue[a];
        }
    },

    /**
     * Find move with highest score and move it to the front of the list.
     */
    selectBest: function( /*MoveGen.MoveList*/ moves, /*int*/ startIdx) {
        var /*int*/ bestIdx = startIdx;
        var /*int*/ bestScore = moves.m[bestIdx].score;
        for (var /*int*/ i = startIdx + 1; i < moves.size; i++) {
            var /*int*/ sc = moves.m[i].score;
            if (sc > bestScore) {
                bestIdx = i;
                bestScore = sc;
            }
        }
        if (bestIdx != startIdx) {
            var /*Move*/ m = moves.m[startIdx];
            moves.m[startIdx] = moves.m[bestIdx];
            moves.m[bestIdx] = m;
        }
    },

    /** If hashMove exists in the move list, move the hash move to the front of the list. */
    /*boolean*/  selectHashMove: function( /*MoveGen.MoveList*/ moves, /*Move*/ hashMove) {
        if (hashMove == null) return false;
        for (var /*int*/ i = 0; i < moves.size; i++) {
            var /*Move*/ m = moves.m[i];
            if (equalsMove(m,hashMove)) {
                moves.m[i] = moves.m[0];
                moves.m[0] = m;
                m.score = 10000;
                return true;
            }
        }
        return false;
    },

    /*public*/ /*boolean*/  canClaimDraw50: function(/*Position*/ pos) {
        return (pos.halfMoveClock >= 100);
    },
    
    /*public*/ /*boolean*/  canClaimDrawRep: function(/*Position*/ pos, /*long[]*/ posHashList,
             /*int*/ posHashListSize, /*int*/ posHashFirstNew) {
        var /*int*/ reps = 0;
        for (var /*int*/ i = posHashListSize - 4; i >= 0; i -= 2) {
            if (pos.zobristHash() == posHashList[i]) {
                reps++;
                if (i >= posHashFirstNew) {
                    reps++;
                    break;
                }
            }
        }
        return (reps >= 2);
    }
};

/*=====================================  TranspositionTable */

/*public*/ /*class*/ TTEntry = {

        /*public*/ clone: function () { return new clone(this) },

        /*long*/ key:0,                      // Zobrist hash key
        /*private*/ /*short*/ move:0,        // from + (to<<6) + (promote<<12)
        /*private*/ /*short*/ score:0,       // Score from search
        /*private*/ /*short*/ depthSlot:0,   // Search depth (bit 0-14) and hash slot (bit 15).
        /*byte*/ generation:0,               // Increase when OTB position changes
        /*public*/ /*byte*/ Stype:3,         // exact score, lower bound, upper bound (T_EMPTY)
        /*short*/ evalScore:0,               // Score from evaluation 
        
        /*public*/ /*int*/ T_EXACT:0,   // Exact score
        /*public*/ /*int*/ T_GE:1,      // True score >= this.score
        /*public*/ /*int*/ T_LE:2,      // True score <= this.score
        /*public*/ /*int*/ T_EMPTY:3,   // Empty hash slot
        
        /** Return true if this object is more valuable than the other, false otherwise. */
        /*public*/ /*boolean*/  betterThan: function( /*TTEntry*/ other, /*int*/ currGen) {
            if ((this.generation == currGen) != (other.generation == currGen)) {
                return this.generation == currGen;   // Old entries are less valuable
            }
            if ((this.Stype == TTEntry.T_EXACT) != (other.Stype == TTEntry.T_EXACT)) {
                return this.Stype == TTEntry.T_EXACT;         // Exact score more valuable than lower/upper bound
            }
            if (this.getDepth() != other.getDepth()) {
                return this.getDepth() > other.getDepth();     // Larger depth is more valuable
            }
            return false;   // Otherwise, pretty much equally valuable
        },

        /** Return true if entry is good enough to spend extra time trying to afunction overwriting it. */
        /*public*/ /*boolean*/  valuable: function(/*int*/ currGen) {
            if (this.generation != currGen)
                return false;
            return (this.Stype == TTEntry.T_EXACT) || (this.getDepth() > 3 * Search.plyScale);
        },

        /*public*/ getMove: function(/*Move*/ m) {
            m.from = this.move & 63;
            m.to = (this.move >>> 6) & 63;
            m.promoteTo = (this.move >>> 12) & 15;
        },
        
        /*public*/ setMove: function(/*Move*/ m) {
            this.move = (m.from + (m.to << 6) + (m.promoteTo << 12));
        },
        
        /** Get the score from the hash entry, and convert from "mate in x" to "mate at ply". */
        /*public*/ /*int*/ getScore: function(/*int*/ ply) {
            var /*int*/ sc = this.score;
            if (sc > Search.MATE0 - 1000) {
                sc -= ply;
            } else if (sc < -(Search.MATE0 - 1000)) {
                sc += ply;
            }
            return sc;
        },
        
        /** Convert score from "mate at ply" to "mate in x", and store in hash entry. */
        /*public*/ setScore: function(/*int*/ score, /*int*/ ply) {
            if (score > Search.MATE0 - 1000) {
                score += ply;
            } else if (score < -(Search.MATE0 - 1000)) {
                score -= ply;
            }
            this.score = score;
        },

        /** Get depth from the hash entry. */
        /*public*/ /*int*/ getDepth: function() {
            return this.depthSlot & 0x7fff;
        },

        /** Set depth. */
        /*public*/ setDepth: function(/*int*/ d) {
            this.depthSlot &= 0x8000;
            this.depthSlot |= d & 0x7fff;
        },

        /*int*/ getHashSlot: function() {
            return this.depthSlot >>> 15;
        },

        /*public*/ setHashSlot: function(/*int*/ s) {
            this.depthSlot &= 0x7fff;
            this.depthSlot |= (s << 15);
        }
    };

/*public*/ /*class*/ TranspositionTable = {

    /*public*/ clone: function () { return new clone(this) },

    /*TTEntry[]*/ table:[],
    /*TTEntry*/ emptySlot: TTEntry.clone(),
    /*byte*/ generation:0,
    /*long*/ stores:0,
    /*long*/ hits:0,

    /*public*/ insert: function(/*long*/ key, /*Move*/ sm, /*int*/ Stype, /*int*/ ply,
                 /*int*/ depth, /*int*/ evalScore) {
        if (depth < 0) depth = 0;
        var /*int*/ idx0 = this.h0(key);
        var /*int*/ idx1 = this.h1(key);
        var /*TTEntry*/ ent = this.table[idx0];
        var /*byte*/ hashSlot = 0;
        if (ent.key != key) {
            ent = this.table[idx1];
            hashSlot = 1;
            this.stores++;
        }
        if (ent.key != key) {
            if (this.table[idx1].betterThan(this.table[idx0], this.generation)) {
                ent = this.table[idx0];
                hashSlot = 0;
                this.stores++;
            }
            if (ent.valuable(this.generation)) {
                var /*int*/ altEntIdx = (ent.getHashSlot() == 0) ? this.h1(ent.key) : this.h0(ent.key);
                if (ent.betterThan(this.table[altEntIdx], this.generation)) {
                    var /*TTEntry*/ altEnt = this.table[altEntIdx];
                    altEnt.key = ent.key;
                    altEnt.move = ent.move;
                    altEnt.score = ent.score;
                    altEnt.depthSlot = ent.depthSlot;
                    altEnt.generation = ent.generation;
                    altEnt.Stype = ent.Stype;
                    altEnt.setHashSlot(1 - ent.getHashSlot());
                    altEnt.evalScore = ent.evalScore;
                }
            }
        }
        var /*boolean*/  doStore = true;
        if ((ent.key == key) && (ent.getDepth() > depth) && (ent.Stype == Stype)) {
            if (Stype == TTEntry.T_EXACT) {
                doStore = false;
            } else if ((Stype == TTEntry.T_GE) && (sm.score <= ent.score)) {
                doStore = false;
            } else if ((Stype == TTEntry.T_LE) && (sm.score >= ent.score)) {
                doStore = false;
            }
        }
        if (doStore) {
            if ((ent.key != key) || (sm.from != sm.to))
                ent.setMove(sm);
            ent.key = key;
            ent.setScore(sm.score, ply);
            ent.setDepth(depth);
            ent.generation = this.generation;
            ent.Stype = Stype;
            ent.setHashSlot(hashSlot);
            ent.evalScore = evalScore;
        }
    },

    /** Retrieve an entry from the hash table corresponding to "pos". */
    /*public*/ /*TTEntry*/ probe: function(/*long*/ key) {
        var /*int*/ idx0 = this.h0(key);
        var /*TTEntry*/ ent = this.table[idx0];
        if (ent.key == key)
            {
             this.hits++;
             return ent;
            }
        var /*int*/ idx1 = this.h1(key);
        ent = this.table[idx1];
        if (ent.key == key)
            {
             this.hits++;
             return ent;
            }
        return this.emptySlot;
    },

    /**
     * Increase hash table generation. This means that subsequent inserts will be considered
     * more valuable than the entries currently present in the hash table.
     */
    /*public*/ nextGeneration: function() {  this.generation++; },

    /** Clear the transposition table. */
    /*public*/ clearTable: function() {
        //for(var i in this.table) this.table[i].Stype = TTEntry.T_EMPTY;
        this.table = [];
        this.stores = 0;
        this.hits = 0;

    },

    /**
     * Extract a list of PV moves, starting from "rootPos" and first move "mv".
     */
    /*public*/ /*ArrayList<Move>*/ extractPVMoves: function(/*Position*/ rootPos, /*Move*/ mv) {
        var /*Position*/ pos = rootPos.clone();
        var m = new Move( mv.from, mv.to, mv.promoteTo, mv.score );
        var /*ArrayList<Move>*/ ret = [];   /*new ArrayList<Move>() */
        var /*UndoInfo*/ ui = new UndoInfo();
        var /*List<Long>*/ hashHistory = [];    /* new ArrayList<Long>() */
        while (true) {
            ret.push(m);
            pos.makeMove(m, ui);

            if (ArrContains( hashHistory, pos.zobristHash())) break;

            hashHistory.push(pos.zobristHash());
            var /*TTEntry*/ ent = this.probe(pos.historyHash());
            if (ent.Stype == TTEntry.T_EMPTY) break;
            m = new Move(0,0,0,0);
            ent.getMove(m);
            
            var /*MoveGen.MoveList*/ moves = MoveGen.pseudoLegalMoves(pos);
            MoveGen.removeIllegal(pos, moves);      

            var /*boolean*/ containz = false;
            for (var /*int*/ mi = 0; mi < moves.size; mi++)
                if (equalsMove(moves.m[mi],m)) {
                    containz = true;
                    break;
                }
            if (!containz) break;
        }
        return ret;
    },

    /** Extract the PV starting from pos, using hash entries, both exact scores and bounds. */
    /*public*/ /*String*/ extractPV: function(/*Position*/ posx) {
        var ret = "";
        var pos = posx.clone();    // To afunction modifying the input parameter
        var /*boolean*/  first = true;
        var /*TTEntry*/ ent = this.probe(pos.historyHash());
        var /*UndoInfo*/ ui = new UndoInfo();
        var /*ArrayList<Long>*/ hashHistory = [];   /*new ArrayList<Long>()*/
        var /*boolean*/  repetition = false;
        while (ent.Stype != TTEntry.T_EMPTY) {
            var /*String*/ Ztype = "";
            if (ent.Stype == TTEntry.T_LE) {
                Ztype = "<";
            } else if (ent.Stype == TTEntry.T_GE) {
                Ztype = ">";
            }
            var /*Move*/ m = new Move(0,0,0,0);
            ent.getMove(m);
            var /*MoveGen.MoveList*/ moves = MoveGen.pseudoLegalMoves(pos);
            MoveGen.removeIllegal(pos, moves);
            var /*boolean*/  containz = false;
            for (var/*int*/ mi = 0; mi < moves.size; mi++)
                if (equalsMove(moves.m[mi],m)) {
                    containz = true;
                    break;
                }
            if  (!containz) break;
            var /*String*/ moveStr = TextIO.moveToString(pos, m, false);
            if (repetition) break;
            if (!first) ret+=" ";
            ret += Ztype + moveStr;
            pos.makeMove(m, ui);

            if (ArrContains( hashHistory, pos.zobristHash())) repetition = true;

            hashHistory.push(pos.zobristHash());
            ent = this.probe(pos.historyHash());
            first = false;
        }
        return ret;
    },

    /** Print hash table statistics. */
    /*public*/ printStats: function() {
        var /*int*/ unused = 0;
        var /*int*/ thisGen = 0;
        var /*int*/ maxDepth = 20;
        var /*List<Integer>*/ depHist = e0a.slice(0,maxDepth);     /*new ArrayList<Integer>()*/
        var s="Hash stats ";
        s+="stores: " + this.stores.toString();
        s+=",hits: " + this.hits.toString();
        printf("%s \n", s);
    },
    
    /*private*/ /*int*/ h0: function(/*long*/ key) {
        var tk = (key >>> 0) & 0xFFFFFF;
        if(typeof(this.table[tk]) == "undefined") this.table[tk]= TTEntry.clone();
        return tk;
    },
    
    /*private*/ /*int*/ h1: function(/*long*/ key) {
        var tk = (key >>> 1) & 0xFFFFFF;
        if(typeof(this.table[tk]) == "undefined") this.table[tk]= TTEntry.clone();
        return tk;
    }
};

/*=====================================  TextIO */

/*public*/ /*class*/ TextIO = {
    /*public*/ /*String*/ startPosFEN: "rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1",

    /** Parse a FEN string and return a chess Position object. */
    /*public*/ /*Position*/ readFEN: function(/*String*/ fen) /*throws ChessParseError*/ {
        var /*Position*/ pos = Position.clone();
       	if(fen==null || fen.length==0) fen=TextIO.startPosFEN;
        var /*String[]*/ words = fen.split(" ");
        if (words.length < 2) {
            /*throw new*/ ChessParseError("Too few spaces");
        }
        
        // Piece placement
        var /*int*/ row = 7;
        var /*int*/ col = 0;
	var g = words[0].split('');
        for (var /*int*/ i in g) {
            var /*char*/ c = g[i];
            switch (c) {
                case '1': col += 1; break;
                case '2': col += 2; break;
                case '3': col += 3; break;
                case '4': col += 4; break;
                case '5': col += 5; break;
                case '6': col += 6; break;
                case '7': col += 7; break;
                case '8': col += 8; break;
                case '/': row--; col = 0; break;
                case 'P': TextIO.safeSetPiece(pos, col, row, Piece.WPAWN);   col++; break;
                case 'N': TextIO.safeSetPiece(pos, col, row, Piece.WKNIGHT); col++; break;
                case 'B': TextIO.safeSetPiece(pos, col, row, Piece.WBISHOP); col++; break;
                case 'R': TextIO.safeSetPiece(pos, col, row, Piece.WROOK);   col++; break;
                case 'Q': TextIO.safeSetPiece(pos, col, row, Piece.WQUEEN);  col++; break;
                case 'K': TextIO.safeSetPiece(pos, col, row, Piece.WKING);   col++; break;
                case 'p': TextIO.safeSetPiece(pos, col, row, Piece.BPAWN);   col++; break;
                case 'n': TextIO.safeSetPiece(pos, col, row, Piece.BKNIGHT); col++; break;
                case 'b': TextIO.safeSetPiece(pos, col, row, Piece.BBISHOP); col++; break;
                case 'r': TextIO.safeSetPiece(pos, col, row, Piece.BROOK);   col++; break;
                case 'q': TextIO.safeSetPiece(pos, col, row, Piece.BQUEEN);  col++; break;
                case 'k': TextIO.safeSetPiece(pos, col, row, Piece.BKING);   col++; break;
                default: /*throw new*/ ChessParseError("Invalid piece");
            }
        }
        if (words[1].length == 0) {
            /*throw new*/ ChessParseError("Invalid side");
        }
        pos.setWhiteMove(words[1].charAt(0) == 'w');

        // Castling rights
        var /*int*/ castleMask = 0;
        if (words.length > 2) {
            var q = words[2].split('');
            for (var /*int*/ i in q) {
                var /*char*/ c = q[i];
                switch (c) {
                    case 'K':
                        castleMask |= (Position.H1_CASTLE);
                        break;
                    case 'Q':
                        castleMask |= (Position.A1_CASTLE);
                        break;
                    case 'k':
                        castleMask |= (Position.H8_CASTLE);
                        break;
                    case 'q':
                        castleMask |= (Position.A8_CASTLE);
                        break;
                    case '-':
                        break;
                    default:
                        /*throw new*/ ChessParseError("Invalid castling flags");
                }
            }
        }
        pos.setCastleMask(castleMask);

        if (words.length > 3) {
            // En passant target square
            var /*String*/ epString = words[3];
            if (!(epString=="-")) {
                if (epString.length < 2) {
                    /*throw new*/ ChessParseError("Invalid en passant square");
                }
                pos.setEpSquare(getSquare(epString));
            }
        }

        try {
            if (words.length > 4) {
                pos.halfMoveClock = parseInt(words[4]);
            }
            if (words.length > 5) {
                pos.fullMoveCounter = parseInt(words[5]);
            }
        } catch (e) {
            // Ignore errors here, since the fields are optional
        }

        // Each side must have exactly one king
        var /*int*/ wKings = 0;
        var /*int*/ bKings = 0;
        for (var /*int*/ x = 0; x < 8; x++) {
            for (var /*int*/ y = 0; y < 8; y++) {
                var /*int*/ p = pos.getPiece(Position.getSquare(x, y));
                if (p == Piece.WKING) {
                    wKings++;
                } else if (p == Piece.BKING) {
                    bKings++;
                }
            }
        }
        if (wKings != 1) {
            /*throw new*/ ChessParseError("White must have exactly one king");
        }
        if (bKings != 1) {
            /*throw new*/ ChessParseError("Black must have exactly one king");
        }


        // Make sure king can not be captured
        var /*Position*/ pos2 = pos.clone();
        pos2.setWhiteMove(!pos.whiteMove);
        
        if (MoveGen.inCheck(pos2)) {
            /*throw new*/ ChessParseError("King capture possible");
        }

        TextIO.fixupEPSquare(pos);

        return pos;
    },

    /** Remove pseudo-legal EP square if it is not legal, ie would leave king in check. */
    /*public*/  fixupEPSquare: function(/*Position*/ pos) {
        var /*int*/ epSquare = pos.getEpSquare();
        if (epSquare >= 0) {
                var /*MoveGen.MoveList*/ moves = MoveGen.pseudoLegalMoves(pos);
                MoveGen.removeIllegal(pos, moves);      
                var /*boolean*/ epValid = false;
                for (var/*int*/ mi = 0; mi < moves.size; mi++) {
                    var /*Move*/ m = moves.m[mi];
                    if (m.to == epSquare) {
                        if (pos.getPiece(m.from) == (pos.whiteMove ? Piece.WPAWN : Piece.BPAWN)) {
                            epValid = true;
                            break;
                        }
                    }
                }
                if (!epValid) pos.setEpSquare(-1);
        }
    },

    /*private*/ safeSetPiece: function(/*Position*/ pos, /*int*/ col, /*int*/ row, /*int*/ p) /*throws ChessParseError*/ {
        if (row < 0) /*throw new*/ ChessParseError("Too many rows");
        if (col > 7) /*throw new*/ ChessParseError("Too many columns");
        if ((p == Piece.WPAWN) || (p == Piece.BPAWN)) {
            if ((row == 0) || (row == 7))
                /*throw new*/ ChessParseError("Pawn on first/last rank");
        }
        pos.setPiece(Position.getSquare(col, row), p);
    },
    
    /** Return a FEN string corresponding to a chess Position object. */
    /*public*/ /*String*/ toFEN: function(/*Position*/ pos) {
        var ret = "";
        // Piece placement
        for (var /*int*/ r = 7; r >=0; r--) {
            var /*int*/ numEmpty = 0;
            for (/*int*/ c = 0; c < 8; c++) {
                var /*int*/ p = pos.getPiece(Position.getSquare(c, r));
                if (p == Piece.EMPTY) {
                    numEmpty++;
                } else {
                    if (numEmpty > 0) {
                        ret+=numEmpty.toString();
                        numEmpty = 0;
                    }
                    switch (p) {
                        case Piece.WKING:   ret+=('K'); break;
                        case Piece.WQUEEN:  ret+=('Q'); break;
                        case Piece.WROOK:   ret+=('R'); break;
                        case Piece.WBISHOP: ret+=('B'); break;
                        case Piece.WKNIGHT: ret+=('N'); break;
                        case Piece.WPAWN:   ret+=('P'); break;
                        case Piece.BKING:   ret+=('k'); break;
                        case Piece.BQUEEN:  ret+=('q'); break;
                        case Piece.BROOK:   ret+=('r'); break;
                        case Piece.BBISHOP: ret+=('b'); break;
                        case Piece.BKNIGHT: ret+=('n'); break;
                        case Piece.BPAWN:   ret+=('p'); break;
                        default: /*throw new*/ RuntimeException();
                    }
                }
            }
            if (numEmpty > 0) {
                ret+=numEmpty.toString();
            }
            if (r > 0) {
                ret+=('/');
            }
        }
        ret+=(pos.whiteMove ? " w " : " b ");

        // Castling rights
        var /*boolean*/  anyCastle = false;
        if (pos.h1Castle()) {
            ret+=('K');
            anyCastle = true;
        }
        if (pos.a1Castle()) {
            ret+=('Q');
            anyCastle = true;
        }
        if (pos.h8Castle()) {
            ret+=('k');
            anyCastle = true;
        }
        if (pos.a8Castle()) {
            ret+=('q');
            anyCastle = true;
        }
        if (!anyCastle) {
            ret+=('-');
        }
        
        // En passant target square
        {
            ret+=(' ');
            if (pos.getEpSquare() >= 0) {
                var /*int*/ x = Position.getX(pos.getEpSquare());
                var /*int*/ y = Position.getY(pos.getEpSquare());
                ret+=String.fromCharCode(97 + x);
                ret+=String.fromCharCode(49 + y);
            } else {
                ret+=('-');
            }
        }

        // Move counters
        ret+=(' ');
        ret+=pos.halfMoveClock.toString();
        ret+=(' ');
        ret+=pos.fullMoveCounter.toString();

        return ret;
    },
    
    /**
     * Convert a chess move to human readable form.
     * @param pos      The chess position.
     * @param move     The executed move.
     * @param longForm If true, use long notation, eg Ng1-f3.
     *                 Otherwise, use short notation, eg Nf3
     */
    /*public*/ /*String*/ moveToString: function(/*Position*/ pos, /*Move*/ move, /*boolean*/  longForm) {
        var /*MoveGen.MoveList*/ moves = MoveGen.pseudoLegalMoves(pos);
        MoveGen.removeIllegal(pos, moves);
        return this.moveGToString(pos, move, longForm, moves);
    },
    
    /*private*/ /*String*/ moveGToString: function(/*Position*/ pos, /*Move*/ move, /*boolean*/  longForm, /*MoveGen.MoveList*/ moves) {
        var ret = "";
        var /*int*/ wKingOrigPos = Position.getSquare(4, 0);
        var /*int*/ bKingOrigPos = Position.getSquare(4, 7);
        if (move.from == wKingOrigPos && pos.getPiece(wKingOrigPos) == Piece.WKING) {
            // Check white castle
            if (move.to == Position.getSquare(6, 0)) {
                    ret+=("O-O");
            } else if (move.to == Position.getSquare(2, 0)) {
                ret+=("O-O-O");
            }
        } else if (move.from == bKingOrigPos && pos.getPiece(bKingOrigPos) == Piece.BKING) {
            // Check white castle
            if (move.to == Position.getSquare(6, 7)) {
                ret+=("O-O");
            } else if (move.to == Position.getSquare(2, 7)) {
                ret+=("O-O-O");
            }
        }
        if (ret.length == 0) {
            var /*int*/ p = pos.getPiece(move.from);
            ret+=this.pieceToChar(p);
            var /*int*/ x1 = Position.getX(move.from);
            var /*int*/ y1 = Position.getY(move.from);
            var /*int*/ x2 = Position.getX(move.to);
            var /*int*/ y2 = Position.getY(move.to);
            if (longForm) {
                ret+=String.fromCharCode(97 + x1);
                ret+=String.fromCharCode(49 + y1);
                ret+=(this.isCapture(pos, move) ? 'x' : '-');
            } else {
                if (p == (pos.whiteMove ? Piece.WPAWN : Piece.BPAWN)) {
                    if (this.isCapture(pos, move)) {
                        ret+=String.fromCharCode(97 + x1);
                    }
                } else {
                    var /*int*/ numSameTarget = 0;
                    var /*int*/ numSameFile = 0;
                    var /*int*/ numSameRow = 0;
                    for (var/*int*/ mi = 0; mi < moves.size; mi++) {
                        var /*Move*/ m = moves.m[mi];
                        if (m == null) break;
                        if ((pos.getPiece(m.from) == p) && (m.to == move.to)) {
                            numSameTarget++;
                            if (Position.getX(m.from) == x1)
                                numSameFile++;
                            if (Position.getY(m.from) == y1)
                                numSameRow++;
                        }
                    }
                    if (numSameTarget < 2) {
                        // No file/row info needed
                    } else if (numSameFile < 2) {
                        ret+=String.fromCharCode(97 + x1);   // Only file info needed
                    } else if (numSameRow < 2) {
                        ret+=String.fromCharCode(49 + y1);   // Only row info needed
                    } else {
                        ret+=String.fromCharCode(97 + x1);
                        ret+=String.fromCharCode(49 + y1);
                    }
                }
                if (this.isCapture(pos, move)) {
                    ret+='x';
                }
            }
            ret+=String.fromCharCode(97 + x2);
            ret+=String.fromCharCode(49 + y2);
            if (move.promoteTo != Piece.EMPTY) {
                ret+="="+this.pieceToChar(move.promoteTo);
            }
        }
        var /*UndoInfo*/ ui = new UndoInfo();

        if (MoveGen.givesCheck(pos, move)) {
            pos.makeMove(move, ui);
            var /*MoveGen.MoveList*/ nextMoves = MoveGen.pseudoLegalMoves(pos);
            MoveGen.removeIllegal(pos, nextMoves);
            if (nextMoves.size == 0) ret+=('#');
            else ret+=('+');
            pos.unMakeMove(move, ui);
        }

        return ret;
    },

    /** Convert a move object to UCI string format. */
    /*public*/ /*String*/ moveToUCIString: function(/*Move*/ m) {
        var /*String*/ ret = this.squareToString(m.from) + this.squareToString(m.to);
        var p = m.promoteTo;   var i=(p>6 ? p-6 : p);
        return ret + (i<2 ? "" : ("  qrbn").charAt(i) );
    },

    /*String*/ dispMoves: function(/*Position*/ pos ) {
        var /*MoveGen.MoveList*/ moves = MoveGen.pseudoLegalMoves(pos);
        MoveGen.removeIllegal(pos, moves);
        var ret = "";
        for(var i=0;i<moves.size;i++) ret += (i>0?",": "") + this.moveGToString(pos, moves.m[i],
		 false /*in short format*/, moves);
        return ret;        
    },
    
    /**
     * Convert a string to a Move object.
     * @return A move object, or null if move has invalid syntax
     */
    /*public*/ /*Move*/ uciStringToMove: function(/*String*/ move) {
        var /*Move*/ m = null;
        if ((move.length < 4) || (move.length > 5)) return m;
        var /*int*/ fromSq = TextIO.getSquare(move.substr(0, 2));
        var /*int*/ toSq   = TextIO.getSquare(move.substr(2, 4));
        if ((fromSq < 0) || (toSq < 0)) return m;
        var /*char*/ prom = ' ';
        var /*boolean*/ white = true;
        if (move.length == 5) {
            prom = move.charAt(4);
            if (Position.getY(toSq) == 7) {
                white = true;
            } else if (Position.getY(toSq) == 0) {
                white = false;
            } else {
                return m;
            }
        }
        
        var /*int*/ promoteTo = 0;
        switch (prom) {
            case ' ':
                promoteTo = Piece.EMPTY;
                break;
            case 'q':
                promoteTo = white ? Piece.WQUEEN : Piece.BQUEEN;
                break;
            case 'r':
                promoteTo = white ? Piece.WROOK : Piece.BROOK;
                break;
            case 'b':
                promoteTo = white ? Piece.WBISHOP : Piece.BBISHOP;
                break;
            case 'n':
                promoteTo = white ? Piece.WKNIGHT : Piece.BKNIGHT;
                break;
            default:
                return m;
        }
        m = new Move(fromSq, toSq, promoteTo, 0);
        return m;
    },

    /*private*/ /*boolean*/  isCapture: function(/*Position*/ pos, /*Move*/ move) {
        if (pos.getPiece(move.to) == Piece.EMPTY) {
            var /*int*/ p = pos.getPiece(move.from);
            return ((p == (pos.whiteMove ? Piece.WPAWN : Piece.BPAWN)) && (move.to == pos.getEpSquare()));
        } else {
            return true;
        }
    },

    /**
     * Convert a chess move string to a Move object.
     * Just verifies UCI move.
     */
    /*public*/ /*Move*/ stringToMove: function(/*Position*/ pos, /*String*/ sMove) {
        var m = this.uciStringToMove( sMove );
        var /*MoveGen.MoveList*/ moves = MoveGen.pseudoLegalMoves(pos);
        MoveGen.removeIllegal(pos, moves);
        for (var /*int*/ mi = 0; mi < moves.size; mi++)
              if( equalsMove( m, moves.m[mi] )) return m;
        return null;
    },

    /**
     * Convert a string, such as "e4" to a square number.
     * @return The square number, or -1 if not a legal square.
     */
    /*public*/ /*int*/ getSquare: function(/*String*/ s) {
        var /*int*/ x = s.charCodeAt(0) - 97;
        var /*int*/ y = s.charCodeAt(1) - 49;
        if ((x < 0) || (x > 7) || (y < 0) || (y > 7)) return -1;
        return Position.getSquare(x, y);
    },

    /**
     * Convert a square number to a string, such as "e4".
     */
    /*public*/ /*String*/ squareToString: function(/*int*/ square) {
        var ret = "";
        var /*int*/ x = Position.getX(square);
        var /*int*/ y = Position.getY(square);
        ret+=String.fromCharCode(97 + x);
        ret+=String.fromCharCode(49 + y);
        return ret;
    },

    /**
     * Create a representation of a position.
     * in a DIV container as a table
     */
    /*public*/ /*String*/ asciiBoard: function(/*Position*/ pos) {
        var ret = ''; var nl = '<br>';
        var ln = "    +----+----+----+----+----+----+----+----+";
        ret+=ln+nl;
        for (var /*int*/ y = 7; y >= 0; y--) {
            ret+="    |";
            for (var /*int*/ x = 0; x < 8; x++) {
                var /*int*/ p = pos.getPiece(Position.getSquare(x, y));
                var /*boolean*/  dark = Position.darkSquare(x, y);
                if (p == Piece.EMPTY) {
                    ret+=(dark ? "////" : "....") + "|";
                } else {
                    ret+=(dark ? "/" : ".") + (Piece.isWhite(p) ? 'w' : 'b');
                    var /*String*/ pieceName = TextIO.pieceToChar(p);
                    if (pieceName.length == 0) pieceName = "P";
                    ret+=pieceName;
                    ret+=(dark ? "/" : ".") + "|";
                }
            }
            ret+=nl+ln+nl;
        }
        document.getElementById("div-board").innerHTML = ret;
    },

    /**
     * Convert move String to lower case and remove special check/mate symbols.
     */
    /*private*/ /*String*/ normalizeMoveString: function(/*String*/ str) {
        if (str.length > 0) {
            var /*char*/ lastchar = str.charAt(str.length - 1);
            if ((lastchar == '#') || (lastchar == '+')) {
                str = str.toLowerCase().substr(0, str.length - 1);
            }
        }
        return str;
    },
    
    /*private*/ /*String*/ pieceToChar: function(/*int*/ p) {
        var i=(p>6 ? p-6 : p);
        return (i==6 ? "" : (" KQRBN").charAt(i) );
    }
};



/*------------------------------------- Javascript helping functions ---------------*/

function isdigit(s)
 {
  return ( (s>="0" && s<="9") );
 }

function isalpha(s)
 {
  return ( (s>="a" && s<="z") || (s>="A" && s<="Z") );
 }

/* Javascript printf (source: http://www.webtoolkit.info ) */

sprintfWrapper = {
 
	init : function () {
 
		if (typeof arguments == "undefined") { return null; }
		if (arguments.length < 1) { return null; }
		if (typeof arguments[0] != "string") { return null; }
		if (typeof RegExp == "undefined") { return null; }
		if (arguments.length == 1)
		 {
		  outp_to_div(arguments[0]);
		  return arguments[0];
		 }
 
		var string = arguments[0];
		var exp = new RegExp(/(%([%]|(\-)?(\+|\x20)?(0)?(\d+)?(\.(\d)?)?([bcdfosxX])))/g);
		var matches = new Array();
		var strings = new Array();
		var convCount = 0;
		var stringPosStart = 0;
		var stringPosEnd = 0;
		var matchPosEnd = 0;
		var newString = '';
		var match = null;
 
		while (match = exp.exec(string)) {
			if (match[9]) { convCount += 1; }
 
			stringPosStart = matchPosEnd;
			stringPosEnd = exp.lastIndex - match[0].length;
			strings[strings.length] = string.substr(stringPosStart, stringPosEnd);
 
			matchPosEnd = exp.lastIndex;
			matches[matches.length] = {
				match: match[0],
				left: match[3] ? true : false,
				sign: match[4] || '',
				pad: match[5] || ' ',
				min: match[6] || 0,
				precision: match[8],
				code: match[9] || '%',
				negative: parseInt(arguments[convCount]) < 0 ? true : false,
				argument: String(arguments[convCount])
			};
		}
		strings[strings.length] = string.substr(matchPosEnd);
 
		if (matches.length == 0) { return string; }
		if ((arguments.length - 1) < convCount) { return null; }
 
		var code = null;
		var match = null;
		var i = null;
 
		for (i=0; i<matches.length; i++) {
 
			if (matches[i].code == '%') { substitution = '%' }
			else if (matches[i].code == 'b') {
				matches[i].argument = String(Math.abs(parseInt(matches[i].argument)).toString(2));
				substitution = sprintfWrapper.convert(matches[i], true);
			}
			else if (matches[i].code == 'c') {
				matches[i].argument = String(String.fromCharCode(parseInt(Math.abs(parseInt(matches[i].argument)))));
				substitution = sprintfWrapper.convert(matches[i], true);
			}
			else if (matches[i].code == 'd') {
				matches[i].argument = String(Math.abs(parseInt(matches[i].argument)));
				substitution = sprintfWrapper.convert(matches[i]);
			}
			else if (matches[i].code == 'f') {
				matches[i].argument = String(Math.abs(parseFloat(matches[i].argument)).toFixed(matches[i].precision ? matches[i].precision : 6));
				substitution = sprintfWrapper.convert(matches[i]);
			}
			else if (matches[i].code == 'o') {
				matches[i].argument = String(Math.abs(parseInt(matches[i].argument)).toString(8));
				substitution = sprintfWrapper.convert(matches[i]);
			}
			else if (matches[i].code == 's') {
				matches[i].argument = matches[i].argument.substr(0, matches[i].precision ? matches[i].precision : matches[i].argument.length);
				substitution = sprintfWrapper.convert(matches[i], true);
			}
			else if (matches[i].code == 'x') {
				matches[i].argument = String(Math.abs(parseInt(matches[i].argument)).toString(16));
				substitution = sprintfWrapper.convert(matches[i]);
			}
			else if (matches[i].code == 'X') {
				matches[i].argument = String(Math.abs(parseInt(matches[i].argument)).toString(16));
				substitution = sprintfWrapper.convert(matches[i]).toUpperCase();
			}
			else {
				substitution = matches[i].match;
			}
 
			newString += strings[i];
			newString += substitution;
 
		}
		newString += strings[i];

		outp_to_div(newString);
 
		return newString;
 
	},
 
	convert : function(match, nosign){
		if (nosign) {
			match.sign = '';
		} else {
			match.sign = match.negative ? '-' : match.sign;
		}
		var l = match.min - match.argument.length + 1 - match.sign.length;
		var pad = new Array(l < 0 ? 0 : l).join(match.pad);
		if (!match.left) {
			if (match.pad == "0" || nosign) {
				return match.sign + pad + match.argument;
			} else {
				return pad + match.sign + match.argument;
			}
		} else {
			if (match.pad == "0" || nosign) {
				return match.sign + match.argument + pad.replace(/0/g, ' ');
			} else {
				return match.sign + match.argument + pad;
			}
		}
	}
};
 
sprintf = sprintfWrapper.init;			// print to string

printf = sprintfWrapper.init;			// print to div the same

DIVprint = true;

function outp_to_div(s)
{
  if(DIVprint)
  {
    s = s.replace( '\n', '<br>' );
    s = s.replace( '\0', '' );

    var odiv = document.getElementById('div-output');	// output DIV element to see printing
    if(!(odiv == null))
	{
	 var sh = odiv.innerHTML + s;
	 if(sh.length>3000)
		{
		 sh = sh.substr(1000);
		 var i = sh.indexOf("<br>");
		 if(i>=0) sh=sh.substr(i+4);
		}
	 odiv.innerHTML = sh;
	}
  }

}

function sscanf (str, format) {
    // original by: Brett Zamir (http://brett-zamir.me)

    var retArr = [], num = 0, _NWS = /\S/,  args = arguments, digit;

    var _setExtraConversionSpecs = function (offset) {
       var matches = format.slice(offset).match(/%[cdeEufgosxX]/g);
       if (matches) {
            var lgth = matches.length;
            while (lgth--) {
                retArr.push(null);
            }
        }
        return _finish();
    };

    var _finish = function () {
        if (args.length === 2) {
            return retArr;
        }
        for (var i in retArr) {
            this.window[args[i + 2]] = retArr[i];
        }
        return i;
    };

    var _addNext = function (j, regex, cb) {
        if (assign) {
            var remaining = str.slice(j);
            var check = width ? remaining.substr(0, width) : remaining;
            var match = regex.exec(check);
            var testNull = retArr[digit !== undefined ? digit : retArr.length] = match ? (cb ? cb.apply(null, match) : match[0]) : null;
            return j + match[0].length;
        }
        return j;
    };


    for (var i = 0, j = 0; i < format.length; i++) {

        var width = 0,
            assign = true;

        if (format.charAt(i) === '%') {
            if (format.charAt(i + 1) === '%') {
                if (str.charAt(j) === '%') {
                    ++i, ++j;
                    continue;
                }
                return _setExtraConversionSpecs(i + 2);
            }

            var prePattern = new RegExp('^(?:(\\d+)\\$)?(\\*)?(\\d*)([hlL]?)', 'g');

            var preConvs = prePattern.exec(format.slice(i + 1));

            var tmpDigit = digit;

            digit = preConvs[1] ? parseInt(preConvs[1], 10) - 1 : undefined;

            assign = !preConvs[2];
            width = parseInt(preConvs[3], 10);
            var sizeCode = preConvs[4];
            i += prePattern.lastIndex;

            try {
                switch (format.charAt(i + 1)) {
                case 'i':
                    // Integer with base detection
                    j = _addNext(j, /([+-])?(?:(?:0x([\da-fA-F]+))|(?:0([0-7]+))|(\d+))/, function (num, sign, hex, oct, dec) {
                        return hex ? parseInt(num, 16) : oct ? parseInt(num, 8) : parseInt(num, 10);
                    });
                    break;
                case 'n':
                    // Number of characters processed so far
                    retArr[digit !== undefined ? digit : retArr.length - 1] = j;
                    break;
                case 'c':
                    // Get character
                    j = _addNext(j, new RegExp('.{1,' + (width || 1) + '}'));
                    break;
                case 'd':
                    // decimal integer
                    j = _addNext(j, /([+-])?(?:0*)(\d+)/, function (num, sign, dec) {
                        var decInt = parseInt((sign || '') + dec, 10);
			return decInt;
                    });
                    break;
                case 'f':
                    j = _addNext(j, /([+-])?(?:0*)(\d*\.?\d*(?:[eE]?\d+)?)/, function (num, sign, dec) {
                        if (dec === '.') {
                            return 0;
                        }
                        return parseFloat((sign || '') + dec);
                    });
                    break;
                case 'u':
                    // unsigned decimal integer
                    j = _addNext(j, /([+-])?(?:0*)(\d+)/, function (num, sign, dec) {
                        var decInt = parseInt(dec, 10);
			return decInt;
                    });
                    break;
                case 'o':
                    // Octal integer
                    j = _addNext(j, /([+-])?(?:0([0-7]+))/, function (num, sign, oct) {
                        return parseInt(num, 8);
                    });
                    break;
                case 's':
                    // Greedy match
                    j = _addNext(j, /\S+/);
                    break;
                case 'X':
                    // Same as 'x'?
                case 'x':
                    j = _addNext(j, /([+-])?(?:(?:0x)?([\da-fA-F]+))/, function (num, sign, hex) {
                        return parseInt(num, 16);
                    });
                    break;
                default:

                }
            } catch (e) {
                if (e === 'No match in string') {
                    return _setExtraConversionSpecs(i + 2);
                }
            }++i;
        } else if (format.charAt(i) !== str.charAt(j)) {

            _NWS.lastIndex = 0;
            if ((_NWS).test(str.charAt(j)) || str.charAt(j) === '') {
                return _setExtraConversionSpecs(i + 1);
            } else {
                str = str.slice(0, j) + str.slice(j + 1);
                i--;
            }
        } else {
            j++;
        }
    }

    return _finish();
}


function rtime()
{
 return ( (new Date()).getTime() );
}

/* time difference */
function rdifftime(t1,t2)
{
 return (t1-t2);
}

//--------------------------------------
// SCRIPTS for int 64-bit cases
//--------------------------------------

cs64 = { MIN_VALUE:i64(), MAX_VALUE:i64_ax(0xFFFFFFFF,0xFFFFFFFF) };

	//powers of 10 in 64 bits	
pw10=[ i64_ax(0,0x1),
i64_ax(0,0xA),
i64_ax(0,0x64),
i64_ax(0,0x3E8),
i64_ax(0,0x2710),
i64_ax(0,0x186A0),
i64_ax(0,0xF4240),
i64_ax(0,0x989680),
i64_ax(0,0x5F5E100),
i64_ax(0,0x3B9ACA00),
i64_ax(0x2,0x540BE400),
i64_ax(0x17,0x4876E800),
i64_ax(0xE8,0xD4A51000),
i64_ax(0x918,0x4E72A000),
i64_ax(0x5AF3,0x107A4000),
i64_ax(0x38D7E,0xA4C68000),
i64_ax(0x2386F2,0x6FC10000),
i64_ax(0x1634578,0x5D8A0000),
i64_ax(0xDE0B6B3,0xA7640000),
i64_ax(0x8AC72304,0x89E80000) ];

/* Define */

function i64() { return { h:0, l:0 } }	// constructor
function i64_al(v) { return { h:0, l:v } }	// +assign 32-bit value
function i64_ax(h,l) { return { h:h, l:l } }	// +assign 64-bit v.as 2 regs
function i64v(v) { return { h:0, l:v } }	// +assign 32-bit value
function i64c(a) { return { h:a.h, l:a.l } }	// clone
function i64s(s)
 {
  var a=s.indexOf("0x"); if(a<0) return "input error";
  return { h: parseInt("0x"+s.substr(a+2,8)), l: parseInt("0x"+s.substr(a+10,8)) }	// +assign by string
 }
function i64_clone(a) { return { h:a.h, l:a.l } }	// clone object
function i64_fromString(s)
 {
  var a=s.indexOf("0x"); if(a<0) return "input error";
  return { h: parseInt("0x"+s.substr(a+2,8)), l: parseInt("0x"+s.substr(a+10,8)) }	// +assign by string
 }

// originally was ((x>>>1) + (x&1) )% 0x100000000
// modulus is slow and >>>0 is faster
function i64u(x) { return ( ((x >>> 0) & 0xFFFFFFFF) >>> 0 ); }	// keeps [0..0xFFFFFFFFF] 

/* Type conversions */

function i64_toint(a)
 {	// Javascript supports value=2^53
  return (a.h>0 ? (a.l + (a.h * 0x100000000)) : a.l);
 }

function i64_toString(a)
 {
  var s1=(a.l).toString(16);
  var s2=(a.h).toString(16);
  var s3="0000000000000000";
  s3=s3.substr(0,16-s1.length)+s1;
  s3=s3.substr(0,8-s2.length)+s2+s3.substr(8);
  return "0x"+s3.toUpperCase();
  }

/* Bitwise operators (the main functionality) */

function i64_and(a,b) { return { h:( a.h & b.h )>>>0, l:( a.l & b.l )>>>0 } }
function i64_or(a,b) { return { h:( a.h | b.h )>>>0, l:( a.l | b.l )>>>0 } }
function i64_xor(a,b) { return { h:( a.h ^ b.h )>>>0, l:( a.l ^ b.l )>>>0 } }
function i64_not(a) { return { h:(~a.h)>>>0, l:(~a.l)>>>0 } }

/* Simple Math-functions */

// just to add, not rounded for overflows
function i64_add(a,b)
 { var o = { h:a.h+b.h, l:a.l+b.l }; var r = o.l-0xFFFFFFFF;
   if( r>0 ) { o.h++; o.l =--r; } return o; }

// verify a>=b before usage
function i64_sub(a,b)
 { var o = { h:a.h-b.h, l:a.l-b.l };
   if( o.l<0 ) { o.h--; o.l+=0x100000000; } return o; }

// x n-times, better not to use for large n
function i64_txmul(a,n)
{ var o = { h:a.h, l:a.l }; for(var i=1; i<n; i++ ) o = i64_add(o,a); return o; }


// multiplication arithmetic
// (it gives small mistake without checksum for too large numbers)
function i64_mul(a,b)
 {

 /*
	// slow but working checksum to get right result
 if(i64_bithighestat(a)+i64_bithighestat(b)>63)
  {
  return i64_bitmul(a,b);
  }
 */

  var o = { h:(a.h * b.l) + (b.h * a.l), l:(a.l * b.l) };

  var o3 = (o.h & 0xFFFFFFFF)>>>0;	// to see how
  var o4 = ((o.h+1) & 0xFFFFFFFF)>>>0;	// it changes
  if( o3==o4 ) return i64_bitmul(a,b);	//fast checksum for 53-bit javascript overflow

  var oC=0;
  if( o.l>0xFFFFFFFF )
   {
    oC=Math.floor(o.l/0x100000000);
    o.h += oC;
    o.l =(o.l & 0xFFFFFFFF)>>>0;
   }
  o.h =(o.h & 0xFFFFFFFF)>>>0;

  if( (o3+oC)!=o.h ) return i64_bitmul(a,b);	// checksum

  return o;
 }

// multiplication by shifting bits, good for few-bits manipulation
// (slow)
function i64_bitmul(a,b)
 {
  var o = { h:0, l:0 };
  var m = { h:b.h, l:b.l };
  var t = { h:a.h, l:a.l };
  for(var j=63;j>0;j--)
   {
    if(!(m.l | m.h)) break; 
    if(m.l & 1) o = i64_add(o,t);
    m = i64_rshift(m,1);
    t = i64_lshift(t,1);
   }
  o.l =(o.l & 0xFFFFFFFF)>>>0;
  o.h =(o.h & 0xFFFFFFFF)>>>0;
  return o;
 }
// bitwise division calculates and returns [rs,rm],
// where a=(b*rs)+rm, better not to use for simple math
function i64_bitdiv(a,b)
 {
  var rs = { h:0,l:0 };
  var rm = { h:a.h,l:a.l };
  if( (b.l | b.h) && i64_gt(a,b) )
  {
  var d = { h:b.h, l:b.l };
  var p = { h:d.h, l:d.l };
  var y = 0;
  var w = i64_bithighestat(d);
  while((w<64) && (d.l | d.h) && i64_le(d,rm) )
   {
    p = { h:d.h, l:d.l };
    var hbit=i64_bithighestat(d);
    d = i64_lshift(d,1);
    y++; w++;
   }

  while( (y>0) || (p.l | p.h) )
   {
    rs=i64_lshift(rs,1); y--;
    if( i64_le(p,rm) ) { rm=i64_sub(rm,p); rs.l=(rs.l|1)>>>0; }
    if( (y<=0) && i64_gt(b,rm) ) break;
    p = i64_rshift(p,1);
   }
  }
  return [rs,rm];
 }

// x n-times multiplied by self, slow
function i64_txpow(a,n)
{ var o = { h:a.h, l:a.l }; for(var i=1; i<n; i++ ) o = i64_mul(o,a); return o; }


/* Bit-shifting */

function i64_lshift(a,n)
 {
  switch(n)
  {
  case 0: return { h:a.h, l:a.l };
  case 32: return { h:a.l, l:0 };
  default:
   if(n<32) return { h:((a.h << n)>>>0)+(a.l >>> (32-n)), l:(a.l << n)>>>0 };
   else return { h:(a.l << (n-32))>>>0, l:0 };
  }
 }

function i64_rshift(a,n)
 {
  switch(n)
  {
  case 0: return { h:a.h, l:a.l };
  case 32: return { h:0, l:a.h };
  default:
   if(n<32) return {  h:(a.h >>> n), l:(a.l >>> n)+(a.h << (32-n))>>>0 };
   else return { h:0, l:(a.h >>> (n-32)) };
  }
 }

// gets bit at position n
function i64_bitat(a,n) { return ( (n<32) ? (a.l & (1<<n)) : (a.h & (1<<(n-32))) ); } 

// sets bit at position n (on)
function i64_bitset(a,n)
 { if(n<32) a.l = (a.l | (1<<n))>>>0; else a.h = (a.h | (1<<(n-32)))>>>0; } 

// clears bit at position n (off)
function i64_bitclear(a,n)
 { if(n<32) a.l = (a.l & (~(1<<n)))>>>0; else a.h = (a.h & (~(1<<(n-32))))>>>0; } 

// toggles bit at position n (on/off)
function i64_bittoggle(a,n)
 { if(n<32) a.l = (a.l ^ (1<<n))>>>0; else a.h = (a.h ^ (1<<(n-32)))>>>0; } 

/* bitwise calcs just fast as possible */

// calculates count of bits =[0..63]
function i64_bitcount(a)
{

  return ( bitcnt_[ (a.l & 0xFFFF)>>>0 ] + bitcnt_[ ((a.l>>>16) & 0xFFFF)>>>0 ] +
           bitcnt_[ (a.h & 0xFFFF)>>>0 ] + bitcnt_[ ((a.h>>>16) & 0xFFFF)>>>0 ] );
}


// finds lowest bit (position [0..63] or -1)
function i64_bitlowestat(a)
{

  var q = (a.l & 0xFFFF)>>>0;
  if(q) return bitlsb_[q];
  q = ((a.l>>>16) & 0xFFFF)>>>0;
  if(q) return (bitlsb_[q] | 16);
  q = (a.h & 0xFFFF)>>>0;
  if(q) return (bitlsb_[q] | 32);
  q = ((a.h>>>16) & 0xFFFF)>>>0;
  if(q) return (bitlsb_[q] | 48);
  return -1;
}

// finds highest bit (position [0..63] or -1)
function i64_bithighestat(a)
{

  var q = ((a.h>>>16) & 0xFFFF)>>>0;
  if(q) return (bithsb_[q] | 48);
  q = (a.h & 0xFFFF)>>>0;
  if(q) return (bithsb_[q] | 32);
  q = ((a.l>>>16) & 0xFFFF)>>>0;
  if(q) return (bithsb_[q] | 16);
  var q = (a.l & 0xFFFF)>>>0;
  if(q) return bithsb_[q];
  return -1;
}

// returns object for a bit[0..63]
function i64bitObj(n)
{
 if(typeof(bitObjat_)=="undefined")
   {
   bitObjat_ = [];
   var o=i64_al(1);
   for(var i=0;i<64;i++) { bitObjat_[i]={h:o.h,l:o.l}; o=i64_lshift(o,1); }
   }
 return bitObjat_[n];
}

/* Comparisons */

function i64_eq(a,b) { return ((a.h == b.h) && (a.l == b.l)); }
function i64_ne(a,b) { return ((a.h != b.h) || (a.l != b.l)); }
function i64_gt(a,b) { return ((a.h > b.h) || ((a.h == b.h) && (a.l >  b.l))); }
function i64_ge(a,b) { return ((a.h > b.h) || ((a.h == b.h) && (a.l >= b.l))); }
function i64_lt(a,b) { return ((a.h < b.h) || ((a.h == b.h) && (a.l <  b.l))); }
function i64_le(a,b) { return ((a.h < b.h) || ((a.h == b.h) && (a.l <= b.l))); }
function i64_is0(a) { return ((a.l | a.h)==0); }	// is zero?
function i64_not0(a) { return ((a.l | a.h)!=0); }	// not zero?

/* Decimal conversions */

// converts to decimal string
function i64_todecString(a)
{
 var s=""; var rz=[ {h:0,l:0},{h:a.h,l:a.l} ];
 for(var n=19;n>=0;n--)
  {
   rz=i64_bitdiv(rz[1],pw10[n]); var d=rz[0].l;
   if((!n && (!s.length)) || (d>0) || (s.length>0)) s+=d.toString();
  }
 return s;
}

// converts decimal String to 64-bit object
function i64_fromdecString(s)
{
 var o={h:0,l:0};
 var c=s.length;
 for(var n=0;(n<=19) && ((c--)>0);n++)
  {
   var d=s.charCodeAt(c)-48;
   if((d>0) && (d<=9))
    {
     var o2=i64_bitmul(pw10[n],i64_al(d)); o=i64_add(o,o2);
    }
  }
 return o;
}

/* Other basic functions */


function i64_max(a,b) { return (i64_ge(a,b) ? a : b ); }	// max
function i64_min(a,b) { return (i64_le(a,b) ? a : b ); }	// min
function i64_neg(a)   { return i64_add(i64_not(a),i64_al(1)); }	// negative value as unsigned
function i64_mod(a,b) { var rz=i64_bitdiv(a,b); return rz[1]; }	// modulus
function i64_isodd(a,b) { return ((a.l&1)==0); }	// is odd
function i64_next(a) { return i64_add(a,{h:0,l:1}); }	// next number
function i64_pre(a) { return i64_sub(a,{h:0,l:1}); }	// previous number

// fast if can do it, otherwise reminder contains unprocessed value
function i64_primesIfCan(a)	// returns  array [ [primes a=a1*a2*...*an], reminder object ]
 {
  var ret=[]; var k=0;
  var rz=[ {h:0,l:0},{h:a.h,l:a.l} ];
  var primes=[2,3,5,7,11,13,17,19,23,29,31,37,41,43,47,53,59,61,67,71,73,79,83,89,97];
  var o=rz[1];
  for(var i=0; (o.l | o.h) && i<primes.length;)
   {
    var o2=i64_al(primes[i]); var op={h:o.h,l:o.l};
    rz=i64_bitdiv(o,o2); o=rz[1];
    if(!(o.l | o.h))  { ret[k++]=o2; o=rz[0]; }
    else { i++; o=op; }
    if( i64_eq(o,o2) ) { ret[k++]=o2; o={h:0,l:0}; }
   }
  return [ret,o];
 }

//--------------------------------------
// END OF SCRIPTS for int 64-bit cases
//--------------------------------------

/* some additional */
 

/*------------------------------------- Javascript helping functions ---------------*/


function RuntimeException(s) { if(s.length>0) alert("RuntimeException: " + s); }

function ChessParseError(s)  { if(s.length>0) alert("ChessParseError: " + s); }

function startsWith(s,s1)
{ return ( s.length>= s1.length ? s.substr(0,s1.length)==s1 : false ); }

function replaceAll(s,s1,s2)
{ var regex = new RegExp(s1, "g"); return s.replace(regex,s2); }

function ArrContains( arr, val )
{ return (("," + arr.valueOf().toString() + ",").indexOf("," + val.toString() + ",") >= 0); }

    /**
     * Holder of data Files
     */
/*---------- king-pawn endgame datas --------*/
function KPKbitbase()
    {
          // File kpk.bitbase - king positions
        var A = 255,  B = 254,  C = 253,  D = 128,  E = 251,  F = 247,  G = 252,  H = 224,  I = 248,  J = 240,  K = 192,  L = 127,  M = 126,  N = 191,  O = 250,  P = 131,  Q = 193,  R = 239,  S = 244,  T = 143,  U = 175,  V = 199,  W = 227,  X = 223,  Y = 241,  Z = 135,  a = 130,  b = 246,  c = 245,  d = 194,  e = 243,  f = 242,  g = 118,  h = 249,  i = 229,  j = 133,  k = 202,  l = 234,  m = 226,  n = 231,  o = 167,  p = 138,  q = 197,  r = 195,  s = 148,  t = 122,  u = 119,  v = 112,  w = 213,  x = 171,  y = 134,  z = 124;
        return [
0,0,0,0,0,0,A,A,A,A,A,A,A,A,A,A,A,A,A,A,A,A,A,A,A,A,A,A,A,A,A,A,A,A,A,A,A,A,A,A,A,A,A,A,A,A,A,A,B,A,A,A,A,A,C,A,A,A,A,A,E,A,A,A,A,A,F,A,A,A,A,A,R,A,A,A,A,A,X,A,A,A,A,A,N,A,A,A,A,A,L,A,A,A,A,A,d,K,A,A,A,A,j,D,A,A,A,A,10,0,A,A,A,A,22,2,A,A,A,A,46,6,A,A,A,A,94,14,A,A,A,A,U,15,A,A,A,A,95,31,A,A,A,A,K,d,H,A,A,A,D,j,K,A,A,A,0,10,D,A,A,A,2,20,0,A,A,A,6,42,0,A,A,A,14,86,3,A,A,A,15,U,7,A,A,A,31,95,15,A,A,A,K,K,m,J,A,A,D,D,q,H,A,A,0,0,p,K,A,A,2,0,20,D,A,A,6,2,40,1,A,A,14,6,83,3,A,A,15,15,o,7,A,A,31,31,79,15,A,A,K,K,H,f,I,A,D,D,K,i,J,A,0,0,D,k,H,A,2,0,0,s,Q,A,6,2,0,41,P,A,14,6,3,83,7,A,15,15,7,o,15,A,31,31,15,79,31,A,K,K,H,J,O,G,D,D,K,H,c,I,0,0,D,K,l,Y,2,0,0,D,w,W,6,2,0,1,x,V,14,6,3,3,87,T,15,15,7,7,U,31,31,31,15,15,95,63,B,K,H,J,I,B,B,D,K,H,J,C,30,0,D,K,H,E,30,0,0,D,Q,F,30,2,0,1,P,R,30,6,3,3,7,X,31,15,7,7,15,N,31,31,15,15,31,L,A,A,A,A,A,A,0,0,0,0,0,0,A,A,A,A,A,A,A,A,A,A,A,A,A,A,A,A,A,A,A,A,A,A,A,A,A,A,A,A,A,A,A,A,A,A,A,A,B,A,A,A,A,A,C,A,A,A,A,A,E,A,A,A,A,A,F,A,A,A,A,A,R,A,A,A,A,A,X,A,A,A,A,A,N,A,A,A,A,A,L,A,A,A,A,A,O,S,A,A,A,A,j,D,A,A,A,A,10,0,A,A,A,A,22,2,A,A,A,A,46,6,A,A,A,A,94,14,A,A,A,A,N,31,A,A,A,A,95,31,A,A,A,A,I,f,H,A,A,A,D,j,K,A,A,A,0,10,D,A,A,A,2,20,0,A,A,A,6,42,0,A,A,A,14,86,3,A,A,A,31,U,7,A,A,A,31,95,15,A,A,A,I,J,m,J,A,A,D,D,q,H,A,A,0,0,p,K,A,A,2,0,20,D,A,A,6,2,40,1,A,A,14,6,83,3,A,A,31,15,o,7,A,A,31,31,79,15,A,A,I,J,H,f,I,A,D,D,K,i,J,A,0,0,D,k,H,A,2,0,0,s,Q,A,6,2,0,41,P,A,14,6,3,83,7,A,31,15,7,o,15,A,31,31,15,79,31,A,I,J,H,J,O,G,D,D,K,H,c,I,0,0,D,K,l,Y,2,0,0,D,w,W,6,2,0,1,x,V,14,6,3,3,87,T,31,15,7,7,U,31,31,31,15,15,95,63,B,J,H,J,I,B,B,D,K,H,J,C,M,0,D,K,H,E,62,0,0,D,Q,F,62,2,0,1,P,R,62,6,3,3,7,X,63,15,7,7,15,N,63,31,15,15,31,L,A,A,A,A,A,A,A,A,A,A,A,A,0,0,0,0,0,0,A,A,A,A,A,A,A,A,A,A,A,A,A,A,A,A,A,A,A,A,A,A,A,A,A,A,A,A,A,A,B,A,A,A,A,A,C,A,A,A,A,A,E,A,A,A,A,A,F,A,A,A,A,A,R,A,A,A,A,A,X,A,A,A,A,A,N,A,A,A,A,A,L,A,A,A,A,A,B,G,A,A,A,A,c,232,A,A,A,A,10,0,A,A,A,A,20,2,A,A,A,A,46,6,A,A,A,A,94,14,A,A,A,A,N,31,A,A,A,A,L,63,A,A,A,A,G,O,J,A,A,A,J,i,K,A,A,A,0,10,D,A,A,A,0,20,0,A,A,A,6,42,0,A,A,A,14,86,3,A,A,A,31,U,7,A,A,A,63,95,15,A,A,A,G,I,f,J,A,A,J,H,q,H,A,A,0,0,p,K,A,A,0,0,20,D,A,A,6,2,40,1,A,A,14,6,83,3,A,A,31,15,o,7,A,A,63,31,79,15,A,A,G,I,J,f,I,A,J,H,K,i,J,A,0,0,D,k,H,A,0,0,0,s,Q,A,6,2,0,41,P,A,14,6,3,83,7,A,31,15,7,o,15,A,63,31,15,79,31,A,G,I,J,J,O,G,J,H,K,H,c,I,0,0,D,K,l,Y,0,0,0,D,w,W,6,2,0,1,x,V,14,6,3,3,87,T,31,15,7,7,U,31,63,31,15,15,95,63,B,I,J,J,I,B,B,H,K,H,J,C,M,0,D,K,H,E,M,0,0,D,Q,F,M,2,0,1,P,R,M,6,3,3,7,X,L,15,7,7,15,N,L,31,15,15,31,L,A,A,A,A,A,A,A,A,A,A,A,A,A,A,A,A,A,A,0,0,0,0,0,0,A,A,A,A,A,A,A,A,A,A,A,A,A,A,A,A,A,A,A,A,A,A,A,A,B,A,A,A,A,A,C,A,A,A,A,A,E,A,A,A,A,A,F,A,A,A,A,A,R,A,A,A,A,A,X,A,A,A,A,A,N,A,A,A,A,A,L,A,A,A,A,A,B,G,A,A,A,A,C,I,A,A,A,A,106,80,A,A,A,A,20,0,A,A,A,A,42,4,A,A,A,A,94,14,A,A,A,A,N,31,A,A,A,A,L,63,A,A,A,A,G,O,J,A,A,A,I,c,H,A,A,A,96,74,D,A,A,A,0,20,0,A,A,A,2,40,0,A,A,A,14,86,3,A,A,A,31,U,7,A,A,A,63,95,15,A,A,A,G,I,f,J,A,A,I,J,i,H,A,A,96,64,p,K,A,A,0,0,20,D,A,A,2,0,40,1,A,A,14,6,83,3,A,A,31,15,o,7,A,A,63,31,79,15,A,A,G,I,J,f,I,A,I,J,H,i,J,A,96,64,D,k,H,A,0,0,0,s,Q,A,2,0,0,41,P,A,14,6,3,83,7,A,31,15,7,o,15,A,63,31,15,79,31,A,G,I,J,J,O,G,I,J,H,H,c,I,96,64,D,K,l,Y,0,0,0,D,w,W,2,0,0,1,x,V,14,6,3,3,87,T,31,15,7,7,U,31,63,31,15,15,95,63,B,I,J,J,I,B,B,J,H,H,J,C,M,64,D,K,H,E,M,0,0,D,Q,F,M,0,0,1,P,R,M,6,3,3,7,X,L,15,7,7,15,N,L,31,15,15,31,L,B,A,A,A,A,A,B,A,A,A,A,A,B,A,A,A,A,A,B,A,A,A,A,A,B,A,A,A,A,A,B,A,A,A,A,A,B,A,A,A,A,A,B,A,A,A,A,A,0,0,0,0,0,0,G,A,A,A,A,A,O,A,A,A,A,A,b,A,A,A,A,A,238,A,A,A,A,A,222,A,A,A,A,A,190,A,A,A,A,A,M,A,A,A,A,A,B,B,A,A,A,A,B,C,A,A,A,A,10,2,A,A,A,A,22,7,A,A,A,A,46,7,A,A,A,A,94,15,A,A,A,A,174,15,A,A,A,A,94,31,A,A,A,A,K,d,H,A,A,A,D,j,K,A,A,A,2,10,D,A,A,A,6,22,2,A,A,A,6,46,6,A,A,A,14,94,15,A,A,A,14,U,15,A,A,A,30,95,31,A,A,A,B,K,m,J,A,A,B,D,q,H,A,A,2,0,p,K,A,A,6,2,20,D,A,A,6,6,42,1,A,A,14,14,87,3,A,A,14,15,U,7,A,A,30,31,95,15,A,A,K,K,H,f,I,A,D,D,K,i,J,A,2,0,D,k,H,A,6,2,0,s,Q,A,6,6,2,41,P,A,14,14,7,83,7,A,14,15,15,o,15,A,30,31,31,79,31,A,B,K,H,J,O,G,B,D,K,H,c,I,14,0,D,K,l,Y,14,2,0,D,w,W,14,6,2,1,x,V,14,14,7,3,87,T,14,15,15,7,U,31,30,31,31,15,95,63,B,K,H,J,I,B,B,D,K,H,J,C,30,0,D,K,H,E,30,2,0,D,Q,F,30,6,2,1,P,R,30,14,7,3,7,X,30,15,15,7,15,N,30,31,31,15,31,L,C,A,A,A,A,A,C,A,A,A,A,A,C,A,A,A,A,A,C,A,A,A,A,A,C,A,A,A,A,A,C,A,A,A,A,A,C,A,A,A,A,A,C,A,A,A,A,A,G,A,A,A,A,A,0,0,0,0,0,0,h,A,A,A,A,A,c,A,A,A,A,A,237,A,A,A,A,A,221,A,A,A,A,A,189,A,A,A,A,A,125,A,A,A,A,A,C,B,A,A,A,A,C,C,A,A,A,A,C,E,A,A,A,A,21,7,A,A,A,A,45,15,A,A,A,A,93,15,A,A,A,A,189,31,A,A,A,A,93,31,A,A,A,A,G,O,S,A,A,A,D,j,K,A,A,A,0,10,D,A,A,A,4,22,2,A,A,A,12,46,6,A,A,A,12,94,15,A,A,A,29,N,31,A,A,A,29,95,31,A,A,A,G,I,f,J,A,A,G,D,q,H,A,A,z,0,p,K,A,A,4,2,20,D,A,A,12,6,42,1,A,A,12,14,87,3,A,A,29,31,U,7,A,A,29,31,95,15,A,A,G,I,J,f,I,A,D,D,K,i,J,A,0,0,D,k,H,A,4,2,0,s,Q,A,12,6,2,41,P,A,12,14,7,83,7,A,29,31,15,o,15,A,29,31,31,79,31,A,G,I,J,J,O,G,G,D,K,H,c,I,z,0,D,K,l,Y,28,2,0,D,w,W,28,6,2,1,x,V,28,14,7,3,87,T,29,31,15,7,U,31,29,31,31,15,95,63,G,I,J,J,I,B,G,D,K,H,J,C,z,0,D,K,H,E,60,2,0,D,Q,F,60,6,2,1,P,R,60,14,7,3,7,X,61,31,15,7,15,N,61,31,31,15,31,L,E,A,A,A,A,A,E,A,A,A,A,A,E,A,A,A,A,A,E,A,A,A,A,A,E,A,A,A,A,A,E,A,A,A,A,A,E,A,A,A,A,A,E,A,A,A,A,A,O,A,A,A,A,A,h,A,A,A,A,A,0,0,0,0,0,0,e,A,A,A,A,A,235,A,A,A,A,A,219,A,A,A,A,A,187,A,A,A,A,A,123,A,A,A,A,A,O,B,A,A,A,A,E,C,A,A,A,A,E,E,A,A,A,A,E,F,A,A,A,A,43,15,A,A,A,A,91,31,A,A,A,A,187,31,A,A,A,A,123,63,A,A,A,A,O,B,G,A,A,A,I,c,232,A,A,A,0,10,D,A,A,A,2,20,2,A,A,A,10,46,6,A,A,A,26,94,15,A,A,A,27,N,31,A,A,A,59,L,63,A,A,A,O,G,O,J,A,A,O,J,i,H,A,A,t,0,p,K,A,A,t,0,20,D,A,A,10,6,42,1,A,A,26,14,87,3,A,A,27,31,U,7,A,A,59,63,95,15,A,A,O,G,I,f,I,A,I,J,H,i,J,A,0,0,D,k,H,A,2,0,0,s,Q,A,10,6,2,41,P,A,26,14,7,83,7,A,27,31,15,o,15,A,59,63,31,79,31,A,O,G,I,J,O,G,O,J,H,H,c,I,t,0,D,K,l,Y,t,0,0,D,w,W,58,6,2,1,x,V,58,14,7,3,87,T,59,31,15,7,U,31,59,63,31,15,95,63,O,G,I,J,I,B,O,J,H,H,J,C,t,0,D,K,H,E,t,0,0,D,Q,F,t,6,2,1,P,R,t,14,7,3,7,X,123,31,15,7,15,N,123,63,31,15,31,L,F,A,A,A,A,A,F,A,A,A,A,A,F,A,A,A,A,A,F,A,A,A,A,A,F,A,A,A,A,A,F,A,A,A,A,A,F,A,A,A,A,A,F,A,A,A,A,A,b,A,A,A,A,A,c,A,A,A,A,A,e,A,A,A,A,A,0,0,0,0,0,0,n,A,A,A,A,A,215,A,A,A,A,A,183,A,A,A,A,A,u,A,A,A,A,A,b,B,A,A,A,A,c,G,A,A,A,A,F,E,A,A,A,A,F,F,A,A,A,A,F,R,A,A,A,A,87,31,A,A,A,A,183,63,A,A,A,A,u,63,A,A,A,A,b,B,G,A,A,A,S,C,I,A,A,A,v,106,208,A,A,A,0,20,0,A,A,A,6,42,4,A,A,A,22,94,15,A,A,A,55,N,31,A,A,A,55,L,63,A,A,A,b,G,O,J,A,A,S,I,c,H,A,A,g,96,k,K,A,A,g,0,20,D,A,A,g,2,40,1,A,A,22,14,87,3,A,A,55,31,U,7,A,A,55,63,95,15,A,A,b,G,I,f,I,A,S,I,J,i,J,A,v,96,K,k,H,A,0,0,0,s,Q,A,6,2,0,41,P,A,22,14,7,83,7,A,55,31,15,o,15,A,55,63,31,79,31,A,b,G,I,J,O,G,b,I,J,H,c,I,g,96,K,K,l,Y,g,0,0,D,w,W,g,2,0,1,x,V,g,14,7,3,87,T,u,31,15,7,U,31,u,63,31,15,95,63,b,G,I,J,I,B,b,I,J,H,J,C,g,96,K,K,H,E,g,0,0,D,Q,F,g,2,0,1,P,R,g,14,7,3,7,X,u,31,15,7,15,N,u,63,31,15,31,L,A,B,A,A,A,A,A,B,A,A,A,A,A,B,A,A,A,A,A,B,A,A,A,A,A,B,A,A,A,A,A,B,A,A,A,A,A,B,A,A,A,A,A,B,A,A,A,A,B,B,A,A,A,A,C,B,A,A,A,A,E,B,A,A,A,A,F,B,A,A,A,A,R,B,A,A,A,A,X,B,A,A,A,A,N,B,A,A,A,A,L,B,A,A,A,A,0,0,0,0,0,0,A,G,A,A,A,A,10,2,A,A,A,A,23,6,A,A,A,A,47,6,A,A,A,A,95,14,A,A,A,A,U,14,A,A,A,A,95,30,A,A,A,A,A,B,B,A,A,A,A,B,C,A,A,A,2,10,a,A,A,A,7,22,7,A,A,A,7,46,7,A,A,A,15,94,15,A,A,A,15,174,15,A,A,A,31,94,31,A,A,A,K,K,m,J,A,A,B,D,q,H,A,A,2,2,p,K,A,A,6,6,22,a,A,A,6,6,46,7,A,A,14,14,95,15,A,A,15,14,U,15,A,A,31,30,95,31,A,A,B,B,H,f,I,A,B,B,K,i,J,A,6,2,D,k,H,A,6,6,2,s,Q,A,6,6,6,43,P,A,14,14,15,87,7,A,15,14,15,U,15,A,31,30,31,95,31,A,B,K,H,J,O,G,B,D,K,H,c,I,14,2,D,K,l,Y,14,6,2,D,w,W,14,6,6,3,x,V,14,14,15,7,87,T,15,14,15,15,U,31,31,30,31,31,95,63,B,B,H,J,I,B,B,B,K,H,J,C,30,14,D,K,H,E,30,14,2,D,Q,F,30,14,6,3,P,R,30,14,15,7,7,X,31,14,15,15,15,N,31,30,31,31,31,L,A,C,A,A,A,A,A,C,A,A,A,A,A,C,A,A,A,A,A,C,A,A,A,A,A,C,A,A,A,A,A,C,A,A,A,A,A,C,A,A,A,A,A,C,A,A,A,A,B,C,A,A,A,A,C,C,A,A,A,A,E,C,A,A,A,A,F,C,A,A,A,A,R,C,A,A,A,A,X,C,A,A,A,A,N,C,A,A,A,A,L,C,A,A,A,A,A,G,A,A,A,A,0,0,0,0,0,0,A,h,A,A,A,A,23,5,A,A,A,A,47,13,A,A,A,A,95,13,A,A,A,A,N,29,A,A,A,A,95,29,A,A,A,A,A,C,B,A,A,A,A,C,C,A,A,A,A,C,E,A,A,A,7,21,7,A,A,A,15,45,15,A,A,A,15,93,15,A,A,A,31,189,31,A,A,A,31,93,31,A,A,A,B,G,O,S,A,A,D,D,q,H,A,A,M,0,p,K,A,A,6,4,22,a,A,A,14,12,46,7,A,A,14,12,95,15,A,A,31,29,N,31,A,A,31,29,95,31,A,A,B,G,I,f,I,A,B,G,K,i,J,A,M,z,D,k,H,A,14,4,2,s,Q,A,14,12,6,43,P,A,14,12,15,87,7,A,31,29,31,U,15,A,31,29,31,95,31,A,B,G,I,J,O,G,B,D,K,H,c,I,M,0,D,K,l,Y,30,4,2,D,w,W,30,12,6,3,x,V,30,12,15,7,87,T,31,29,31,15,U,31,31,29,31,31,95,63,B,G,I,J,I,B,B,G,K,H,J,C,M,z,D,K,H,E,62,28,2,D,Q,F,62,28,6,3,P,R,62,28,15,7,7,X,63,29,31,15,15,N,63,29,31,31,31,L,A,E,A,A,A,A,A,E,A,A,A,A,A,E,A,A,A,A,A,E,A,A,A,A,A,E,A,A,A,A,A,E,A,A,A,A,A,E,A,A,A,A,A,E,A,A,A,A,B,E,A,A,A,A,C,E,A,A,A,A,E,E,A,A,A,A,F,E,A,A,A,A,R,E,A,A,A,A,X,E,A,A,A,A,N,E,A,A,A,A,L,E,A,A,A,A,B,O,A,A,A,A,A,h,A,A,A,A,0,0,0,0,0,0,A,e,A,A,A,A,47,11,A,A,A,A,95,27,A,A,A,A,N,27,A,A,A,A,L,59,A,A,A,A,B,O,B,A,A,A,A,E,C,A,A,A,A,E,E,A,A,A,A,E,F,A,A,A,15,43,15,A,A,A,31,91,31,A,A,A,31,187,31,A,A,A,63,123,63,A,A,A,B,O,B,G,A,A,B,I,c,232,A,A,0,0,p,K,A,A,M,2,20,a,A,A,14,10,46,7,A,A,30,26,95,15,A,A,31,27,N,31,A,A,63,59,L,63,A,A,B,O,G,O,I,A,B,O,J,i,J,A,M,t,D,k,H,A,M,t,0,s,Q,A,30,10,6,43,P,A,30,26,15,87,7,A,31,27,31,U,15,A,63,59,63,95,31,A,B,O,G,I,O,G,B,I,J,H,c,I,M,0,D,K,l,Y,M,2,0,D,w,W,62,10,6,3,x,V,62,26,15,7,87,T,63,27,31,15,U,31,63,59,63,31,95,63,B,O,G,I,I,B,B,O,J,H,J,C,M,t,D,K,H,E,M,t,0,D,Q,F,M,58,6,3,P,R,M,58,15,7,7,X,L,59,31,15,15,N,L,59,63,31,31,L,A,F,A,A,A,A,A,F,A,A,A,A,A,F,A,A,A,A,A,F,A,A,A,A,A,F,A,A,A,A,A,F,A,A,A,A,A,F,A,A,A,A,A,F,A,A,A,A,B,F,A,A,A,A,C,F,A,A,A,A,E,F,A,A,A,A,F,F,A,A,A,A,R,F,A,A,A,A,X,F,A,A,A,A,N,F,A,A,A,A,L,F,A,A,A,A,B,b,A,A,A,A,C,S,A,A,A,A,A,e,A,A,A,A,0,0,0,0,0,0,A,n,A,A,A,A,95,23,A,A,A,A,N,55,A,A,A,A,L,55,A,A,A,A,B,b,B,A,A,A,G,c,G,A,A,A,A,F,E,A,A,A,A,F,F,A,A,A,A,F,R,A,A,A,31,87,31,A,A,A,63,183,63,A,A,A,63,u,63,A,A,A,B,b,B,G,A,A,G,S,C,I,A,A,M,v,l,208,A,A,0,0,20,D,A,A,M,6,42,5,A,A,30,22,95,15,A,A,63,55,N,31,A,A,63,55,L,63,A,A,B,b,G,O,I,A,B,S,I,c,J,A,M,g,H,k,H,A,M,g,0,s,Q,A,M,g,2,41,P,A,62,22,15,87,7,A,63,55,31,U,15,A,63,55,63,95,31,A,B,b,G,I,O,G,B,S,I,J,c,I,M,v,H,K,l,Y,M,0,0,D,w,W,M,6,2,1,x,V,M,22,15,7,87,T,L,55,31,15,U,31,L,55,63,31,95,63,B,b,G,I,I,B,B,b,I,J,J,C,M,g,H,K,H,E,M,g,0,D,Q,F,M,g,2,1,P,R,M,g,15,7,7,X,L,u,31,15,15,N,L,u,63,31,31,L,A,A,B,A,A,A,A,A,B,A,A,A,A,A,B,A,A,A,A,A,B,A,A,A,A,A,B,A,A,A,A,A,B,A,A,A,A,A,B,A,A,A,A,A,B,A,A,A,B,A,B,A,A,A,C,A,B,A,A,A,E,A,B,A,A,A,F,A,B,A,A,A,R,A,B,A,A,A,X,A,B,A,A,A,N,A,B,A,A,A,L,A,B,A,A,A,A,B,B,A,A,A,A,C,B,A,A,A,11,3,B,A,A,A,23,7,B,A,A,A,47,7,B,A,A,A,95,15,B,A,A,A,U,15,B,A,A,A,95,31,B,A,A,A,0,0,0,0,0,0,A,A,G,A,A,A,2,10,a,A,A,A,7,23,6,A,A,A,7,47,6,A,A,A,15,95,14,A,A,A,15,U,14,A,A,A,31,95,30,A,A,A,A,A,B,B,A,A,A,A,B,C,A,A,2,2,p,d,A,A,7,7,22,Z,A,A,7,7,46,7,A,A,15,15,94,15,A,A,15,15,174,15,A,A,31,31,94,31,A,A,B,K,H,f,I,A,B,B,K,i,J,A,6,2,a,k,H,A,6,6,6,150,r,A,6,6,6,47,Z,A,15,15,14,95,15,A,15,15,14,U,15,A,31,31,30,95,31,A,B,B,B,I,B,G,B,B,B,H,C,I,14,6,a,K,l,Y,14,6,6,a,215,W,14,6,6,7,U,V,15,15,14,15,95,T,15,15,14,15,U,31,31,31,30,31,95,63,B,B,H,I,G,B,B,B,K,H,I,C,30,14,a,K,H,E,30,14,6,a,r,F,30,14,6,7,Z,R,31,15,14,15,15,X,31,15,14,15,15,N,31,31,30,31,31,L,A,A,C,A,A,A,A,A,C,A,A,A,A,A,C,A,A,A,A,A,C,A,A,A,A,A,C,A,A,A,A,A,C,A,A,A,A,A,C,A,A,A,A,A,C,A,A,A,B,A,C,A,A,A,C,A,C,A,A,A,E,A,C,A,A,A,F,A,C,A,A,A,R,A,C,A,A,A,X,A,C,A,A,A,N,A,C,A,A,A,L,A,C,A,A,A,A,B,C,A,A,A,A,C,C,A,A,A,A,E,C,A,A,A,23,7,C,A,A,A,47,15,C,A,A,A,95,15,C,A,A,A,N,31,C,A,A,A,95,31,C,A,A,A,A,A,G,A,A,A,0,0,0,0,0,0,A,A,h,A,A,A,7,23,5,A,A,A,15,47,13,A,A,A,15,95,13,A,A,A,31,N,29,A,A,A,31,95,29,A,A,A,A,A,C,B,A,A,A,A,C,C,A,A,A,A,C,E,A,A,7,7,21,Z,A,A,15,15,45,15,A,A,15,15,93,15,A,A,31,31,189,31,A,A,31,31,93,31,A,A,B,B,G,O,G,A,B,D,K,i,J,A,M,M,D,k,H,A,14,6,4,150,r,A,14,14,12,47,Z,A,15,15,13,95,15,A,31,31,29,N,31,A,31,31,29,95,31,A,B,B,G,I,B,G,B,B,G,J,C,I,M,M,G,K,O,Y,30,14,4,a,215,W,30,14,12,7,U,V,31,15,13,15,95,T,31,31,29,31,N,31,31,31,29,31,95,63,B,B,G,I,G,B,B,B,K,J,I,C,M,M,D,K,J,E,62,30,4,a,r,F,62,30,12,7,Z,R,63,31,13,15,15,X,63,31,29,31,31,N,63,31,29,31,31,L,A,A,E,A,A,A,A,A,E,A,A,A,A,A,E,A,A,A,A,A,E,A,A,A,A,A,E,A,A,A,A,A,E,A,A,A,A,A,E,A,A,A,A,A,E,A,A,A,B,A,E,A,A,A,C,A,E,A,A,A,E,A,E,A,A,A,F,A,E,A,A,A,R,A,E,A,A,A,X,A,E,A,A,A,N,A,E,A,A,A,L,A,E,A,A,A,B,B,E,A,A,A,A,C,E,A,A,A,A,E,E,A,A,A,A,F,E,A,A,A,47,15,E,A,A,A,95,31,E,A,A,A,N,31,E,A,A,A,L,63,E,A,A,A,B,B,O,A,A,A,A,A,h,A,A,A,0,0,0,0,0,0,A,A,e,A,A,A,15,47,11,A,A,A,31,95,27,A,A,A,31,N,27,A,A,A,63,L,59,A,A,A,B,B,O,B,A,A,A,A,E,C,A,A,A,A,E,E,A,A,A,A,E,F,A,A,15,15,43,15,A,A,31,31,91,31,A,A,31,31,187,31,A,A,63,63,123,63,A,A,B,B,O,B,G,A,B,B,I,c,I,A,M,0,D,k,H,A,M,M,2,s,r,A,30,14,10,47,Z,A,31,31,27,95,15,A,31,31,27,N,31,A,63,63,59,L,63,A,B,B,O,G,B,G,B,B,O,J,C,I,M,M,O,H,O,Y,M,M,t,D,F,W,62,30,10,7,U,V,63,31,27,15,95,T,63,31,27,31,N,31,63,63,59,63,L,63,B,B,O,G,G,B,B,B,I,J,I,C,M,M,D,H,J,E,M,M,2,D,W,F,M,62,10,7,Z,R,L,63,27,15,15,X,L,63,27,31,31,N,L,63,59,63,63,L,A,A,F,A,A,A,A,A,F,A,A,A,A,A,F,A,A,A,A,A,F,A,A,A,A,A,F,A,A,A,A,A,F,A,A,A,A,A,F,A,A,A,A,A,F,A,A,A,B,A,F,A,A,A,C,A,F,A,A,A,E,A,F,A,A,A,F,A,F,A,A,A,R,A,F,A,A,A,X,A,F,A,A,A,N,A,F,A,A,A,L,A,F,A,A,A,B,B,F,A,A,A,C,G,F,A,A,A,A,E,F,A,A,A,A,F,F,A,A,A,A,R,F,A,A,A,95,31,F,A,A,A,N,63,F,A,A,A,L,63,F,A,A,A,B,B,b,A,A,A,G,C,S,A,A,A,A,A,e,A,A,A,0,0,0,0,0,0,A,A,n,A,A,A,31,95,23,A,A,A,63,N,55,A,A,A,63,L,55,A,A,A,B,B,b,B,A,A,G,G,c,G,A,A,A,A,F,E,A,A,A,A,F,F,A,A,A,A,F,R,A,A,31,31,87,31,A,A,63,63,183,63,A,A,63,63,u,63,A,A,B,B,b,B,G,A,B,G,S,C,I,A,B,B,J,l,J,A,M,0,0,s,Q,A,M,M,6,43,Z,A,63,31,23,95,15,A,63,63,55,N,31,A,63,63,55,L,63,A,B,B,b,G,B,G,B,B,S,I,C,I,B,B,b,H,O,Y,M,M,g,K,F,W,M,M,g,3,R,V,L,63,23,15,95,T,L,63,55,31,N,31,L,63,55,63,L,63,B,B,b,G,G,B,B,B,S,I,I,C,B,B,J,H,J,E,M,M,0,K,W,F,M,M,6,3,V,R,L,L,23,15,15,X,L,L,55,31,31,N,L,L,55,63,63,L,A,A,A,B,A,A,A,A,A,B,A,A,A,A,A,B,A,A,A,A,A,B,A,A,A,A,A,B,A,A,A,A,A,B,A,A,A,A,A,B,A,A,A,A,A,B,A,A,B,A,A,B,A,A,C,A,A,B,A,A,E,A,A,B,A,A,F,A,A,B,A,A,R,A,A,B,A,A,X,A,A,B,A,A,N,A,A,B,A,A,L,A,A,B,A,A,B,B,A,B,A,A,C,129,A,B,A,A,11,3,A,B,A,A,23,7,A,B,A,A,47,7,A,B,A,A,95,15,A,B,A,A,U,15,A,B,A,A,95,31,A,B,A,A,A,A,B,B,A,A,A,A,C,B,A,A,2,10,P,B,A,A,7,23,7,B,A,A,7,47,7,B,A,A,15,95,15,B,A,A,15,U,15,B,A,A,31,95,31,B,A,A,0,0,0,0,0,0,A,A,A,G,A,A,2,2,p,d,A,A,7,7,23,y,A,A,7,7,47,6,A,A,15,15,95,14,A,A,15,15,U,14,A,A,31,31,95,30,A,A,A,A,A,B,B,A,A,A,A,B,C,A,6,2,a,k,m,A,7,7,7,150,V,A,7,7,7,46,Z,A,15,15,15,94,15,A,15,15,15,174,15,A,31,31,31,94,31,A,B,B,H,G,B,G,B,B,B,H,C,I,14,6,a,d,l,Y,14,6,6,y,215,W,15,7,7,6,U,V,15,15,15,14,95,T,15,15,15,14,U,31,31,31,31,30,95,63,B,B,B,B,B,B,B,B,B,B,G,A,30,14,y,198,m,A,30,14,6,y,V,F,31,15,7,6,Z,R,31,15,15,14,15,X,31,15,15,14,15,N,31,31,31,30,31,L,A,A,A,C,A,A,A,A,A,C,A,A,A,A,A,C,A,A,A,A,A,C,A,A,A,A,A,C,A,A,A,A,A,C,A,A,A,A,A,C,A,A,A,A,A,C,A,A,B,A,A,C,A,A,C,A,A,C,A,A,E,A,A,C,A,A,F,A,A,C,A,A,R,A,A,C,A,A,X,A,A,C,A,A,N,A,A,C,A,A,L,A,A,C,A,A,B,B,A,C,A,A,C,C,A,C,A,A,E,3,A,C,A,A,23,7,A,C,A,A,47,15,A,C,A,A,95,15,A,C,A,A,N,31,A,C,A,A,95,31,A,C,A,A,A,A,B,C,A,A,A,A,C,C,A,A,A,A,E,C,A,A,7,23,7,C,A,A,15,47,15,C,A,A,15,95,15,C,A,A,31,N,31,C,A,A,31,95,31,C,A,A,A,A,A,G,A,A,0,0,0,0,0,0,A,A,A,h,A,A,7,7,23,j,A,A,15,15,47,13,A,A,15,15,95,13,A,A,31,31,N,29,A,A,31,31,95,29,A,A,A,A,A,C,B,A,A,A,A,C,C,A,A,A,A,C,E,A,15,7,7,149,V,A,15,15,15,45,T,A,15,15,15,93,15,A,31,31,31,189,31,A,31,31,31,93,31,A,B,B,B,G,B,G,B,B,K,I,C,I,M,M,B,K,O,Y,30,14,6,132,215,W,31,15,15,13,U,V,31,15,15,13,95,T,31,31,31,29,N,31,31,31,31,29,95,63,B,B,B,G,B,B,B,B,B,G,G,A,M,M,B,G,O,A,62,30,14,140,V,A,63,31,15,13,T,R,63,31,15,13,15,X,63,31,31,29,31,N,63,31,31,29,31,L,A,A,A,E,A,A,A,A,A,E,A,A,A,A,A,E,A,A,A,A,A,E,A,A,A,A,A,E,A,A,A,A,A,E,A,A,A,A,A,E,A,A,A,A,A,E,A,A,B,A,A,E,A,A,C,A,A,E,A,A,E,A,A,E,A,A,F,A,A,E,A,A,R,A,A,E,A,A,X,A,A,E,A,A,N,A,A,E,A,A,L,A,A,E,A,A,B,B,A,E,A,A,C,G,A,E,A,A,E,E,A,E,A,A,F,7,A,E,A,A,47,15,A,E,A,A,95,31,A,E,A,A,N,31,A,E,A,A,L,63,A,E,A,A,B,B,B,E,A,A,A,A,C,E,A,A,A,A,E,E,A,A,A,A,F,E,A,A,15,47,15,E,A,A,31,95,31,E,A,A,31,N,31,E,A,A,63,L,63,E,A,A,B,B,B,O,A,A,A,A,A,h,A,A,0,0,0,0,0,0,A,A,A,e,A,A,15,15,47,11,A,A,31,31,95,27,A,A,31,31,N,27,A,A,63,63,L,59,A,A,B,B,B,O,B,A,A,A,A,E,C,A,A,A,A,E,E,A,A,A,A,E,F,A,31,15,15,43,T,A,31,31,31,91,31,A,31,31,31,187,31,A,63,63,63,123,63,A,B,B,B,O,B,G,B,B,B,I,C,I,M,M,D,J,O,Y,M,M,M,a,F,W,63,31,15,11,U,V,63,31,31,27,95,T,63,31,31,27,N,31,63,63,63,59,L,63,B,B,B,O,B,B,B,B,B,O,G,A,M,M,B,O,O,A,M,M,M,O,F,A,L,63,31,27,T,A,L,63,31,27,31,X,L,63,31,27,31,N,L,63,63,59,63,L,A,A,A,F,A,A,A,A,A,F,A,A,A,A,A,F,A,A,A,A,A,F,A,A,A,A,A,F,A,A,A,A,A,F,A,A,A,A,A,F,A,A,A,A,A,F,A,A,B,A,A,F,A,A,C,A,A,F,A,A,E,A,A,F,A,A,F,A,A,F,A,A,R,A,A,F,A,A,X,A,A,F,A,A,N,A,A,F,A,A,L,A,A,F,A,A,B,B,A,F,A,A,C,G,A,F,A,A,E,I,A,F,A,A,F,F,A,F,A,A,R,15,A,F,A,A,95,31,A,F,A,A,N,63,A,F,A,A,L,63,A,F,A,A,B,B,B,F,A,A,G,C,G,F,A,A,A,A,E,F,A,A,A,A,F,F,A,A,A,A,R,F,A,A,31,95,31,F,A,A,63,N,63,F,A,A,63,L,63,F,A,A,B,B,B,b,A,A,G,G,C,S,A,A,A,A,A,e,A,A,0,0,0,0,0,0,A,A,A,n,A,A,31,31,95,23,A,A,63,63,N,55,A,A,63,63,L,55,A,A,B,B,B,b,B,A,B,G,G,c,G,A,A,A,A,F,E,A,A,A,A,F,F,A,A,A,A,F,R,A,63,31,31,87,31,A,63,63,63,183,63,A,63,63,63,u,63,A,B,B,B,b,B,G,B,B,G,S,C,I,B,B,B,J,O,Y,M,M,0,m,F,W,L,L,L,7,R,V,L,63,31,23,95,T,L,63,63,55,N,31,L,63,63,55,L,63,B,B,B,b,B,B,B,B,B,b,G,A,B,B,B,b,O,A,M,M,M,b,F,A,L,L,L,u,R,A,L,L,63,55,31,A,L,L,63,55,63,N,L,L,63,55,63,L,A,A,A,A,B,A,A,A,A,A,B,A,A,A,A,A,B,A,A,A,A,A,B,A,A,A,A,A,B,A,A,A,A,A,B,A,A,A,A,A,B,A,A,A,A,A,B,A,B,A,A,A,B,A,C,A,A,A,B,A,E,A,A,A,B,A,F,A,A,A,B,A,R,A,A,A,B,A,X,A,A,A,B,A,N,A,A,A,B,A,L,A,A,A,B,A,B,G,A,A,B,A,C,D,A,A,B,A,11,1,A,A,B,A,23,3,A,A,B,A,47,7,A,A,B,A,95,15,A,A,B,A,U,15,A,A,B,A,95,31,A,A,B,A,K,d,B,A,B,A,D,j,Q,A,B,A,0,10,P,A,B,A,3,23,7,A,B,A,7,47,7,A,B,A,15,95,15,A,B,A,15,U,15,A,B,A,31,95,31,A,B,A,A,A,A,B,B,A,A,A,A,C,B,A,2,2,p,r,B,A,7,7,23,Z,B,A,7,7,47,7,B,A,15,15,95,15,B,A,15,15,U,15,B,A,31,31,95,31,B,A,0,0,0,0,0,0,A,A,A,A,G,A,2,2,a,k,m,A,7,7,7,151,198,A,7,7,7,47,y,A,15,15,15,95,14,A,15,15,15,U,14,A,31,31,31,95,30,A,A,A,A,A,B,B,A,A,A,A,B,C,14,6,a,d,l,e,15,7,7,Z,214,n,15,7,7,7,174,V,15,15,15,15,94,T,15,15,15,15,174,31,31,31,31,31,94,63,B,B,B,B,G,B,B,B,B,B,B,C,30,14,y,198,m,A,31,15,7,Z,198,F,31,15,7,7,y,R,31,15,15,15,14,X,31,15,15,15,14,N,31,31,31,31,30,L,A,A,A,A,C,A,A,A,A,A,C,A,A,A,A,A,C,A,A,A,A,A,C,A,A,A,A,A,C,A,A,A,A,A,C,A,A,A,A,A,C,A,A,A,A,A,C,A,B,A,A,A,C,A,C,A,A,A,C,A,E,A,A,A,C,A,F,A,A,A,C,A,R,A,A,A,C,A,X,A,A,A,C,A,N,A,A,A,C,A,L,A,A,A,C,A,B,G,A,A,C,A,C,I,A,A,C,A,E,1,A,A,C,A,23,3,A,A,C,A,47,7,A,A,C,A,95,15,A,A,C,A,N,31,A,A,C,A,95,31,A,A,C,A,G,B,B,A,C,A,D,j,C,A,C,A,1,11,P,A,C,A,3,23,7,A,C,A,7,47,15,A,C,A,15,95,15,A,C,A,31,N,31,A,C,A,31,95,31,A,C,A,A,A,A,B,C,A,A,A,A,C,C,A,A,A,A,E,C,A,7,7,23,Z,C,A,15,15,47,15,C,A,15,15,95,15,C,A,31,31,N,31,C,A,31,31,95,31,C,A,A,A,A,A,G,A,0,0,0,0,0,0,A,A,A,A,h,A,7,7,7,151,q,A,15,15,15,47,141,A,15,15,15,95,13,A,31,31,31,N,29,A,31,31,31,95,29,A,A,A,A,A,C,B,A,A,A,A,C,C,A,A,A,A,C,E,31,15,7,Z,w,n,31,15,15,15,173,207,31,15,15,15,93,T,31,31,31,31,189,31,31,31,31,31,93,63,B,B,B,B,G,B,B,B,B,B,I,A,M,M,B,B,C,E,63,31,15,T,q,A,63,31,15,15,141,R,63,31,15,15,13,X,63,31,31,31,29,N,63,31,31,31,29,L,A,A,A,A,E,A,A,A,A,A,E,A,A,A,A,A,E,A,A,A,A,A,E,A,A,A,A,A,E,A,A,A,A,A,E,A,A,A,A,A,E,A,A,A,A,A,E,A,B,A,A,A,E,A,C,A,A,A,E,A,E,A,A,A,E,A,F,A,A,A,E,A,R,A,A,A,E,A,X,A,A,A,E,A,N,A,A,A,E,A,L,A,A,A,E,A,B,G,A,A,E,A,C,I,A,A,E,A,E,Y,A,A,E,A,F,3,A,A,E,A,47,7,A,A,E,A,95,15,A,A,E,A,N,31,A,A,E,A,L,63,A,A,E,A,G,B,B,A,E,A,I,C,G,A,E,A,0,10,E,A,E,A,3,23,7,A,E,A,7,47,15,A,E,A,15,95,31,A,E,A,31,N,31,A,E,A,63,L,63,A,E,A,B,B,B,B,E,A,A,A,A,C,E,A,A,A,A,E,E,A,A,A,A,F,E,A,15,15,47,15,E,A,31,31,95,31,E,A,31,31,N,31,E,A,63,63,L,63,E,A,B,B,B,B,O,A,A,A,A,A,h,A,0,0,0,0,0,0,A,A,A,A,e,A,15,15,15,47,139,A,31,31,31,95,27,A,31,31,31,N,27,A,63,63,63,L,59,A,B,B,B,B,O,B,A,A,A,A,E,C,A,A,A,A,E,E,A,A,A,A,E,F,63,31,15,15,x,207,63,31,31,31,91,159,63,31,31,31,187,31,63,63,63,63,123,63,B,B,B,B,I,B,B,B,B,B,O,C,M,M,B,B,Y,A,L,L,L,A,E,F,L,63,31,31,139,A,L,63,31,31,27,X,L,63,31,31,27,N,L,63,63,63,59,L,A,A,A,A,F,A,A,A,A,A,F,A,A,A,A,A,F,A,A,A,A,A,F,A,A,A,A,A,F,A,A,A,A,A,F,A,A,A,A,A,F,A,A,A,A,A,F,A,B,A,A,A,F,A,C,A,A,A,F,A,E,A,A,A,F,A,F,A,A,A,F,A,R,A,A,A,F,A,X,A,A,A,F,A,N,A,A,A,F,A,L,A,A,A,F,A,B,G,A,A,F,A,C,I,A,A,F,A,E,J,A,A,F,A,F,W,A,A,F,A,R,7,A,A,F,A,95,15,A,A,F,A,N,31,A,A,F,A,L,63,A,A,F,A,G,B,B,A,F,A,I,C,G,A,F,A,J,O,I,A,F,A,0,20,F,A,F,A,7,47,15,A,F,A,15,95,31,A,F,A,31,N,63,A,F,A,63,L,63,A,F,A,B,B,B,B,F,A,G,G,C,G,F,A,A,A,A,E,F,A,A,A,A,F,F,A,A,A,A,R,F,A,31,31,95,31,F,A,63,63,N,63,F,A,63,63,L,63,F,A,B,B,B,B,b,A,G,G,G,C,S,A,A,A,A,A,e,A,0,0,0,0,0,0,A,A,A,A,n,A,31,31,31,95,23,A,63,63,63,N,55,A,63,63,63,L,55,A,B,B,B,B,b,B,B,B,G,G,c,G,A,A,A,A,F,E,A,A,A,A,F,F,A,A,A,A,F,R,L,63,31,31,87,159,L,63,63,63,183,63,L,63,63,63,u,63,B,B,B,B,b,B,B,B,B,B,S,A,B,B,B,B,b,E,M,M,M,B,W,A,L,L,L,L,F,R,L,L,63,63,23,A,L,L,63,63,55,N,L,L,63,63,55,L,A,A,A,A,A,B,A,A,A,A,A,B,A,A,A,A,A,B,A,A,A,A,A,B,A,A,A,A,A,B,A,A,A,A,A,B,A,A,A,A,A,B,A,A,A,A,A,B,B,A,A,A,A,B,C,A,A,A,A,B,E,A,A,A,A,B,F,A,A,A,A,B,R,A,A,A,A,B,X,A,A,A,A,B,N,A,A,A,A,B,L,A,A,A,A,B,d,K,A,A,A,B,j,D,A,A,A,B,10,0,A,A,A,B,21,1,A,A,A,B,43,3,A,A,A,B,87,7,A,A,A,B,U,15,A,A,A,B,95,31,A,A,A,B,K,d,G,A,A,B,D,j,K,A,A,B,0,10,129,A,A,B,1,21,3,A,A,B,3,43,7,A,A,B,7,87,15,A,A,B,15,U,15,A,A,B,31,95,31,A,A,B,K,K,m,B,A,B,D,D,q,225,A,B,0,0,p,r,A,B,1,3,23,Z,A,B,3,7,47,7,A,B,7,15,95,15,A,B,15,15,U,15,A,B,31,31,95,31,A,B,A,A,A,A,B,B,A,A,A,A,C,B,2,2,a,k,W,B,7,7,7,151,V,B,7,7,7,47,Z,B,15,15,15,95,15,B,15,15,15,U,15,B,31,31,31,95,31,B,0,0,0,0,0,0,A,A,A,A,A,G,2,2,a,d,l,f,7,7,7,Z,215,230,7,7,7,7,U,198,15,15,15,15,95,142,15,15,15,15,U,30,31,31,31,31,95,62,A,A,A,A,A,B,A,A,A,A,A,B,30,14,y,198,m,B,31,15,7,Z,V,b,31,15,7,7,Z,238,31,15,15,15,15,222,31,15,15,15,15,190,31,31,31,31,31,M,A,A,A,A,A,C,A,A,A,A,A,C,A,A,A,A,A,C,A,A,A,A,A,C,A,A,A,A,A,C,A,A,A,A,A,C,A,A,A,A,A,C,A,A,A,A,A,C,B,A,A,A,A,C,C,A,A,A,A,C,E,A,A,A,A,C,F,A,A,A,A,C,R,A,A,A,A,C,X,A,A,A,A,C,N,A,A,A,A,C,L,A,A,A,A,C,O,I,A,A,A,C,j,D,A,A,A,C,10,0,A,A,A,C,21,1,A,A,A,C,43,3,A,A,A,C,87,7,A,A,A,C,U,15,A,A,A,C,95,31,A,A,A,C,I,O,G,A,A,C,D,j,I,A,A,C,0,10,129,A,A,C,1,21,3,A,A,C,3,43,7,A,A,C,7,87,15,A,A,C,15,U,31,A,A,C,31,95,31,A,A,C,I,G,B,B,A,C,D,D,q,C,A,C,0,0,139,r,A,C,1,3,23,Z,A,C,3,7,47,15,A,C,7,15,95,15,A,C,15,31,N,31,A,C,31,31,95,31,A,C,A,A,A,A,B,C,A,A,A,A,C,C,A,A,A,A,E,C,7,7,7,151,V,C,15,15,15,47,T,C,15,15,15,95,15,C,31,31,31,N,31,C,31,31,31,95,31,C,A,A,A,A,A,G,0,0,0,0,0,0,A,A,A,A,A,h,7,7,7,Z,215,i,15,15,15,15,U,205,15,15,15,15,95,141,31,31,31,31,N,29,31,31,31,31,95,61,A,A,A,A,A,C,A,A,A,A,A,C,A,A,A,A,A,C,63,31,15,T,V,C,63,31,15,15,T,237,63,31,15,15,15,221,63,31,31,31,31,189,63,31,31,31,31,125,A,A,A,A,A,E,A,A,A,A,A,E,A,A,A,A,A,E,A,A,A,A,A,E,A,A,A,A,A,E,A,A,A,A,A,E,A,A,A,A,A,E,A,A,A,A,A,E,B,A,A,A,A,E,C,A,A,A,A,E,E,A,A,A,A,E,F,A,A,A,A,E,R,A,A,A,A,E,X,A,A,A,A,E,N,A,A,A,A,E,L,A,A,A,A,E,O,I,A,A,A,E,c,J,A,A,A,E,10,0,A,A,A,E,21,1,A,A,A,E,43,3,A,A,A,E,87,7,A,A,A,E,U,15,A,A,A,E,95,31,A,A,A,E,I,O,G,A,A,E,J,c,I,A,A,E,0,10,Y,A,A,E,1,21,3,A,A,E,3,43,7,A,A,E,7,87,15,A,A,E,15,U,31,A,A,E,31,95,63,A,A,E,I,G,B,B,A,E,J,I,C,G,A,E,0,0,p,E,A,E,1,3,23,Z,A,E,3,7,47,15,A,E,7,15,95,31,A,E,15,31,N,31,A,E,31,63,L,63,A,E,B,B,B,B,B,E,A,A,A,A,C,E,A,A,A,A,E,E,A,A,A,A,F,E,15,15,15,47,T,E,31,31,31,95,31,E,31,31,31,N,31,E,63,63,63,L,63,E,B,B,B,B,B,O,A,A,A,A,A,h,0,0,0,0,0,0,A,A,A,A,A,e,15,15,15,15,U,203,31,31,31,31,95,155,31,31,31,31,N,27,63,63,63,63,L,59,B,B,B,B,B,O,A,A,A,A,A,E,A,A,A,A,A,E,A,A,A,A,A,E,L,63,31,31,T,E,L,63,31,31,31,219,L,63,31,31,31,187,L,63,63,63,63,123,A,A,A,A,A,F,A,A,A,A,A,F,A,A,A,A,A,F,A,A,A,A,A,F,A,A,A,A,A,F,A,A,A,A,A,F,A,A,A,A,A,F,A,A,A,A,A,F,B,A,A,A,A,F,C,A,A,A,A,F,E,A,A,A,A,F,F,A,A,A,A,F,R,A,A,A,A,F,X,A,A,A,A,F,N,A,A,A,A,F,L,A,A,A,A,F,O,I,A,A,A,F,c,J,A,A,A,F,l,H,A,A,A,F,20,0,A,A,A,F,43,3,A,A,A,F,87,7,A,A,A,F,U,15,A,A,A,F,95,31,A,A,A,F,I,O,G,A,A,F,J,c,I,A,A,F,H,l,J,A,A,F,0,20,W,A,A,F,3,43,7,A,A,F,7,87,15,A,A,F,15,U,31,A,A,F,31,95,63,A,A,F,I,G,B,B,A,F,J,I,C,G,A,F,H,J,O,I,A,F,0,0,20,F,A,F,3,7,47,15,A,F,7,15,95,31,A,F,15,31,N,63,A,F,31,63,L,63,A,F,B,B,B,B,B,F,G,G,G,C,G,F,A,A,A,A,E,F,A,A,A,A,F,F,A,A,A,A,R,F,31,31,31,95,31,F,63,63,63,N,63,F,63,63,63,L,63,F,B,B,B,B,B,b,G,G,G,G,C,S,A,A,A,A,A,e,0,0,0,0,0,0,A,A,A,A,A,n,31,31,31,31,95,151,63,63,63,63,N,55,63,63,63,63,L,55,B,B,B,B,B,b,B,B,B,B,G,F,A,A,A,A,A,F,A,A,A,A,A,F,A,A,A,A,A,F,L,L,63,63,31,F,L,L,63,63,63,183,L,L,63,63,63,u,A,A,A,A,A,A,A,A,A,A,A,A,A,A,A,A,A,A,A,A,A,A,A,A,A,A,A,A,A,A,A,A,A,A,A,A,A,A,A,A,A,A,A,A,A,A,A,A,B,A,A,A,A,A,C,A,A,A,A,A,E,A,A,A,A,A,F,A,A,A,A,A,R,A,A,A,A,A,X,A,A,A,A,A,N,A,A,A,A,A,L,A,A,A,A,A,d,K,A,A,A,A,j,D,A,A,A,A,10,0,A,A,A,A,20,0,A,A,A,A,41,1,A,A,A,A,83,3,A,A,A,A,o,7,A,A,A,A,79,15,A,A,A,A,K,d,H,A,A,A,D,j,K,A,A,A,0,10,D,A,A,A,0,20,1,A,A,A,1,41,3,A,A,A,3,83,7,A,A,A,7,o,15,A,A,A,15,79,31,A,A,A,K,K,m,G,A,A,D,D,q,H,A,A,0,0,p,Q,A,A,0,0,21,P,A,A,1,1,43,7,A,A,3,3,87,15,A,A,7,7,U,15,A,A,15,15,95,31,A,A,K,K,H,f,B,A,D,D,K,i,J,A,0,0,D,k,W,A,0,0,3,151,V,A,1,3,7,47,Z,A,3,7,15,95,15,A,7,15,15,U,15,A,15,31,31,95,31,A,A,A,A,A,A,B,A,A,A,A,A,C,2,2,a,d,l,f,7,7,7,Z,215,n,7,7,7,7,U,V,15,15,15,15,95,T,15,15,15,15,U,31,31,31,31,31,95,63,0,0,0,0,0,0,A,A,A,A,A,A,2,2,a,d,m,O,7,7,7,Z,V,F,7,7,7,7,Z,R,15,15,15,15,15,X,15,15,15,15,15,N,31,31,31,31,31,L,A,A,A,A,A,A,A,A,A,A,A,A,A,A,A,A,A,A,A,A,A,A,A,A,A,A,A,A,A,A,A,A,A,A,A,A,A,A,A,A,A,A,A,A,A,A,A,A,B,A,A,A,A,A,C,A,A,A,A,A,E,A,A,A,A,A,F,A,A,A,A,A,R,A,A,A,A,A,X,A,A,A,A,A,N,A,A,A,A,A,L,A,A,A,A,A,f,J,A,A,A,A,j,D,A,A,A,A,10,0,A,A,A,A,20,0,A,A,A,A,41,1,A,A,A,A,83,3,A,A,A,A,o,7,A,A,A,A,79,15,A,A,A,A,J,f,I,A,A,A,D,j,K,A,A,A,0,10,D,A,A,A,0,20,1,A,A,A,1,41,3,A,A,A,3,83,7,A,A,A,7,o,15,A,A,A,15,79,31,A,A,A,J,J,O,G,A,A,D,D,q,I,A,A,0,0,p,Q,A,A,0,0,21,P,A,A,1,1,43,7,A,A,3,3,87,15,A,A,7,7,U,31,A,A,15,15,95,31,A,A,J,I,G,B,B,A,D,D,K,i,C,A,0,0,D,203,W,A,0,0,3,151,V,A,1,3,7,47,T,A,3,7,15,95,15,A,7,15,31,N,31,A,15,31,31,95,31,A,A,A,A,A,A,B,A,A,A,A,A,C,A,A,A,A,A,E,7,7,7,Z,215,n,15,15,15,15,U,207,15,15,15,15,95,T,31,31,31,31,N,31,31,31,31,31,95,63,A,A,A,A,A,A,0,0,0,0,0,0,A,A,A,A,A,A,7,7,7,Z,V,F,15,15,15,15,T,R,15,15,15,15,15,X,31,31,31,31,31,N,31,31,31,31,31,L,A,A,A,A,A,A,A,A,A,A,A,A,A,A,A,A,A,A,A,A,A,A,A,A,A,A,A,A,A,A,A,A,A,A,A,A,A,A,A,A,A,A,A,A,A,A,A,A,B,A,A,A,A,A,C,A,A,A,A,A,E,A,A,A,A,A,F,A,A,A,A,A,R,A,A,A,A,A,X,A,A,A,A,A,N,A,A,A,A,A,L,A,A,A,A,A,f,J,A,A,A,A,i,H,A,A,A,A,10,0,A,A,A,A,20,0,A,A,A,A,41,1,A,A,A,A,83,3,A,A,A,A,o,7,A,A,A,A,79,15,A,A,A,A,J,f,I,A,A,A,H,i,J,A,A,A,0,10,D,A,A,A,0,20,1,A,A,A,1,41,3,A,A,A,3,83,7,A,A,A,7,o,15,A,A,A,15,79,31,A,A,A,J,J,O,G,A,A,H,H,c,I,A,A,0,0,p,Y,A,A,0,0,21,P,A,A,1,1,43,7,A,A,3,3,87,15,A,A,7,7,U,31,A,A,15,15,95,63,A,A,J,I,G,B,B,A,H,J,I,C,G,A,0,0,D,k,E,A,0,0,3,151,V,A,1,3,7,47,T,A,3,7,15,95,31,A,7,15,31,N,31,A,15,31,63,L,63,A,B,B,B,B,B,B,A,A,A,A,A,C,A,A,A,A,A,E,A,A,A,A,A,F,15,15,15,15,U,207,31,31,31,31,95,159,31,31,31,31,N,31,63,63,63,63,L,63,B,B,B,B,B,B,A,A,A,A,A,A,0,0,0,0,0,0,A,A,A,A,A,A,15,15,15,15,T,R,31,31,31,31,31,X,31,31,31,31,31,N,63,63,63,63,63,L,A,A,A,A,A,A,A,A,A,A,A,A,A,A,A,A,A,A,A,A,A,A,A,A,A,A,A,A,A,A,A,A,A,A,A,A,A,A,A,A,A,A,A,A,A,A,A,A,B,A,A,A,A,A,C,A,A,A,A,A,E,A,A,A,A,A,F,A,A,A,A,A,R,A,A,A,A,A,X,A,A,A,A,A,N,A,A,A,A,A,L,A,A,A,A,A,f,J,A,A,A,A,i,H,A,A,A,A,k,K,A,A,A,A,20,0,A,A,A,A,41,1,A,A,A,A,83,3,A,A,A,A,o,7,A,A,A,A,79,15,A,A,A,A,J,f,I,A,A,A,H,i,J,A,A,A,K,k,H,A,A,A,0,20,0,A,A,A,1,41,3,A,A,A,3,83,7,A,A,A,7,o,15,A,A,A,15,79,31,A,A,A,J,J,O,G,A,A,H,H,c,I,A,A,K,K,l,J,A,A,0,0,20,W,A,A,1,1,43,7,A,A,3,3,87,15,A,A,7,7,U,31,A,A,15,15,95,63,A,A,J,I,G,B,B,A,H,J,I,C,G,A,K,H,J,O,I,A,0,0,0,s,F,A,1,3,7,47,T,A,3,7,15,95,31,A,7,15,31,N,63,A,15,31,63,L,63,A,B,B,B,B,B,B,G,G,G,G,C,G,A,A,A,A,A,E,A,A,A,A,A,F,A,A,A,A,A,R,31,31,31,31,95,159,63,63,63,63,N,63,63,63,63,63,L,63,B,B,B,B,G,B,G,G,G,G,G,C,A,A,A,A,A,A,0,0,0,0,0,0,A,A,A,A,A,A,31,31,31,31,31,X,63,63,63,63,63,N,63,63,63,63,63,L,0,0,0,0,0,0,A,A,A,A,A,A,e,A,A,A,A,A,W,A,A,A,A,A,V,A,A,A,A,A,T,A,A,A,A,A,31,A,A,A,A,A,63,A,A,A,A,A,B,A,A,A,A,A,C,A,A,A,A,A,0,0,A,A,A,A,2,0,A,A,A,A,6,2,A,A,A,A,14,6,A,A,A,A,14,14,A,A,A,A,15,15,A,A,A,A,D,D,K,A,A,A,0,0,D,A,A,A,0,0,0,A,A,A,0,0,0,A,A,A,2,0,0,A,A,A,6,2,0,A,A,A,14,6,3,A,A,A,15,15,7,A,A,A,D,D,K,H,A,A,0,0,D,K,A,A,0,0,0,D,A,A,0,0,0,0,A,A,2,0,0,0,A,A,6,2,0,1,A,A,14,6,3,3,A,A,15,15,7,7,A,A,D,D,K,H,J,A,0,0,D,K,H,A,0,0,0,D,K,A,0,0,0,0,D,A,2,0,0,0,1,A,6,2,0,1,3,A,14,6,3,3,7,A,15,15,7,7,15,A,D,D,K,H,J,I,0,0,D,K,H,J,0,0,0,D,K,H,0,0,0,0,D,Q,2,0,0,0,1,P,6,2,0,1,3,7,14,6,3,3,7,15,15,15,7,7,15,31,D,D,K,H,J,I,0,0,D,K,H,J,0,0,0,D,K,H,0,0,0,0,D,Q,2,0,0,0,1,P,6,2,0,1,3,7,14,6,3,3,7,15,15,15,7,7,15,31,D,D,K,H,J,I,0,0,D,K,H,J,0,0,0,D,K,H,0,0,0,0,D,Q,2,0,0,0,1,P,6,2,0,1,3,7,14,6,3,3,7,15,15,15,7,7,15,31,A,A,A,A,A,A,0,0,0,0,0,0,A,A,A,A,A,A,n,A,A,A,A,A,V,A,A,A,A,A,T,A,A,A,A,A,31,A,A,A,A,A,63,A,A,A,A,A,B,A,A,A,A,A,C,A,A,A,A,A,E,A,A,A,A,A,2,0,A,A,A,A,6,2,A,A,A,A,14,6,A,A,A,A,30,14,A,A,A,A,31,31,A,A,A,A,D,D,K,A,A,A,0,0,D,A,A,A,0,0,0,A,A,A,0,0,0,A,A,A,2,0,0,A,A,A,6,2,0,A,A,A,14,6,3,A,A,A,31,15,7,A,A,A,D,D,K,H,A,A,0,0,D,K,A,A,0,0,0,D,A,A,0,0,0,0,A,A,2,0,0,0,A,A,6,2,0,1,A,A,14,6,3,3,A,A,31,15,7,7,A,A,D,D,K,H,J,A,0,0,D,K,H,A,0,0,0,D,K,A,0,0,0,0,D,A,2,0,0,0,1,A,6,2,0,1,3,A,14,6,3,3,7,A,31,15,7,7,15,A,D,D,K,H,J,I,0,0,D,K,H,J,0,0,0,D,K,H,0,0,0,0,D,Q,2,0,0,0,1,P,6,2,0,1,3,7,14,6,3,3,7,15,31,15,7,7,15,31,D,D,K,H,J,I,0,0,D,K,H,J,0,0,0,D,K,H,0,0,0,0,D,Q,2,0,0,0,1,P,6,2,0,1,3,7,14,6,3,3,7,15,31,15,7,7,15,31,D,D,K,H,J,I,0,0,D,K,H,J,0,0,0,D,K,H,0,0,0,0,D,Q,2,0,0,0,1,P,6,2,0,1,3,7,14,6,3,3,7,15,31,15,7,7,15,31,B,A,A,A,A,A,A,A,A,A,A,A,0,0,0,0,0,0,A,A,A,A,A,A,207,A,A,A,A,A,T,A,A,A,A,A,31,A,A,A,A,A,63,A,A,A,A,A,S,232,A,A,A,A,C,A,A,A,A,A,E,A,A,A,A,A,F,A,A,A,A,A,4,2,A,A,A,A,14,6,A,A,A,A,30,14,A,A,A,A,63,31,A,A,A,A,J,H,K,A,A,A,0,0,D,A,A,A,0,0,0,A,A,A,0,0,0,A,A,A,0,0,0,A,A,A,6,2,0,A,A,A,14,6,3,A,A,A,31,15,7,A,A,A,J,H,K,H,A,A,0,0,D,K,A,A,0,0,0,D,A,A,0,0,0,0,A,A,0,0,0,0,A,A,6,2,0,1,A,A,14,6,3,3,A,A,31,15,7,7,A,A,J,H,K,H,J,A,0,0,D,K,H,A,0,0,0,D,K,A,0,0,0,0,D,A,0,0,0,0,1,A,6,2,0,1,3,A,14,6,3,3,7,A,31,15,7,7,15,A,J,H,K,H,J,I,0,0,D,K,H,J,0,0,0,D,K,H,0,0,0,0,D,Q,0,0,0,0,1,P,6,2,0,1,3,7,14,6,3,3,7,15,31,15,7,7,15,31,J,H,K,H,J,I,0,0,D,K,H,J,0,0,0,D,K,H,0,0,0,0,D,Q,0,0,0,0,1,P,6,2,0,1,3,7,14,6,3,3,7,15,31,15,7,7,15,31,J,H,K,H,J,I,0,0,D,K,H,J,0,0,0,D,K,H,0,0,0,0,D,Q,0,0,0,0,1,P,6,2,0,1,3,7,14,6,3,3,7,15,31,15,7,7,15,31,G,A,A,A,A,A,G,A,A,A,A,A,A,A,A,A,A,A,0,0,0,0,0,0,A,A,A,A,A,A,159,A,A,A,A,A,31,A,A,A,A,A,63,A,A,A,A,A,G,I,A,A,A,A,104,80,A,A,A,A,E,A,A,A,A,A,F,A,A,A,A,A,R,A,A,A,A,A,10,4,A,A,A,A,30,14,A,A,A,A,63,31,A,A,A,A,I,J,H,A,A,A,96,64,D,A,A,A,0,0,0,A,A,A,0,0,0,A,A,A,0,0,0,A,A,A,2,0,0,A,A,A,14,6,3,A,A,A,31,15,7,A,A,A,I,J,H,H,A,A,96,64,D,K,A,A,0,0,0,D,A,A,0,0,0,0,A,A,0,0,0,0,A,A,2,0,0,1,A,A,14,6,3,3,A,A,31,15,7,7,A,A,I,J,H,H,J,A,96,64,D,K,H,A,0,0,0,D,K,A,0,0,0,0,D,A,0,0,0,0,1,A,2,0,0,1,3,A,14,6,3,3,7,A,31,15,7,7,15,A,I,J,H,H,J,I,96,64,D,K,H,J,0,0,0,D,K,H,0,0,0,0,D,Q,0,0,0,0,1,P,2,0,0,1,3,7,14,6,3,3,7,15,31,15,7,7,15,31,I,J,H,H,J,I,96,64,D,K,H,J,0,0,0,D,K,H,0,0,0,0,D,Q,0,0,0,0,1,P,2,0,0,1,3,7,14,6,3,3,7,15,31,15,7,7,15,31,I,J,H,H,J,I,96,64,D,K,H,J,0,0,0,D,K,H,0,0,0,0,D,Q,0,0,0,0,1,P,2,0,0,1,3,7,14,6,3,3,7,15,31,15,7,7,15,31,B,A,A,A,A,A,B,A,A,A,A,A,f,A,A,A,A,A,m,A,A,A,A,A,198,A,A,A,A,A,142,A,A,A,A,A,30,A,A,A,A,A,62,A,A,A,A,A,0,0,0,0,0,0,G,A,A,A,A,A,2,2,A,A,A,A,2,2,A,A,A,A,6,7,A,A,A,A,14,7,A,A,A,A,14,15,A,A,A,A,14,15,A,A,A,A,B,B,A,A,A,A,B,C,A,A,A,A,0,0,0,A,A,A,2,2,0,A,A,A,6,6,2,A,A,A,6,6,6,A,A,A,14,14,15,A,A,A,14,15,15,A,A,A,D,D,K,H,A,A,0,0,D,K,A,A,0,0,0,D,A,A,2,0,0,0,A,A,6,2,0,0,A,A,6,6,2,1,A,A,14,14,7,3,A,A,14,15,15,7,A,A,D,D,K,H,J,A,0,0,D,K,H,A,0,0,0,D,K,A,2,0,0,0,D,A,6,2,0,0,1,A,6,6,2,1,3,A,14,14,7,3,7,A,14,15,15,7,15,A,D,D,K,H,J,I,0,0,D,K,H,J,0,0,0,D,K,H,2,0,0,0,D,Q,6,2,0,0,1,P,6,6,2,1,3,7,14,14,7,3,7,15,14,15,15,7,15,31,D,D,K,H,J,I,0,0,D,K,H,J,0,0,0,D,K,H,2,0,0,0,D,Q,6,2,0,0,1,P,6,6,2,1,3,7,14,14,7,3,7,15,14,15,15,7,15,31,B,D,K,H,J,I,14,0,D,K,H,J,14,0,0,D,K,H,14,0,0,0,D,Q,14,2,0,0,1,P,14,6,2,1,3,7,14,14,7,3,7,15,14,15,15,7,15,31,C,A,A,A,A,A,C,A,A,A,A,A,C,A,A,A,A,A,i,A,A,A,A,A,q,A,A,A,A,A,141,A,A,A,A,A,29,A,A,A,A,A,61,A,A,A,A,A,G,A,A,A,A,A,0,0,0,0,0,0,h,A,A,A,A,A,5,7,A,A,A,A,5,7,A,A,A,A,13,15,A,A,A,A,29,15,A,A,A,A,29,31,A,A,A,A,C,B,A,A,A,A,C,C,A,A,A,A,C,E,A,A,A,A,0,2,0,A,A,A,4,6,2,A,A,A,12,14,6,A,A,A,12,14,15,A,A,A,29,31,31,A,A,A,D,D,K,H,A,A,0,0,D,K,A,A,0,0,0,D,A,A,0,0,0,0,A,A,4,2,0,0,A,A,12,6,2,1,A,A,12,14,7,3,A,A,29,31,15,7,A,A,D,D,K,H,J,A,0,0,D,K,H,A,0,0,0,D,K,A,0,0,0,0,D,A,4,2,0,0,1,A,12,6,2,1,3,A,12,14,7,3,7,A,29,31,15,7,15,A,D,D,K,H,J,I,0,0,D,K,H,J,0,0,0,D,K,H,0,0,0,0,D,Q,4,2,0,0,1,P,12,6,2,1,3,7,12,14,7,3,7,15,29,31,15,7,15,31,D,D,K,H,J,I,0,0,D,K,H,J,0,0,0,D,K,H,0,0,0,0,D,Q,4,2,0,0,1,P,12,6,2,1,3,7,12,14,7,3,7,15,29,31,15,7,15,31,G,D,K,H,J,I,z,0,D,K,H,J,28,0,0,D,K,H,28,0,0,0,D,Q,28,2,0,0,1,P,28,6,2,1,3,7,28,14,7,3,7,15,29,31,15,7,15,31,O,A,A,A,A,A,E,A,A,A,A,A,E,A,A,A,A,A,E,A,A,A,A,A,203,A,A,A,A,A,139,A,A,A,A,A,27,A,A,A,A,A,59,A,A,A,A,A,O,B,A,A,A,A,h,A,A,A,A,A,0,0,0,0,0,0,e,A,A,A,A,A,11,15,A,A,A,A,11,15,A,A,A,A,27,31,A,A,A,A,59,31,A,A,A,A,I,S,232,A,A,A,E,C,A,A,A,A,E,E,A,A,A,A,E,F,A,A,A,A,2,4,2,A,A,A,10,14,6,A,A,A,26,30,15,A,A,A,27,31,31,A,A,A,I,J,H,H,A,A,0,0,D,K,A,A,0,0,0,D,A,A,0,0,0,0,A,A,2,0,0,0,A,A,10,6,2,1,A,A,26,14,7,3,A,A,27,31,15,7,A,A,I,J,H,H,J,A,0,0,D,K,H,A,0,0,0,D,K,A,0,0,0,0,D,A,2,0,0,0,1,A,10,6,2,1,3,A,26,14,7,3,7,A,27,31,15,7,15,A,I,J,H,H,J,I,0,0,D,K,H,J,0,0,0,D,K,H,0,0,0,0,D,Q,2,0,0,0,1,P,10,6,2,1,3,7,26,14,7,3,7,15,27,31,15,7,15,31,I,J,H,H,J,I,0,0,D,K,H,J,0,0,0,D,K,H,0,0,0,0,D,Q,2,0,0,0,1,P,10,6,2,1,3,7,26,14,7,3,7,15,27,31,15,7,15,31,O,J,H,H,J,I,t,0,D,K,H,J,t,0,0,D,K,H,58,0,0,0,D,Q,58,0,0,0,1,P,58,6,2,1,3,7,58,14,7,3,7,15,59,31,15,7,15,31,S,A,A,A,A,A,S,A,A,A,A,A,F,A,A,A,A,A,F,A,A,A,A,A,F,A,A,A,A,A,151,A,A,A,A,A,23,A,A,A,A,A,55,A,A,A,A,A,S,G,A,A,A,A,S,G,A,A,A,A,e,A,A,A,A,A,0,0,0,0,0,0,n,A,A,A,A,A,23,31,A,A,A,A,23,31,A,A,A,A,55,63,A,A,A,A,S,G,I,A,A,A,v,104,208,A,A,A,F,E,A,A,A,A,F,F,A,A,A,A,F,R,A,A,A,A,6,10,4,A,A,A,22,30,15,A,A,A,55,63,31,A,A,A,S,I,J,H,A,A,v,96,K,K,A,A,0,0,0,D,A,A,0,0,0,0,A,A,0,0,0,0,A,A,6,2,0,1,A,A,22,14,7,3,A,A,55,31,15,7,A,A,S,I,J,H,J,A,v,96,K,K,H,A,0,0,0,D,K,A,0,0,0,0,D,A,0,0,0,0,1,A,6,2,0,1,3,A,22,14,7,3,7,A,55,31,15,7,15,A,S,I,J,H,J,I,v,96,K,K,H,J,0,0,0,D,K,H,0,0,0,0,D,Q,0,0,0,0,1,P,6,2,0,1,3,7,22,14,7,3,7,15,55,31,15,7,15,31,S,I,J,H,J,I,v,96,K,K,H,J,0,0,0,D,K,H,0,0,0,0,D,Q,0,0,0,0,1,P,6,2,0,1,3,7,22,14,7,3,7,15,55,31,15,7,15,31,b,I,J,H,J,I,g,96,K,K,H,J,g,0,0,D,K,H,g,0,0,0,D,Q,g,0,0,0,1,P,g,2,0,1,3,7,g,14,7,3,7,15,u,31,15,7,15,31,A,B,A,A,A,A,E,B,A,A,A,A,e,B,A,A,A,A,W,B,A,A,A,A,V,B,A,A,A,A,T,B,A,A,A,A,31,B,A,A,A,A,63,B,A,A,A,A,B,B,A,A,A,A,C,B,A,A,A,A,2,2,A,A,A,A,2,2,A,A,A,A,7,6,A,A,A,A,15,6,A,A,A,A,15,14,A,A,A,A,15,14,A,A,A,A,0,0,0,0,0,0,A,G,A,A,A,A,2,2,2,A,A,A,2,2,2,A,A,A,7,6,7,A,A,A,7,6,7,A,A,A,15,14,15,A,A,A,15,14,15,A,A,A,A,B,B,A,A,A,A,B,C,A,A,A,2,0,0,D,A,A,2,2,2,0,A,A,6,6,6,2,A,A,6,6,6,7,A,A,14,14,15,15,A,A,15,14,15,15,A,A,B,D,K,H,J,A,0,0,D,K,H,A,2,0,0,D,K,A,2,2,0,0,D,A,6,6,2,0,1,A,6,6,6,3,3,A,14,14,15,7,7,A,15,14,15,15,15,A,K,D,K,H,J,I,0,0,D,K,H,J,2,0,0,D,K,H,2,2,0,0,D,Q,6,6,2,0,1,P,6,6,6,3,3,7,14,14,15,7,7,15,15,14,15,15,15,31,B,D,K,H,J,I,6,0,D,K,H,J,6,0,0,D,K,H,6,2,0,0,D,Q,6,6,2,0,1,P,6,6,6,3,3,7,14,14,15,7,7,15,15,14,15,15,15,31,B,D,K,H,J,I,14,0,D,K,H,J,14,0,0,D,K,H,14,2,0,0,D,Q,14,6,2,0,1,P,14,6,6,3,3,7,14,14,15,7,7,15,15,14,15,15,15,31,A,C,A,A,A,A,A,C,A,A,A,A,F,C,A,A,A,A,n,C,A,A,A,A,V,C,A,A,A,A,T,C,A,A,A,A,31,C,A,A,A,A,63,C,A,A,A,A,B,C,A,A,A,A,C,C,A,A,A,A,E,C,A,A,A,A,7,5,A,A,A,A,7,5,A,A,A,A,15,13,A,A,A,A,31,13,A,A,A,A,31,29,A,A,A,A,A,G,A,A,A,A,0,0,0,0,0,0,A,h,A,A,A,A,7,5,7,A,A,A,7,5,7,A,A,A,15,13,15,A,A,A,15,13,15,A,A,A,31,29,31,A,A,A,A,C,B,A,A,A,A,C,C,A,A,A,A,C,E,A,A,A,6,0,2,0,A,A,6,4,6,2,A,A,14,12,14,7,A,A,14,12,15,15,A,A,31,29,31,31,A,A,D,D,K,H,J,A,M,0,D,K,H,A,0,0,0,D,K,A,6,0,0,0,D,A,6,4,2,0,1,A,14,12,6,3,3,A,14,12,15,7,7,A,31,29,31,15,15,A,D,D,K,H,J,I,0,0,D,K,H,J,0,0,0,D,K,H,6,0,0,0,D,Q,6,4,2,0,1,P,14,12,6,3,3,7,14,12,15,7,7,15,31,29,31,15,15,31,B,D,K,H,J,I,M,0,D,K,H,J,14,0,0,D,K,H,14,0,0,0,D,Q,14,4,2,0,1,P,14,12,6,3,3,7,14,12,15,7,7,15,31,29,31,15,15,31,B,D,K,H,J,I,M,0,D,K,H,J,30,0,0,D,K,H,30,0,0,0,D,Q,30,4,2,0,1,P,30,12,6,3,3,7,30,12,15,7,7,15,31,29,31,15,15,31,B,E,A,A,A,A,B,E,A,A,A,A,A,E,A,A,A,A,R,E,A,A,A,A,207,E,A,A,A,A,T,E,A,A,A,A,31,E,A,A,A,A,63,E,A,A,A,A,B,O,A,A,A,A,C,E,A,A,A,A,E,E,A,A,A,A,F,E,A,A,A,A,15,11,A,A,A,A,15,11,A,A,A,A,31,27,A,A,A,A,63,27,A,A,A,A,B,O,B,A,A,A,A,h,A,A,A,A,0,0,0,0,0,0,A,e,A,A,A,A,15,11,15,A,A,A,15,11,15,A,A,A,31,27,31,A,A,A,31,27,31,A,A,A,B,I,S,232,A,A,A,E,C,A,A,A,A,E,E,A,A,A,A,E,F,A,A,A,14,2,4,2,A,A,14,10,14,7,A,A,30,26,31,15,A,A,31,27,31,31,A,A,B,I,J,H,J,A,0,0,D,K,H,A,M,0,0,D,K,A,0,0,0,0,D,A,14,2,0,0,1,A,14,10,6,3,3,A,30,26,15,7,7,A,31,27,31,15,15,A,B,I,J,H,J,I,0,0,D,K,H,J,0,0,0,D,K,H,0,0,0,0,D,Q,14,2,0,0,1,P,14,10,6,3,3,7,30,26,15,7,7,15,31,27,31,15,15,31,B,I,J,H,J,I,M,0,D,K,H,J,M,0,0,D,K,H,30,0,0,0,D,Q,30,2,0,0,1,P,30,10,6,3,3,7,30,26,15,7,7,15,31,27,31,15,15,31,B,I,J,H,J,I,M,0,D,K,H,J,M,0,0,D,K,H,62,0,0,0,D,Q,62,2,0,0,1,P,62,10,6,3,3,7,62,26,15,7,7,15,63,27,31,15,15,31,G,F,A,A,A,A,G,F,A,A,A,A,C,F,A,A,A,A,A,F,A,A,A,A,X,F,A,A,A,A,159,F,A,A,A,A,31,F,A,A,A,A,63,F,A,A,A,A,G,S,A,A,A,A,G,S,A,A,A,A,E,F,A,A,A,A,F,F,A,A,A,A,R,F,A,A,A,A,31,23,A,A,A,A,31,23,A,A,A,A,63,55,A,A,A,A,G,S,G,A,A,A,G,S,G,A,A,A,A,e,A,A,A,A,0,0,0,0,0,0,A,n,A,A,A,A,31,23,31,A,A,A,31,23,31,A,A,A,63,55,63,A,A,A,G,S,G,I,A,A,z,v,232,208,A,A,A,F,E,A,A,A,A,F,F,A,A,A,A,F,R,A,A,A,30,6,10,5,A,A,30,22,31,15,A,A,63,55,63,31,A,A,G,S,I,J,J,A,z,v,H,K,H,A,0,0,0,D,K,A,M,0,0,0,D,A,0,0,0,0,1,A,30,6,2,1,3,A,30,22,15,7,7,A,63,55,31,15,15,A,G,S,I,J,J,I,z,v,H,K,H,J,0,0,0,D,K,H,0,0,0,0,D,Q,0,0,0,0,1,P,30,6,2,1,3,7,30,22,15,7,7,15,63,55,31,15,15,31,B,S,I,J,J,I,M,v,H,K,H,J,M,0,0,D,K,H,M,0,0,0,D,Q,62,0,0,0,1,P,62,6,2,1,3,7,62,22,15,7,7,15,63,55,31,15,15,31,B,S,I,J,J,I,M,v,H,K,H,J,M,0,0,D,K,H,M,0,0,0,D,Q,M,0,0,0,1,P,M,6,2,1,3,7,M,22,15,7,7,15,L,55,31,15,15,31,G,A,B,A,A,A,I,A,B,A,A,A,Y,A,B,A,A,A,W,A,B,A,A,A,V,A,B,A,A,A,T,A,B,A,A,A,31,A,B,A,A,A,63,A,B,A,A,A,G,A,B,A,A,A,8,3,B,A,A,A,1,3,B,A,A,A,3,3,B,A,A,A,7,7,B,A,A,A,15,7,B,A,A,A,15,15,B,A,A,A,15,15,B,A,A,A,A,B,B,A,A,A,A,C,B,A,A,A,0,2,2,A,A,A,2,2,2,A,A,A,7,7,6,A,A,A,7,7,6,A,A,A,15,15,14,A,A,A,15,15,14,A,A,A,0,0,0,0,0,0,A,A,G,A,A,A,2,2,2,a,A,A,2,2,2,2,A,A,7,7,6,7,A,A,7,7,6,7,A,A,15,15,14,15,A,A,15,15,14,15,A,A,A,A,B,B,A,A,A,A,B,C,A,A,2,2,0,D,K,A,2,2,2,2,D,A,6,6,6,6,3,A,6,6,6,7,7,A,15,15,14,15,15,A,15,15,14,15,15,A,B,B,K,H,J,I,2,0,D,K,H,J,2,2,0,D,K,H,2,2,2,0,D,Q,6,6,6,2,3,P,6,6,6,7,7,7,15,15,14,15,15,15,15,15,14,15,15,31,B,K,K,H,J,I,6,0,D,K,H,J,6,2,0,D,K,H,6,2,2,0,D,Q,6,6,6,2,3,P,6,6,6,7,7,7,15,15,14,15,15,15,15,15,14,15,15,31,B,B,K,H,I,I,14,6,D,K,H,J,14,6,0,D,K,H,14,6,2,0,D,Q,14,6,6,2,3,P,14,6,6,7,7,7,15,15,14,15,15,15,15,15,14,15,15,31,G,A,C,A,A,A,I,A,C,A,A,A,Y,A,C,A,A,A,W,A,C,A,A,A,V,A,C,A,A,A,T,A,C,A,A,A,31,A,C,A,A,A,63,A,C,A,A,A,G,A,C,A,A,A,I,A,C,A,A,A,17,7,C,A,A,A,3,7,C,A,A,A,7,7,C,A,A,A,15,15,C,A,A,A,31,15,C,A,A,A,31,31,C,A,A,A,A,B,C,A,A,A,A,C,C,A,A,A,A,E,C,A,A,A,3,7,5,A,A,A,7,7,5,A,A,A,15,15,13,A,A,A,15,15,13,A,A,A,31,31,29,A,A,A,A,A,G,A,A,A,0,0,0,0,0,0,A,A,h,A,A,A,7,7,5,7,A,A,7,7,5,7,A,A,15,15,13,15,A,A,15,15,13,15,A,A,31,31,29,31,A,A,A,A,C,B,A,A,A,A,C,C,A,A,A,A,C,E,A,A,6,6,0,2,D,A,6,6,4,6,3,A,14,14,12,15,7,A,15,15,13,15,15,A,31,31,29,31,31,A,B,D,K,H,J,I,M,M,D,K,H,J,6,0,0,D,K,H,6,6,0,0,D,Q,6,6,4,2,3,P,14,14,12,7,7,7,15,15,13,15,15,15,31,31,29,31,31,31,B,D,K,H,J,I,M,0,D,K,H,J,14,0,0,D,K,H,14,6,0,0,D,Q,14,6,4,2,3,P,14,14,12,7,7,7,15,15,13,15,15,15,31,31,29,31,31,31,B,B,K,J,I,I,M,M,D,K,J,J,30,14,0,D,K,H,30,14,0,0,D,Q,30,14,4,2,3,P,30,14,12,7,7,7,31,15,13,15,15,15,31,31,29,31,31,31,G,A,E,A,A,A,I,A,E,A,A,A,Y,A,E,A,A,A,W,A,E,A,A,A,V,A,E,A,A,A,T,A,E,A,A,A,31,A,E,A,A,A,63,A,E,A,A,A,G,B,E,A,A,A,I,B,E,A,A,A,Y,A,E,A,A,A,35,15,E,A,A,A,7,15,E,A,A,A,15,15,E,A,A,A,31,31,E,A,A,A,63,31,E,A,A,A,G,B,O,A,A,A,A,C,E,A,A,A,A,E,E,A,A,A,A,F,E,A,A,A,7,15,11,A,A,A,15,15,11,A,A,A,31,31,27,A,A,A,31,31,27,A,A,A,B,B,O,B,A,A,A,A,h,A,A,A,0,0,0,0,0,0,A,A,e,A,A,A,15,15,11,15,A,A,15,15,11,15,A,A,31,31,27,31,A,A,31,31,27,31,A,A,B,B,I,S,I,A,A,A,E,C,A,A,A,A,E,E,A,A,A,A,E,F,A,A,14,14,2,4,3,A,14,14,10,15,7,A,31,31,27,31,15,A,31,31,27,31,31,A,B,B,I,J,I,I,M,0,D,K,H,J,M,M,0,D,K,H,14,0,0,0,D,Q,14,14,2,0,3,P,14,14,10,7,7,7,31,31,27,15,15,15,31,31,27,31,31,31,B,B,I,J,I,I,M,0,D,K,H,J,M,0,0,D,K,H,30,0,0,0,D,Q,30,14,2,0,3,P,30,14,10,7,7,7,31,31,27,15,15,15,31,31,27,31,31,31,B,B,I,J,I,I,M,M,D,H,J,J,M,M,0,D,H,H,62,30,0,0,D,Q,62,30,2,0,3,P,62,30,10,7,7,7,63,31,27,15,15,15,63,31,27,31,31,31,G,A,F,A,A,A,I,A,F,A,A,A,Y,A,F,A,A,A,W,A,F,A,A,A,V,A,F,A,A,A,T,A,F,A,A,A,31,A,F,A,A,A,63,A,F,A,A,A,G,G,F,A,A,A,I,G,F,A,A,A,Y,G,F,A,A,A,W,A,F,A,A,A,71,31,F,A,A,A,15,31,F,A,A,A,31,31,F,A,A,A,63,63,F,A,A,A,G,G,S,A,A,A,I,G,S,A,A,A,A,E,F,A,A,A,A,F,F,A,A,A,A,R,F,A,A,A,15,31,23,A,A,A,31,31,23,A,A,A,63,63,55,A,A,A,G,G,S,G,A,A,G,G,S,G,A,A,A,A,e,A,A,A,0,0,0,0,0,0,A,A,n,A,A,A,31,31,23,31,A,A,31,31,23,31,A,A,63,63,55,63,A,A,G,G,S,G,I,A,G,G,J,232,J,A,A,A,F,E,A,A,A,A,F,F,A,A,A,A,F,R,A,A,30,30,6,11,7,A,31,31,23,31,15,A,63,63,55,63,31,A,G,G,S,I,I,I,G,G,J,H,J,J,z,0,0,D,K,H,M,M,0,0,D,Q,30,0,0,0,1,P,30,30,6,3,7,7,31,31,23,15,15,15,63,63,55,31,31,31,B,G,S,I,I,I,B,G,J,H,J,J,M,0,0,D,K,H,M,0,0,0,D,Q,62,0,0,0,1,P,62,30,6,3,7,7,63,31,23,15,15,15,63,63,55,31,31,31,B,B,S,I,I,I,B,B,J,H,J,J,M,M,0,K,H,H,M,M,0,0,K,Q,M,62,0,0,3,P,M,62,6,3,7,7,L,63,23,15,15,15,L,63,55,31,31,31,G,A,A,B,A,A,I,A,A,B,A,A,Y,A,A,B,A,A,W,A,A,B,A,A,V,A,A,B,A,A,T,A,A,B,A,A,31,A,A,B,A,A,63,A,A,B,A,A,G,D,A,B,A,A,8,0,A,B,A,A,1,1,A,B,A,A,3,3,A,B,A,A,7,7,A,B,A,A,15,7,A,B,A,A,15,15,A,B,A,A,15,15,A,B,A,A,G,D,A,B,A,A,0,0,P,B,A,A,0,0,3,B,A,A,2,2,3,B,A,A,7,7,7,B,A,A,7,7,7,B,A,A,15,15,15,B,A,A,15,15,15,B,A,A,A,A,B,B,A,A,A,A,C,B,A,A,0,0,2,a,A,A,2,2,2,2,A,A,7,7,7,6,A,A,7,7,7,6,A,A,15,15,15,14,A,A,15,15,15,14,A,A,0,0,0,0,0,0,A,A,A,G,A,A,2,2,2,a,d,A,2,2,2,2,a,A,7,7,7,6,7,A,7,7,7,6,7,A,15,15,15,14,15,A,15,15,15,14,15,A,A,A,A,B,B,A,A,A,A,B,C,A,2,2,2,D,K,H,2,2,2,2,a,Q,6,6,6,6,7,P,7,7,7,6,7,7,15,15,15,14,15,15,15,15,15,14,15,31,B,B,B,H,G,I,6,2,D,K,H,J,6,2,2,D,K,H,6,2,2,2,a,Q,6,6,6,6,7,P,7,7,7,6,7,7,15,15,15,14,15,15,15,15,15,14,15,31,B,B,H,H,G,I,14,6,D,K,H,J,14,6,2,D,K,H,14,6,2,2,a,Q,14,6,6,6,7,P,15,7,7,6,7,7,15,15,15,14,15,15,15,15,15,14,15,31,G,A,A,C,A,A,I,A,A,C,A,A,Y,A,A,C,A,A,W,A,A,C,A,A,V,A,A,C,A,A,T,A,A,C,A,A,31,A,A,C,A,A,63,A,A,C,A,A,G,G,A,C,A,A,I,0,A,C,A,A,17,1,A,C,A,A,3,3,A,C,A,A,7,7,A,C,A,A,15,15,A,C,A,A,31,15,A,C,A,A,31,31,A,C,A,A,G,G,A,C,A,A,I,0,A,C,A,A,1,1,7,C,A,A,3,3,7,C,A,A,7,7,7,C,A,A,15,15,15,C,A,A,15,15,15,C,A,A,31,31,31,C,A,A,A,A,B,C,A,A,A,A,C,C,A,A,A,A,E,C,A,A,3,3,7,5,A,A,7,7,7,5,A,A,15,15,15,13,A,A,15,15,15,13,A,A,31,31,31,29,A,A,A,A,A,G,A,A,0,0,0,0,0,0,A,A,A,h,A,A,7,7,7,5,Z,A,7,7,7,5,7,A,15,15,15,13,15,A,15,15,15,13,15,A,31,31,31,29,31,A,A,A,A,C,B,A,A,A,A,C,C,A,A,A,A,C,E,A,6,6,6,0,a,Q,6,6,6,4,7,P,15,15,15,13,15,7,15,15,15,13,15,15,31,31,31,29,31,31,B,B,K,I,G,I,M,M,B,K,I,J,14,6,0,D,K,H,14,6,6,0,a,Q,14,6,6,4,7,P,15,15,15,13,15,7,15,15,15,13,15,15,31,31,31,29,31,31,B,B,K,I,G,I,M,M,K,K,I,J,30,14,0,D,K,H,30,14,6,0,a,Q,30,14,6,4,7,P,31,15,15,13,15,7,31,15,15,13,15,15,31,31,31,29,31,31,G,A,A,E,A,A,I,A,A,E,A,A,Y,A,A,E,A,A,W,A,A,E,A,A,V,A,A,E,A,A,T,A,A,E,A,A,31,A,A,E,A,A,63,A,A,E,A,A,G,G,A,E,A,A,I,I,A,E,A,A,Y,0,A,E,A,A,35,3,A,E,A,A,7,7,A,E,A,A,15,15,A,E,A,A,31,31,A,E,A,A,63,31,A,E,A,A,G,G,B,E,A,A,I,I,B,E,A,A,Y,0,A,E,A,A,3,3,15,E,A,A,7,7,15,E,A,A,15,15,15,E,A,A,31,31,31,E,A,A,31,31,31,E,A,A,G,G,B,O,A,A,A,A,C,E,A,A,A,A,E,E,A,A,A,A,F,E,A,A,7,7,15,11,A,A,15,15,15,11,A,A,31,31,31,27,A,A,31,31,31,27,A,A,B,B,B,O,B,A,A,A,A,h,A,A,0,0,0,0,0,0,A,A,A,e,A,A,15,15,15,11,15,A,15,15,15,11,15,A,31,31,31,27,31,A,31,31,31,27,31,A,B,B,B,I,G,I,A,A,A,E,C,A,A,A,A,E,E,A,A,A,A,E,F,A,14,14,14,2,7,P,15,15,15,11,15,7,31,31,31,27,31,15,31,31,31,27,31,31,B,B,B,I,G,I,M,M,D,J,I,J,M,M,M,D,J,H,30,14,0,0,a,Q,30,14,14,2,7,P,31,15,15,11,15,7,31,31,31,27,31,15,31,31,31,27,31,31,B,B,B,I,G,I,M,M,D,J,I,J,M,M,0,D,J,H,62,30,0,0,a,Q,62,30,14,2,7,P,63,31,15,11,15,7,63,31,31,27,31,15,63,31,31,27,31,31,G,A,A,F,A,A,I,A,A,F,A,A,Y,A,A,F,A,A,W,A,A,F,A,A,V,A,A,F,A,A,T,A,A,F,A,A,31,A,A,F,A,A,63,A,A,F,A,A,G,G,A,F,A,A,I,I,A,F,A,A,Y,J,A,F,A,A,W,0,A,F,A,A,71,7,A,F,A,A,15,15,A,F,A,A,31,31,A,F,A,A,63,63,A,F,A,A,G,G,G,F,A,A,I,I,G,F,A,A,J,J,G,F,A,A,W,0,A,F,A,A,7,7,31,F,A,A,15,15,31,F,A,A,31,31,31,F,A,A,63,63,63,F,A,A,G,G,G,S,A,A,I,I,G,S,A,A,A,A,E,F,A,A,A,A,F,F,A,A,A,A,R,F,A,A,15,15,31,23,A,A,31,31,31,23,A,A,63,63,63,55,A,A,G,G,G,S,G,A,G,G,G,S,G,A,A,A,A,e,A,A,0,0,0,0,0,0,A,A,A,n,A,A,31,31,31,23,31,A,31,31,31,23,31,A,63,63,63,55,63,A,G,G,G,S,G,I,G,G,G,J,I,J,A,A,A,F,E,A,A,A,A,F,F,A,A,A,A,F,R,A,31,31,31,7,15,7,31,31,31,23,31,15,63,63,63,55,63,31,B,G,G,S,G,I,B,G,G,J,I,J,M,z,0,H,J,H,M,M,M,0,m,Q,62,30,0,2,7,P,63,31,31,7,15,7,63,31,31,23,31,15,63,63,63,55,63,31,B,B,G,S,G,I,B,B,G,J,I,J,M,M,0,H,J,H,M,M,0,0,m,Q,M,62,0,2,7,P,L,63,31,7,15,7,L,63,31,23,31,15,L,63,63,55,63,31,G,A,A,A,B,A,I,A,A,A,B,A,Y,A,A,A,B,A,W,A,A,A,B,A,V,A,A,A,B,A,T,A,A,A,B,A,31,A,A,A,B,A,63,A,A,A,B,A,G,D,A,A,B,A,8,0,A,A,B,A,1,0,A,A,B,A,3,1,A,A,B,A,7,3,A,A,B,A,15,7,A,A,B,A,15,15,A,A,B,A,15,15,A,A,B,A,D,D,K,A,B,A,0,0,D,A,B,A,0,0,1,A,B,A,0,0,3,A,B,A,3,3,7,A,B,A,7,7,7,A,B,A,15,15,15,A,B,A,15,15,15,A,B,A,D,D,K,A,B,A,0,0,D,r,B,A,0,0,0,P,B,A,0,0,2,3,B,A,3,3,7,7,B,A,7,7,7,7,B,A,15,15,15,15,B,A,15,15,15,15,B,A,A,A,A,B,B,A,A,A,A,C,B,A,0,0,0,a,d,A,0,2,2,2,a,A,3,7,7,7,6,A,7,7,7,7,6,A,15,15,15,15,14,A,15,15,15,15,14,A,0,0,0,0,0,0,A,A,A,A,G,A,2,2,2,a,d,W,2,2,2,2,a,r,7,7,7,7,6,Z,7,7,7,7,6,7,15,15,15,15,14,15,15,15,15,15,14,31,A,A,A,A,B,B,A,A,A,A,B,C,2,2,2,a,d,225,2,2,2,2,a,r,7,7,7,7,6,Z,7,7,7,7,6,7,15,15,15,15,14,15,15,15,15,15,14,31,B,B,B,B,B,I,14,6,a,d,H,f,14,6,2,a,d,225,14,6,2,2,a,r,15,7,7,7,6,Z,15,7,7,7,6,7,15,15,15,15,14,15,15,15,15,15,14,31,G,A,A,A,C,A,I,A,A,A,C,A,Y,A,A,A,C,A,W,A,A,A,C,A,V,A,A,A,C,A,T,A,A,A,C,A,31,A,A,A,C,A,63,A,A,A,C,A,G,I,A,A,C,A,I,0,A,A,C,A,17,0,A,A,C,A,3,1,A,A,C,A,7,3,A,A,C,A,15,7,A,A,C,A,31,15,A,A,C,A,31,31,A,A,C,A,D,D,G,A,C,A,0,0,D,A,C,A,0,0,1,A,C,A,1,1,3,A,C,A,3,3,7,A,C,A,7,7,15,A,C,A,15,15,15,A,C,A,31,31,31,A,C,A,D,D,G,A,C,A,0,0,D,A,C,A,0,0,1,Z,C,A,1,1,3,7,C,A,3,3,7,7,C,A,7,7,15,15,C,A,15,15,15,15,C,A,31,31,31,31,C,A,A,A,A,B,C,A,A,A,A,C,C,A,A,A,A,E,C,A,1,3,3,7,j,A,3,7,7,7,5,A,7,15,15,15,13,A,15,15,15,15,13,A,31,31,31,31,29,A,A,A,A,A,G,A,0,0,0,0,0,0,A,A,A,A,h,A,7,7,7,7,j,V,7,7,7,7,5,Z,15,15,15,15,13,15,15,15,15,15,13,15,31,31,31,31,29,31,A,A,A,A,C,B,A,A,A,A,C,C,A,A,A,A,C,E,6,6,6,6,j,r,7,7,7,7,5,Z,15,15,15,15,13,15,15,15,15,15,13,15,31,31,31,31,29,31,B,B,B,B,I,O,M,M,B,B,G,I,30,14,6,y,K,n,30,14,6,6,j,r,31,15,7,7,5,Z,31,15,15,15,13,15,31,15,15,15,13,15,31,31,31,31,29,31,G,A,A,A,E,A,I,A,A,A,E,A,Y,A,A,A,E,A,W,A,A,A,E,A,V,A,A,A,E,A,T,A,A,A,E,A,31,A,A,A,E,A,63,A,A,A,E,A,G,I,A,A,E,A,I,J,A,A,E,A,Y,0,A,A,E,A,35,1,A,A,E,A,7,3,A,A,E,A,15,7,A,A,E,A,31,15,A,A,E,A,63,31,A,A,E,A,I,I,G,A,E,A,0,0,I,A,E,A,0,0,0,A,E,A,0,0,3,A,E,A,3,3,7,A,E,A,7,7,15,A,E,A,15,15,31,A,E,A,31,31,31,A,E,A,I,I,G,B,E,A,0,0,I,B,E,A,0,0,0,A,E,A,0,0,3,15,E,A,3,3,7,15,E,A,7,7,15,15,E,A,15,15,31,31,E,A,31,31,31,31,E,A,I,G,G,B,O,A,A,A,A,C,E,A,A,A,A,E,E,A,A,A,A,F,E,A,3,7,7,15,11,A,7,15,15,15,11,A,15,31,31,31,27,A,31,31,31,31,27,A,B,B,B,B,O,B,A,A,A,A,h,A,0,0,0,0,0,0,A,A,A,A,e,A,15,15,15,15,11,T,15,15,15,15,11,15,31,31,31,31,27,31,31,31,31,31,27,31,B,B,B,B,I,G,A,A,A,A,E,C,A,A,A,A,E,E,A,A,A,A,E,F,15,15,15,15,11,Z,15,15,15,15,11,15,31,31,31,31,27,31,31,31,31,31,27,31,B,B,B,B,O,G,M,M,B,B,J,B,M,M,M,B,O,Y,62,30,14,14,129,207,63,31,15,15,11,Z,63,31,15,15,11,15,63,31,31,31,27,31,63,31,31,31,27,31,G,A,A,A,F,A,I,A,A,A,F,A,Y,A,A,A,F,A,W,A,A,A,F,A,V,A,A,A,F,A,T,A,A,A,F,A,31,A,A,A,F,A,63,A,A,A,F,A,G,I,A,A,F,A,I,J,A,A,F,A,Y,H,A,A,F,A,W,0,A,A,F,A,71,3,A,A,F,A,15,7,A,A,F,A,31,15,A,A,F,A,63,31,A,A,F,A,I,I,G,A,F,A,J,J,I,A,F,A,0,0,J,A,F,A,0,0,0,A,F,A,0,0,7,A,F,A,7,7,15,A,F,A,15,15,31,A,F,A,31,31,63,A,F,A,I,I,G,G,F,A,J,J,I,G,F,A,0,0,J,G,F,A,0,0,0,A,F,A,0,0,7,31,F,A,7,7,15,31,F,A,15,15,31,31,F,A,31,31,63,63,F,A,I,G,G,G,S,A,J,I,I,G,S,A,A,A,A,E,F,A,A,A,A,F,F,A,A,A,A,R,F,A,7,15,15,31,23,A,15,31,31,31,23,A,31,63,63,63,55,A,G,G,G,G,S,G,G,G,G,G,S,G,A,A,A,A,e,A,0,0,0,0,0,0,A,A,A,A,n,A,31,31,31,31,23,31,31,31,31,31,23,31,63,63,63,63,55,63,G,G,G,G,S,G,G,G,G,G,S,I,A,A,A,A,F,E,A,A,A,A,F,F,A,A,A,A,F,R,31,31,31,31,23,15,31,31,31,31,23,31,63,63,63,63,55,63,B,B,G,G,S,G,B,B,G,G,S,I,M,M,z,G,H,G,M,M,M,M,b,W,M,62,30,30,3,159,L,63,31,31,23,15,L,63,31,31,23,31,L,63,63,63,55,63,G,A,A,A,A,B,I,A,A,A,A,B,Y,A,A,A,A,B,W,A,A,A,A,B,V,A,A,A,A,B,T,A,A,A,A,B,31,A,A,A,A,B,63,A,A,A,A,B,D,D,A,A,A,B,0,0,A,A,A,B,0,0,A,A,A,B,0,0,A,A,A,B,1,1,A,A,A,B,3,3,A,A,A,B,7,7,A,A,A,B,15,15,A,A,A,B,D,D,K,A,A,B,0,0,D,A,A,B,0,0,0,A,A,B,0,0,1,A,A,B,1,1,3,A,A,B,3,3,7,A,A,B,7,7,15,A,A,B,15,15,15,A,A,B,D,D,K,H,A,B,0,0,D,K,A,B,0,0,0,129,A,B,0,0,0,3,A,B,1,1,3,7,A,B,3,3,7,7,A,B,7,7,15,15,A,B,15,15,15,15,A,B,D,D,K,H,A,B,0,0,D,K,W,B,0,0,0,D,r,B,0,0,0,2,P,B,1,1,3,7,7,B,3,3,7,7,7,B,7,7,15,15,15,B,15,15,15,15,15,B,A,A,A,A,B,B,A,A,A,A,C,B,0,0,0,D,d,m,0,0,2,2,a,d,1,3,7,7,7,y,3,7,7,7,7,6,7,15,15,15,15,14,15,15,15,15,15,30,0,0,0,0,0,0,A,A,A,A,A,G,2,2,2,a,d,m,2,2,2,2,a,d,7,7,7,7,7,y,7,7,7,7,7,6,15,15,15,15,15,14,15,15,15,15,15,30,A,A,A,A,A,B,A,A,A,A,A,B,2,2,2,a,d,m,2,2,2,2,a,d,7,7,7,7,7,y,7,7,7,7,7,6,15,15,15,15,15,14,15,15,15,15,15,30,G,A,A,A,A,C,I,A,A,A,A,C,Y,A,A,A,A,C,W,A,A,A,A,C,V,A,A,A,A,C,T,A,A,A,A,C,31,A,A,A,A,C,63,A,A,A,A,C,D,D,A,A,A,C,0,0,A,A,A,C,0,0,A,A,A,C,0,0,A,A,A,C,1,1,A,A,A,C,3,3,A,A,A,C,7,7,A,A,A,C,15,15,A,A,A,C,D,D,I,A,A,C,0,0,D,A,A,C,0,0,0,A,A,C,0,0,1,A,A,C,1,1,3,A,A,C,3,3,7,A,A,C,7,7,15,A,A,C,15,15,31,A,A,C,D,D,K,G,A,C,0,0,D,K,A,C,0,0,0,129,A,C,0,0,1,3,A,C,1,1,3,7,A,C,3,3,7,15,A,C,7,7,15,15,A,C,15,15,31,31,A,C,D,D,K,G,A,C,0,0,D,K,A,C,0,0,0,129,V,C,0,0,1,3,Z,C,1,1,3,7,7,C,3,3,7,15,15,C,7,7,15,15,15,C,15,15,31,31,31,C,A,A,A,A,B,C,A,A,A,A,C,C,A,A,A,A,E,C,0,0,3,3,Z,q,1,3,7,7,7,j,3,7,15,15,15,13,7,15,15,15,15,13,15,31,31,31,31,29,A,A,A,A,A,G,0,0,0,0,0,0,A,A,A,A,A,h,7,7,7,7,Z,q,7,7,7,7,7,j,15,15,15,15,15,13,15,15,15,15,15,13,31,31,31,31,31,29,A,A,A,A,A,C,A,A,A,A,A,C,A,A,A,A,A,C,7,7,7,7,Z,q,7,7,7,7,7,j,15,15,15,15,15,13,15,15,15,15,15,13,31,31,31,31,31,29,G,A,A,A,A,E,I,A,A,A,A,E,Y,A,A,A,A,E,W,A,A,A,A,E,V,A,A,A,A,E,T,A,A,A,A,E,31,A,A,A,A,E,63,A,A,A,A,E,J,J,A,A,A,E,0,0,A,A,A,E,0,0,A,A,A,E,0,0,A,A,A,E,1,1,A,A,A,E,3,3,A,A,A,E,7,7,A,A,A,E,15,15,A,A,A,E,J,J,I,A,A,E,0,0,J,A,A,E,0,0,0,A,A,E,0,0,1,A,A,E,1,1,3,A,A,E,3,3,7,A,A,E,7,7,15,A,A,E,15,15,31,A,A,E,J,J,I,G,A,E,0,0,D,I,A,E,0,0,0,D,A,E,0,0,0,3,A,E,1,1,3,7,A,E,3,3,7,15,A,E,7,7,15,31,A,E,15,15,31,31,A,E,J,J,I,G,B,E,0,0,D,I,B,E,0,0,0,D,A,E,0,0,0,3,T,E,1,1,3,7,15,E,3,3,7,15,15,E,7,7,15,31,31,E,15,15,31,31,31,E,J,I,G,G,B,O,A,A,A,A,C,E,A,A,A,A,E,E,A,A,A,A,F,E,1,3,7,7,15,139,3,7,15,15,15,11,7,15,31,31,31,27,15,31,31,31,31,27,B,B,B,B,B,O,A,A,A,A,A,h,0,0,0,0,0,0,A,A,A,A,A,e,15,15,15,15,15,139,15,15,15,15,15,11,31,31,31,31,31,27,31,31,31,31,31,27,B,B,B,B,G,O,A,A,A,A,A,E,A,A,A,A,A,E,A,A,A,A,A,E,15,15,15,15,15,139,15,15,15,15,15,11,31,31,31,31,31,27,31,31,31,31,31,27,G,A,A,A,A,F,I,A,A,A,A,F,Y,A,A,A,A,F,W,A,A,A,A,F,V,A,A,A,A,F,T,A,A,A,A,F,31,A,A,A,A,F,63,A,A,A,A,F,J,J,A,A,A,F,H,H,A,A,A,F,0,0,A,A,A,F,0,0,A,A,A,F,0,0,A,A,A,F,3,3,A,A,A,F,7,7,A,A,A,F,15,15,A,A,A,F,J,J,I,A,A,F,H,H,J,A,A,F,0,0,H,A,A,F,0,0,0,A,A,F,0,0,3,A,A,F,3,3,7,A,A,F,7,7,15,A,A,F,15,15,31,A,A,F,J,J,I,G,A,F,H,H,J,I,A,F,0,0,0,J,A,F,0,0,0,0,A,F,0,0,0,7,A,F,3,3,7,15,A,F,7,7,15,31,A,F,15,15,31,63,A,F,J,J,I,G,G,F,H,H,J,I,G,F,0,0,0,J,G,F,0,0,0,0,A,F,0,0,0,7,31,F,3,3,7,15,31,F,7,7,15,31,31,F,15,15,31,63,63,F,J,I,G,G,G,S,H,J,I,I,G,S,A,A,A,A,E,F,A,A,A,A,F,F,A,A,A,A,R,F,3,7,15,15,31,23,7,15,31,31,31,23,15,31,63,63,63,55,G,G,G,G,G,S,G,G,G,G,G,S,A,A,A,A,A,e,0,0,0,0,0,0,A,A,A,A,A,n,31,31,31,31,31,23,31,31,31,31,31,23,63,63,63,63,63,55,G,G,G,G,G,S,G,G,G,G,G,S,A,A,A,A,A,F,A,A,A,A,A,F,A,A,A,A,A,F,31,31,31,31,31,23,31,31,31,31,31,23,63,63,63,63,63,55,G,A,A,A,A,A,I,A,A,A,A,A,Y,A,A,A,A,A,W,A,A,A,A,A,V,A,A,A,A,A,T,A,A,A,A,A,31,A,A,A,A,A,63,A,A,A,A,A,D,D,A,A,A,A,0,0,A,A,A,A,0,0,A,A,A,A,0,0,A,A,A,A,0,0,A,A,A,A,1,1,A,A,A,A,3,3,A,A,A,A,7,7,A,A,A,A,D,D,K,A,A,A,0,0,D,A,A,A,0,0,0,A,A,A,0,0,0,A,A,A,0,0,1,A,A,A,1,1,3,A,A,A,3,3,7,A,A,A,7,7,15,A,A,A,D,D,K,H,A,A,0,0,D,K,A,A,0,0,0,D,A,A,0,0,0,1,A,A,0,0,1,3,A,A,1,1,3,7,A,A,3,3,7,15,A,A,7,7,15,15,A,A,D,D,K,H,J,A,0,0,D,K,H,A,0,0,0,D,K,A,0,0,0,0,P,A,0,0,1,3,7,A,1,1,3,7,7,A,3,3,7,15,15,A,7,7,15,15,15,A,D,D,K,H,J,A,0,0,D,K,H,f,0,0,0,D,K,m,0,0,0,0,a,d,0,0,1,3,7,Z,1,1,3,7,7,7,3,3,7,15,15,15,7,7,15,15,15,31,A,A,A,A,A,B,A,A,A,A,A,C,0,0,0,D,K,m,0,0,0,2,a,d,0,0,3,7,7,Z,1,3,7,7,7,7,3,7,15,15,15,15,7,15,15,15,15,31,0,0,0,0,0,0,A,A,A,A,A,A,2,2,2,a,d,m,2,2,2,2,a,d,7,7,7,7,7,Z,7,7,7,7,7,7,15,15,15,15,15,15,15,15,15,15,15,31,G,A,A,A,A,A,I,A,A,A,A,A,Y,A,A,A,A,A,W,A,A,A,A,A,V,A,A,A,A,A,T,A,A,A,A,A,31,A,A,A,A,A,63,A,A,A,A,A,D,D,A,A,A,A,0,0,A,A,A,A,0,0,A,A,A,A,0,0,A,A,A,A,0,0,A,A,A,A,1,1,A,A,A,A,3,3,A,A,A,A,7,7,A,A,A,A,D,D,K,A,A,A,0,0,D,A,A,A,0,0,0,A,A,A,0,0,0,A,A,A,0,0,1,A,A,A,1,1,3,A,A,A,3,3,7,A,A,A,7,7,15,A,A,A,D,D,K,I,A,A,0,0,D,K,A,A,0,0,0,D,A,A,0,0,0,1,A,A,0,0,1,3,A,A,1,1,3,7,A,A,3,3,7,15,A,A,7,7,15,31,A,A,D,D,K,H,G,A,0,0,D,K,H,A,0,0,0,D,Q,A,0,0,0,1,P,A,0,0,1,3,7,A,1,1,3,7,15,A,3,3,7,15,15,A,7,7,15,31,31,A,D,D,K,H,G,A,0,0,D,K,H,A,0,0,0,D,Q,n,0,0,0,1,P,V,0,0,1,3,7,Z,1,1,3,7,15,15,3,3,7,15,15,15,7,7,15,31,31,31,A,A,A,A,A,B,A,A,A,A,A,C,A,A,A,A,A,E,0,0,0,3,P,V,0,0,3,7,7,Z,1,3,7,15,15,15,3,7,15,15,15,15,7,15,31,31,31,31,A,A,A,A,A,A,0,0,0,0,0,0,A,A,A,A,A,A,7,7,7,7,Z,V,7,7,7,7,7,Z,15,15,15,15,15,15,15,15,15,15,15,15,31,31,31,31,31,31,G,A,A,A,A,A,I,A,A,A,A,A,Y,A,A,A,A,A,W,A,A,A,A,A,V,A,A,A,A,A,T,A,A,A,A,A,31,A,A,A,A,A,63,A,A,A,A,A,H,H,A,A,A,A,0,0,A,A,A,A,0,0,A,A,A,A,0,0,A,A,A,A,0,0,A,A,A,A,1,1,A,A,A,A,3,3,A,A,A,A,7,7,A,A,A,A,H,H,J,A,A,A,0,0,D,A,A,A,0,0,0,A,A,A,0,0,0,A,A,A,0,0,1,A,A,A,1,1,3,A,A,A,3,3,7,A,A,A,7,7,15,A,A,A,H,H,J,I,A,A,0,0,D,J,A,A,0,0,0,D,A,A,0,0,0,1,A,A,0,0,1,3,A,A,1,1,3,7,A,A,3,3,7,15,A,A,7,7,15,31,A,A,H,H,J,I,G,A,0,0,D,K,I,A,0,0,0,D,K,A,0,0,0,0,P,A,0,0,1,3,7,A,1,1,3,7,15,A,3,3,7,15,31,A,7,7,15,31,31,A,H,H,J,I,G,B,0,0,D,K,I,B,0,0,0,D,K,A,0,0,0,0,P,207,0,0,1,3,7,T,1,1,3,7,15,15,3,3,7,15,31,31,7,7,15,31,31,31,H,J,I,G,G,B,A,A,A,A,A,C,A,A,A,A,A,E,A,A,A,A,A,F,0,0,3,7,7,T,1,3,7,15,15,15,3,7,15,31,31,31,7,15,31,31,31,31,B,B,B,B,G,B,A,A,A,A,A,A,0,0,0,0,0,0,A,A,A,A,A,A,15,15,15,15,15,T,15,15,15,15,15,15,31,31,31,31,31,31,31,31,31,31,31,31,G,A,A,A,A,A,I,A,A,A,A,A,Y,A,A,A,A,A,W,A,A,A,A,A,V,A,A,A,A,A,T,A,A,A,A,A,31,A,A,A,A,A,63,A,A,A,A,A,H,H,A,A,A,A,K,K,A,A,A,A,0,0,A,A,A,A,0,0,A,A,A,A,0,0,A,A,A,A,1,1,A,A,A,A,3,3,A,A,A,A,7,7,A,A,A,A,H,H,J,A,A,A,K,K,H,A,A,A,0,0,0,A,A,A,0,0,0,A,A,A,0,0,0,A,A,A,1,1,3,A,A,A,3,3,7,A,A,A,7,7,15,A,A,A,H,H,J,I,A,A,K,K,H,J,A,A,0,0,0,H,A,A,0,0,0,0,A,A,0,0,0,3,A,A,1,1,3,7,A,A,3,3,7,15,A,A,7,7,15,31,A,A,H,H,J,I,G,A,K,K,H,J,I,A,0,0,0,D,J,A,0,0,0,0,D,A,0,0,0,0,7,A,1,1,3,7,15,A,3,3,7,15,31,A,7,7,15,31,63,A,H,H,J,I,G,G,K,K,H,J,I,G,0,0,0,D,J,G,0,0,0,0,D,A,0,0,0,0,7,159,1,1,3,7,15,31,3,3,7,15,31,31,7,7,15,31,63,63,H,J,I,G,G,G,K,H,J,I,I,G,A,A,A,A,A,E,A,A,A,A,A,F,A,A,A,A,A,R,1,3,7,15,15,31,3,7,15,31,31,31,7,15,31,63,63,63,G,G,G,G,G,G,G,G,G,G,G,G,A,A,A,A,A,A,0,0,0,0,0,0,A,A,A,A,A,A,31,31,31,31,31,31,31,31,31,31,31,31,63,63,63,63,63,63
];
    }

Evaluate.kpkTable = KPKbitbase();

/*---------- Opening datas --------*/
function Bookbin()
    {
        // File book.bin - openings
        var A = 203,  B = 243,  C = 126,  D = 129,  E = 138,  F = 132,  G = 185,  H = 178,  I = 188,  J = 133,  K = 189,  L = 182,  M = 228,  N = 226,  O = 213,  P = 237,  Q = 142,  R = 220,  S = 197,  T = 125,  U = 247,  V = 130,  W = 249,  X = 218,  Y = 195,  Z = 186,  a = 113,  b = 193,  c = 251,  d = 219,  e = 207,  f = 187,  g = 139,  h = 210,  i = 163,  j = 122,  k = 250,  l = 236,  m = 253,  n = 194,  o = 177,  p = 145,  q = 137,  r = 131,  s = 123,  t = 117,  u = 100;

        return [
7,12,9,52,5,70,10,B,6,A,6,M,6,O,11,C,4,D,13,61,7,66,15,I,2,Y,8,235,8,R,8,P,8,h,8,c,8,91,7,35,0,0,7,12,9,52,5,70,E,B,6,A,6,M,6,Y,11,C,4,D,13,61,9,V,10,G,8,69,15,I,0,0,7,12,9,52,5,70,E,B,6,A,12,W,6,J,10,H,9,149,11,254,1,F,10,115,0,0,7,12,9,52,6,A,6,M,4,E,4,155,6,J,2,82,2,66,8,B,8,X,11,C,13,99,13,124,14,Y,6,T,0,0,7,12,9,52,6,A,6,M,F,E,8,B,8,R,8,c,6,h,10,G,5,70,7,Z,3,5,6,T,4,D,5,94,5,76,6,i,0,0,7,12,9,52,6,A,6,M,F,E,8,B,8,R,8,c,5,70,10,G,6,h,0,0,7,12,9,52,134,A,6,M,F,E,8,B,8,R,8,c,5,70,10,G,3,5,11,C,0,0,7,12,9,52,134,A,6,M,F,E,8,B,8,R,8,c,5,70,10,G,3,5,7,Z,1,F,14,I,6,h,0,0,7,12,9,52,134,A,6,M,6,Y,10,G,5,27,11,C,4,D,6,T,2,n,15,I,0,F,15,61,6,J,10,B,0,0,7,12,9,52,5,70,10,G,6,A,6,M,6,O,11,C,4,D,6,T,10,155,10,o,4,S,8,B,8,R,0,0,7,12,9,52,5,70,10,G,6,A,6,M,6,O,8,K,5,2,11,s,4,E,13,62,0,0,7,12,9,52,5,70,10,G,6,A,6,M,6,O,8,K,4,91,10,98,6,8,8,48,4,D,11,s,3,3,13,62,0,0,7,12,9,52,6,A,6,M,5,70,10,G,6,O,11,C,10,155,10,o,9,28,13,59,3,3,8,P,6,E,10,58,4,73,11,L,7,77,11,t,4,2,13,116,2,204,10,99,0,0,7,12,9,52,5,70,10,G,6,J,8,K,4,A,10,B,4,D,11,C,9,V,11,U,0,0,7,12,9,52,5,70,10,G,6,J,8,K,4,E,11,C,6,A,6,M,6,h,6,98,2,n,2,217,2,b,8,B,8,R,8,P,4,67,13,42,0,0,7,12,9,52,5,70,10,G,6,J,8,K,4,E,13,59,6,A,10,98,1,F,10,B,5,e,11,C,1,5,15,I,4,1,0,0,7,12,9,52,5,70,10,G,6,J,8,K,4,E,11,C,4,A,10,B,1,F,13,59,6,73,10,98,6,8,10,48,0,0,7,12,9,52,5,70,10,G,6,J,11,C,4,D,7,45,7,18,8,B,4,X,0,0,7,12,9,52,5,70,10,G,6,J,11,C,9,149,8,B,8,R,8,42,8,90,10,H,10,i,10,o,3,33,11,U,5,102,7,36,9,21,6,c,0,0,7,12,9,52,5,70,10,G,6,J,11,C,9,149,8,B,8,R,8,42,8,90,10,H,10,i,10,o,3,33,11,U,5,102,7,36,9,21,12,f,0,0,7,12,9,52,5,70,10,G,6,J,11,C,4,A,8,K,4,D,10,B,9,V,11,U,0,0,7,12,9,52,5,70,10,G,6,J,11,C,6,A,6,M,1,F,8,K,9,28,7,173,7,66,10,B,10,M,10,N,1,5,15,124,10,221,10,c,4,E,8,171,0,0,7,12,9,52,5,70,10,G,6,J,8,K,1,F,11,C,6,A,6,M,9,28,8,B,11,u,6,i,1,5,11,58,9,149,8,c,4,D,9,99,0,0,7,12,9,52,5,70,10,G,6,A,6,M,6,J,11,C,1,F,8,K,0,0,7,12,9,52,5,70,10,G,6,A,6,M,6,J,8,K,1,F,11,C,0,0,7,12,9,52,5,70,10,G,6,J,11,C,6,A,6,M,1,F,7,45,1,5,8,B,8,X,8,c,4,D,8,35,7,18,11,58,0,0,7,12,9,52,5,70,10,G,6,A,6,M,6,J,11,C,9,28,8,B,8,90,7,45,6,O,12,k,10,161,10,o,1,F,13,61,5,77,8,156,7,85,7,34,0,0,7,12,9,52,5,70,10,G,6,A,6,M,6,J,11,C,9,28,8,B,8,90,7,45,6,O,12,k,10,161,10,o,1,F,8,K,5,2,15,I,5,77,9,156,2,Y,11,t,1,198,11,38,0,0,7,12,9,52,5,70,10,G,6,A,6,M,6,J,8,K,1,F,10,B,4,E,7,Z,4,67,5,94,13,90,15,124,5,78,9,42,6,h,6,N,0,0,7,12,9,52,5,70,10,G,6,A,6,M,4,E,8,B,8,R,8,c,6,h,7,Z,3,5,6,T,4,D,5,94,5,76,6,i,4,67,0,0,7,12,9,52,5,70,10,G,6,J,13,61,6,A,10,B,9,27,9,43,14,Y,14,244,0,0,7,12,9,52,5,70,10,G,4,D,11,C,8,69,6,234,9,21,13,59,5,u,8,91,8,82,7,52,3,3,3,28,3,4,8,P,0,0,7,12,9,52,5,70,11,C,F,D,10,G,6,A,6,M,6,O,0,0,7,12,9,52,5,70,11,C,9,21,10,B,5,u,7,45,3,3,13,59,4,A,11,92,9,V,12,W,4,D,3,52,3,5,11,U,7,230,11,L,0,0,7,12,9,52,5,70,11,C,9,21,10,B,5,u,7,45,6,A,8,235,4,S,13,61,1,F,10,G,1,5,7,Z,6,E,11,92,8,X,8,P,4,D,15,I,7,19,11,30,0,0,7,12,9,52,5,70,g,C,6,A,7,45,9,27,8,B,2,b,10,G,0,0,7,12,9,52,7,77,7,u,5,70,8,B,8,R,11,C,4,D,8,P,8,h,8,c,6,A,13,61,6,E,7,35,3,5,10,G,0,0,7,12,9,52,7,77,7,u,5,70,13,61,6,J,11,C,9,28,7,173,1,F,10,G,6,A,8,B,10,M,10,c,0,0,7,12,9,52,5,70,10,G,8,69,10,B,6,A,12,k,4,D,11,C,1,F,13,61,1,5,6,M,6,O,15,I,0,0,7,12,9,52,5,70,10,G,8,69,11,C,1,F,7,45,1,5,10,R,9,21,13,61,4,225,15,I,0,0,7,12,9,52,5,70,10,G,8,69,11,C,1,F,7,45,6,A,10,R,0,0,7,12,9,52,5,70,10,G,8,69,10,48,6,33,11,C,1,F,13,61,1,5,8,a,4,88,15,I,4,E,10,B,5,e,11,U,6,A,15,61,2,b,15,116,1,75,12,j,5,J,8,42,2,p,6,160,6,8,8,235,0,0,7,12,9,52,5,70,10,G,8,69,10,48,6,33,11,C,1,F,13,61,1,5,8,a,4,88,10,B,4,E,15,I,6,A,7,Z,8,d,8,42,2,p,10,H,5,e,14,158,10,i,12,f,2,b,10,H,1,75,6,160,0,0,7,12,9,52,5,70,10,G,8,69,10,48,6,33,11,C,1,F,13,61,1,5,8,a,4,88,10,B,4,E,8,42,2,p,8,H,6,A,10,160,8,d,0,0,7,12,9,52,5,70,10,G,8,69,10,48,6,33,11,C,1,F,13,61,1,5,8,a,4,88,10,B,4,E,8,42,2,p,8,H,6,A,10,160,5,e,12,f,8,d,0,0,7,12,9,52,5,70,10,G,8,69,10,48,6,33,11,C,1,F,13,61,1,5,8,a,4,88,10,B,4,E,8,42,2,p,8,H,6,A,6,N,6,h,12,f,0,0,7,12,9,52,5,70,10,G,8,69,10,48,6,33,11,C,1,F,13,61,1,5,8,a,4,88,10,B,4,E,8,42,2,p,8,H,4,A,10,160,2,b,15,I,1,75,15,61,5,e,11,U,5,5,15,116,0,0,J,70,10,G,7,12,9,52,8,69,10,48,6,33,11,C,1,F,7,45,6,A,8,a,4,88,8,B,9,27,11,58,4,E,8,K,0,0,7,12,9,52,5,70,10,G,8,69,10,48,6,33,10,B,1,F,12,k,4,E,11,L,6,A,13,K,1,5,13,62,5,2,15,I,2,b,11,U,9,27,9,43,4,88,10,a,6,8,0,0,7,12,9,52,5,70,10,G,8,69,10,48,6,33,10,B,4,E,12,k,6,A,13,62,4,88,11,U,2,b,11,180,6,g,13,61,5,26,15,I,0,0,7,12,9,52,5,70,10,G,8,69,11,C,1,F,8,K,4,E,15,I,6,A,10,98,9,V,11,U,7,230,10,B,6,8,8,48,1,5,6,M,10,161,10,o,6,O,0,0,7,12,9,52,5,70,10,G,8,69,g,C,1,F,8,K,9,21,9,42,6,A,10,48,6,33,7,45,3,3,13,34,7,12,11,164,0,0,7,12,9,52,5,70,10,G,8,69,g,C,1,F,136,K,F,D,15,I,4,A,10,B,0,0,7,12,8,B,8,R,8,c,4,D,8,35,6,A,11,C,5,70,9,j,6,J,11,52,2,n,10,H,3,3,6,T,9,21,12,W,12,M,12,P,4,8,0,0,7,12,8,B,8,R,8,c,4,D,8,35,6,A,10,H,5,70,11,C,6,J,7,Z,5,e,9,222,7,Q,11,167,2,n,10,96,3,3,0,0,7,12,8,B,8,R,11,C,6,A,8,P,6,E,10,99,5,70,11,L,4,D,13,K,5,2,15,I,5,e,10,G,2,Y,9,52,8,d,0,0,7,12,136,B,8,R,11,C,6,A,8,P,5,70,11,L,6,E,10,99,0,0,7,12,136,B,8,R,11,C,6,A,8,P,5,70,11,L,3,5,13,K,1,F,15,I,6,E,10,99,4,D,10,G,8,d,9,42,0,0,6,A,8,B,6,E,6,i,5,70,11,C,5,12,7,Z,6,J,11,52,5,e,9,222,4,D,0,0,6,A,8,B,6,E,134,i,5,12,11,C,5,70,7,Z,0,0,6,A,8,B,6,E,134,i,J,12,11,C,5,70,11,52,6,J,8,H,1,F,10,48,0,0,6,A,8,B,6,E,11,52,4,D,11,C,9,V,12,W,5,12,13,61,5,70,15,I,0,128,10,H,0,0,6,E,11,52,6,A,8,B,5,70,13,61,4,D,11,C,9,V,15,I,5,12,11,U,0,0,6,E,11,C,4,D,11,52,5,70,8,B,6,A,13,61,9,V,15,I,5,12,12,W,2,r,8,H,0,0,134,E,11,C,4,D,11,52,5,70,8,B,6,A,13,61,5,12,15,I,4,S,8,H,0,0,J,70,8,B,6,A,11,C,6,E,11,52,4,D,13,61,7,66,15,I,5,12,8,H,8,155,8,180,2,r,10,G,0,0,6,A,8,B,6,E,10,H,5,70,11,C,4,D,11,52,5,12,12,W,4,S,6,i,6,147,8,a,4,X,10,48,1,F,0,0,6,A,8,B,6,E,E,H,8,X,8,234,4,D,11,C,7,66,10,G,5,12,10,48,0,0,6,A,8,B,6,E,E,H,4,D,11,C,5,12,11,52,5,70,0,0,6,A,8,B,6,E,11,52,4,D,8,H,8,X,8,l,5,70,10,G,5,Q,11,C,3,J,13,61,1,F,15,I,9,V,6,N,6,O,11,U,0,0,6,A,11,C,6,E,9,52,9,27,7,173,5,70,8,K,5,12,10,G,3,5,9,30,1,F,10,B,0,0,7,12,8,H,5,70,10,G,6,A,6,N,6,O,11,52,4,D,12,f,0,0,7,12,8,H,5,70,10,G,6,A,6,N,6,O,11,C,4,D,9,52,8,91,10,B,9,V,10,48,4,33,8,a,0,0,7,12,8,H,5,70,10,B,6,A,6,N,6,O,11,C,4,D,10,48,7,77,9,52,5,91,12,f,4,S,0,0,7,12,8,H,5,70,10,B,6,A,6,N,6,O,11,C,4,D,10,48,7,77,11,52,5,67,10,s,4,91,12,169,0,0,7,12,8,H,5,70,10,B,6,A,6,N,6,O,11,C,4,D,10,48,7,77,140,W,3,5,0,0,7,12,8,H,5,70,10,B,6,A,6,N,6,O,11,C,4,D,10,48,3,5,9,52,4,91,13,61,1,F,15,I,5,2,0,0,7,12,8,H,5,70,10,B,6,A,6,N,6,O,11,C,4,D,10,48,5,2,9,52,4,91,11,58,2,Y,12,W,5,77,8,a,0,0,7,12,8,H,5,70,10,B,6,A,6,N,6,O,11,C,4,D,10,48,9,V,11,52,7,77,13,61,5,67,12,f,0,F,12,W,7,Q,8,a,0,0,7,12,8,H,5,70,11,52,6,A,6,N,6,O,10,48,4,S,11,C,1,F,12,f,3,3,10,B,6,E,11,L,4,D,13,K,0,S,15,I,0,0,7,12,8,H,5,70,11,52,6,A,6,N,6,O,11,C,4,D,10,B,3,5,10,48,1,F,13,61,7,77,15,I,0,0,7,12,8,H,5,70,11,52,6,A,6,N,6,O,10,G,4,D,12,f,5,2,10,48,4,S,11,C,1,F,9,42,5,e,8,K,3,3,10,B,0,0,7,12,8,H,5,70,g,52,4,D,10,G,6,A,6,N,6,O,12,f,5,2,10,48,2,Y,11,C,0,F,13,61,0,0,7,12,8,H,4,D,10,G,3,6,11,L,6,A,6,N,6,204,13,K,5,2,11,C,6,J,15,I,4,90,10,B,0,0,7,12,8,H,F,D,10,G,5,Q,11,L,3,J,13,K,4,A,10,B,7,77,11,52,5,70,13,62,1,F,15,I,0,0,7,12,8,H,F,D,11,52,5,70,10,G,6,A,6,N,6,O,12,f,3,5,10,48,1,F,11,C,5,2,6,T,0,0,7,12,8,H,F,D,10,48,5,70,10,B,6,A,6,N,6,O,11,C,0,0,F,D,8,H,5,70,10,G,6,A,6,N,6,O,11,C,7,12,10,B,9,V,11,52,2,Y,10,48,0,F,12,k,7,77,8,a,0,0,7,12,8,H,6,A,6,N,4,E,4,155,4,D,10,G,5,70,10,B,6,J,11,52,1,F,11,C,3,3,13,61,0,S,9,44,0,0,7,12,8,H,4,E,8,B,8,R,8,c,6,A,11,C,5,70,7,Z,3,5,11,52,1,F,10,G,5,2,6,N,6,h,13,61,0,0,7,12,8,H,5,70,10,B,6,A,6,N,6,O,11,C,4,D,11,L,5,2,13,K,5,77,15,I,2,Y,10,G,6,J,12,k,0,F,14,184,4,90,9,42,7,e,6,164,6,p,6,Z,7,Q,8,59,0,0,6,A,11,52,7,12,8,B,8,R,8,l,5,70,11,C,4,S,10,m,1,F,15,I,9,V,7,Z,2,b,12,W,4,E,10,H,2,r,12,f,0,0,7,12,11,52,6,A,8,B,8,R,8,l,4,S,10,m,5,70,11,C,0,0,7,12,11,52,6,A,8,B,8,R,8,l,4,S,10,m,5,70,13,62,1,F,15,I,9,V,11,t,2,230,9,j,0,0,7,12,11,52,6,A,8,B,9,28,8,H,4,E,10,G,5,70,10,s,3,5,6,N,6,h,13,62,4,D,9,116,6,18,8,41,2,n,6,T,4,g,0,0,7,12,11,52,6,A,8,B,9,28,8,H,4,E,10,G,5,70,10,s,4,S,6,N,6,h,12,k,0,0,7,12,11,52,6,A,8,B,9,28,8,H,4,E,10,G,5,70,10,s,4,8,6,162,2,b,8,42,3,5,12,k,0,0,7,12,11,52,6,A,8,B,4,D,11,C,9,V,13,61,9,28,12,P,13,38,0,0,7,12,11,52,6,A,8,B,2,b,11,C,9,28,12,P,4,S,8,H,4,E,10,G,3,6,6,N,6,h,11,t,11,u,11,115,0,0,7,12,g,52,6,A,8,B,4,D,11,C,9,28,12,P,7,77,8,H,5,70,10,G,5,2,6,N,6,O,8,K,2,Y,15,I,0,F,10,48,0,0,7,12,g,52,6,A,8,B,4,D,6,T,9,28,8,H,4,8,4,153,4,q,13,62,7,r,12,f,13,158,15,191,13,246,6,N,3,6,10,G,7,77,12,k,0,0,7,12,g,52,6,A,8,B,4,D,6,T,9,28,8,H,7,r,0,0,7,12,g,52,6,A,136,H,8,d,8,l,8,R,10,B,0,0,7,12,10,H,6,A,8,B,4,D,7,35,7,18,9,j,5,156,11,165,7,e,11,U,5,70,12,W,0,0,7,12,10,H,6,A,8,B,2,b,7,35,7,11,0,0,7,12,10,H,6,A,8,B,8,R,8,234,6,E,11,C,4,D,11,52,5,70,13,61,0,0,7,12,10,H,6,A,8,B,9,28,9,j,5,70,11,52,3,5,8,170,1,F,10,G,4,E,6,N,6,h,13,62,4,D,14,180,5,2,10,j,0,128,13,61,0,0,7,12,E,H,4,A,8,B,2,b,9,52,5,70,10,m,5,Q,11,C,3,J,15,I,1,F,0,0,7,12,E,H,6,A,8,B,4,D,7,35,7,18,9,j,5,156,11,165,5,70,12,W,7,e,11,U,9,223,13,238,4,S,4,U,4,Y,11,52,0,0,7,12,11,C,9,28,8,P,6,A,10,B,6,E,10,99,10,M,10,242,5,2,11,L,0,0,7,12,11,C,9,28,8,P,6,E,10,99,6,A,10,B,10,M,10,242,5,70,11,L,3,5,13,K,1,F,15,I,4,D,10,G,5,2,7,Z,4,73,8,235,0,0,7,12,11,C,9,28,8,P,6,A,10,B,5,70,7,Z,3,5,11,52,6,E,10,99,10,M,10,242,0,0,7,12,g,C,4,D,8,B,9,28,12,P,6,A,11,52,7,77,8,H,5,70,10,G,5,2,10,48,2,Y,8,a,8,155,8,K,8,148,8,179,0,0,7,12,g,C,4,D,8,B,9,28,12,P,6,A,11,52,7,77,8,H,5,70,10,G,5,2,6,N,6,O,8,K,2,Y,15,I,0,F,10,48,0,0,7,12,g,C,9,28,8,P,6,E,10,99,6,A,10,B,5,70,7,Z,10,M,10,244,3,5,13,61,1,F,15,I,4,D,10,G,4,73,11,116,5,2,8,235,0,0,6,A,11,C,6,E,11,L,4,D,13,K,7,12,10,B,5,70,15,I,3,5,9,52,1,F,10,G,0,0,6,A,11,C,6,E,11,L,4,D,13,K,5,Q,15,I,3,J,10,B,5,70,12,W,1,F,9,52,0,0,6,E,11,C,5,70,11,L,6,A,13,K,4,D,15,I,7,12,10,B,3,5,0,0,6,E,11,C,4,D,11,L,6,A,8,B,8,X,8,P,7,12,4,i,4,q,13,K,6,J,8,H,0,0,6,A,11,C,6,E,11,L,4,D,8,B,8,X,8,P,7,12,4,i,4,q,13,K,5,70,8,H,0,0,J,70,11,C,6,E,11,L,4,D,13,K,6,A,15,I,0,0,6,A,11,C,6,E,11,52,5,70,10,a,5,Q,12,j,3,J,13,61,0,0,6,E,11,C,6,A,11,52,5,70,10,a,4,D,12,j,4,8,8,B,8,X,8,P,2,r,0,0,6,A,11,52,5,70,11,C,6,E,10,a,5,Q,12,j,3,J,6,T,2,n,2,217,2,Y,0,0,6,A,11,52,6,E,11,C,4,D,6,T,9,V,11,U,7,230,8,H,8,d,10,B,0,0,6,E,11,52,6,A,11,C,4,D,6,T,4,8,4,153,4,q,8,H,5,77,8,B,0,0,6,A,11,C,6,E,11,52,4,D,6,T,2,r,8,B,4,8,4,153,4,E,7,45,2,146,10,G,5,12,9,44,0,0,6,A,11,C,6,E,11,52,4,D,6,T,5,70,15,I,9,V,8,H,0,128,6,N,0,0,6,A,11,C,6,E,11,52,4,D,6,T,5,77,8,B,0,0,6,A,11,C,6,E,11,52,4,D,6,T,5,12,8,H,4,S,8,B,5,70,15,I,1,F,10,G,0,0,6,A,11,C,6,E,11,52,4,D,6,T,5,12,15,I,4,S,8,B,5,70,8,H,1,F,0,0,6,E,11,C,4,D,11,52,6,A,0,0,6,A,11,C,5,70,11,52,6,E,8,H,8,d,8,l,8,X,10,B,4,D,11,L,7,12,13,K,0,0,6,A,11,C,6,E,8,H,8,d,11,52,4,D,8,l,8,X,10,B,7,12,11,L,7,77,13,K,8,69,12,P,6,8,15,I,5,70,10,57,1,F,12,168,0,0,6,A,8,H,8,d,11,C,6,E,11,52,4,D,8,l,8,X,10,B,5,70,11,L,7,66,10,48,6,8,13,K,7,12,15,I,0,0,6,E,11,C,6,A,11,L,4,D,13,K,5,Q,15,I,3,J,8,H,8,d,11,52,5,70,8,l,8,X,10,B,1,F,0,0,5,70,8,B,5,Q,11,L,3,J,13,K,1,F,9,52,4,A,13,62,2,b,15,I,6,E,10,H,0,0,5,70,11,C,5,Q,11,L,6,E,13,K,3,J,15,I,1,F,8,H,6,A,10,B,8,d,10,57,4,D,12,168,0,0,J,70,11,C,5,Q,8,B,6,A,8,H,3,J,10,G,1,F,11,L,0,0,J,70,11,C,5,Q,8,B,3,J,10,H,6,A,9,j,1,F,11,L,0,0,J,70,8,B,6,A,11,C,6,E,11,52,5,Q,6,i,3,J,10,G,6,3,6,T,2,n,8,P,6,75,6,99,1,F,14,120,0,0,J,70,8,H,6,E,11,C,4,D,11,52,5,Q,13,61,3,J,15,I,1,F,10,48,6,A,6,N,6,O,12,f,0,0,5,Q,11,L,3,J,13,K,6,E,11,C,4,D,15,I,5,70,10,B,6,A,12,W,1,F,9,52,7,12,0,0,J,Q,8,B,3,J,11,C,5,70,10,H,1,F,9,j,4,A,11,52,2,b,11,U,4,73,13,61,2,66,15,I,0,0,J,Q,11,C,3,J,8,B,4,A,10,H,2,b,9,52,7,12,10,m,5,70,15,I,1,F,0,0,J,Q,8,B,5,70,11,C,3,J,11,52,1,F,13,61,4,A,15,I,2,b,8,H,7,12,10,G,0,0,J,Q,9,52,3,J,8,B,4,A,11,C,5,70,10,G,1,F,13,61,6,E,15,I,8,X,8,P,4,D,11,58,0,0,6,A,9,t,5,Q,11,C,3,J,11,L,5,70,13,K,1,F,15,I,6,E,10,B,4,D,15,59,8,d,10,57,0,0,6,A,q,t,6,E,11,C,4,D,11,L,5,70,13,K,5,12,15,I,3,5,10,B,1,F,10,G,0,0,6,A,q,t,5,70,11,C,5,Q,11,L,3,J,13,K,6,E,15,I,4,D,10,B,1,F,10,G,8,d,9,42,0,0,F,D,8,B,7,12,6,227,3,18,9,52,5,140,11,58,5,70,12,W,4,E,8,H,8,69,10,m,1,F,10,48,0,0,F,D,8,B,6,A,11,C,9,V,12,W,5,70,11,U,7,230,10,H,5,12,11,52,4,S,13,61,1,F,15,I,0,0,F,D,8,B,7,12,10,H,5,70,7,Z,5,e,9,222,6,A,0,0,F,D,9,52,7,12,11,C,6,J,10,G,4,A,6,T,9,V,11,U,11,102,4,153,4,q,11,s,3,6,10,B,0,0,6,E,9,52,4,D,11,C,5,70,10,G,5,Q,8,B,8,X,8,P,3,J,10,99,1,F,13,61,4,A,15,I,4,8,11,58,6,73,0,0,6,E,9,52,4,D,11,C,5,Q,8,B,8,X,8,P,3,J,10,99,5,70,10,G,1,F,13,61,4,8,15,I,6,73,11,58,4,A,0,0,7,77,8,B,5,70,11,C,5,12,11,L,3,5,13,K,1,F,15,I,4,A,8,H,1,3,10,G,4,D,15,61,0,0,7,77,8,B,5,70,11,L,5,Q,13,K,3,J,11,C,1,F,15,I,4,A,8,H,4,D,6,227,7,18,0,0,135,77,8,B,5,70,11,L,5,12,13,K,3,5,11,C,0,0,135,77,8,B,5,12,11,C,5,70,7,Z,3,5,12,W,9,21,0,0,F,73,9,52,2,66,10,G,5,12,8,B,8,69,10,m,5,70,13,59,6,E,11,C,0,0,7,12,g,L,6,A,13,K,4,D,10,B,7,77,11,C,5,70,15,I,4,S,10,G,1,F,9,52,0,0,7,12,10,B,6,A,11,C,4,D,11,L,5,70,13,K,0,0,7,12,E,B,6,A,11,L,4,D,13,K,5,70,11,C,0,0,6,A,E,B,7,12,11,C,4,D,11,L,5,2,13,K,2,Y,10,H,0,0,6,A,11,L,7,12,13,K,5,70,10,B,4,D,11,C,3,5,15,I,1,F,10,H,5,e,12,f,7,66,12,W,9,28,9,43,9,21,9,51,9,29,10,114,0,0,6,A,11,L,6,E,13,K,4,D,10,B,7,12,11,C,5,70,15,I,3,5,9,52,1,F,0,0,6,A,g,L,7,12,13,K,6,E,10,B,4,D,10,G,5,2,9,52,8,d,13,42,0,0,6,E,11,C,5,Q,11,L,3,J,13,K,4,D,15,I,7,12,10,B,3,6,8,H,1,F,10,G,4,A,10,48,5,e,14,120,6,8,0,0,F,73,11,52,2,66,11,C,5,12,8,H,5,70,13,61,6,A,15,I,4,S,8,B,1,F,10,G,0,0
];

 }

Book.Book_data = Bookbin();

// Interface to html (avoid obfuscator)
function Book_initBook() { Book.initBook(); }
function Book_getAllBookMoves() { return Book.getAllBookMoves( Game.pos ); }
function ComputerPlayer_Init() { ComputerPlayer.Init(); }
function Game_CreaGame() { Game.CreaGame( HumanPlayer, ComputerPlayer ); }
function Game_getGameStateString() { return Game.getGameStateString(); }
function Game_processString(c) { Game.processString(c); }
function Game_listMoves() { Game.listMoves(); }
function TextIO_toFEN() { return TextIO.toFEN( Game.pos ); }
function TextIO_dispMoves() { return TextIO.dispMoves( Game.pos ); }
function TextIO_asciiBoard() { TextIO.asciiBoard( Game.pos ); }
function Evaluate_evalPos() { return Evaluate.evalPos( Game.pos );}
function GetCommand()
 { return ( Game.pos.whiteMove ? Game.whitePlayer : Game.blackPlayer ).getCommand( Game.pos, false ); }
function AdjTimeDepth(c)
 {
 if(c>0)
 {
  ComputerPlayer.minTimeMillis+=1000;
  ComputerPlayer.maxTimeMillis+=1100;
  if(ComputerPlayer.maxDepth<8) ComputerPlayer.maxDepth++;
 }
 if(c<0)
 {
  if(ComputerPlayer.minTimeMillis>2*1000)
	{
	ComputerPlayer.minTimeMillis-=1000;
	ComputerPlayer.maxTimeMillis-=1100;
	if(ComputerPlayer.maxDepth>2) ComputerPlayer.maxDepth--;
	}
 }
 return (ComputerPlayer.minTimeMillis/1000);
 }
 
// should run when ready to wait, before everything
function InitBitBoardAll() { InitBitDataAll_(); }
