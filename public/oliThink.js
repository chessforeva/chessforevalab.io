/*
--
-- OliThink5 Java(c) Oliver Brausch 28.Jan.2010, ob112@web.de, http://home.arcor.de/dreamlike
-- Javascript port by http://chessforeva.blogspot.com
-- Very experimental, 64-bit emulated in 32-bits, slow

*/

// constants
g_FF=0xFFFFFFFF;
g_F=0xFFFF;
g_10=0x100000000;
g_1=0x10000;
g_F0=i64_ax(g_FF,0);
g_0F=i64_ax(0,g_FF);
g_FF0=i64_ax(0,0xFF0000);
g_Fo=i64_ax(0xFF00,0);
g_32=32000;
g_37=32768;

function CHR(n) { return String.fromCharCode(n); }
function woutput(txt){      // messages on screen
 var o=document.getElementById("div-print");
 if(o!=null) o.innerHTML=txt;
}
function print(s) { console.log(s); }

/*
 Chess Engine
*/

g_sd = 6;	// kind of depth
g_tm = 8;	// search time

g_VER = "OliThink 5.3.0 Java port to Javascript";

g_movemade = "";
g_pgn = "";
g_ex = 0;

g_PAWN = 1;
g_KNIGHT = 2;
g_KING = 3;
g_ENP = 4;
g_BISHOP = 5;
g_ROOK = 6;
g_QUEEN = 7;

g_HEUR = 9900000;
g_pval = [ 0, 100, 290, 0, 100, 310, 500, 950 ];

g_cag_p_val = [ 0, g_HEUR+1, g_HEUR+2, 0, g_HEUR+1, g_HEUR+2, g_HEUR+3, g_HEUR+4 ];

g_pawnrun = [ 0, 0, 1, 8, 16, 32, 64, 128 ];

g_HSIZEB = 0x200000;
g_HMASKB = g_HSIZEB-1;
g_HINVB =  i64_or(g_F0, i64_and(g_0F, i64_not( i64v(g_HMASKB) )));

g_HSIZEP = 0x400000;
g_HMASKP = g_HSIZEP-1;
g_HINVP =  i64_or(g_F0, i64_and(g_0F, i64_not( i64v(g_HMASKP) )));

g_hashDB = [];	//g_HSIZEB
g_hashDP = [];	//g_HSIZEP
g_hashb = i64();
g_hstack = [];	//0x800
g_mstack = [];	//0x800
g_hc = 0;

g_hashxor = [];	//0x4096
g_rays = [];	//0x10000
g_pmoves = [[],[]];	//64 x 2
g_pcaps = [[],[]];	//192 x 2
g_nmoves = [];	//64
g_kmoves = [];	//64
g_knight = [ -17,-10,6,15,17,10,-6,-15 ];

g_king = [ -9,-1,7,8,9,1,-7,-8 ];

g_BITi = [];	//64
g_crevoke = [];	//64
g_nmobil = [];	//64
g_kmobil = [];	//64
g_pawnprg = [[],[]];	//64 x 2
g_pawnfree = [[],[]];	//64 x 2
g_pawnfile = [[],[]];	//64 x 2
g_pawnhelp = [[],[]];	//64 x 2
g_movelist = [];	//256 x 64
g_movenum = [];	//64
g_p_v = [];	//64 x 64
g_pvlength = [];	//64
g_kvalue = [];	//64
g_iter = 0;
g_pieceChar = ["*", "P", "N", "K", ".", "B", "R", "Q"];
g_starttime = 0;
g_sabort = 0;

g_count = 0;
g_flags = 0;
g_mat_ = 0;
g_onmove = 0;
g_engine = -1;
g_kingpos = [];	//2
g_pieceb = [];	//8
g_colorb = [];	//2
g_irbuf = "";

g_sfen = "rnbqkbnr/pppppppp/////PPPPPPPP/RNBQKBNR w KQkq - 0 1";

g_r_x = 30903;
g_r_y = 30903;
g_r_z = 30903;
g_r_w = 30903;
g_r_c = 0;

g_bmask45 = [];	//64
g_bmask135 = []; //64
g_killer = [];	//128
g_history = [];	//0x1000

g_eval1 = 0;

g_nds = 0;
g_nodes = 0;

g_Nonevar = [ 13, 43, 149, 519, 1809, 6311, 22027 ];


g_mps = 0;
g_base = 5;
g_inc = 0;
g_post_ = 1;

g_0=i64();

function ISRANK(c) { return (c>="1" && c<="8"); }
function ISFILE(c) { return (c>="a" && c<="h"); }
function FROM(x) { return (x&63); }
function TO(x) { return ((x>>6)&63); }
function PROM(x) { return ((x>>12)&7); }
function PIECE(x) { return ((x>>15)&7); }
function ONMV(x) { return ((x>>18)&1); }
function CAP(x) { return ((x>>19)&7); }
function _TO(x) { return (x<<6); }
function _PROM(x) { return (x<<12); }
function _PIECE(x) { return (x<<15); }
function _ONMV(x) { return (x<<18); }
function _CAP(x) { return (x<<19); }
function PREMOVE(f, p, c) { return (f | _ONMV(c) | _PIECE(p)); }
function RATT1(f) { return g_rays[(f<<7) | key000(BOARD(),f)]; }
function RATT2(f) { return g_rays[(f<<7) | key090(BOARD(),f) | 0x2000]; }
function BATT3(f) {
var p1=key045(BOARD(),f);
var p2=(f<<7) | key045(BOARD(),f) | 0x4000;
var p3=g_rays[p2];
 return g_rays[(f<<7) | key045(BOARD(),f) | 0x4000]; }
function BATT4(f) {
var p1=key135(BOARD(),f);
var p2=(f<<7) | key135(BOARD(),f) | 0x6000;
var p3=g_rays[p2];

 return g_rays[(f<<7) | key135(BOARD(),f) | 0x6000]; }
function RXRAY1(f) { return g_rays[(f<<7) | key000(BOARD(),f) | 0x8000]; }
function RXRAY2(f) { return g_rays[(f<<7) | key090(BOARD(),f) | 0xA000]; }
function BXRAY3(f) { return g_rays[(f<<7) | key045(BOARD(),f) | 0xC000]; }
function BXRAY4(f) { return g_rays[(f<<7) | key135(BOARD(),f) | 0xE000]; }
function ROCC1(f) { return i64_and(RATT1(f), BOARD()); }
function ROCC2(f) { return i64_and(RATT2(f), BOARD()); }
function BOCC3(f) { return i64_and(BATT3(f), BOARD()); }
function BOCC4(f) { return i64_and(BATT4(f), BOARD()); }
function RMOVE1(f) { return i64_and(RATT1(f), i64_not(BOARD())); }
function RMOVE2(f) { return i64_and(RATT2(f), i64_not(BOARD())); }
function BMOVE3(f) { return i64_and(BATT3(f), i64_not(BOARD())); }
function BMOVE4(f) { return i64_and(BATT4(f), i64_not(BOARD())); }
function RCAP1(f,c) { return i64_and(RATT1(f), g_colorb[c^1]); }
function RCAP2(f,c) { return i64_and(RATT2(f), g_colorb[c^1]); }
function BCAP3(f,c) { return i64_and(BATT3(f), g_colorb[c^1]); }
function BCAP4(f,c) { return i64_and(BATT4(f), g_colorb[c^1]); }
function ROCC(f) { return i64_or(ROCC1(f), ROCC2(f)); }
function BOCC(f) { return i64_or(BOCC3(f), BOCC4(f)); }
function RMOVE(f) { return i64_or(RMOVE1(f), RMOVE2(f)); }
function BMOVE(f) { return i64_or(BMOVE3(f), BMOVE4(f)); }
function RCAP(f,c) { return i64_and(ROCC(f), g_colorb[c^1]); }
function BCAP(f,c) { return i64_and(BOCC(f), g_colorb[c^1]); }
function SHORTMOVE(x) { return i64_and( x, i64_xor(x, BOARD())); }
function SHORTOCC(x) { return i64_and( x, BOARD()); }
function SHORTCAP(x,c) { return i64_and(x, g_colorb[c^1]); }
function NMOVE(x) { return (SHORTMOVE(g_nmoves[x])); }
function KMOVE(x) { return (SHORTMOVE(g_kmoves[x])); }
function PMOVE(x,c) { return i64_and( g_pmoves[c][x], i64_not(BOARD())); }
function NOCC(x) { return (SHORTOCC(g_nmoves[x])); }
function KOCC(x) { return (SHORTOCC(g_kmoves[x])); }
function POCC(x,c) { return i64_and(g_pcaps[c][x], BOARD()); }
function NCAP(x,c) { return (SHORTCAP(g_nmoves[x], c)); }
function KCAP(x,c) { return (SHORTCAP(g_kmoves[x], c)); }
function PCAP(x,c) { return i64_and(g_pcaps[c][x], g_colorb[c^1]); }
function PCA3(x,c) {
  var b=i64_and( g_BITi[g_ENPASS()], (c==1?g_FF0:g_Fo) );
  return  i64_and( g_pcaps[c][x|64], i64_or(g_colorb[c^1], b ));
}
function PCA4(x,c) {
  var b=i64_and( g_BITi[g_ENPASS()], (c==1?g_FF0:g_Fo) );
  return  i64_and( g_pcaps[c][x|128], i64_or(g_colorb[c^1], b ));
}
function RANK(x,y) { return ((x&0x38)==y); }
function TEST(f,b) { return i64_nz( i64_and( g_BITi[f], b)); }
function g_ENPASS() { return (g_flags&63); }
function CASTLE() { return (g_flags&960); }
function COUNT() { return (g_count&0x3FF); }
function BOARD() { return i64_or(g_colorb[0], g_colorb[1]); }
function RQU() { return i64_or(g_pieceb[g_QUEEN], g_pieceb[g_ROOK]); }
function BQU() { return i64_or(g_pieceb[g_QUEEN], g_pieceb[g_BISHOP]); }
function getLowestBit(bb) { return i64_and( bb, i64_neg(bb) ); }
function _getpiece(s,c)
{
 for(var i=0;i<8;i++)
 if (g_pieceChar[i] == s) { c[0] = 0; return i; }
 else if (g_pieceChar[i] == s.toUpperCase()) { c[0] = 1; return i; }
 return 0;
}

function printboard()
{
 var b="",s ="",i,k,c,cr=CHR(13),bo=[];
 for(i=0;i<64;i++)
 {
  c = ".";
  for(k=0;k<8;k++)
  {
   if( i64_gt( i64_and( g_pieceb[k], g_BITi[i] ), g_0 ) )
    {
    c = g_pieceChar[k];
    if( i64_eq( i64_and( g_colorb[0] , g_BITi[i] ) , g_0 ) ) c = c.toLowerCase();
    }
  }
  s+=c;
  if((i&7)==7)
  {
  bo.push(s);
  s="";
  }
 }
for(i=8;i>0;) print(bo[--i]);
}


function _parse_fen(fen)
{
var f=fen.split(' '),col=0,row=7,s,r,p,cp = [0],t,bo = i64(),i,j,t,q,
 pos=f[0],mv=f[1].charAt(0),cas=f[2],enps=f[3],halfm=f[4],fullm=f[5];

i=0; while(i<8) g_pieceb[i++] = i64();
i=0; while(i<2) { g_colorb[i] = i64(); g_kingpos[i++] = -1; }
i=0;
while(i<64) {
 g_p_v[i] = []; g_movelist[i] = [];
 for(j=0;j<256;j++){ g_p_v[i][j] = 0; g_movelist[i][j] = 0; }
 g_movenum[i] = 0;
 g_pvlength[i] = 0;
 g_kvalue[i] = 0;
 g_crevoke[i] = 0x3FF;
 i++;
}

g_hashDB = [];
g_hashb = i64();
g_mstack = [];
g_mat_ = 0;

i = 0;
while(i<pos.length){
 s = pos.charAt(i);
 if (s == "/") { row--;col = 0; }
 else if(ISRANK(s)) col += s.charCodeAt(0)-48;
 else {
  p = _getpiece(s, cp);
  c = cp[0]; q=(row<<3)|col;
  if(p == g_KING) g_kingpos[c] = (row<<3) + col;
  else { t=g_pval[p]; g_mat_+= (c == 1?-t:t); }
  t = q | i << 6 | (c == 1 ? 512 : 0);
  bo = g_BITi[q];
  g_hashb = i64_xor( g_hashb, g_hashxor[t] );
  g_pieceb[p] = i64_or( g_pieceb[p], bo );
  g_colorb[c] = i64_or( g_colorb[c], bo );
  col++;
 }
 i++;
}

g_onmove = (mv =="b"?1:0);
g_flags = 0;
i = 0;
while(i<cas.length)
{
s = cas.charAt(i);
if (s == "K") g_flags|=g_BITi[6].l ;
if (s == "k") g_flags|=g_BITi[7].l ;
if (s == "Q") g_flags|=g_BITi[8].l ;
if (s == "q") g_flags|=g_BITi[9].l ;
i++;
}
s = enps.charAt(0); r = enps.charAt(1);
if (ISFILE(s) && ISRANK(r)) g_flags|=((r.charCodeAt(0)-49)<<3)+(r.charCodeAt(1)-97);
g_count = ((fullm - 1)<<1) + g_onmove + (halfm<<10);
j=COUNT();
i=0; while(i<j) g_hstack[i++] = i64();
}

function _startpos()
{
 _parse_fen(g_sfen);
  g_engine = 1;
}

function LOW16(x) { return (x & g_F); }

function _rand_32()
{
g_r_x = i64u((g_r_x*69069) + 1);
g_r_y^=(g_r_y<<13);
g_r_y^=(g_r_y<<17);
g_r_y^=(g_r_y<<5);
g_r_y = i64u(g_r_y);
var t = i64u(g_r_w<<1) + g_r_z + g_r_c;
g_r_c = i64u( ((g_r_z>>2)+(g_r_w>>3)+(g_r_c>>2))>>30 );
g_r_z = g_r_w;
g_r_w = i64u(t);
return i64u(g_r_x + g_r_y + g_r_w);
}

function _rand_64() { return i64_ax( _rand_32(), _rand_32() ); }

function identPiece(f)
{
 if (TEST(f, g_pieceb[g_PAWN])) return g_PAWN;
 if (TEST(f, g_pieceb[g_KNIGHT])) return g_KNIGHT;
 if (TEST(f, g_pieceb[g_BISHOP])) return g_BISHOP;
 if (TEST(f, g_pieceb[g_ROOK])) return g_ROOK;
 if (TEST(f, g_pieceb[g_QUEEN])) return g_QUEEN;
 if (TEST(f, g_pieceb[g_KING])) return g_KING;
 return g_ENP;
}

function key000(b,f)
{
  var a = i64_rshift( b, (f&56) );
  return ( a.l  & 0x7E);
}

function key090(b,f)
{
  var a = i64_rshift( b, (f&7) ), L = a.l, H = (a.h<<1);
  L = ( (L & 0x1010101) | (H & 0x2020202));
  L = ( (L & 0x303) | ((L>>14) & 0xC0C));
  return ( (L & 0xE) | ((L>>4)& 0x70));
}

function keyDiag(b)
{
 var L = (b.l | b.h);
 L |= L >> 16;
 L |= L >>  8;
 return (L & 0x7E);
}

function key045(b,f) { return keyDiag( i64_and(b, g_bmask45[f]) ); }
function key135(b,f) { return keyDiag( i64_and(b, g_bmask135[f]) ); }
function DUALATT(x,y,c) { return (battacked(x, c)|battacked(y, c)); }
function battacked(f,c) 
{
 if ( i64_nz( i64_and(PCAP(f,c), g_pieceb[g_PAWN])) ) return 1;
 if ( i64_nz( i64_and(NCAP(f,c), g_pieceb[g_KNIGHT])) ) return 1;
 if ( i64_nz( i64_and(KCAP(f,c), g_pieceb[g_KING])) ) return 1;
 if ( i64_nz( i64_and(RCAP1(f,c), RQU())) ) return 1;
 if ( i64_nz( i64_and(RCAP2(f,c), RQU())) ) return 1;
 if ( i64_nz( i64_and(BCAP3(f,c), BQU())) ) return 1;
 if ( i64_nz( i64_and(BCAP4(f,c), BQU())) ) return 1;
 return 0;
}

function reach(f,c)
{
  var t = i64_and( NCAP(f,c), g_pieceb[g_KNIGHT] );
  t = i64_or( t, i64_and( RCAP1(f,c), RQU()) );
  t = i64_or( t, i64_and( RCAP2(f,c), RQU()) );
  t = i64_or( t, i64_and( BCAP3(f,c), BQU()) );
  return i64_or( t, i64_and( BCAP4(f,c), BQU()) );
}

function  attacked(f,c) {
  return i64_or( i64_and(PCAP(f, c), g_pieceb[g_PAWN] ), reach(f, c) );
}

function _init_pawns(moves,caps,freep,filep,helpp,c)
{
 var rank,file,jrank,jfile,dfile,m,n,j,i;
 for(i=0;i<64;i++)
 {
 moves[i] = i64(); caps[i] = i64(); freep[i] = i64();
 filep[i] = i64(); helpp[i] = i64();
 rank = (i>>3);
 file = (i&7);
 m = i+(c==1?-8:8);
 g_pawnprg[c][i] = g_pawnrun[(c==1?7-rank:rank)];
 for(j=0;j<64;j++)
  {
  jrank = (j>>3);
  jfile = (j&7);
  dfile = (jfile - file)*(jfile - file);
  if(dfile <= 1)
   {
   if(((c==1)&&(jrank < rank)) || ((c==0)&&(jrank>rank)))
	{
	if(dfile==0) filep[i] = i64_or( filep[i], g_BITi[j] );
	freep[i] = i64_or( freep[i], g_BITi[j] );
	}
   else
	{
	if (dfile != 0 && (jrank - rank)*(jrank - rank)<=1)
	helpp[i] = i64_or( helpp[i], g_BITi[j] );
	}
   }
  }
  
  if((m>=0)&&(m<=63))
  {
  moves[i] = i64_or( moves[i], g_BITi[m] );
  if(file>0)
   {
   m=i+(c==1?-9:7);
   if ((m>=0)&&(m<=63))
	{
	caps[i] = i64_or( caps[i], g_BITi[m] );
	n = i + (64*(2 - c));
	caps[n] = i64_or( caps[n], g_BITi[m] );
	}
   }
  if((m>=0)&&(m<=63)&&(file<7))
   {
    m=i+(c==1?-7:9);
    if((m>=0)&&(m<=63))
	{
	caps[i] = i64_or( caps[i], g_BITi[m] );
	n=i+(64*(c+1));
	caps[n] = i64_or( caps[n], g_BITi[m] );
	}
    }
   }
  }

}

function _init_shorts(moves,m)
{
 var j,i,n,q;
 for (i = 0; i < 64; i++)
  for (j = 0; j < 8; j++)
  {
  n = i+m[j];
  if((n<64)&&(n>=0))
	{
	q=(n&7)-(i&7);
	if((q*q) <= 4) moves[i] = i64_or( moves[i], g_BITi[n] );
	}
  }
}

function _occ_free_board(bc,dl,fr)
{
 var i,fr1 = i64c(fr), perm = i64c(fr), low, nlow;
 for (i = 0; i < bc; i++)
 {
  low = getLowestBit(fr1);	//object, not i64_bitlowestat value 0..63
  nlow = i64_not(low);
  fr1 = i64_and( fr1, nlow );
  if (!TEST(i,dl)) perm = i64_and( perm, nlow );
 }
 return perm;
}

function _init_g_rays1()
{
 var f,k,i,m,bc,ix,iperm,bo,move,occ,xray;
 for(f=0;f<64;f++)
 { 
 m=i64_or( _rook0(f, g_0, 0) , g_BITi[f] );
 bc = i64_bitcount(m);
 iperm =(1<<bc);
 for(i=0;i<iperm;i++)
  {
  bo = _occ_free_board(bc, i64v(i), m);
  move = _rook0(f, bo, 1);
  occ = _rook0(f, bo, 2);
  xray = _rook0(f, bo, 3);
  ix = key000(bo, f);
  k = (f<<7) + ix;
  g_rays[k] = i64_or( occ , move );
  g_rays[k + 0x8000] = xray;
  }
 }
}

function _init_g_rays2()
{
 var f,k,i,m,bc,ix,iperm,bo,move,occ,xray;
 for(f=0;f<64;f++)
 {
 m = i64_or( _rook90(f, g_0, 0) , g_BITi[f] );
 bc = i64_bitcount(m);
 iperm =(1<<bc);
 for(i=0;i<iperm;i++)
  {
  bo = _occ_free_board(bc, i64v(i), m);
  move = _rook90(f, bo, 1);
  occ = _rook90(f, bo, 2);
  xray = _rook90(f, bo, 3);
  ix = key090(bo, f);
  k = (f<<7) + ix + 0x2000;
  g_rays[k] = i64_or( occ , move );
  g_rays[k + 0x8000] = xray;
  }
 }
}

function _init_g_rays3()
{
 var f,k,i,m,bc,ix,iperm,bo,move,occ,xray;
 for(f=0;f<64;f++)
 {
 m = i64_or( _bishop45(f, g_0, 0) , g_BITi[f] );
 bc = i64_bitcount(m);
 iperm =(1<<bc);
 for(i=0;i<iperm;i++)
  {
  bo = _occ_free_board(bc, i64v(i), m);
  move = _bishop45(f, bo, 1);
  occ = _bishop45(f, bo, 2);
  xray = _bishop45(f, bo, 3);
  ix = key045(bo, f);
  k = (f<<7) + ix + 0x4000;
  g_rays[k] = i64_or( occ , move );
  g_rays[k + 0x8000] = xray;
  }
 }
}

function _init_g_rays4()
{
 var f,k,i,m,bc,ix,iperm,bo,move,occ,xray;
 for(f=0;f<64;f++)
 {
 m = i64_or( _bishop135(f, g_0, 0) , g_BITi[f] );
 bc = i64_bitcount(m);
 iperm =(1<<bc);
 for(i=0;i<iperm;i++)
  {
  bo = _occ_free_board(bc, i64v(i), m);
  move = _bishop135(f, bo, 1);
  occ = _bishop135(f, bo, 2);
  xray = _bishop135(f, bo, 3);
  ix = key135(bo, f);
  k = (f<<7) + ix + 0x6000;
  g_rays[k] = i64_or( occ , move );
  g_rays[k + 0x8000] = xray;
  }
 }
}

function _rook0(f, board, t)
{
 var b,i,fr=i64(),occ=i64(),xray=i64();
 
 for(b=0, i=f+1; (i<64)&&((i%8)!=0); i++)
 {
 if(TEST(i,board))
  {
  if(b) { xray = i64_or( xray, g_BITi[i] ); break; }
  else { occ = i64_or( occ, g_BITi[i] ); b = 1; }
  }
 if(!b) fr = i64_or( fr, g_BITi[i] );
 }

 for(b=0, i=f-1; (i>=0)&&((i%8)!=7); i--)
 {
 if(TEST(i,board))
  {
  if (b) { xray = i64_or( xray, g_BITi[i] ); break; }
  else { occ = i64_or( occ, g_BITi[i] ); b = 1; }
  }
 if(!b) fr = i64_or( fr, g_BITi[i] );
 }

 return (t<2) ? fr : (t==2 ? occ : xray);
}

function _rook90(f,board,t)
{
 var b,i,fr=i64(),occ=i64(),xray=i64();
 
 for(b=0, i=f-8; i>=0; i-=8)
 {
 if(TEST(i, board))
  {
  if(b) { xray = i64_or( xray, g_BITi[i] ); break; }
  else { occ = i64_or( occ, g_BITi[i] ); b = 1; }
  }
 if(!b) fr = i64_or( fr, g_BITi[i] );
 }
 
 for(b=0, i=f+8; i<64; i+=8)
 {
 if(TEST(i, board))
  {
  if(b) { xray = i64_or( xray, g_BITi[i] ); break; }
  else { occ = i64_or( occ, g_BITi[i] ); b = 1; }
  }
 if(!b) fr = i64_or( fr, g_BITi[i] );
 }

 return (t<2) ? fr : (t==2 ? occ : xray);
}

function _bishop45(f,board,t)
{
 var b,i,fr=i64(),occ=i64(),xray=i64();

 for(b=0, i=f+9; i<64 &&(i%8!=0);i+=9)
 {
 if(TEST(i, board))
  {
  if(b) { xray = i64_or( xray, g_BITi[i] ); break; }
  else { occ = i64_or( occ, g_BITi[i] ); b = 1; }
  }
 if(!b) fr = i64_or( fr, g_BITi[i] );
 }

 for(b=0, i=f-9; i>=0 && ((i%8)!=7);i-=9)
 {
 if(TEST(i, board))
  {
  if(b) { xray = i64_or( xray, g_BITi[i] ); break; }
  else { occ = i64_or( occ, g_BITi[i] ); b = 1; }
  }
 if(!b) fr = i64_or( fr, g_BITi[i] );
 }

 return (t<2) ? fr : (t==2 ? occ : xray);
}

function _bishop135(f,board,t)
{
 var b,i,fr=i64(),occ=i64(),xray=i64();

 for(b=0, i=f-7; i>=0 && ((i%8)!=0);i-=7)
 {
 if(TEST(i, board))
  {
  if(b) { xray = i64_or( xray, g_BITi[i] ); break; }
  else { occ = i64_or( occ, g_BITi[i] ); b = 1; }
  }
 if(!b) fr = i64_or( fr, g_BITi[i] );
 }
 
 for (b=0, i=f+7; i<64 && (i%8!=7); i+=7)
 {
 if(TEST(i, board))
  {
  if(b) { xray = i64_or( xray, g_BITi[i] ); break; }
  else { occ = i64_or( occ, g_BITi[i] ); b = 1; }
  }
 if(!b) fr = i64_or( fr, g_BITi[i] );
 }

 return (t<2) ? fr : (t==2 ? occ : xray);
}

function displaym(m) { print(mvstr(m,false)); }
function piece_S(p) { return (p==g_PAWN ? "" :g_pieceChar[p]); }

function mvstr(m,l)
{
  var s="",q,p=PROM(m),f=FROM(m),t=TO(m);
  if(l) s+=piece_S(PIECE(m));
  s+=CHR(97+(f&7)) + CHR(49+(f>>3));
  if(l) s+=(CAP(m)?"x":"-");
  s+=CHR(97+(t&7)) + CHR(49+(t>>3));
  if(p){ q=piece_S(p); s+=( l ? "="+q : q.toLowerCase() ); }
  return s;
}


function displaypv()
{
 var s="",i;
 for(i=0; i<g_pvlength[0]; i++) s+=mvstr(g_p_v[0][i])+" ";
 print(s);
}

function ig_sdraw(hp,nrep)
{
 var c = 0,n,i;

 if (g_count > 0xFFF)
 {
 if (g_count >= (0x400)*100) return 2;
 i=COUNT();
 n = i-(g_count>>10);
 for (i-=2; i>=n; i--) 
  {
  if ((i>0) && i64_eq( g_hstack[i], hp) && ((++c)==nrep)) return 1;
  }
 }
 else if ( i64_eq( i64_or(g_pieceb[g_PAWN], RQU()), g_0 ) )
	{
	if ( (i64_bitcount(g_colorb[0])<=2) && (i64_bitcount(g_colorb[1])<=2) ) return 3;
	}
 return 0;
}

function pinnedPieces(f,oc)
{
 var t,b,p=i64();

 b = i64_and( i64_and( i64_or(RXRAY1(f) , RXRAY2(f)) , g_colorb[oc]) , RQU() );
 while(b.l||b.h)
 {
 t = i64_bitlowestat(b);
 b = i64_xor( b, g_BITi[t] );
 p = i64_or( p, i64_and( RCAP(t, oc) , ROCC(f) ) );
 }

 b = i64_and( i64_and( i64_or(BXRAY3(f) , BXRAY4(f)) , g_colorb[oc]) , BQU() );
 while(b.l||b.h)
 {
 t = i64_bitlowestat(b);
 b = i64_xor( b, g_BITi[t] );
 p = i64_or( p, i64_and( BCAP(t, oc) , BOCC(f) ) );
 }
 return p;
}

function getDir(f,t)
{
 if (!((f ^ t) & 56)) return 8;
 if (!((f ^ t) & 7)) return 16;
 return (((f-t)%7)?32:64);
}

function move(m,c)
{
 var f = FROM(m),t = TO(m),p = PIECE(m),a = CAP(m),q,c1=c^1,x,w;

 g_colorb[c] = i64_xor( g_colorb[c], g_BITi[f] );
 g_pieceb[p] = i64_xor( g_pieceb[p], g_BITi[f] );
 g_colorb[c] = i64_xor( g_colorb[c], g_BITi[t] );
 g_pieceb[p] = i64_xor( g_pieceb[p], g_BITi[t] );
 q = (p<<6)|(c<<9);
 g_hashb = i64_xor( g_hashb, g_hashxor[(f|q)] );
 g_hashb = i64_xor( g_hashb, g_hashxor[(t|q)] );
 g_flags&=960;
 g_count+=0x401;
 if(a)
 {
 if (a==g_ENP) { t = (t&7)|(f&56); a = g_PAWN; }
 else if((a==g_ROOK) && CASTLE()) g_flags&=g_crevoke[t];
 
 g_pieceb[a] = i64_xor( g_pieceb[a], g_BITi[t] );
 g_colorb[c1] = i64_xor( g_colorb[c1], g_BITi[t] );
 q = (a<<6)|(c<<9);
 g_hashb = i64_xor( g_hashb, g_hashxor[(t|q)] );
 g_count&=0x3FF;
 x = g_pval[a];
 g_mat_+=(c==1?-x:x);
 }
 
 if (p==g_PAWN)
 {
  if (!((f^t)&8)) g_flags|=f^24;
  else
  {
  w = (t&56);
  if((!w)||(w==56))
  {
  g_pieceb[g_PAWN] = i64_xor( g_pieceb[g_PAWN], g_BITi[t] );
  w = PROM(m);
  g_pieceb[w] = i64_xor( g_pieceb[w], g_BITi[t] );

  q = (g_PAWN<<6)|(c<<9);
  g_hashb = i64_xor( g_hashb, g_hashxor[(t|q)] );
  q = (w<<6)|(c<<9);
  g_hashb = i64_xor( g_hashb, g_hashxor[(t|q)] );

  x = -g_pval[g_PAWN] + g_pval[w];
  g_mat_+=(c==1?-x:x);
  }
  }
  g_count&=0x3FF;
 }
 else
 if (p==g_KING)
 { 
  g_kingpos[c] = (g_kingpos[c]==f?t:f);
  g_flags&= (~(320<<c));
  if(((f^t)&3)==2)
  {
  if(t==6){f=7;t=5;} else if(t==2){f=0;t=3;} else if(t==62){f=63;t=61;} else {f=56;t=59;}
  g_colorb[c] = i64_xor( g_colorb[c], g_BITi[f] );
  g_pieceb[g_ROOK] = i64_xor( g_pieceb[g_ROOK], g_BITi[f] );
  g_colorb[c] = i64_xor( g_colorb[c], g_BITi[t] );
  g_pieceb[g_ROOK] = i64_xor( g_pieceb[g_ROOK], g_BITi[t] );
  q = (g_ROOK<<6)|(c<<9);
  g_hashb = i64_xor( g_hashb, g_hashxor[(f|q)] );
  g_hashb = i64_xor( g_hashb, g_hashxor[(t|q)] );
  }
 }
 else
 if ((p==g_ROOK) && CASTLE()) g_flags&=g_crevoke[f];
}

function doMove(m,c)
{
 g_mstack[COUNT()] = {count:g_count, flags:g_flags, mat:g_mat_, move:m};
 move(m,c);
}

function undoMove(m,c)
{
 var o=g_mstack[COUNT()-1];
 if(m==null) m=o.move;
 move(m,c);
 g_count = o.count;
 g_flags = o.flags;
 g_mat_ = o.mat;
}

function registerCaps(m,bc,mlist,mn)
{
 var t,b=i64c(bc);
 while(b.l||b.h)
 {
 t = i64_bitlowestat(b);
 b = i64_xor( b, g_BITi[t] );
 mlist[(mn[0]++)] = m | _TO(t) | _CAP(identPiece(t));
 }
}

function registerMoves(m,bc,bm,mlist,mn)
{
 var t,b=i64c(bc),u=i64c(bm);

 while(b.l||b.h)
 {
 t = i64_bitlowestat(b);
 b = i64_xor( b, g_BITi[t] );
 mlist[(mn[0]++)] =  m | _TO(t) | _CAP(identPiece(t));
 }

 while(u.l||u.h)
 {
 t = i64_bitlowestat(u);
 u = i64_xor( u, g_BITi[t] );
 mlist[(mn[0]++)] = m  | _TO(t);
 }
}

function registerProms(f,c,bc,bm,mlist,mn)
{
 var w=(f | _ONMV(c) | _PIECE(g_PAWN)) , t,m,n, b=i64c(bc), u=i64c(bm);

 while(b.l||b.h)
 {
 t = i64_bitlowestat(b);
 b = i64_xor( b, g_BITi[t] );
 m = w | _TO(t) | _CAP(identPiece(t));
 n=mn[0];
 mlist[n++] = m | _PROM(g_QUEEN);
 mlist[n++] = m | _PROM(g_KNIGHT);
 mlist[n++] = m | _PROM(g_ROOK);
 mlist[n++] = m | _PROM(g_BISHOP);
 mn[0]=n;
 }
 
 while(u.l||u.h)
 {
 t = i64_bitlowestat(u);
 u = i64_xor( u, g_BITi[t] );
 m = w | _TO(t);
 n=mn[0];
 mlist[n++] = m | _PROM(g_QUEEN);
 mlist[n++] = m | _PROM(g_KNIGHT);
 mlist[n++] = m | _PROM(g_ROOK);
 mlist[n++] = m | _PROM(g_BISHOP);
 mn[0]=n;
 }
}

function registerKing(m,bc,bm,mlist,mn,c)
{
 var t,b=i64c(bc),u=i64c(bm);

 while(b.l||b.h)
 {
 t = i64_bitlowestat(b);
 b = i64_xor( b, g_BITi[t] );
 if (!battacked(t, c)) mlist[(mn[0]++)] = m | _TO(t) | _CAP(identPiece(t));
 }
 while(u.l||u.h)
 {
 t = i64_bitlowestat(u);
 u = i64_xor( u, g_BITi[t] );
 if (!battacked(t, c)) mlist[(mn[0]++)] = m | _TO(t);
 }
}

function generateCheckEsc(ch,P,c,k,ml,mn)
{
 var bf,cf,cc,fl,ww,p,d,f,q ,c1=c^1, r=g_BITi[k];

 bf = i64_bitcount(ch);
 g_colorb[c] = i64_xor( g_colorb[c], r );
 registerKing(PREMOVE(k, g_KING, c), KCAP(k, c), KMOVE(k), ml, mn, c);
 g_colorb[c] = i64_xor( g_colorb[c], r );

 if(bf>1) return bf;

 bf = i64_bitlowestat(ch);
 cc = i64_and( attacked(bf, c1), P );

 while(cc.l||cc.h)
 {
 cf = i64_bitlowestat(cc);
 cc = i64_xor( cc, g_BITi[cf] );
 p = identPiece(cf);
 if((p==g_PAWN) && RANK(cf, (c ? 0x08 : 0x30) )) registerProms(cf, c, ch, g_0, ml, mn);
 else registerMoves(PREMOVE(cf, p, c), ch, g_0, ml, mn);
 }
 
 if((g_ENPASS()) && i64_nz( i64_and(ch, g_pieceb[g_PAWN])) )
 {
  cc = i64_and( i64_and( PCAP(g_ENPASS(), c1) , g_pieceb[g_PAWN] ), P );
  while(cc.l||cc.h)
  {
  cf = i64_bitlowestat(cc);
  cc = i64_xor( cc, g_BITi[cf] );
  registerMoves(PREMOVE(cf, g_PAWN, c), g_BITi[g_ENPASS()], g_0, ml, mn);
  }
 }
 
 if ( i64_nz(i64_and(ch, i64_or(g_nmoves[k], g_kmoves[k]))) ) return 1;

 d = getDir(bf, k);
 fl = ((d&8) ? i64_and( RMOVE1(bf) , RMOVE1(k) ) :
	((d&16) ? i64_and( RMOVE2(bf) , RMOVE2(k) ) :
	((d&32) ? i64_and( BMOVE3(bf) , BMOVE3(k) ):
	i64_and( BMOVE4(bf) , BMOVE4(k) ))));

 while(fl.l||fl.h)
 {
 f = i64_bitlowestat(fl);
 fl = i64_xor( fl, g_BITi[f] );
 cc = i64_and( reach(f, c1), P );
 while(cc.l||cc.h)
  {
  cf = i64_bitlowestat(cc);
  cc = i64_xor( cc, g_BITi[cf] );
  p = identPiece(cf);
  registerMoves(PREMOVE(cf, p, c), g_0, g_BITi[f], ml, mn);
  }
 bf = (c ? f+8 : f-8);
 if(bf>=0 && bf<=63)
  {
	ww = i64_and( g_BITi[bf] , g_pieceb[g_PAWN] );
	ww = i64_and( ww , g_colorb[c] );
	ww = i64_and( ww , P );
	if( i64_nz(ww) )
	{
	if(RANK(bf, (c ? 0x08 : 0x30) )) registerProms(bf, c, g_0, g_BITi[f], ml, mn);
	else registerMoves(PREMOVE(bf, g_PAWN, c), g_0, g_BITi[f], ml, mn);
	}

	if(RANK(f, (c ? 0x20 : 0x18) ))
	{
	q=(c ? f+16 :f-16);
	ww = i64_and( g_BITi[q] , g_pieceb[g_PAWN] );
	ww = i64_and( ww , g_colorb[c] );
	ww = i64_and( ww , P );
	if ( i64_nz(ww) && i64_is0( i64_and( BOARD() , g_BITi[bf] )) )
	 registerMoves(PREMOVE(q, g_PAWN, c), g_0, g_BITi[f], ml, mn);
	}
  }
 }
 return 1;
}

function generateMoves(ch,c,ply)
{
var r,t,c1=c^1,q3=(c?0x30:0x08),q8=(c?0x08:0x30),
 f = g_kingpos[c], cb = g_colorb[c], P = pinnedPieces(f, c1),
 nP = i64_not(P), ml = g_movelist[ply], mn = [0], b,m,a,h,C,b1,b2;

if(ch.l||ch.h) 
{ r=generateCheckEsc(ch, nP, c, f, ml, mn); g_movenum[ply]=mn[0]; return r; }

registerKing(PREMOVE(f, g_KING, c), KCAP(f, c), KMOVE(f), ml, mn, c);

cb = i64_and( g_colorb[c] , nP );
b = i64_and( g_pieceb[g_PAWN] , cb );
while(b.l||b.h)
{
 f = i64_bitlowestat(b);
 b = i64_xor( b, g_BITi[f] );
 m = PMOVE(f, c);
 a = PCAP(f, c);
 if((m.l||m.h) && RANK(f,q3)) m = i64_or( m, PMOVE( (c?f-8:f+8), c) );
 
 if(RANK(f,q8)) registerProms(f, c, a, m, ml, mn);
 else
 {
 if(g_ENPASS() &&  i64_nz( i64_and(g_BITi[g_ENPASS()] , g_pcaps[c][f])) )
  {
  C = g_ENPASS()^8;
  g_colorb[c] = i64_xor( g_colorb[c], g_BITi[C] );
  h = ROCC1(f);
  b1 = i64_and(h, g_BITi[g_kingpos[c]]);
  b2 = i64_and( i64_and(h, g_colorb[c1]), RQU() );
  if (  i64_is0(b1) || i64_is0(b2) ) a = i64_or( a, g_BITi[g_ENPASS()] );
  g_colorb[c] = i64_xor( g_colorb[c] , g_BITi[C] );
  }
 registerMoves(PREMOVE(f, g_PAWN, c), a, m, ml, mn);
 }
}

b = i64_and( P, g_pieceb[g_PAWN] );
while(b.l||b.h)
{
 f = i64_bitlowestat(b);
 b = i64_xor( b, g_BITi[f] );

 t = getDir(f, g_kingpos[c]);
 if(!(t&8))
 {
 a = i64(); m=i64();
 if(t&16)
  {
  m = PMOVE(f, c);
  if ((m.l||m.h) && RANK(f,q3)) m = i64_or( m, PMOVE((c?f-8:f+8 ), c) );
  }
 else a= ((t&32) ? PCA3(f, c) : PCA4(f, c));

 if (RANK(f,q8)) registerProms(f, c, a, m, ml, mn);
 else registerMoves(PREMOVE(f, g_PAWN, c), a, m, ml, mn);
 }
}

b = i64_and( g_pieceb[g_KNIGHT] , cb );
while(b.l||b.h)
{
 f = i64_bitlowestat(b);
 b = i64_xor( b, g_BITi[f] );
 registerMoves(PREMOVE(f, g_KNIGHT, c), NCAP(f, c), NMOVE(f), ml, mn);
}

b = i64_and( g_pieceb[g_ROOK] , cb );
while(b.l||b.h)
{
 f = i64_bitlowestat(b);
 b = i64_xor( b, g_BITi[f] );
 registerMoves(PREMOVE(f, g_ROOK, c), RCAP(f, c), RMOVE(f), ml, mn);
 if(CASTLE() && i64_is0(ch))
 {
 if(c)
  {
  b1 = i64_and( RMOVE1(63) , g_BITi[61] );
  if((g_flags&128) && (f==63) && (b1.l||b1.h) && (!DUALATT(61, 62, c)))
	registerMoves(PREMOVE(60, g_KING, c), g_0, g_BITi[62], ml, mn);

  b1 = i64_and( RMOVE1(56) , g_BITi[59] );
  if((g_flags&512) && (f==56) && (b1.l||b1.h) && (!DUALATT(59, 58, c)))
	registerMoves(PREMOVE(60, g_KING, c), g_0, g_BITi[58], ml, mn);
  }
 else
  {
  b1 = i64_and( RMOVE1(7) , g_BITi[5] );
  if((g_flags&64) && (f==7) && (b1.l||b1.h) && (!DUALATT(5, 6, c)))
	registerMoves(PREMOVE(4, g_KING, c), g_0, g_BITi[6], ml, mn);

  b1 = i64_and( RMOVE1(0) , g_BITi[3] );
  if(((g_flags&256) && (f==0) && (b1.l||b1.h)) && (!DUALATT(3, 2, c)))
	registerMoves(PREMOVE(4, g_KING, c), g_0, g_BITi[2], ml, mn);
  }
 }
}

b = i64_and( g_pieceb[g_BISHOP] , cb );
while(b.l||b.h)
{
 f = i64_bitlowestat(b);
 b = i64_xor( b, g_BITi[f] );
 var u1=PREMOVE(f, g_BISHOP, c);
 var u2=BCAP(f, c);
 var u3=BMOVE(f);
 registerMoves(PREMOVE(f, g_BISHOP, c), BCAP(f, c), BMOVE(f), ml, mn);
}

b = i64_and( g_pieceb[g_QUEEN] , cb );
while(b.l||b.h)
{
 f = i64_bitlowestat(b);
 b = i64_xor( b, g_BITi[f] );
 b1 = i64_or( RCAP(f, c), BCAP(f,c) );
 b2 = i64_or( RMOVE(f), BMOVE(f) );
 registerMoves(PREMOVE(f, g_QUEEN, c), b1, b2, ml, mn);
}

b = i64_or( i64_or( g_pieceb[g_ROOK], g_pieceb[g_BISHOP] ), g_pieceb[g_QUEEN] );
b = i64_and( P, b );

while(b.l||b.h)
{
 f = i64_bitlowestat(b);
 b = i64_xor( b, g_BITi[f] );

 p = identPiece(f);
 t = ( p | getDir(f, g_kingpos[c]) );

 if ((t&10)==10) registerMoves(PREMOVE(f, p, c), RCAP1(f, c), RMOVE1(f), ml, mn);
 if ((t&18)==18) registerMoves(PREMOVE(f, p, c), RCAP2(f, c), RMOVE2(f), ml, mn);
 if ((t&33)==33) registerMoves(PREMOVE(f, p, c), BCAP3(f, c), BMOVE3(f), ml, mn);
 if ((t&65)==65) registerMoves(PREMOVE(f, p, c), BCAP4(f, c), BMOVE4(f), ml, mn);
}

g_movenum[ply] = mn[0];

return 0;
}

function generateCaps(ch,c,ply)
{
var r,t,f = g_kingpos[c], cb = g_colorb[c], c1=c^1, q8=(c?0x08:0x30),
 P = pinnedPieces(f, c1), nP = i64_not(P), ml = g_movelist[ply],
 mn = [0], b,m,a,C, b1,b2,h,p;

if (ch.l||ch.h)
 { r=generateCheckEsc(ch, nP, c, f, ml, mn); g_movenum[ply] = mn[0]; return r; }

registerKing(PREMOVE(f, g_KING, c), KCAP(f, c), g_0, ml, mn, c);

cb = i64_and( g_colorb[c] , nP );
b = i64_and( g_pieceb[g_PAWN] , cb );
while(b.l||b.h)
{
 f = i64_bitlowestat(b);
 b = i64_xor( b, g_BITi[f] );

 a = PCAP(f, c);
 if(RANK(f, q8))
  registerMoves(( PREMOVE(f, g_PAWN, c) | _PROM(g_QUEEN) ), a, PMOVE(f, c), ml, mn);
 else
  {
  if (g_ENPASS() && i64_nz( i64_and(g_BITi[g_ENPASS()] , g_pcaps[c][f]) ))
	{
	C = ( g_ENPASS() ^ 8 );
	g_colorb[c] = i64_xor( g_colorb[c], g_BITi[C] );
	h = ROCC1(f);
	b1 = i64_and(h, g_BITi[g_kingpos[c]]);
	b2 = i64_and( i64_and(h, g_colorb[c1]), RQU() );
	if (  i64_is0(b1) || i64_is0(b2) ) a = i64_or( a, g_BITi[g_ENPASS()] );
	g_colorb[c] = i64_xor( g_colorb[c] , g_BITi[C] );
	}
  registerCaps(PREMOVE(f, g_PAWN, c), a, ml, mn);
  }
}

b = i64_and( P, g_pieceb[g_PAWN] );
while(b.l||b.h)
{
 f = i64_bitlowestat(b);
 b = i64_xor( b, g_BITi[f] );

 t = getDir(f, g_kingpos[c]);
 if(!(t&8))
 {
 m = i64(); a = i64();
 if(t&16) m = PMOVE(f, c); else a = ((t&32) ? PCA3(f, c) : PCA4(f, c));
 
 if(RANK(f,q8))
  registerMoves( ( PREMOVE(f, g_PAWN, c) | _PROM(g_QUEEN) ), a, m, ml, mn);
 else registerCaps(PREMOVE(f, g_PAWN, c), a, ml, mn);
 }
}

b = i64_and( g_pieceb[g_KNIGHT] , cb );
while(b.l||b.h)
{
 f = i64_bitlowestat(b);
 b = i64_xor( b, g_BITi[f] );
 registerCaps(PREMOVE(f, g_KNIGHT, c), NCAP(f, c), ml, mn);
}

b = i64_and( g_pieceb[g_BISHOP] , cb );
while(b.l||b.h)
{
 f = i64_bitlowestat(b);
 b = i64_xor( b, g_BITi[f] );
 registerCaps(PREMOVE(f, g_BISHOP, c), BCAP(f, c), ml, mn);
}

b = i64_and( g_pieceb[g_ROOK] , cb );
while(b.l||b.h)
{
 f = i64_bitlowestat(b);
 b = i64_xor( b, g_BITi[f] );
 registerCaps(PREMOVE(f, g_ROOK, c), RCAP(f, c), ml, mn);
}

b = i64_and( g_pieceb[g_QUEEN] , cb );
while(b.l||b.h)
{
 f = i64_bitlowestat(b);
 b = i64_xor( b, g_BITi[f] );
 registerCaps(PREMOVE(f, g_QUEEN, c), i64_or(RCAP(f, c) , BCAP(f,c)), ml, mn);
}

b = i64_or( i64_or( g_pieceb[g_ROOK], g_pieceb[g_BISHOP] ), g_pieceb[g_QUEEN] );
b = i64_and( P, b );

while(b.l||b.h)
{
 f = i64_bitlowestat(b);
 b = i64_xor( b, g_BITi[f] );
 p = identPiece(f);
 t = p | getDir(f, g_kingpos[c]);
 
 if((t&10)==10) registerCaps(PREMOVE(f, p, c), RCAP1(f, c), ml, mn);
 if((t&18)==18) registerCaps(PREMOVE(f, p, c), RCAP2(f, c), ml, mn);
 if((t&33)==33) registerCaps(PREMOVE(f, p, c), BCAP3(f, c), ml, mn);
 if((t&65)==65) registerCaps(PREMOVE(f, p, c), BCAP4(f, c), ml, mn);
}

g_movenum[ply] = mn[0];
return 0;
}

function swap(m)	// SEE
{
var a, f = FROM(m), t = TO(m), onmv = ONMV(m), a_piece = g_pval[CAP(m)], piece = PIECE(m),
 c = onmv^1, nc = 1, d, T, mT, b1, C0 = i64c( g_colorb[0] ), C1 = i64c( g_colorb[1] ),
 k_list=[]; //32

a = i64_or( attacked(t, 0) , attacked(t, 1) );

k_list[0] = a_piece;
a_piece = g_pval[piece];
g_colorb[onmv] = i64_xor( g_colorb[onmv], g_BITi[f] );
if((piece&4)||(piece==1))
{
 d = getDir(f, t);
 if((d==32)||(d==64)) a = i64_or( a, i64_and( BOCC(t) , BQU() ) );
 if((d==8)||(d==16)) a = i64_or( a, i64_and( ROCC(t) , RQU() ) );
}

a = i64_and( a, BOARD() );

while(a.l||a.h)
{
 b1=i64_and( g_colorb[c] , a );
 T = i64_and( g_pieceb[g_PAWN], b1 );
 if(T.l||T.h) piece = g_PAWN;
 else
 {
 T = i64_and( g_pieceb[g_KNIGHT], b1 );
 if(T.l||T.h) piece = g_KNIGHT;
 else
 {
 T = i64_and( g_pieceb[g_BISHOP], b1 );
 if(T.l||T.h) piece = g_BISHOP;
 else
 {
 T = i64_and( g_pieceb[g_ROOK], b1 );
 if(T.l||T.h) piece = g_ROOK;
 else
 {
 T = i64_and( g_pieceb[g_QUEEN], b1 );
 if(T.l||T.h) piece = g_QUEEN;
 else
 {
 T = i64_and( g_pieceb[g_KING], b1 );
 if(T.l||T.h) piece = g_KING;
 else break;
 }
 }
 }
 }
 }

 mT = i64_neg( T );
 T = i64_and( T, mT );

 g_colorb[c] = i64_xor( g_colorb[c], T );
 if((piece&4)||(piece==1))
 {
  if(piece&1) a = i64_or( a, i64_and( BOCC(t) , BQU() ) );
  if(piece&2) a = i64_or( a, i64_and( ROCC(t) , RQU() ) );
 }
 a = i64_and( a, BOARD() );
 k_list[nc] = -k_list[nc-1] + a_piece;
 a_piece = g_pval[piece];
 nc++;
 c^=1; 
}

while((--nc)) if(k_list[nc] > -k_list[nc-1]) k_list[nc-1] = -k_list[nc];

g_colorb[0] = i64c( C0 );
g_colorb[1] = i64c( C1 );
return k_list[0];
}

function qpick(ml,mn,s)
{
var p=0,v=-g_HEUR,i,m,t;

for(i=s;i<mn;i++)
 {
 m=ml[i];
 t=g_cag_p_val[CAP(m)];
 if(t>v) {v=t; p=i;}
 }

m = ml[p];

if(p!=s) ml[p]=ml[s];
return m;
}

function spick(ml,mn,s,ply)
{
var p=0,v=-g_HEUR,i,m,t,c,r;

for(i=s;i<mn;i++)
 {
 m=ml[i];
 c = CAP(m);
 if(c)
  {
  t = g_cag_p_val[c];
  if(t>v) {v=t;p=i;}
  }
 if((v<g_HEUR) && (m==g_killer[ply])) {v=g_HEUR;p=i;}
 r=m&0xFFF;
 if(v<g_history[r]) {v=g_history[r];p=i;}
 }

m=ml[p];
if(p!=s) ml[p]=ml[s];
return m;
}

function evalc(c,sf)
{
var mn=0,K=0,oc=c^1,ocb = g_colorb[oc],kn = g_kmoves[g_kingpos[oc]],
 P = pinnedPieces(g_kingpos[c], oc), nP = i64_not(P), b,Y,f,m,a,w,e;

b = i64_and( g_pieceb[g_PAWN] , g_colorb[c] );
while(b.l||b.h)
{
 Y = 0;
 f = i64_bitlowestat(b);
 b = i64_xor( b, g_BITi[f] );
 Y = g_pawnprg[c][f];

 m = PMOVE(f, c);
 a = POCC(f, c);

 w = i64_and(a, kn);
 if(w.l||w.h) K+=(i64_bitcount(w)<<4);

 w = i64_and(g_BITi[f] , P);
 if(w.l||w.h)
	{
	if(!(getDir(f, g_kingpos[c])&16)) m=i64();
	}
 else
	{
	w = i64_and(a , g_pieceb[g_PAWN]);
	w = i64_and(w , g_colorb[c]);
	Y+=(i64_bitcount(w)<<2);
	}

 Y+=(m?8:-8);

 w = i64_and( i64_and(g_pawnfile[c][f] , g_pieceb[g_PAWN]), ocb );
 if(!(w.l|w.h))
 {
  w = i64_and( i64_and(g_pawnfree[c][f] , g_pieceb[g_PAWN]), ocb );
  if(!(w.l|w.h)) Y*=2;
  w = i64_and( i64_and(g_pawnhelp[c][f] , g_pieceb[g_PAWN]), g_colorb[c] );
  if(!(w.l|w.h)) Y-=33;
 }
 mn+=Y;
}

cb = i64_and( g_colorb[c] , nP );
b = i64_and( g_pieceb[g_KNIGHT] , cb );
while(b.l||b.h)
{
 sf[0]+=1;
 f = i64_bitlowestat(b);
 b = i64_xor( b, g_BITi[f] );
 a = g_nmoves[f];
 w = i64_and(a, kn);
 if(w.l||w.h) K+=(i64_bitcount(w)<<4);
 mn+=g_nmobil[f];
}

b = i64_and( g_pieceb[g_KNIGHT] , P );
while(b.l||b.h)
{
 sf[0]+=1;
 f = i64_bitlowestat(b);
 b = i64_xor( b, g_BITi[f] );
 a = g_nmoves[f];
 w = i64_and(a, kn);
 if(w.l||w.h) K+=(i64_bitcount(w)<<4);
}

g_colorb[oc] = i64_xor( g_colorb[oc], g_BITi[g_kingpos[oc]] );
b = i64_and( g_pieceb[g_QUEEN] , cb );

while(b.l||b.h)
{
 sf[0]+=4;
 f = i64_bitlowestat(b);
 b = i64_xor( b, g_BITi[f] );
 a = i64_or( RATT1(f) , RATT2(f) );
 a = i64_or(a, i64_or( BATT3(f) , BATT4(f) ) );
 w = i64_and(a, kn);
 if(w.l||w.h) K+=(i64_bitcount(w)<<4);
 mn+=i64_bitcount(a);
}

g_colorb[oc] = i64_xor( g_colorb[oc], i64_and( RQU() , ocb ) );

b = i64_and( g_pieceb[g_BISHOP] , cb );

while(b.l||b.h)
{
 sf[0]+=1;
 f = i64_bitlowestat(b);
 b = i64_xor( b, g_BITi[f] );
 a = i64_or( BATT3(f) , BATT4(f) );
 w = i64_and(a, kn);
 if(w.l||w.h) K+=(i64_bitcount(w)<<4);
 mn+=(i64_bitcount(a)<<3);
}

g_colorb[oc] = i64_xor( g_colorb[oc], i64_and( g_pieceb[g_ROOK] , ocb ) );
g_colorb[c] = i64_xor( g_colorb[c], i64_and( g_pieceb[g_ROOK] , cb ) );
b = i64_and( g_pieceb[g_ROOK] , cb );

while(b.l||b.h)
{
 sf[0]+=2;
 f = i64_bitlowestat(b);
 b = i64_xor( b, g_BITi[f] );
 a = i64_or( RATT1(f) , RATT2(f) );
 w = i64_and(a, kn);
 if(w.l||w.h) K+=(i64_bitcount(w)<<4);
 mn+=(i64_bitcount(a)<<2);
}

g_colorb[c] = i64_xor( g_colorb[c], i64_and( g_pieceb[g_ROOK] , cb ) );
b = i64_or( i64_or( g_pieceb[g_ROOK] , g_pieceb[g_BISHOP] ), g_pieceb[g_QUEEN] );
b = i64_and(P, b);

while(b.l||b.h)
{
 f = i64_bitlowestat(b);
 b = i64_xor( b, g_BITi[f] );
 p = identPiece(f);
 if(p==g_BISHOP)
	{
	sf[0]+=1;
	a = i64_or( BATT3(f) , BATT4(f) );
	w = i64_and(a, kn);
	if(w.l||w.h) K+=(i64_bitcount(w)<<4);
	}
 else if(p==g_ROOK)
	{
	sf[0]+=2;
	a = i64_or( RATT1(f) , RATT2(f) );
	w = i64_and(a, kn);
	if(w.l||w.h) K+=(i64_bitcount(w)<<4);
	}
 else
	{
	sf[0]+=4;
	a = i64_or( RATT1(f) , RATT2(f) );
	a = i64_or( a, i64_or( BATT3(f) , BATT4(f) ) );
	w = i64_and(a, kn);
	if(w.l||w.h) K+=(i64_bitcount(w)<<4);
	}
 t = (p | getDir(f, g_kingpos[c]));
 if((t&10)==10) mn+=i64_bitcount(RATT1(f));
 if((t&18)==18) mn+=i64_bitcount(RATT2(f));
 if((t&33)==33) mn+=i64_bitcount(BATT3(f));
 if((t&65)==65) mn+=i64_bitcount(BATT4(f));
}

g_colorb[oc] = i64_xor( g_colorb[oc], i64_and( g_pieceb[g_QUEEN] , ocb ) );
g_colorb[oc] = i64_xor( g_colorb[oc], g_BITi[g_kingpos[oc]] );
w = i64_and(g_pieceb[g_PAWN] , g_colorb[c]);
e=sf[0];
if((e==1) && (!(w.l|w.h))) mn =- 200;
if(e<7) K=(K*(e/7))|0;
if(e<2) sf[0]=2;
return (mn+K+((Math.random()*20)|0));
}

function eval0(c)
{
var sf0=0,sf1=0,sfp=[0], ev0=evalc(0, sfp), ev1;
sf0 = sfp[0];
sfp[0] = sf1;
ev1 = evalc(1, sfp);
sf1 = sfp[0];
g_eval1++;

if(sf1<6) ev0+=(g_kmobil[g_kingpos[0]]*(6-sf1));
if(sf0<6) ev1+=(g_kmobil[g_kingpos[1]]*(6-sf0));
return (c?(ev1-ev0):(ev0-ev1));
}

function quiesce(ch,c,ply,alpha,beta)
{
var best = -g_32, cmat =(c==1?-g_mat_:g_mat_), i,m,w,c1=c^1, cont=0;

if(ply>=g_sd) return (eval0(c)+cmat);

if(!(ch.l|ch.h))
 {
 if (cmat-200 >= beta) return beta;
 if (cmat+200 > alpha)
 {
 best = eval0(c) + cmat;
 if(best>alpha)
	{
	alpha = best;
	if(best >= beta) return beta;
	}
 }
 }

generateCaps(ch, c, ply);

if((ch.l||ch.h) && (!g_movenum[ply])) return (-g_32 + ply);

for(i=0; i<g_movenum[ply] && (!g_sabort); i++)
{
 m = qpick(g_movelist[ply], g_movenum[ply], i);
 cont = (!(ch.l|ch.h)) && (!PROM(m)) && (g_pval[PIECE(m)] > g_pval[CAP(m)]) && (swap(m) < 0);
 if(!cont)
 {
 doMove(m, c);
 g_nodes++;
 w = -quiesce(attacked(g_kingpos[c1], c1), c1, ply+1, -beta, -alpha);
 undoMove(m, c);

 if(w>best) best = w;
 if(w>alpha)
	{
	alpha = w;
	if(w>=beta) return beta;
	}
 }
}
return ( best>=alpha ? best : eval0(c) + cmat );
}

function retPVMove(c,ply)
{
 var i,m;

 generateMoves(attacked(g_kingpos[c], c), c, 0);

 for (i=0; i<g_movenum[0]; i++)
 {
 m = g_movelist[0][i];
 if (m==g_p_v[0][ply]) return m;
 }
 return 0;
}

function g_0variance(d)
{
var r = 0;
if(d>=4)
 {
  for(r=1;r<=g_Nonevar.length;r++)
   if (d<g_Nonevar[r-1]) break;
 }
return r;
}

function HASHP(c)
{
 return i64_xor(g_hashb, g_hashxor[(g_flags | 1024 | (c<<11))]);
}

function HASHB(c,d)
{
  var b=( g_flags | 1024 ), z=( c | 2048 | (d<<1));
  return i64_xor( i64_xor(g_hashb, g_hashxor[b]) , g_hashxor[z]);
}

// current time counter in seconds
function sectime()
{
 return (((new Date()).getTime()/1000)|0);
}

function search(ch,c,d,ply,alpha,beta,pvnode,isNone)
{
var hp,hb,he,w,c1=c^1,b1,R,v,H,M,m,n,i,j,B,A,F,ext,nch,C=0;

g_pvlength[ply] = ply;

if(!((g_nds++) & 15) && ((sectime() - g_starttime) > g_tm)) g_sabort = 1;

if(g_sabort || ply>=g_sd) return (eval0(c) + (c?-g_mat_:g_mat_));

hp = HASHP(c);
if(ply && ig_sdraw(hp, 1)) return 0;

if(!d) return quiesce(ch, c, ply, alpha, beta);

g_hstack[COUNT()] = hp;
hb = HASHB(c, d);

v = (hb.l & g_HMASKB);
he = (typeof(g_hashDB[v])=="undefined" ? null : g_hashDB[v]);
if ((he!=null) && i64_is0( i64_and(i64_xor(he,hb), g_HINVB)))
 {
 g_hc++;
 w = LOW16(he.l) - g_37;
 if (he.l & g_1)
	{
	isNone = 0;
	if(w <= alpha) return alpha;
	}
 else
	{
	if(w >= beta) return beta;
	}
 }
else
 {
  w = ( c? -g_mat_ : g_mat_ );
 }

b1 = i64_and( g_colorb[c], i64_not( g_pieceb[g_PAWN] ) );
b1 = i64_and( b1, i64_not( pinnedPieces(g_kingpos[c], c1) ) );

if((!pvnode) && (!(ch.l|ch.h)) && isNone && (d>1) && (i64_bitcount(b1)>2))
{
 g_flagstore = g_flags;
 R = ( (10 + d + g_0variance(w - beta))/4 )|0;
 if(R>d) R = d;

 g_flags &= 960;
 g_count += 0x401;

 w = -search(g_0, c1, d-R, ply+1, -beta, -alpha, 0, 0);
 g_flags = g_flagstore;
 g_count-= 0x401;
 if((!g_sabort) && (w >= beta))
	{
	g_hashDB[(hb.l & g_HMASKB)] = i64_or( i64_and(hb, g_HINVB) , i64v(w + g_37) );
	return beta;
	}
}
H = 0;
M = 0;
if(ply>0)
 {
 v =(hp.l & g_HMASKP);
 he = (typeof(g_hashDP[v])=="undefined" ? null : g_hashDP[v]);
 if((he!=null) && i64_is0( i64_and(i64_xor(he,hp), g_HINVP)) )
	{
	H = (he.l & g_HMASKP);
	M = H;
	}
 if((d>=4) && (M==0))
	{
	w = search(ch, c, d-3, ply, alpha, beta, pvnode, 0);
	v = (hp.l & g_HMASKP);
	he = (typeof(g_hashDP[v])=="undefined" ? null : g_hashDP[v]);
	if ((he!=null) && i64_is0( i64_and(i64_xor(he,hp), g_HINVP)) )
	 {
	 H = (he.l & g_HMASKP);
	 M = H;
	 }
	}
 }
 else
 {
 M = retPVMove(c, ply);
 }

n = -1;
i = 0;
B = (pvnode ? alpha : -(g_32+1) );
A = alpha;
F = 1;

while ((i++)!=n)
 {
 ext = 0;
 if(M)
  {
  m = M;
  M = 0;
  i--;
  }
 else
  {
  if(n==-1)
	{
	generateMoves(ch, c, ply);
	n = g_movenum[ply];
	if(!n) return( (ch.l||ch.h) ? -g_32+ply : 0 );
	}
  m = spick(g_movelist[ply], n, i, ply);
  C = (H && (m == H));
  }

 if(!C)
 {
 doMove(m, c);
 nch = attacked(g_kingpos[c1], c1);

 if(nch.l||nch.h) ext++;
 else
 {
  if((d>=3)&&(i>=4)&&(!pvnode))
  {
  if((!CAP(m))&&(!PROM(m)))
	{
	b1 = i64_and( g_pawnfree[c][TO(m)], g_pieceb[g_PAWN] );
	b1 = i64_and( b1, g_colorb[c1] );
	if((PIECE(m)!=g_PAWN) || (b1.l||b1.h)) ext--;
	}
  }
 }
 if(F && pvnode)
  {
  w = -search(nch, c1, d-1+ext, ply+1, -beta, -alpha, 1, 1)
  }
 else
  {
  w = -search(nch, c1, d-1+ext, ply+1, -alpha-1, -alpha, 0, 1);
  if((w>alpha) && (ext<0)) w = -search(nch, c1, d-1, ply+1, -alpha-1, -alpha, 0, 1);
  if((w>alpha) && (w<beta) && pvnode) w = -search(nch, c1, d-1+ext, ply+1, -beta, -alpha, 1, 1);
  }

 undoMove(m, c);

 if((!g_sabort) && (w > B))
 {
  if(w > alpha)
	{
	v = (hp.l & g_HMASKP);
	g_hashDP[v] = i64_or( i64_and(hp,g_HINVP) , i64v(m) );
	alpha = w;
	}
  if(w >= beta)
	{
	if(!CAP(m))
	{
	g_killer[ply] = m;
	v = (m & 0xFFF);
	g_history[v]++;
	}
	v = (hb.l & g_HMASKB);
	g_hashDB[v] = i64_or( i64_and(hb,g_HINVB) , i64v(w + g_37) );
	return beta;
	}
  if(pvnode &&  (w >= alpha))
	{
	g_p_v[ply][ply] = m;
	for(j=ply+1; j<g_pvlength[ply+1];j++) g_p_v[ply][j] = g_p_v[ply+1][j];
	g_pvlength[ply] = g_pvlength[ply+1];
	if(w == 31999 - ply) return w;
	}
  B = w;
 }

 F = 0;

 }
 }
 
if ((!g_sabort) && (pvnode || (A == alpha)))
	{
	v = (hb.l & g_HMASKB);
	b1 = i64_or( g_1, i64v(B + g_37) );
	b1 = i64_or( b1, i64_and(hb,g_HINVB) );
	g_hashDB[v] = b1;
	}
return alpha;
}

function execMove(m)
{
var c,i,d="1/2-1/2 ";

doMove(m, g_onmove);
g_onmove^=1;
c = g_onmove;

g_hstack[COUNT()] = HASHP(c);
for(i=0;i<127;i++) g_killer[i] = g_killer[i+1];
for(i=0;i<0x1000;i++) g_history[i] = 0;

i = generateMoves(attacked(g_kingpos[c], c), c, 0);

if(!(g_movenum[0]))
 {
 if(!i) { print(d+"Stalemate"); return 4; }
 else
  {
  print( (c==1 ? "1-0 White" : "0-1 Black") + " mates" );
  return(5+c);
  }
 }

c = ig_sdraw(HASHP(c), 2);
if(c)
 {
 if(c==1) d+="Draw by Repetition";
 else if(c==2) d+="Draw by Fifty Move Rule";
 else if(c==3) d+="Insufficient material";
 else c = 0;
 if(c) print(d);
}

return c;
}

function ismove(m,to,fr,piece,prom,h)
{
 var f=FROM(m),t=TO(m),u;
 if(t!=to) return 0;
 if((fr<0) && (PIECE(m)!=piece)) return 0;
 if((fr>=0) && (f!=fr)) return 0;
 u=CHR(h);
 if(ISFILE(u) && ((f&7) != (h-97) )) return 0;
 if(ISRANK(u) && (((f&56)>>3) != (h-49) )) return 0;
 if(prom && (PROM(m)!=prom)) return 0;
 return 1;
}

function parseMove(s,u,p)
{
var fr=-1,to=-1, piece = g_PAWN, prom=0, ip=[1], c,c1,c2,h=0,i,sp=0;

if(s.substr(0,5)=="O-O-O") s=(u?"Kc8":"Kc1");
else if (s.substr(0,3)=="O-O") s=(u?"Kg8":"Kg1");

c = s[sp];
if(c>="A" && c<="Z")
 {
 piece = _getpiece(c, ip); sp++
 }
if(piece<1) return -1;
if(s[sp]=="x") sp++;
c = s[sp];
if(ISRANK(c))
  {
  h = c.charCodeAt(0); sp++;
  }
if(s[sp]=="x") sp++;
c = s[sp];
if(!ISFILE(c)) return -1;
c1 = s[sp++];
if(s[sp]=="x") sp++;
c = s[sp];
if(ISFILE(c))
 {
 h = c1.charCodeAt(0); sp++;
 }

c2 = s[sp++];
if(!ISRANK(c2)) return -1;
if(s.length>sp)
 {
 c = s[sp];
 if(c=="=") prom = _getpiece( s[sp+1], ip);
 else if (c!="+")
	{
	fr = (c1.charCodeAt(0)-97)+((c2.charCodeAt(0)-49)<<3);
	c1 = s[sp++];
	c2 = s[sp++];
	sp = sp+1;
	if((!ISFILE(c1)) || (!ISRANK(c2))) return -1;
	if(s.length>sp) prom = _getpiece( s[sp], ip);
	}
 }

to = (c1.charCodeAt(0)-97)+((c2.charCodeAt(0)-49)<<3);
if(p)
 {
 if(ismove(p, to, fr, piece, prom, h)) return p;
 return 0;
 }

generateMoves(attacked(g_kingpos[u], u), u, 0);

for(i=0; i < g_movenum[0]; i++) 
{
 if(ismove(g_movelist[0][i], to, fr, piece, prom, h)) return (g_movelist[0][i]);
}

return 0;
}

function parseMoveNExec(s,c,m)
{
 m[0] = parseMove(s, c, 0);
 var u=m[0];
 if(u==-1) print("UNKNOWN COMMAND: "+s);
 else if(u==0) print("Illegal move: "+s);
 else return execMove(u);
 return -1;
}

function undo()
{
 g_onmove^=1;
 undoMove(null, g_onmove);
}

function calc()
{
var s="",t1=0, ch = attacked(g_kingpos[g_onmove], g_onmove);

g_eval1 = 0;
g_iter = 0;
g_kvalue[0] = 0;
g_sabort = 0;
g_nodes = 0;
g_nds = 0;
g_hc = 0;

g_movemade = "";

g_starttime = sectime();

for(g_iter = 1; (!g_sabort) && g_iter <= g_sd; g_iter++)
{
 g_kvalue[g_iter] = search(ch, g_onmove, g_iter, 0, -g_32, g_32, 1, 0);
 t1 = (sectime() - g_starttime);
 if(g_sabort)break;
 if ((g_post_) &&  (g_pvlength[0] > 0))
 {
 s = ''+g_iter+". "+g_kvalue[g_iter]+" "+(t1|0)+"s "+(g_nds+g_nodes)+" nodes ";
 print(s);
 displaypv();
 }
}

if (g_post_)
{
s = "kibitz W: "+g_kvalue[  (g_iter > g_sd ? g_sd : g_iter) ] +
" Nodes: " + g_nds + " QNodes: " + g_nodes + " HashNd: " + g_hc +
" Evals: " + g_eval1 + " Secs: " + (t1|0) + " knps: " + (((g_nds+g_nodes)/(t1+1))|0);
print(s);
}

g_movemade = mvstr(g_p_v[0][0]);
print( "move "+g_movemade );

return execMove(g_p_v[0][0]);
}

function gomove()
{
g_engine = g_onmove
g_ex = calc();
}

function entermove(buf)
{
var m = [1];
g_ex = parseMoveNExec(buf, g_onmove, m);
}

function init()
{
setTimeout('init2(0)',999);
woutput("Inits...(1/6)");
}

function init2(step)
{
var i,w=i64v(1);
if(step==0)
{
for(i=0;i<192;i++) { g_pcaps[0][i] = i64(); g_pcaps[1][i] = i64(); }
for(i=0;i<0x1000;i++) g_history[i] = 0;
for(i=0;i<4096;i++) g_hashxor[i] = _rand_64();
for(i=0;i<64;i++)
 {
 g_crevoke[i] = 0x3FF; g_nmoves[i] = i64(); g_kmoves[i] = i64();
 g_pmoves[0][i] = i64(); g_pmoves[1][i] = i64();
 g_BITi[i] = i64c(w); w = i64_lshift(w,1);
 }
g_crevoke[7]^=g_BITi[6].l;
g_crevoke[63]^=g_BITi[7].l ;
g_crevoke[0]^=g_BITi[8].l ;
g_crevoke[56]^=g_BITi[9].l ;

for(i=0;i<0x10000;i++) g_rays[i] = i64();
for(i=0;i<64;i++)
 {
 g_bmask45[i] = i64_or( _bishop45(i, g_0, 0) , g_BITi[i] );
 g_bmask135[i] = i64_or( _bishop135(i, g_0, 0) , g_BITi[i] );
 }
}

if(step==1) _init_g_rays1();
if(step==2) _init_g_rays2();
if(step==3) _init_g_rays3();
if(step==4) _init_g_rays4();
if(step==5)
{
 _init_shorts(g_nmoves, g_knight);
 _init_shorts(g_kmoves, g_king);
 _init_pawns(g_pmoves[0], g_pcaps[0], g_pawnfree[0], g_pawnfile[0], g_pawnhelp[0], 0);
 _init_pawns(g_pmoves[1], g_pcaps[1], g_pawnfree[1], g_pawnfile[1], g_pawnhelp[1], 1);
 _startpos();
 for(i=0;i<64;i++)
	{
	g_nmobil[i] = (i64_bitcount( g_nmoves[i] )-1)*6;
	g_kmobil[i] = i64_bitcount( g_nmoves[i] )*2;
	}
 start();	// should be
 //autogame();
}

step++;
if(step<6)
 {
 setTimeout('init2('+(step)+')',600);
 woutput("Preparing...("+(step+1)+"/6) 64-bit-rays");
 }
}

function newgame()
{
g_pgn = "";
_startpos();
}

// AI vs AI game in console
function autogame()
{
g_engine = g_onmove;
g_pgn = "";

printboard();

while(1)
{
 g_ex = calc();

 printboard();
  
 var M = COUNT();
 if(M%2>0) g_pgn+=''+((M>>1)+1)+".";
 g_pgn+=mvstr( g_p_v[0][0], 1)+" ";
 print(g_pgn);
 
 if(g_ex) break;
}
 woutput("Finished");
}


// starting
init();


//--------------------------------------
// SCRIPTS for int 64-bit cases
//--------------------------------------

cs64 = { MIN_VALUE:i64(), MAX_VALUE:i64_ax(g_FF,g_FF) };

	//powers of 10 in 64 bits	
pw10=[ i64_ax(0,0x1),
i64_ax(0,0xA),
i64_ax(0,0x64),
i64_ax(0,0x3E8),
i64_ax(0,0x2710),
i64_ax(0,0x186A0),
i64_ax(0,0xF4240),
i64_ax(0,0x989680),
i64_ax(0,0x5F5E100),
i64_ax(0,0x3B9ACA00),
i64_ax(0x2,0x540BE400),
i64_ax(0x17,0x4876E800),
i64_ax(0xE8,0xD4A51000),
i64_ax(0x918,0x4E72A000),
i64_ax(0x5AF3,0x107A4000),
i64_ax(0x38D7E,0xA4C68000),
i64_ax(0x2386F2,0x6FC10000),
i64_ax(0x1634578,0x5D8A0000),
i64_ax(0xDE0B6B3,0xA7640000),
i64_ax(0x8AC72304,0x89E80000) ];

/* Define */

function i64() { return { h:0, l:0 } }	// constructor
function i64_al(v) { return { h:0, l:v } }	// +assign 32-bit value
function i64_ax(h,l) { return { h:h, l:l } }	// +assign 64-bit v.as 2 regs
function i64s(s)				// +assign by string
 {
  var a=s.indexOf("0x"); if(a<0) return "input error";
  return { h: parseInt("0x"+s.substr(a+2,8)), l: parseInt("0x"+s.substr(a+10,8)) }
 }

// for convenience
function i64v(v) { return { h:0, l:v } }	// +assign 32-bit value
function i64c(a) { return { h:a.h, l:a.l } }	// clone

function i64_fromString(s)
 {
  var a=s.indexOf("0x"); if(a<0) return "input error";
  return { h: parseInt("0x"+s.substr(a+2,8)), l: parseInt("0x"+s.substr(a+10,8)) }	// +assign by string
 }

// originally was ((x>>>1) + (x&1) )% g_10
// modulus is slow and >>>0 is faster
function i64u(x) { return ( ((x >>> 0) & g_FF) >>> 0 ); }	// keeps [0..g_FFF] 

/* Type conversions */

function i64_toint(a)
 {	// Javascript supports value=2^53
  return (a.h>0 ? (a.l + (a.h * g_10)) : a.l);
 }

function i64_toString(a)
 {
  var s1=(a.l).toString(16);
  var s2=(a.h).toString(16);
  var s3="0000000000000000";
  s3=s3.substr(0,16-s1.length)+s1;
  s3=s3.substr(0,8-s2.length)+s2+s3.substr(8);
  return "0x"+s3.toUpperCase();
  }

/* Bitwise operators (the main functionality) */

function i64_and(a,b) { return { h:( a.h & b.h )>>>0, l:( a.l & b.l )>>>0 } }
function i64_or(a,b) { return { h:( a.h | b.h )>>>0, l:( a.l | b.l )>>>0 } }
function i64_xor(a,b) { return { h:( a.h ^ b.h )>>>0, l:( a.l ^ b.l )>>>0 } }
function i64_not(a) { return { h:(~a.h)>>>0, l:(~a.l)>>>0 } }

/* Simple Math-functions */

// just to add, not rounded for overflows
function i64_add(a,b)
 { var o = { h:a.h+b.h, l:a.l+b.l }; var r = o.l-g_FF;
   if( r>0 ) { o.h++; o.l =--r; } return o; }

// verify a>=b before usage
function i64_sub(a,b)
 { var o = { h:a.h-b.h, l:a.l-b.l };
   if( o.l<0 ) { o.h--; o.l+=g_10; } return o; }

// x n-times, better not to use for large n
function i64_txmul(a,n)
{ var o = { h:a.h, l:a.l }; for(var i=1; i<n; i++ ) o = i64_add(o,a); return o; }


// multiplication arithmetic
// (it gives small mistake without checksum for too large numbers)
function i64_mul(a,b)
 {

 /*
	// slow but working checksum to get right result
 if(i64_bithighestat(a)+i64_bithighestat(b)>63)
  {
  return i64_bitmul(a,b);
  }
 */

  var o = { h:(a.h * b.l) + (b.h * a.l), l:(a.l * b.l) };

  var o3 = (o.h & g_FF)>>>0;	// to see how
  var o4 = ((o.h+1) & g_FF)>>>0;	// it changes
  if( o3==o4 ) return i64_bitmul(a,b);	//fast checksum for 53-bit javascript overflow

  var oC=0;
  if( o.l>g_FF )
   {
    oC=Math.floor(o.l/g_10);
    o.h += oC;
    o.l =(o.l & g_FF)>>>0;
   }
  o.h =(o.h & g_FF)>>>0;

  if( (o3+oC)!=o.h ) return i64_bitmul(a,b);	// checksum

  return o;
 }

// multiplication by shifting bits, good for few-bits manipulation
// (slow)
function i64_bitmul(a,b)
 {
  var o = { h:0, l:0 };
  var m = { h:b.h, l:b.l };
  var t = { h:a.h, l:a.l };
  for(var j=63;j>0;j--)
   {
    if( !(m.l | m.h) ) break; 
    if(m.l & 1) o=i64_add(o,t);
    m = i64_rshift(m,1);
    t = i64_lshift(t,1);
   }
  o.l =(o.l & g_FF)>>>0;
  o.h =(o.h & g_FF)>>>0;
  return o;
 }
// bitwise division calculates and returns [rs,rm],
// where a=(b*rs)+rm, better not to use for simple math
function i64_bitdiv(a,b)
 {
  var rs = { h:0,l:0 };
  var rm = { h:a.h,l:a.l };
  if( (b.l | b.h) && i64_gt(a,b) )
  {
  var d = { h:b.h, l:b.l };
  var p = { h:d.h, l:d.l };
  var y = 0;
  var w = i64_bithighestat(d);
  while((w<64) && (d.l | d.h) && i64_le(d,rm) )
   {
    p = { h:d.h, l:d.l };
    var hbit=i64_bithighestat(d);
    d = i64_lshift(d,1);
    y++; w++;
   }

  while((y>0) || (p.l | p.h))
   {
    rs=i64_lshift(rs,1); y--;
    if( i64_le(p,rm) ) { rm=i64_sub(rm,p); rs.l=(rs.l|1)>>>0; }
    if( (y<=0) && i64_gt(b,rm) ) break;
    p = i64_rshift(p,1);
   }
  }
  return [rs,rm];
 }

// x n-times multiplied by self, slow
function i64_txpow(a,n)
{ var o = { h:a.h, l:a.l }; for(var i=1; i<n; i++ ) o = i64_mul(o,a); return o; }


/* Bit-shifting */

function i64_lshift(a,n)
 {
  switch(n)
  {
  case 0: return { h:a.h, l:a.l };
  case 32: return { h:a.l, l:0 };
  default:
   if(n<32) return { h:((a.h << n)>>>0)+(a.l >>> (32-n)), l:(a.l << n)>>>0 };
   else return { h:(a.l << (n-32))>>>0, l:0 };
  }
 }

function i64_rshift(a,n)
 {
  switch(n)
  {
  case 0: return { h:a.h, l:a.l };
  case 32: return { h:0, l:a.h };
  default:
   if(n<32) return {  h:(a.h >>> n), l:(a.l >>> n)+(a.h << (32-n))>>>0 };
   else return { h:0, l:(a.h >>> (n-32)) };
  }
 }

// gets bit at position n
function i64_bitat(a,n) { return ( (n<32) ? (a.l & (1<<n)) : (a.h & (1<<(n-32))) ); } 

// sets bit at position n (on)
function i64_bitset(a,n)
 { if(n<32) a.l = (a.l | (1<<n))>>>0; else a.h = (a.h | (1<<(n-32)))>>>0; } 

// clears bit at position n (off)
function i64_bitclear(a,n)
 { if(n<32) a.l = (a.l & (~(1<<n)))>>>0; else a.h = (a.h & (~(1<<(n-32))))>>>0; } 

// toggles bit at position n (on/off)
function i64_bittoggle(a,n)
 { if(n<32) a.l = (a.l ^ (1<<n))>>>0; else a.h = (a.h ^ (1<<(n-32)))>>>0; } 

/* bitwise calcs just fast as possible */

bitcnt_ = [];
bitlsb_ = [];
bithsb_ = [];
function i64_prep_bclwbhb()
{
var i,c,o,m,k;
for(i=0;i<g_1;i++)
 {
 c=0; o=i;
 for(var k=0;k<32;k++) { c+=((o&1)>>>0); o>>>=1; };
 bitcnt_[i] = c;
 }

for(i=0;i<g_1;i++)
 {
 c=-1; o=i;
 for(var k=0;c<0 && k<32;k++) { if(o&1) c=k; o>>>=1; };
 bitlsb_[i] = c;
 }

for(i=0;i<g_1;i++)
{
 c=-1; o=i; m=(1<<31);
 for(k=31;c<0 && k>=0;k--) { if(o&m) c=k; m>>>=1; };
 bithsb_[i] = c;
 }
}
i64_prep_bclwbhb();

// calculates count of bits =[0..63]
function i64_bitcount(a)
{

  return ( bitcnt_[ (a.l & g_F)>>>0 ] + bitcnt_[ ((a.l>>>16) & g_F)>>>0 ] +
     bitcnt_[ (a.h & g_F)>>>0 ] + bitcnt_[ ((a.h>>>16) & g_F)>>>0 ] );
}


// finds lowest bit (position [0..63] or -1)
function i64_bitlowestat(a)
{

  var q = (a.l & g_F)>>>0;
  if(q) return bitlsb_[q];
  q = ((a.l>>>16) & g_F)>>>0;
  if(q) return (bitlsb_[q] | 16);
  q = (a.h & g_F)>>>0;
  if(q) return (bitlsb_[q] | 32);
  q = ((a.h>>>16) & g_F)>>>0;
  if(q) return (bitlsb_[q] | 48);
  return -1;
}

// finds highest bit (position [0..63] or -1)
function i64_bithighestat(a)
{

  var q = ((a.h>>>16) & g_F)>>>0;
  if(q) return (bithsb_[q] | 48);
  q = (a.h & g_F)>>>0;
  if(q) return (bithsb_[q] | 32);
  q = ((a.l>>>16) & g_F)>>>0;
  if(q) return (bithsb_[q] | 16);
  var q = (a.l & g_F)>>>0;
  if(q) return bithsb_[q];
  return -1;
}

// returns object for a bit[0..63]
function i64bitObj(n)
{
 if(typeof(bitObjat_)=="undefined")
   {
   bitObjat_ = [];
   var o=i64_al(1);
   for(var i=0;i<64;i++) { bitObjat_[i]={h:o.h,l:o.l}; o=i64_lshift(o,1); }
   }
 return bitObjat_[n];
}


/* Comparisons */

function i64_eq(a,b) { return ((a.h == b.h) && (a.l == b.l)); }
function i64_ne(a,b) { return ((a.h != b.h) || (a.l != b.l)); }
function i64_gt(a,b) { return ((a.h > b.h) || ((a.h == b.h) && (a.l >  b.l))); }
function i64_ge(a,b) { return ((a.h > b.h) || ((a.h == b.h) && (a.l >= b.l))); }
function i64_lt(a,b) { return ((a.h < b.h) || ((a.h == b.h) && (a.l <  b.l))); }
function i64_le(a,b) { return ((a.h < b.h) || ((a.h == b.h) && (a.l <= b.l))); }
function i64_is0(a) { return (!(a.l | a.h)); }	// is zero?
function i64_nz(a) { return (a.l|a.h); }	// is not zero?

/* Decimal conversions */

// converts to decimal string
function i64_todecString(a)
{
 var s=""; var rz=[ {h:0,l:0},{h:a.h,l:a.l} ];
 for(var n=19;n>=0;n--)
  {
   rz=i64_bitdiv(rz[1],pw10[n]); var d=rz[0].l;
   if((!n && (!s.length)) || (d>0) || (s.length>0)) s+=d.toString();
  }
 return s;
}

// converts decimal string to 64-bit object
function i64_fromdecString(s)
{
 var o={h:0,l:0};
 var c=s.length;
 for(var n=0;(n<=19) && ((c--)>0);n++)
  {
   var d=s.charCodeAt(c)-48;
   if((d>0) && (d<=9))
    {
     var o2=i64_bitmul(pw10[n],i64_al(d)); o=i64_add(o,o2);
    }
  }
 return o;
}

/* Other basic functions */


function i64_max(a,b) { return (i64_ge(a,b) ? a : b ); }	// max
function i64_min(a,b) { return (i64_le(a,b) ? a : b ); }	// min
function i64_neg(a)   { return i64_add(i64_not(a),i64_al(1)); }	// negative value as unsigned
function i64_mod(a,b) { var rz=i64_bitdiv(a,b); return rz[1]; }	// modulus
function i64_isodd(a,b) { return ((a.l&1)==0); }	// is odd
function i64_next(a) { return i64_add(a,{h:0,l:1}); }	// next number
function i64_pre(a) { return i64_sub(a,{h:0,l:1}); }	// previous number
function i64_is0(a) { return ((a.l | a.h)==0); }	// is zero?
function i64_not0(a) { return ((a.l | a.h)!=0); }	// not zero?

//--------------------------------------
// END OF SCRIPTS for int 64-bit cases
//--------------------------------------

