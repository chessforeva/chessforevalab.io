// just include this in source

// definition
ChristmasTree_ = {
 s:[],
 g:0,	// generated flag
 canv: "ChristmasTreeCanvas",	// id of canvas
 left: 600,     // where to
 top:  80,     //   put Christmas tree
 vert: 360,	  // vertical px
 horz: 240,   // horizontal px
 p: 80,	      // 0-100% of amount to generate
 greens: ["#009900", "#008800", "#007700", "#006600", "#005500", "#004400", "#003300", "#002200" ],
 browns: ["#440000", "#660033"],
 balls:  ["#ccff33", "#cc0033", "#9900ff", "#0000ff", "#33ffff", "#ccccff", "#00ff00" ],
 lights: ["#ffffff", "#ffff33" ],
 bl: [],
 PI2: Math.PI*2,
 e: 0,	// just step of trees
 cs:null,
 cx:null,
 u:0,
 
 starter: function()
  {
   var dt = (new Date);
   var m = dt.getMonth(), d = dt.getDate();
   
   	// Dec 15-31
   if((m==11) && (d>14))
    {
	 setInterval('ChristmasTree_.ticker()',600);
	}
  },
  
 ticker: function()
  {
   if(this.g>4) { /*Do nothing*/ }
   else if(this.g==4)		// draw lights
   {
    this.u++;
    if(this.u>=3)
     {
      this.u=0;
      this.drawLights();
     }
   }
   else
   {
   if(this.g==0)		// generate Christmas tree
     {
	var body = document.body, html = document.documentElement;
	var height = Math.max( body.scrollHeight, body.offsetHeight, body.clientHeight,
                       html.clientHeight, html.scrollHeight, html.offsetHeight ); 
	var width = Math.max( body.scrollWidth, body.offsetWidth, body.clientWidth,
                       html.clientWidth, html.scrollWidth, html.offsetWidth );
	if(width<800 || height<400)	// screen too small, can this.g=5
		{
		this.left = 120;
		setTimeout('ChristmasTree_.HideTree()',20000);
		}
	else
		{
		this.left = (width * 3)/5;
		}

	  this.genTree();
	  this.g++;
	  this.lights=this.lights.concat(this.balls).concat(this.greens);
	 }
   else if(this.g==1)		// create object
     {
	 this.createCanvas();
	 this.g++;
	 }
   else if(this.g==2)		// wait while canvas is ready
     {
	 var o = document.getElementById(this.canv);
	 if(o!=null) this.g++;
	 }
   else if(this.g==3)		// draw Christmas tree
     {
	 this.drawTreeInCanvas();
	 this.g++;
	 }
   }
  },
 
 HideTree: function()
 {
	var o = document.getElementById(this.canv);
	if(o!=null)
	{
	o.style.visibility = "hidden";
	}
 },

 createCanvas: function()
  {
	var o = document.getElementById(this.canv);
	if(o==null)
	{	
	o = document.createElement("CANVAS",this.canv);
	o.id = this.canv;
	o.style.position="absolute";
	o.style.top = this.top + "px";
	o.style.left = this.left + "px";
	o.style.width = this.horz + "px";
	o.style.height = this.vert + "px";
	o.width = this.horz;
	o.height = this.vert;
	o.style.zIndex = 2000;
	document.body.appendChild(o);
	}
  },
  
 genTree: function()
  {
  var i,p,r,n,e,a,b,l,c,j,y,dy, cL, dL, k,L,x1,y1,dx2,dy2,f;
  n = 20*(this.p/100);
  e = ((this.vert-20)*2.2)/n;
  this.e=e;
  a = [];
  e*=0.8;
  y=10;
  dy=e;
  for(i=0;i<10*(this.p/100);i++)
    {
	p = { x: (this.horz*0.9)/2, y: y, dx:0, dy:dy, w: (Math.max(i,1)+2), t:(i==0?1:0), l:Math.max((i+5),2) };
	a.push(p);
	y+=dy;
	}
  this.s.push(a);
  for(l=1;a.length>0 && l<4;)
    {
	b = [];
	for(i=0;i<a.length;i++)
     {
	  if(l>1 || i<(a.length-1))
	  {
	  p = a[i];
	  if(p.l>0)
	  {
	  x1 = p.x+p.dx;
	  y1 = p.y+p.dy;
	  
	  c=Math.max(3,15*(this.p/100));
	  for(j=0;j<c;j++)
	    {
		e = p.l * this.e / 16;
		k = 0.5 - (j<2? j : this.r());
		cL = (p.dx==0 ? 0 : Math.atan(p.dy/p.dx));
		dL = Math.max( 130 *  ((this.PI2/2)/180) * k );
		L = cL + dL;
		dx2 = e * Math.sin(L)* this.horz/200; dy2 = e * Math.cos(L);
		f = Math.max(0,parseInt(p.l-1));
		r = { x:x1 , y:y1, dx:dx2, dy:dy2, w: Math.max(1,p.w/2), t:(l>0), l:f };
		b.push(r);
		}
	  }
	  }
	 }
	 this.s.push(b);
	 a = b;	 
	 l++;
	}
  },
  
 drawTreeInCanvas: function()
  {
	var cs = document.getElementById(this.canv);
	var cx = cs.getContext('2d');
	var l,i,a,p,px,py,e=this.e/10;
	this.cs = cs;
	this.cx = cx;

	for(l in this.s)
	{
	a = this.s[l];
	for(i in a)
	{
	p = a[i];
	cx.beginPath();
	cx.strokeStyle = ( p.t==0 ? this.browns[0] : this.greens[ parseInt(this.greens.length*this.r()) ]);
	cx.fillStyle = cx.strokeStyle;
	cx.moveTo( p.x, p.y );
	cx.lineWidth = p.w;
	cx.lineTo( p.x+p.dx, p.y+p.dy );
	cx.stroke();
	
	if(this.r()*100<1)
	 {
	 cx.beginPath();
	 cx.strokeStyle = 'white';
	 cx.fillStyle = this.balls[ parseInt(this.balls.length*this.r()) ];
	 px = p.x+this.rndp();
     py = p.y+this.rndp();
     cx.moveTo( px+e, py );
	 cx.arc( px, py, e, 0, this.PI2 );
	 cx.fill();
	 cx.stroke();
	 if(this.r()*5<1) this.bl.push({x:px+this.rndp(),y:py+this.rndp()});
	 }
	cx.lineWidth = 1;
	}
	}
  },
 
 r: function() { return Math.random(); },
 
 rndp: function()
  {
   return 4-(8*this.r());
  },
  
 drawLights: function()
  {
    var cs = this.cs, cx = this.cx, o,i;
    for(i in this.bl)
    {
    o = this.bl[i];
    cx.beginPath();
    cx.strokeStyle = this.lights[ parseInt(this.lights.length*this.r()) ];
    cx.fillStyle = cx.strokeStyle;
    cx.arc( o.x, o.y, 1, 0, this.PI2 );
    cx.stroke();
    }
  },
  
 removeCanvas: function()
  {
	var o = document.getElementById(this.canv);
	if(o!=null) document.body.removeChild(o);
  },
 
  };

setTimeout('ChristmasTree_.starter()',2000);		// start after 2 secs.
