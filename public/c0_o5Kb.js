// universal only 5Kb code of main most played chess openings
// parameter moves:e2e4
// returns next variants, strength, ECO code
// chessforeva.blogspot.com

var c0_opn=[0];
c0_opn[1]="A00.a2a31.b1c31.A01.b2b31.A00.b2b41.c2c31.A10.c2c41A30.c7c51A34.b1c35-1A30.g1f34-2A11.c7c61-1A20.e7e52A21.b1c37A25.b8c64-1A22.g8f65g1f39b8c69-4A20.g2g32-2A13.e7e61b1c34-1g1f35-2A10.g7g61b1c39f8g79-3A15.g8f63A16.b1c36A17.e7e63-1A16.g7g66-2A15.g1f31-1g2g31.A00.d2d31.A40.d2d43A43.c7c51d4d59-2A84.d7d52D01.c2c47D10.c7c64b1c33g8f69e2e35-1g1f34-3c4d51c6d59b1c39-3D11.g1f35g8f69D15.b1c37d5c44D16.a2a49D17.c8f59-3D15.e7e65-2D11.e2e32-4D20.d5c41D21.g1f39D23.g8f69D25.e2e39-4D30.e7e64D31.b1c37c7c62-1f8e71-1D35.g8f65D50.c1g56f8e79-2D35.c4d53e6d59D36.c1g59-5D30.g1f32g8f69-4D02.g1f32c7c61-1g8f68c2c49c7c65-1e7e64-5A41.d7d61A42.c2c43-1A41.e2e43-1g1f33-2A40.e7e61c2c47A84.f7f54-1A40.g8f65-2g1f32-2A80.f7f51A81.g2g39g8f69f1g29g7g69-5A40.g7g61c2c46f8g79b1c39-3e2e43f8g79-3A45.g8f65b1c31-1c1g51f6e49-2A50.c2c46A56.c7c51d4d59A57.b7b56c4b59a7a69b5a69-4A60.e7e63b1c39A61.e6d59c4d59d7d69-7A53.d7d61b1c39-2A51.e7e51-1E00.e7e64E20.b1c34d7d51-1f8b48E32.d1c24E38.c7c53-1E33.e8g86a2a39b4c39c2c39-5E40.e2e35E41.c7c54-1E46.e8g85E47.f1d39-5E10.g1f34E12.b7b65a2a32-1E15.g2g37c8a66-1c8b73E16.f1g29-4E10.d7d52b1c39-2E11.f8b41c1d29-3E00.g2g31E01.d7d59-3D70.g7g63b1c38d7d52D85.c4d55f6d59e2e49d5c39D86.b2c39f8g79-6D90.g1f34f8g79-3E61.f8g77E70.e2e49d7d69E73.f1e22e8g89g1f39-3E80.f2f32E81.e8g89c1e39-3E90.g1f34e8g89E91.f1e29E92.e7e59E94.e1g19E97.b8c69d4d59c6e79-12D70.g1f31E60.f8g79g2g39-3D70.g2g31E60.f8g79f1g29-5A46.g1f32c7c51d4d59-2d7d51c2c49-2e7e63c1g51-1c2c44b7b69-2e2e31-1g2g31-2A48.g7g64c1g51f8g79b1d29-3c2c45f8g79b1c39e8g89e2e49d7d69f1e29-7A49.g2g32f8g79f1g29e8g89e1g19.A00.e2e31.A40.e2e45B00.b8c61-1B20.c7c54B23.b1c31b8c67f2f43g7g69g1f39f8g79-4B24.g2g36B25.g7g69f1g29f8g79B26.d2d39d7d69-7B23.d7d61-1e7e61-2B22.c2c31d7d55e4d59d8d59d2d49g8f69g1f39-6g8f64e4e59f6d59d2d49c5d49g1f39-7B20.d2d41c5d49c2c39-3B27.g1f38B28.a7a61-1B30.b8c63b1c31-1B32.d2d47c5d49f3d49d8c71-1e7e51d4b59d7d69-3B34.g7g61B35.b1c34f8g79c1e39g8f69-4B36.c2c45-2B32.g8f66B33.b1c39d7d64c1g59e7e69d1d29-4e7e55d4b59d7d69c1g59a7a69b5a39b7b59c3d55f8e79g5f69e7f69c2c39-5g5f64";
c0_opn[2]="g7f69c3d59f6f59-16B30.f1b52e7e63-1B31.g7g66e1g19f8g79-5B50.d7d64c2c31g8f69-2B53.d2d48c5d49d1d41-1B54.f3d49B55.g8f69B56.b1c39B90.a7a66c1e32e7e59d4b39-3B94.c1g52B95.e7e69B96.f2f49-3B90.f1c41e7e69c4b39-3B92.f1e22e7e59d4b39f8e79-5B56.b8c61B60.c1g59B62.e7e69B63.d1d29-4B70.g7g62B72.c1e39f8g79B75.f2f39B76.e8g89d1d29B77.b8c69-11B53.g8f61b1c39c5d49f3d49-5B51.f1b51B52.c8d79b5d79d8d79-5B40.e7e62b1c31-1c2c31-1d2d31b8c69-2d2d47c5d49B41.f3d49a7a64B43.b1c34d8c79-2B42.f1d35-2B44.b8c62B45.b1c39B47.d8c79-3B41.g8f62b1c39B45.b8c63-1B41.d7d66-7B27.g7g61d2d49-4B10.c7c61d2d49B12.d7d59B15.b1c32d5e49c3e49B17.b8d73-1B18.c8f56e4g39B19.f5g69h2h49h7h69-8B12.b1d21d5e49d2e49c8f59e4g39f5g69-6B13.e4d52c6d59B14.c2c49g8f69b1c39e7e69g1f39-7B12.e4e52c8f59-5B01.d7d51e4d59d8d55b1c39d5a59d2d49g8f69-5g8f64d2d49-4B07.d7d61d2d49g7g61-1g8f68b1c39e7e51-1g7g68B09.f2f45f8g79g1f39-3B08.g1f34f8g79-7C20.e7e52C23.b1c31C25.g8f69-2C23.f1c41g8f69-2C30.f2f41-1C25.g1f38C44.b8c68C46.b1c31g8f69-2C44.d2d41e5d49C45.f3d49f8c55-1g8f64-4C60.f1b56C68.a7a68C70.b5a48C77.g8f69C78.e1g19b7b51a4b39-2C80.f6e41d2d49b7b59a4b39d7d59-5C84.f8e77C87.f1e19C88.b7b59a4b39d7d66C90.c2c39e8g89C92.h2h39c6a59C96.b3c29c7c59d2d49-8C88.e8g83C89.c2c39-9C68.b5c61d7c69e1g19-4C65.g8f61e1g19C67.f6e49d2d49-5C50.f1c41f8c55C53.c2c39g8f69-3C55.g8f64d2d39-4C41.d7d61d2d49-2C42.g8f61b1c32-1f3e57d7d69e5f39f6e49d2d49d6d59f1d39-10C00.e7e61d2d31d7d59b1d29-3d2d49d7d59C01.b1c34C10.d5e41c3e49-2C15.f8b45C16.e4e59C17.c7c59C18.a2a39C19.b4c39b2c39g8e79-7C10.g8f63C11.c1g56-1e4e53f6d79f2f49c7c59g1f39-7C03.b1d23C07.c7c54e4d59-2C05.g8f65C06.e4e59f6d79f1d39c7c59c2c39b8c69g1e29c5d49c3d49-11C01.e4d51e6d59-2C02.e4e51c7c59c2c39b8c69g1f39-8A40.g7g61B06.d2d49f8g79b1c37c7c63-1d7d66-2g1f32-4B02.g8f61e4e59f6d59B03.d2d49d7d69c2c43d5b69-2B04.g1f36.A02.f2f41.A04.g1f31c7c51c2c47b8c69-2g2g32-2A06.d7d52A09.c2c42-1A06.d2d43g8f69c2c49-3A07.g2g33g8f69f1g29-4A04.d7d61-1f7f51-1g7g61-1A05.g8f64c2c45c7c51b1c39-2e7e63b1c34-1g2g35-2g7g64b1c36f8g79e2e49d7d69d2d49e8g89-6g2g33f8g79f1g29-5d2d41-1g2g32d7d53f1g29";
c0_opn[3]="-2g7g66f1g29f8g79e1g19e8g89.A00.g1h31.g2g31d7d59.g2g41";


function c0_Opening(c0_fmoves)
{
var c0_retdata="";

var c0_mvs="";
var c0_s="";
var c0_c="";

var c0_ECO="";
var c0_kf="";

var c0_i=0;
var c0_j=0;

var c0_pt=0;
var c0_nm=0;


var c0_NMoves="";
var c0_OName="";
var c0_op="";

for(c0_i=1; c0_i<c0_opn.length; c0_i++)
	{
	c0_s=c0_opn[ c0_i ];
	for(c0_j=0; c0_j<c0_s.length;)
		{
		c0_c=c0_s.substr(c0_j, 1 );		// Looking for special symbols or type of information...
		if(c0_c=="-")				// Other variant...
			{
			c0_j++;
			for(c0_nm=0; c0_j+c0_nm<c0_s.length &&
				("0123456789").indexOf(c0_s.substr(c0_j+c0_nm,1))>=0; c0_nm++);

							// Next value is length for moves to shorten...
			c0_mvs=c0_mvs.substr(0, c0_mvs.length- (4*parseInt( c0_s.substr(c0_j,c0_nm) )) );
			c0_j+=c0_nm;
			}
		else if(c0_c==".")			// Will be other opening or variant...
			{
			c0_j++;
			c0_mvs="";
			}
		else if(("abcdefgh").indexOf(c0_c)>=0)	// If it is a chess move...
			{
			c0_mvs+=c0_s.substr(c0_j,4);
			c0_j+=4;
			}
		else if(("0123456789").indexOf(c0_c)>=0)	// If it is a coefficient (for best move searches)...
			{
			c0_kf=c0_c;
			if((c0_mvs.length>c0_fmoves.length) && (c0_mvs.substr(0,c0_fmoves.length)==c0_fmoves))
				{
				c0_next= c0_mvs.substr(c0_fmoves.length,4)

				if(c0_NMoves.indexOf(c0_next)<0) c0_NMoves+=c0_next+" ("+c0_kf+") ";
				}
			c0_j++;
			}
		else					// Opening information... ECO code and name (Main name for x00)
			{
			c0_ECO=c0_s.substr(c0_j,3)
			c0_j+=3;
			for(c0_pt=0; c0_s.substr(c0_j+c0_pt,1)!="."; c0_pt++);

			if((c0_mvs.length<=c0_fmoves.length) && (c0_fmoves.substr(0,c0_mvs.length)==c0_mvs))
				{
				if(c0_mvs.length>c0_op.length && c0_op.length<c0_fmoves.length)
					{
					c0_op=c0_mvs;
					c0_OName="ECO "+c0_ECO;
					}
				}

			c0_j+=(c0_pt+1);
			}
		}
	}
					// Sorting by coeff. descending
for(c0_i=1;c0_i<10;c0_i++)
	{
	for(c0_j=6;c0_j<c0_NMoves.length-9;)
		{
		c0_j+=9;
		if( c0_NMoves.substr(c0_j,1)==c0_i.toString() && c0_NMoves.substr(c0_j,1)>=c0_NMoves.substr(6,1) )
			{
			c0_NMoves=c0_NMoves.substr(c0_j-6,9)+c0_NMoves.substr(0,c0_j-6)+c0_NMoves.substr(c0_j-6+9);
			}
		}
	}

if( c0_NMoves.length>0 ) c0_retdata=c0_NMoves + c0_OName;

return c0_retdata;
}
