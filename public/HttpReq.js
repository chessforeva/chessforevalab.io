// HTTP request part...FOR GET METHOD...

var xmlhttp;
var HttpResult="";

function HttpReq(XML_url, XML_parms)
{
xmlhttp=null;
HttpResult="";

if (window.XMLHttpRequest)
  { // code for IE7, Firefox, Opera, etc.
  xmlhttp=new XMLHttpRequest();
  }
else if (window.ActiveXObject)
  { // code for IE6, IE5
  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
if (xmlhttp!=null)
  {
  xmlhttp.onreadystatechange=state_Change;
  xmlhttp.open("GET",XML_url+( XML_parms.length==0 ? "" : "?"+XML_parms ),true);
  xmlhttp.send(null);
  }
else
  {
  alert("Your browser does not support XMLHTTP, this is required!");
  }
}

function state_Change()
{
if (xmlhttp.readyState==4)
  { // 4 = "loaded"
  if (xmlhttp.status==200)
    { // 200 = "OK"
	    //xmlhttp.status - status
	    //xmlhttp.statusText - text OK
	    //xmlhttp.responseText - resulting XML

    HttpResult=xmlhttp.responseText;
    }
  else
    {
//    alert("Problem retrieving XML from server via XMLHTTP");
    }
  }
}

function xmlGetTag(xmltext, xmltag, xml_occur)
{
var xmlRes="";
var xmlat=0;
var xmltext2=xmltext;

for(var xml_2=1; xml_2<xml_occur; xml_2++)
{
xmlat=xmltext2.indexOf("</" + xmltag + ">");
if(xmlat<0) { xmlRes="<none>"; break; }
xmltext2 = xmltext2.substr(xmlat+xmltag.length+3);
}

if(xmlRes.length==0)
	{
	xmlat=xmltext2.indexOf("<"+xmltag+">");
	if(xmlat<0) xmlRes="<none>";
	else 
	  {
	  xmltext2 = xmltext2.substr(xmlat+xmltag.length+2);
	  xmlat=xmltext2.indexOf("</" + xmltag + ">");
	  if(xmlat<0) xmlRes="<none>";
	  else
	    {
	      xmlRes = xmltext2.substr(0,xmlat);
	    }
	  }
	}
return xmlRes;
}

// HTTP request part... FOR POST METHOD...


function createXHR() 
{
    var request = false;
        try {
            request = new ActiveXObject('Msxml2.XMLHTTP');
        }
        catch (err2) {
            try {
                request = new ActiveXObject('Microsoft.XMLHTTP');
            }
            catch (err3) {
		try {
			request = new XMLHttpRequest();
		}
		catch (err1) 
		{
			request = false;
		}
            }
        }
    return request;
}

//
// Syntax: HttpReq2("myurl.php", "content=" + content);
//
function HttpReq2(url, content)		// url is the script and data is a string of parameters
	{ 
		HttpResult="";
		var xhr = createXHR();
 
		xhr.onreadystatechange=function()
		{ 
			if(xhr.readyState == 4)
			{
				// nothing for now
				// alert("sent " + url + " " + content);
			HttpResult=xhr.responseText;
			}
		}; 
		xhr.open("POST", url, true);		
		xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
		xhr.send(content); 
	} 

	