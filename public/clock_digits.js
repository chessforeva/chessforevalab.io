﻿/*
 Addition to the

 ClockOpts.htm
 Clock.htm
 
*/

_ClockDigits = {
	
	
	cs:{},	// canvas	
	cx:{},	// context
	canvas: function(canvas_id) { this.cs = canvas_id; this.cx = this.cs.getContext('2d'); },

	GE: function (id) { return document.getElementById(id) },
	imgs: [ "a0","a1","a2","a3","a4","a5","b0","b1","b2","b3","b4","b5","bg" ],
	I: [],
	
	sgmC:5,
	sgmL:30,
	
	dotOn:[1,1],
	flashDots:0,
	
	rotated:0,
	rotat_180:0,
	
	scaleX:1,
	scaleY:1,
	
	loadImages: function() {
		if( !this.I.length )
		 for(var i=0;i<this.imgs.length;i++) {
			this.I[i] = new Image();
			var s = "img/clock_" + this.imgs[i] + ".png";
			this.I[i].src = s;
			document.write('<img src='+s+' id="dgt_imgs_'+i+'" width="0" height="0">');
		}
	},
	
	// Draw scaled image
	dwImg: function(i,x0,y0,w,h) {
		if(!(typeof(this.I[i])=="undefined")) this.cx.drawImage(this.I[i], x0, y0, w, h );
	},
	
	dwBg: function(x,y,w,h) {
		var r = this.rotated;
		this.dwImg( 12, (r?y:x)*this.scaleX,(r?x:y)*this.scaleY,(r?h:w)*this.scaleX,(r?w:h)*this.scaleY );
	},
	
	dwDot: function( on, x,y,w,h) {
		var r = this.rotated;
		this.dwImg( (r ? (on ? 6 : 0) : (on ? 9 : 3) ), (r?y:x)*this.scaleX,(r?x:y)*this.scaleY,(r?h:w)*this.scaleX,(r?w:h)*this.scaleY );
		y+=h;
		this.dwImg( (r ? (on ? 8 : 2) : (on ? 7 : 1) ), (r?y:x)*this.scaleX,(r?x:y)*this.scaleY,(r?h:w)*this.scaleX,(r?w:h)*this.scaleY );
	},
	
	// Draw a segment vertically or horizontally
	dwSgm: function( col, x0,y0, size, vt ) {
		
		var r = this.rotated;
		var j,h = this.sgmL*size*0.05, w = h;
		var x = x0, y = y0;
		var g;
		
		if(vt) {
				g = (r ? (col ? 6 : 0) : (col ? 9 : 3));
				this.dwImg( g, (r?y:x)*this.scaleX,(r?x:y)*this.scaleY,(r?h:w)*this.scaleX,(r?w:h)*this.scaleY ); y+=h;
				for(j=0;j<this.sgmC-2;j++) {
					g = ( r ? (col ? 11: 5) : (col ? 10 : 4));
					this.dwImg( g, (r?y:x)*this.scaleX,(r?x:y)*this.scaleY,(r?h:w)*this.scaleX,(r?w:h)*this.scaleY ); y+=h;
					}
				g = (r ? (col ? 8 : 2) : (col ? 7 : 1));	
				this.dwImg( g, (r?y:x)*this.scaleX,(r?x:y)*this.scaleY,(r?h:w)*this.scaleX,(r?w:h)*this.scaleY ); y+=h;
				}
		else {
				g = (r ? (col ? 9 : 3) : (col ? 6 : 0));
				this.dwImg( g, (r?y:x)*this.scaleX,(r?x:y)*this.scaleY,(r?h:w)*this.scaleX,(r?w:h)*this.scaleY ); x+=w;
				for(j=0;j<this.sgmC-2;j++) {
					g = ( r ? (col ? 10 : 4) : (col ? 11: 5));
					this.dwImg( g, (r?y:x)*this.scaleX,(r?x:y)*this.scaleY,(r?h:w)*this.scaleX,(r?w:h)*this.scaleY ); x+=w;
					}
				g = (r ? (col ? 7 : 1) : (col ? 8 : 2));
				this.dwImg( g, (r?y:x)*this.scaleX,(r?x:y)*this.scaleY,(r?h:w)*this.scaleX,(r?w:h)*this.scaleY ); x+=w;
			}
	},
	
	
	draw_dots: function( ticker, x,y,size ) {
		var r = this.rotated;
		var l = this.sgmL*size;
		var L = 8*l;
		var xj = x-(L/18), yj = y-(L/18);
		this.dwDot( (this.dotOn[ticker]), (r?yj:xj),(r?xj:yj),l,l);
		if(r) xj+=L/3; else yj+=L/3;
		this.dwDot( (this.dotOn[ticker]), (r?yj:xj),(r?xj:yj),l,l);
	},
	
	
	charmap: function(k) {
	
		if(this.rotat_180) k = 6-k;
		if(k==0) return ( this.rotated ? ("0235689"): ("02356789") );
		if(k==1) return ( this.rotated ? ("0268") : ("045689") );
		if(k==2) return ( this.rotated ? ("013456789") : ("01234789") );
		if(k==3) return ( this.rotated ? ("2345689") : ("2345689") );
		if(k==4) return ( this.rotated ? ("045689") : ("0268") );
		if(k==5) return ( this.rotated ? ("01234789") : ("013456789") );
		if(k==6) return ( this.rotated ? ("02356789") : ("0235689") );	
	},
	
	draw_digit: function( N, x,y,size ) {
		
		if(this.flashDots) return;	// flashing dots on pause
		
		var l = this.sgmL*size;
		var L = 8*l;
		var n = N.toString();
		
		
		var xj, yj = y-(L/18), Lj = l*2.08;
		for( var t=0;t<(this.sgmC*2);t++, yj+=Lj*0.96) {
			xj = x-(L/18);
			for( var i=0;i<this.sgmC;i++, xj+=Lj) this.dwBg(xj,yj,Lj,Lj);
		}
		
		if(N==255) return;
		
		this.dwSgm( (this.charmap(0).indexOf(n)>=0 ? 1 : 0), x+(L/8.5),y,l,0 );
		y+=L/6;
		this.dwSgm( (this.charmap(1).indexOf(n)>=0 ? 1 : 0), x,y,l,1 );
		this.dwSgm( (this.charmap(2).indexOf(n)>=0 ? 1 : 0), x+L,y,l,1 );
		y+=L; y-=L/14;
		this.dwSgm( (this.charmap(3).indexOf(n)>=0 ? 1 : 0), x+(L/8.5),y,l,0 );
		y+=L/6;
		this.dwSgm( (this.charmap(4).indexOf(n)>=0 ? 1 : 0), x,y,l,1 );
		this.dwSgm( (this.charmap(5).indexOf(n)>=0 ? 1 : 0), x+L,y,l,1 );
		y+=L; y-=L/14;
		this.dwSgm( (this.charmap(6).indexOf(n)>=0 ? 1 : 0), x+(L/8.5),y,l,0 );
	},

}
