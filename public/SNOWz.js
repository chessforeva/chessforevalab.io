// just include this on dark screen



// snow definition
mySNOW_ = {
 s:[],
 k:1,	// increase for more snow		
 starter: function()
  {
   var m = (new Date).getMonth();	// Dec,Jan,Feb only
   if(m==11 || m<2) setInterval('mySNOW_.ticker()',50);
  },
  
 ticker: function()
  {
   for(var i=0;i<this.k; i++)
     {
	  // Add a new snow object
	  var o = document.createElement("IMG");
	  var w = (Math.random()*12);
	  var h = 1 + Math.min( parseInt(w/5), 2 ); 
	  o.src = "https://chessforeva.gitlab.io/img/snowz"+h+".png";
	  o.style.position="absolute";
	  o.style.top="0px";
	  var lf = 20+( Math.random()* (screen.width-60) );
	  o.style.left= lf+"px";
	  var z = (2 + (Math.random()* ((h==3)? lf%4 : lf%(h*4)))) + "px";
	  o.style.width = z;
	  o.style.height = z;
	  o.style.zIndex = 5000;
	  this.s.push(o);
	  document.body.appendChild(o);
	 }
   for(var i=0; i<this.s.length; i++)
     {
	  var o = this.s[i];
	  var y = parseInt( o.style.top );
	  o.style.top= (y+2) + "px";

          // if not visible also
	  var body = document.body, html = document.documentElement;
	  var height = Math.max( body.scrollHeight, body.offsetHeight, body.clientHeight,
                       html.clientHeight, html.scrollHeight, html.offsetHeight );
          if(y>height-40)
           {
	   document.body.removeChild(o);
	   this.s.splice(i,1);
           }
     }

   var maxCnt = 60;
   if(this.s.length>maxCnt)		// remove one
	{
	var j = parseInt(Math.random() * maxCnt);
	var q = this.s[j];
	document.body.removeChild(q);
	this.s.splice(j,1);
	}

  },
  
 clear: function()
  {
     for(var i=0; i<this.s.length; i++)
     {
	  var o = this.s[i];
	  document.body.removeChild(o);
	 }
  },
 
  };

setTimeout('mySNOW_.starter()',2000);		// start snowing after 2 secs.
