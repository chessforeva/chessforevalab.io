/*
A javascript port of
 OWL CHESS written in Borland Turbo C (year 1992-95)
done by http://chessforeva.blogspot.com (year 2017)
*/


MAXPLY = 6;		// max.ply
MAXSECS = 12;		// max.seconds to search

function MOVETYPE()	//struct
{
  return  {
        nw1:0, old:0,  /*  new and old square  */
        spe:0,           /*  Indicates special move:
                                 case movepiece of
                                  king: castling
                                  pawn: e.p. capture
                                  else : pawn promotion  */
        movpiece:0,   /* moving piece */
        content:0   /* evt. captured piece  */
    }
}

function cloneMove(m)
{ return { nw1:m.nw1, old:m.old, spe:m.spe, movpiece:m.movpiece, content:m.content }; }
function copyMove(t,f)
{ t.nw1=f.nw1; t.old=f.old; t.spe=f.spe; t.movpiece=f.movpiece; t.content=f.content; }


function BOARDTYPE() { return  { piece:0, color:0, index:0, attacked:0 } }

function PIECETAB() { return  { isquare:0, ipiece: 0 } }

function CASTTYPE() { return  { castsquare:0, cornersquare:0 } }

function ATTACKTABTYPE()
{
 return {
    /*  A set of king..pawn.  gives the pieces, which can move to the
     *  square
     */
    pieceset:0,
    direction:0  /*  The direction from the piece to the square  */
 }
}


/* constants and globals */
empty = 0; king = 1; queen = 2; rook = 3; bishop = 4; knight = 5; pawn = 6;	// pieces
white = 0; black = 1;			// colours
zero = 0; lng = 1; shrt = 2;		// castlings


Player = white; Opponent = black;	// Side to move, opponent
ProgramColor = white;			// AI side


Pieces = [ rook, knight, bishop, queen, king, bishop, knight, rook ];	// [8]
PieceTab=[[],[]];
Board=[];	// [0x78]

MovTab=[];
mc = 0;		// count of moves

OfficerNo = []; PawnNo = [];	// 2

function InsertPiece(p,c,sq)
{
 Board[sq].piece = p;
 Board[sq].color = c;
}

function ResetMoves()
{
   mc = 1;
   MovTab = [ MOVETYPE(), MOVETYPE(), MOVETYPE() ];
   Mo = MovTab[mc]; Mpre = MovTab[mc-1];
} 

function ClearBoard()
{
 for (var sq = 0; sq <= 0x77; sq++) Board[sq] = BOARDTYPE();
}

function ResetGame()
{
 ClearBoard();
 for (var i=0;i<8;i++)
 {
  InsertPiece(Pieces[i], white, i);
  InsertPiece(pawn, white, i+0x10);
  InsertPiece(pawn, black, i+0x60);
  InsertPiece(Pieces[i], black, i+0x70);
 }
 CalcPieceTab();
 Player = white; Opponent = black;	// Side to move, opponent
 ResetMoves();
 UseLib = 200;
}

/*
 *  Clears indexes in board and piecetab
 */

function ClearIndex()
{
    var square, col, index;

    for (square = 0; square <= 0x77; square++)
        Board[square].index = 16;
    for (col = white; col <= black; col++)
        for (index = 0; index <= 16; index++) PieceTab[col][index] = PIECETAB();

    OfficerNo = [ -1, -1 ]; PawnNo = [ -1, -1 ];
}


/*
 *  Calcualates Piece table from scratch
 */

function CalcPieceTab()
{
    var square, piece1, o,p,w,q;

    ClearIndex();

    for (piece1 = king; piece1 <= pawn; piece1++)
     {
        if (piece1 == pawn)
        {
            OfficerNo[white] = PawnNo[white];
            OfficerNo[black] = PawnNo[black];
        }
        square = 0;
        do
        {
            o = Board[square];
            if (o.piece == piece1)
            {
                w = o.color;
                PawnNo[w]++;
		p = PawnNo[w];
		q = PieceTab[w][p];
                q.ipiece = piece1; q.isquare = square;
                o.index = p;
            }
            square ^=  0x77;
            if (!(square & 4))
            {
                if (square >= 0x70)
                    square = (square + 0x11) & 0x73;
                else
                    square += 0x10;
            }
        } while (square);
    }
}


Depth = 1;		// search current depth (originally Depth starts from 0)  1..MAXPLY
AttackTab = [];
BitTab = [0, 1, 2, 4, 8, 0x10, 0x20];	// [7]
DirTab = [ 1, -1, 0x10, -0x10, 0x11, -0x11, 0x0f, -0x0f];	// [8]
KnightDir = [0x0E, -0x0E, 0x12, -0x12, 0x1f, -0x1f, 0x21, -0x21];	// [8]
PawnDir = [0x10, -0x10];	// [2]
BufCount = 0;
BufPnt = 0;
Next = {};
Buffer = [];
ZeroMove = MOVETYPE();

function CSTPE(n,o) { return { castnew:n, castold:o } }

	// [2][2] of new,old squares    
CastMove = [ [CSTPE(2,4), CSTPE(6,4)], [CSTPE(0x72, 0x74), CSTPE(0x76, 0x74)] ];

/* === MOVEGEN === */

function CalcAttackTab()
{
    var dir, sq, i, o;

    for (sq = -0x77; sq <= 0x77; sq++) AttackTab[120+sq] = ATTACKTABTYPE(); 
    for (dir = 7; dir >=0; dir--)
    {
        for (i = 1; i < 8; i++)
        {
	o = AttackTab[120+(DirTab[dir]*i)];
	o.pieceset = BitTab[queen]+BitTab[(dir < 4 ? rook : bishop)];
	o.direction = DirTab[dir];
        }
        o = AttackTab[120+DirTab[dir]];
	o.pieceset += BitTab[king];
	o = AttackTab[120+KnightDir[dir]];
        o.pieceset = BitTab[knight];
        o.direction = KnightDir[dir];
    }
}


/*
 *  calculate whether apiece placed on asquare attacks the square
 */

function PieceAttacks(apiece, acolor, asquare, square)
{
    var x = square - asquare;
    if (apiece == pawn)   /*  pawn attacks  */
        return (Math.abs(x - PawnDir[acolor]) == 1);
    /*  other attacks: can the piece move to the square?  */
    else if (AttackTab[120+x].pieceset & BitTab[apiece])
    {
        if (apiece == king || apiece == knight)
            return 1;
        else
        {
        /*  are there any blocking pieces in between?  */
            var sq = asquare;
            do
            {
                sq += AttackTab[120+x].direction;
            } while (sq != square && Board[sq].piece == empty );
            return (sq == square);
        }
    }
    else
        return 0;
}


/*
 *  calculate whether acolor attacks the square with at pawn
 */

function PawnAttacks(acolor,square)
{
    var o,sq = square - PawnDir[acolor] - 1;  /*  left square  */
    if (!(sq & 0x88))
    {
        o = Board[sq];
        if (o.piece == pawn && o.color == acolor)
            return 1;
    }
    sq += 2;   /*  right square  */
    if (!(sq & 0x88))
    {
        o = Board[sq];
        if (o.piece == pawn && o.color == acolor)
            return 1;
    }
    return 0;
}


/*
 *  Calculates whether acolor attacks the square
 */

function Attacks(acolor, square)
{
    if (PawnAttacks(acolor, square))    /*  pawn attacks  */
        return 1;
    /*  Other attacks:  try all pieces, starting with the smallest  */
    for (var i = OfficerNo[acolor]; i >= 0; i--)
    {
	var o = PieceTab[acolor][i];
        if (o.ipiece != empty)
            if (PieceAttacks(o.ipiece, acolor, o.isquare, square))
                return 1;
    }
    return 0;
}


/*
 *  check whether inpiece is placed on square and has never moved
 */

function Check(square, inpiece, incolor)
{
    var o = Board[square];
    if(o.piece == inpiece && o.color == incolor)
    {
        var dep = mc - 1;
        while (dep>=0 && MovTab[dep].movpiece != empty)
        {
            if (MovTab[dep].nw1 == square)
                return 0;
            dep--;
        }
        return 1;
    }
    return 0;
}


/*
 *  Calculate whether incolor can castle
 */

function CalcCastling(incolor)
{
    var square = 0, cast = zero;

    if (incolor == black) square = 0x70;
    if (Check(square + 4, king, incolor))  /*  check king  */
    {
        if (Check(square, rook, incolor))
            cast += lng;  /*  check a-rook  */
        if (Check(square + 7, rook, incolor))
            cast += shrt;  /*  check h-rook  */
    }
    return cast;
}


/*
 *  check if move is a pawn move or a capture
 */

function RepeatMove(move)
{
    return (move.movpiece != empty && move.movpiece != pawn &&
              move.content == empty && !move.spe);
}


/*
 *  Count the number of moves since last capture or pawn move
 *  The game is a draw when fiftymovecnt = 100
 */

function FiftyMoveCnt()
{
    var cnt = 0;

    while (RepeatMove(MovTab[mc - cnt]))
        cnt++;
    return cnt;
}


/*
 *  Calculate how many times the move has occurred before
 *  The game is a draw when repetition = 3
 *  MovTab contains the previous moves
 *  When immediate is set, only immediate repetition is checked
 */

function Repetition(immediate)
{
    var lastdep, compdep, tracedep, checkdep, samedepth, tracesq, checksq, repeatcount, o;

    repeatcount = 1;
    lastdep = mc;    /*  current position  */
    samedepth = lastdep;
    compdep = samedepth - 4;            /*  First position to compare  */

    /*  MovTab contains previous relevant moves  */
    while (RepeatMove(MovTab[lastdep-1]) && (compdep < lastdep ||
                 !immediate))
        lastdep--;
    if (compdep < lastdep) return 1;
    checkdep = samedepth;
    do
    {
        checkdep--;
        checksq = MovTab[checkdep].nw1;
	var f=1;
        for (tracedep = checkdep + 2; tracedep < samedepth; tracedep += 2)
            if (MovTab[tracedep].old == checksq) { f=0; break; }
	
	if(f)
	{

        /*  Trace the move backward to see if it has been 'undone' earlier  */
        tracedep = checkdep;
        tracesq = MovTab[tracedep].old;
        do
        {
            if (tracedep-2 < lastdep) return repeatcount;
            tracedep -= 2;
            /*  Check if piece has been moved before  */
	    o = MovTab[tracedep];
            if (tracesq == o.nw1) tracesq = o.old;
        } while (tracesq != checksq || tracedep > compdep + 1);
        if (tracedep < compdep)    /*  Adjust evt. compdep  */
        {
            compdep = tracedep;
            if ((samedepth - compdep) % 2 == 1)
            {
                if (compdep == lastdep) return repeatcount;
                compdep --;
            }
            checkdep = samedepth;
        }
	}
	
        /*  All moves between SAMEDEP and compdep have been checked,
            so a repetition is found  */
//TEN :
	if (checkdep <= compdep)
        {
            repeatcount++;
            if (compdep - 2 < lastdep) return repeatcount;
            checkdep = samedepth = compdep;
            compdep -= 2;
        }
    } while (1);
}


/*
 *  Test whether a move is possible
 *
 *  On entry:
 *    Move contains a full description of a move, which
 *    has been legally generated in a different position.
 *    MovTab[mc] contains last performed move.
 *
 *  On exit:
 *    KillMovGen indicates whether the move is possible
 */

function KillMovGen(move)
{
    var castsq, promote, castdir, cast=0, killmov, o, q;

    killmov = 0;
    if (move.spe && (move.movpiece == king))
    {
        cast = CalcCastling(Player);     /*  Castling  */
        castdir = ((move.nw1 > move.old) ? shrt : lng );

        if (cast & castdir)    /*  Has king or rook moved before  */
        {
            castsq =  ((move.nw1 + move.old) / 2);
            /*  Are the squares empty ?  */
            if  (Board[move.nw1].piece == empty)
              if (Board[castsq].piece == empty)
                if ((move.nw1 > move.old) || (Board[move.nw1-1].piece == empty))
                  /*  Are the squares unattacked  */
                  if (!Attacks(Opponent, move.old))
                    if (!Attacks(Opponent, move.nw1))
                      if (!Attacks(Opponent, castsq))
                        killmov = 1;
        }
    }
    else
    {
    if (move.spe && (move.movpiece == pawn))
    {
            /*  E.p. capture  */
            /*  Was the Opponent's move a 2 square move?  */
        if (Mpre.movpiece == pawn)
            if (Math.abs(Mpre.nw1 - Mpre.old) >= 0x20)
	    {
		q = Board[move.old];
                if ((q.piece == pawn) && (q.color == Player))
                        killmov = (move.nw1 == ((Mpre.nw1 + Mpre.old) / 2));
	    }
    }
    else
    {
        if (move.spe)                  /*  Normal test  */
        {
            promote = move.movpiece;   /*  Pawnpromotion  */
            move.movpiece = pawn;
        }

        /*  Is the content of Old and nw1 squares correct?  */
        if (Board[move.old].piece == move.movpiece)
          if (Board[move.old].color == Player)
            if (Board[move.nw1].piece == move.content)
              if (move.content == empty || Board[move.nw1].color == Opponent)
              {
                if (move.movpiece == pawn)   /*  Is the move possible?  */
                {
                  if (Math.abs(move.nw1 - move.old) < 0x20)
                    killmov = 1;
                  else
                    killmov = Board[(move.nw1+move.old) / 2].piece == empty;
                }
                else
                  killmov = PieceAttacks(move.movpiece, Player,
                                 move.old, move.nw1);
              }
              if (move.spe)
                move.movpiece = promote;
    }
    }
    return killmov;
}



/*
 *  Store a move in buffer
 */

function Generate()
{
Buffer[ ++BufCount] = cloneMove(Next); /* new copied MOVETYPE() */
}


/*
 *  Generates pawn promotion
 */

function PawnPromotionGen()
{
    Next.spe = 1;
    for (var promote = queen; promote <= knight; promote++)
    {
        Next.movpiece = promote;
        Generate();
    }
    Next.spe = 0;
}


/*
 *  Generates captures of the piece on nw1 using PieceTab
 */

function CapMovGen()
{
    var nextsq, sq, i,o,p;

    Next.spe = 0;
    Next.content = Board[Next.nw1].piece;
    Next.movpiece = pawn;
    nextsq = Next.nw1 - PawnDir[Player];
    for (sq = nextsq-1; sq <= nextsq+1; sq++)
        if (sq != nextsq)
          if ((sq & 0x88) == 0)
            {
	    o = Board[sq];
            if (o.piece == pawn && o.color == Player)
             {
                Next.old = sq;
                if (Next.nw1 < 8 || Next.nw1 >= 0x70)
                    PawnPromotionGen();
                else
                    Generate();
             }
	    }
            /*  Other captures, starting with the smallest pieces  */
    for (i = OfficerNo[Player]; i >= 0; i--)
    {
	o = PieceTab[Player][i]; p = o.ipiece;
        if (p != empty && p != pawn)
          if (PieceAttacks(p, Player, o.isquare, Next.nw1))
          {
              Next.old = o.isquare;
              Next.movpiece = p;
              Generate();
          }
    }
}


/*
 *  generates non captures for the piece on old
 */

function NonCapMovGen()
{
   var first, last, dir, direction, newsq;

    Next.spe = 0;
    Next.movpiece = Board[Next.old].piece;
    Next.content = empty;
    switch (Next.movpiece)
    {
        case king :
            for (dir = 7; dir >= 0; dir--)
            {
                newsq = Next.old + DirTab[dir];
                if (!(newsq & 0x88))
                  if (Board[newsq].piece == empty)
                  {
                      Next.nw1 = newsq;
                      Generate();
                  }
            }
            break;
        case knight :
            for (dir = 7; dir >= 0; dir--)
            {
                newsq = Next.old + KnightDir[dir];
                if (!(newsq & 0x88))
                  if (Board[newsq].piece == empty)
                  {
                      Next.nw1 = newsq;
                      Generate();
                  }
            }
            break;
        case queen :
        case rook  :
        case bishop:
            first = 7;
            last = 0;
            if (Next.movpiece == rook) first = 3;
            if (Next.movpiece == bishop) last = 4;
            for (dir = first; dir >= last; dir--)
            {
                direction = DirTab[dir];
                newsq = Next.old + direction;
                /*  Generate all non captures in the direction  */
                while (!(newsq & 0x88))
                {
                    if (Board[newsq].piece != empty) break;
                    Next.nw1 = newsq;
                    Generate();
                    newsq = Next.nw1 + direction;
                }
            }
            break;
        case pawn :
            Next.nw1 = Next.old + PawnDir[Player];  /*  one square forward  */
            if (Board[Next.nw1].piece == empty)
            {
                if (Next.nw1 < 8 || Next.nw1 >= 0x70)
                    PawnPromotionGen();
                else
                {
                    Generate();
                    if (Next.old < 0x18 || Next.old >= 0x60)
                    {
                        Next.nw1 += (Next.nw1 - Next.old); /* 2 squares forward */
                        if (Board[Next.nw1].piece == empty) Generate();
                    }
                }
            }
    }  /* switch */
}


/*
 *  The move generator.
 *  InitMovGen generates all possible moves and places them in a buffer.
 *  Movgen will the generate the moves one by one and place them in next.
 *
 *  On entry:
 *    Player contains the color to move.
 *    MovTab[mc-1] the last performed move.
 *
 *  On exit:
 *    Buffer contains the generated moves.
 *
 *    The moves are generated in the order :
 *      Captures
 *      Castlings
 *      Non captures
 *      E.p. captures
 */

function InitMovGen()
{
    var castdir, sq, index, o;
    Next = MOVETYPE();
    Buffer = [];
    BufCount = 0; BufPnt = 0;
    /*  generate all captures starting with captures of
        largest pieces  */
    for (index = 1; index <= PawnNo[Opponent]; index++)
    {
	o = PieceTab[Opponent][index];
        if (o.ipiece != empty)
        {
            Next.nw1 = o.isquare;
            CapMovGen();
        }
    }
    Next.spe = 1;
    Next.movpiece = king;
    Next.content = empty;
    for (castdir = (lng-1); castdir <= shrt-1; castdir++)
    {
        o = CastMove[Player][castdir];
        Next.nw1 = o.castnew;
        Next.old = o.castold;
        if (KillMovGen(Next)) Generate();
    }

    /*  generate non captures, starting with pawns  */
    for (index = PawnNo[Player]; index >= 0; index--)
    {
	o = PieceTab[Player][index];
        if (o.ipiece != empty)
        {
            Next.old = o.isquare;
            NonCapMovGen();
        }
    }
    
    if (Mpre.movpiece == pawn)   /*  E.p. captures  */
        if (Math.abs(Mpre.nw1 - Mpre.old) >= 0x20)
        {
            Next.spe = 1;
            Next.movpiece = pawn;
            Next.content = empty;
            Next.nw1 = (Mpre.nw1 + Mpre.old) / 2;
            for (sq = Mpre.nw1-1; sq <= Mpre.nw1+1;  sq++)
                if (sq != Mpre.nw1)
                    if (!(sq & 0x88))
                    {
                        Next.old = sq;
                        if (KillMovGen(Next)) Generate();
                    }
        }
}


/*
 *  place next move from the buffer in next.  Generate zeromove when there
 *  are no more moves.
 */


 
function MovGen()
{
    if (BufPnt >= BufCount)
        Next = ZeroMove;
    else
    {
        Next = Buffer[ ++BufPnt ];
    }
}

/*
 *  Test if the move is legal for color == player in the
 *  given position
 */

function IllegalMove(move)
{
   Perform(move, 0);
   var illegal = Attacks(Opponent, PieceTab[Player][0].isquare);
   Perform(move, 1);
   return illegal;
}

/*
 *  Prints comment to the game (check, mate, draw, resign)
 */


function Comment()
{
    var s="", check = 0, possiblemove = 0, checkmate = 0;

    InitMovGen();
    for(var i=0;i<BufCount;i++)
    {
        MovGen();
        if (!IllegalMove(Next)) { possiblemove=1; break; }
    }

    check = Attacks(Opponent, PieceTab[Player][0].isquare);  //calculate check
    //  No possible move means checkmate or stalemate
    if (!possiblemove)
    {
        if (check)
        {
            checkmate = 1;
            s+="CheckMate! "+(Opponent==white ? "1-0" : "0-1");
        }
        else
            s+="StaleMate! 1/2-1/2";
    }
    else
        if (MainEvalu >= MATEVALUE - DEPTHFACTOR * 16)
        {
            var nummoves = ((MATEVALUE - MainEvalu + 0x40) / (DEPTHFACTOR * 2))|0;
            if(nummoves>0)
             s+= "Mate in " + nummoves + " move" + ((nummoves > 1) ? "s":"") + "!";
        }
    if (check && !checkmate) s+="Check!";
    else  //test 50 move rule and repetition of moves 
      {
      if (FiftyMoveCnt() >= 100)
         {
         s+="50 Move rule";
         }
      else
         if (Repetition(0) >= 3)
         {
            s+="3 fold Repetition";
         }
         else                //Resign if the position is hopeless
            if (Opponent==ProgramColor && (-25500 < MainEvalu && MainEvalu < -0x880))
               {
               s+=(Opponent==white ? "White" : "Black") + " resigns";
               }
      }
      return s;
}


function CHR(n) { return String.fromCharCode(n) }
function sq2str( square )
{
 return CHR(97+(square&7)) + CHR(49+(square>>>4));
}


/*
 *  convert a move to a string
 */

function MoveStr(move)
{
    if (move.movpiece != empty)
    {
        if (move.spe && move.movpiece == king)  /*  castling  */
        {
            return "O-O" + ((move.nw1 > move.old) ? "" : "-O");
        }
        else
        {
            var s="", piece = Board[ move.old ].piece, ispawn = (piece==pawn);
            var c = move.content || 
			( ispawn && Math.abs( Math.abs(move.nw1 - move.old)-0x10 )==1 );
            var p = ispawn && move.movpiece<6;
            if(!ispawn) s += " KQRBN".charAt(move.movpiece);
            s += sq2str(move.old);
            s += (c ? 'x' : '-');
            s += sq2str(move.nw1);
            if(p) s += "=" + "QRBN".charAt(move.movpiece-2);
            return s;
        }
    }
    return "?";
}



// generates string of possible moves,
// does not include check,checkmate,stalemate flags
function GenMovesStr()
{
    var s = "";
    InitMovGen();
    for(var i=0;i<BufCount;i++)
    {
        MovGen();
        if (!IllegalMove(Next)) s += "," + MoveStr( Next );
    };
  return s.substr(1);
}

function printboard()
{
 for(var v=8;(--v)>=0;)
  {
  for(var s="",h=0;h<8;h++)
   {
    var o = Board[ (v<<4)+h ], p = ".kqrbnp".charAt(o.piece);
    if(o.color == white) p = p.toUpperCase();
    s+=p;
   }
  console.log(s);
  }
}

/* === DO MOVE, UNDO MOVE === */

/*
 *  move a piece to a new location on the board
 */

function MovePiece( nw1, old )
{
    var n = Board[nw1], o = Board[old];
    Board[nw1] = o; Board[old] = n;
    PieceTab[o.color][o.index].isquare = nw1;
}

/*
 *  Calculate the squares for the rook move in castling
 */

function GenCastSquare( nw1, Cast )
{
    if ((nw1 & 7) >= 4)          /* short castle */
    {
        Cast.castsquare = nw1 - 1;
        Cast.cornersquare = nw1 + 1;
    }
    else                           /* long castle */
    {
        Cast.castsquare = nw1 + 1;
        Cast.cornersquare = nw1 - 2;
    }
}


/*
 *  This function used in captures.  insquare must not be empty.
 */

function DeletePiece(insquare)
{
    var o = Board[insquare];
    o.piece = empty;
    PieceTab[o.color][o.index].ipiece = empty;
}


/*
 *  Take back captures
 */

function InsertPTabPiece( inpiece, incolor, insquare )
{
    var o = Board[insquare], q = PieceTab[incolor][o.index];
    o.piece =  inpiece; q.ipiece  = inpiece;
    o.color = incolor;
    q.isquare = insquare;
}


/*
 *  Used for pawn promotion
 */

function ChangeType( newtype, insquare )
{
    var o = Board[insquare];
    o.piece = newtype;
    PieceTab[o.color][o.index].ipiece = newtype;
    if (OfficerNo[o.color] < o.index) OfficerNo[o.color] = o.index;
}


/*
 Do move
*/
function DoMove( move )
{
 Perform( move, 0 );
 Player ^= 1; Opponent ^= 1;
}

/*
 Undo move
*/
function UndoMove( move )
{
 Player ^= 1; Opponent ^= 1;
 unPerform();
}

/*
 *  Perform or take back move (takes back if resetmove is true),
 *  and perform the updating of Board and PieceTab.  Player must
 *  contain the color of the moving player, Opponent the color of the
 *  Opponent.
 *
 *  MovePiece, DeletePiece, InsertPTabPiece and ChangeType are used to update
 *  the Board module.
 */


function sqByAt(square) { return ((square.charCodeAt(0)-97)+(0x10*(square.charCodeAt(1)-49))) }

function DoMoveByStr( mstr )
{
 var ret = "";
 var old = sqByAt( mstr.substr(0,2) ), nw1 = sqByAt( mstr.substr(2,2) );
 InitMovGen();
 for(var i=0;i<BufCount;i++)
 {
        MovGen();
	if(Next.old == old && Next.nw1 == nw1 &&
		(mstr.length<5 ||
		(Next.spe && ("qrbn").indexOf(mstr.charAt(4))==Next.movpiece-2 )))
	{
	ret = MoveStr( Next );
	DoMove( Next ); break;
	}
 };
 return ret;
}

function Perform( move, resetmove )
{
    if (resetmove)
    {
        MovePiece(move.old, move.nw1);
        if (move.content != empty)
            InsertPTabPiece(move.content, Opponent, move.nw1);
    }
    else         
    {
        if (move.content != empty)
            DeletePiece(move.nw1);
        MovePiece(move.nw1, move.old);
    }

    if (move.spe)
    {
        if (move.movpiece == king)
        {
	    var Cast = CASTTYPE();
            GenCastSquare(move.nw1, Cast);
            if (resetmove)
            {
                MovePiece(Cast.cornersquare, Cast.castsquare);
            }
            else
            {
                MovePiece(Cast.castsquare, Cast.cornersquare);
            }
        }
        else
        {
            if (move.movpiece == pawn)
            {
                var epsquare = (move.nw1 & 7) + (move.old & 0x70); /* E.p. capture */
                if (resetmove)
                    InsertPTabPiece(pawn, Opponent, epsquare);
                else
                    DeletePiece(epsquare);
            }
            else
            {
                if (resetmove)
                    ChangeType(pawn, move.old);
                else
                    ChangeType(move.movpiece,move.nw1);
            }
        }
    }

    if(resetmove)
    {
	mc--; MovTab.pop();
    }
    else
    {
	MovTab[mc++] = cloneMove(move);
	if( MovTab.length<=mc ) MovTab[mc] = MOVETYPE();
    }
    Mo = MovTab[mc]; Mpre = MovTab[mc-1];
}

/* simply undo last move in searching */
function unPerform() { Perform( Mpre, 1 ); }

/*
 * Compare two moves
 */

function EqMove( a, b )
{
 return (a.movpiece == b.movpiece && a.nw1 == b.nw1 && a.old == b.old &&
       a.content == b.content && a.spe == b.spe);
}

/* === EVALUATE === */

// creates objects for 3-dim arrays
function arr2xN(n)
 { var a=[[],[]],i=0; for(;i<n;i++) {a[0][i]=[];a[1][i]=[]}; return a; }

TOLERANCE = 8;  /*  Tolerance width  */
EXCHANGEVALUE =32;
     /*  Value for exchanging pieces when ahead (not pawns)  */
ISOLATEDPAWN = 20;
    /*  Isolated pawn.  Double isolated pawn is 3 * 20  */
DOUBLEPAWN = 8;   /*  Double pawn  */
SIDEPAWN = 6;   /*  Having a pawn on the side  */
CHAINPAWN = 3;   /*  Being covered by a pawn  */
COVERPAWN = 3;   /*  covering a pawn  */
NOTMOVEPAWN = 2;   /*  Penalty for moving pawn  */
BISHOPBLOCKVALUE = 20;
    /*  Penalty for bishop blocking d2/e2 pawn  */
ROOKBEHINDPASSPAWN = 16;   /*  Bonus for Rook behind passed pawn  */

/* constants and globals */

PieceValue = [0, 0x1000, 0x900, 0x4c0, 0x300, 0x300, 0x100];	// [7]
distan = [ 3, 2, 1, 0, 0, 1, 2, 3 ];	// [8]
    /*  The value of a pawn is the sum of Rank and file values.
        The file value is equal to PawnFileFactor * (Rank Number + 2) */
pawnrank = [0, 0, 0, 2, 4, 8, 30, 0];	// [8]	
passpawnrank = [0, 0, 10, 20, 40, 60, 70, 0];	// [8]
pawnfilefactor = [0, 0, 2, 5, 6, 2, 0, 0];	// [8]
castvalue = [4, 32];	// [2]  /*  Value of castling  */

filebittab = [1, 2, 4, 8, 0x10, 0x20, 0x40, 0x80];	// [8]
totalmaterial = 0;
pawntotalmaterial = 0;
material = 0;
  /*  Material level of the game
        (early middlegame = 43 - 32, endgame = 0)  */
materiallevel = 0;
squarerankvalue = [ 0, 0, 0, 0, 1, 2, 4, 4];	// [8]

mating = 0;  /*  mating evaluation function is used  */

PVTable = arr2xN(7); //[2][7][0x78]

function PAWNBITTYPE() { return  { one:0, dob:0 } }
function copyPwBt(t,f) { t.one=f.one; t.dob=f.dob; }
function PwBtList() { var a=[],i=0; for(;i<=MAXPLY;i++) a[i]=PAWNBITTYPE(); return a; }
pawnbit = [];

MAXINT = 32767;

RootValue = 0;

bitcount = [];	// count the number of set bits in b (0..255)
function prepareBitCounts()
{
 var i=0,b,c;
 for(var i=0; i<256; i++)
  {
   b=i; c = 0;
   while (b)
    {
    if (b & 1) c++;
    b >>>= 1;
    }
   bitcount[i] = c;
  }
}
prepareBitCounts();

 

/*
 *  Calculate value of the pawn structure in pawnbit[color][depth]
 */

function pawnstrval( depth, color)
{
	/*  contains FILEs with isolated pawns  */

    var o = pawnbit[color][depth], v = o.one, d = o.dob;
    var iso = v &  ~((v << 1) | (v >>> 1));
    return (-(bitcount[d] * DOUBLEPAWN +
            bitcount[iso] * ISOLATEDPAWN +
	    bitcount[iso & d] * ISOLATEDPAWN * 2));
}


/*
 *  calculate the value of the piece on the square
 */

function PiecePosVal( piece, color, square )
{
    return (PieceValue[piece] + PVTable[color][piece][square]);
}

/*
 *  calculates piece-value table for the static evaluation function
 */

function CalcPVTable()
{
    /*  Bit tables for static pawn structure evaluation  */
    var pawnfiletab, bit, oppasstab, behindoppass,
        leftsidetab, rightsidetab, sidetab, leftchaintab,
        rightchaintab, chaintab, leftcovertab, rightcovertab;

    /*  Importance of an attack of the square  */
    var attackvalue = [[],[]];	//[2][0x78]
    
    var pawntab = [[],[]];	// [2][8]
    
    /*  Value of squares controlled from the square  */
    var pvcontrol = arr2xN(5); //[2][5][0x78]

    var losingcolor;     /*  the color which is being mated  */
    var posval;                /*  The positional value of piece  */
    var attval;                /*  The attack value of the square  */
    var line;             /*  The file of the piece  */
    var rank;             /*  The rank of the piece  */
    var dist, kingdist;       /*  Distance to center, to opponents king */
    var cast;             /*  Possible castlings  */
    var direct;              /*  Indicates direct attack  */
    var cnt;                   /*  Counter for attack values  */
    var strval;                /*  Pawnstructure value  */
    var color, oppcolor; /*  Color and opponents color  */
    var piececount;      /*  Piece counter  */
    var square;         /*  Square counter  */
    var dir;               /*  Direction counter  */
    var sq;         /*  Square counter  */
    var t,t2,t3;           /*  temporary junk  */
    var o,p,v;

    /*  Calculate SAMMAT, PAWNSAMMAT and Material  */
    material = 0;
    pawntotalmaterial = 0;
    totalmaterial = 0;
    mating = 0;

    for (square = 0; square < 0x78; square++)
        if (!(square & 0x88))
	{
            o = Board[square]; p = o.piece;
            if (p != empty)
                if (p != king)
                {
                    t = PieceValue[p];
                    totalmaterial += t;
                    if (p == pawn)
                        pawntotalmaterial += PieceValue[pawn];
                    if (o.color == white) t = -t;
                    material -= t;
                }
	}
    materiallevel = (Math.max(0, totalmaterial - 0x2000) / 0x100)|0;
    /*  Set mating if weakest player has less than the equivalence
    of two bishops and the advantage is at least a rook for a bishop  */
    losingcolor = ((material < 0) ? white : black);
    v = Math.abs(material);
    mating = ((totalmaterial - v) / 2 <= PieceValue[bishop] * 2)
        && (v >= PieceValue[rook] - PieceValue[bishop]);
    /*  Calculate ATTACKVAL (importance of each square)  */
    for (rank = 0; rank < 8; rank++)
        for (line = 0; line < 8; line++)
        {
            square = (rank << 4) + line;
            attval = Math.max(0, 8 - 3 * (distan[rank] + distan[line]));
                    /*  center importance */
                    /*  Rank importrance  */
            for (color = white; color <= black; color++)
            {
                attackvalue[color][square] = ((squarerankvalue[rank] * 3 *
                        (materiallevel + 8)) >> 5) + attval;
                square ^= 0x70;
            }
        }
    for (color = white; color <= black; color++)
    {
        oppcolor = (color ^ 1);
        cast = CalcCastling( oppcolor );
        if (cast != shrt && materiallevel > 0)
            /*  Importance of the 8 squares around the opponent's King  */
        for (dir = 0; dir < 8; dir++)
        {
            sq = PieceTab[oppcolor][0].isquare + DirTab[dir];
            if (!(sq & 0x88))
                attackvalue[color][sq] += ((12 * (materiallevel + 8)) >> 5);
        }
    }

    /*  Calculate PVControl  */
    for (square = 0x77; square >=0; square--)
        if(!(square & 0x88))
            for (color = white; color <= black; color++)
                for (piececount = rook; piececount <= bishop; piececount++)
                    pvcontrol[color][piececount][square] = 0;
    for (square = 0x77; square >=0; square--)
        if(!(square & 0x88))
            for (color = white; color <= black; color++)
            {
                for (dir = 7; dir >= 0; dir--)
                {
                    piececount = ((dir < 4) ? rook : bishop);
                /*  Count value of all attacs from the square in
                    the Direction.
                    The Value of attacking a Square is Found in ATTACKVAL.
                    Indirect Attacks (e.g. a Rook attacking through
                    another Rook) counts for a Normal attack,
                    Attacks through another Piece counts half  */
                    cnt = 0;
                    sq = square;
                    direct = 1;
                    do
                    {
                        sq += DirTab[dir];
                        if (sq & 0x88) break;	//goto TEN
                        t = attackvalue[color][sq];
                        if (direct)
                            cnt += t;
                        else
                            cnt += (t >> 1);
                        p = Board[sq].piece;
                        if (p != empty)
                            if ((p != piececount)
                              && (p != queen))
                                direct = 0;
                    } while (p != pawn);
/*TEN:*/            pvcontrol[color][piececount][square] += (cnt >> 2);
                }
            }

    /*  Calculate PVTable, value by value  */
    for (square = 0x77; square >= 0; square--)
      if (!(square & 0x88))
      {
         for (color = white; color <= black; color++)
         {
            oppcolor = (color ^ 1);
            line = square & 7;
            rank = square >> 4;
            if (color == black) rank = 7 - rank;
            dist = distan[rank] + distan[line];
	    v = PieceTab[oppcolor][0].isquare;
            kingdist = Math.abs((square >> 4) - (v >> 4)) + ((square - v) & 7);
            for (piececount = king; piececount <= pawn; piececount++)
            {
                posval = 0;        /*  Calculate POSITIONAL Value for  */
                                   /*  The piece on the Square  */
                if (mating && (piececount != pawn))
                {
                    if (piececount == king)
                        if (color == losingcolor)  /*  Mating evaluation  */
                        {
                            posval = 128 - 16 * distan[rank] - 12 * distan[line];
                            if (distan[rank] == 3)
                                posval -= 16;
                        }
                        else
                        {
                            posval = 128 - 4 * kingdist;
                            if ((distan[rank] >= 2) || (distan[line] == 3))
                                posval -= 16;
                        }
                }
                else
                {
                    t = pvcontrol[color][rook][square];
                    t2 = pvcontrol[color][bishop][square];
                    /*  Normal evaluation function  */
                    switch (piececount)
                    {
                        case king :
                            if (materiallevel <= 0) posval = -2 * dist;
                            break;
                        case queen :
                            posval = (t + t2) >> 2;
                            break;
                        case rook :
                            posval = t;
                            break;
                        case bishop :
                            posval = t2;
                            break;
                        case knight :
                            cnt = 0;
                            for (dir = 0; dir < 8; dir++)
                            {
                                sq = square + KnightDir[dir];
                                if (!(sq & 0x88))
                                    cnt += attackvalue[color][sq];
                            }
                            posval = (cnt >> 1) - dist * 3;
                            break;
                        case pawn :
                            if ((rank != 0) && (rank != 7))
                                posval = pawnrank[rank] +
                                  pawnfilefactor[line] * (rank + 2) - 12;
                    }
                }
                PVTable[color][piececount][square] = posval;
            }
         }
      }

    /*  Calculate pawntab (indicates which squares contain pawns)  */

    for (color = white; color <= black; color++)
        for (rank = 0; rank < 8; rank++)
            pawntab[color][rank] = 0;
    for (square = 0x77; square >= 0; square--)
        if (!(square & 0x88))
	{
            o = Board[square];
            if (o.piece == pawn)
            {
                rank = square >> 4;
                if (o.color == black) rank = 7 - rank;
                pawntab[o.color][rank] |= filebittab[square & 7];
            }
	}
    for (color = white; color <= black; color++)  /*  initialize pawnbit  */
    {
        o = pawnbit[color][0]; o.dob = 0; o.one = 0;
        for (rank = 1; rank < 7; rank++)
        {
            t = pawntab[color][rank];
            o.dob |= (o.one & t);
            o.one |= t;
        }
    }
    /*  Calculate pawnstructurevalue  */
    RootValue = pawnstrval(0, Player) - pawnstrval(0, Opponent);

    /*  Calculate static value for pawn structure  */
    for (color = white; color <= black; color++)
    {
        oppcolor = (color ^ 1);
        pawnfiletab = 0;
	leftsidetab = 0;
	rightsidetab = 0;
	behindoppass = 0;
        oppasstab = 0xff;
        for (rank = 1; rank < 7; rank++)
        /*  Squares where opponents pawns are passed pawns  */
        {
            oppasstab &= (~(pawnfiletab | leftsidetab | rightsidetab));
            /*  Squares behind the opponents passed pawns  */
            behindoppass |= (oppasstab & pawntab[oppcolor][7 - rank]);
            /*  squares which are covered by a pawn  */
            leftchaintab = leftsidetab;
            rightchaintab = rightsidetab;
            pawnfiletab = pawntab[color][rank]; /*  squares w/ pawns  */
            /*  squares w/ a pawn beside them  */
            leftsidetab = (pawnfiletab << 1) & 0xff;
            rightsidetab = (pawnfiletab >> 1) & 0xff;
            sidetab = leftsidetab | rightsidetab;
            chaintab = leftchaintab | rightchaintab;
            /*  squares covering a pawn  */
            t = pawntab[color][rank+1];
            leftcovertab = (t << 1) & 0xff;
            rightcovertab = (t >> 1 ) & 0xff;
            sq = rank << 4;
            if (color == black) sq ^= 0x70;
            bit = 1;
            while (bit)
            {
                strval = 0;
                if (bit & sidetab)
                    strval = SIDEPAWN;
                else if (bit & chaintab)
                    strval = CHAINPAWN;
                if (bit & leftcovertab)
                    strval += COVERPAWN;
                if (bit & rightcovertab)
                    strval += COVERPAWN;
                if (bit & pawnfiletab)
                    strval += NOTMOVEPAWN;
                PVTable[color][pawn][sq] += strval;
                if ((materiallevel <= 0) || (oppcolor != ProgramColor))
                {
                    if (bit & oppasstab)
                        PVTable[oppcolor][pawn][sq] += passpawnrank[7 - rank];
                    if (bit & behindoppass)
                    {
                        t = sq ^ 0x10;
                        for (t3 = black; t3 >= white ; t3--)
                        {
                            PVTable[t3][rook][sq] += ROOKBEHINDPASSPAWN;
                            if (rank == 6)
                                PVTable[t3][rook][t] += ROOKBEHINDPASSPAWN;
                        }
                    }
                }
                sq++;
                bit = (bit << 1) & 0xff;
            }
        }
    }
    /*  Calculate penalty for blocking center pawns with a bishop  */
    for (sq = 3; sq < 5; sq ++)
    {
        o = Board[sq + 0x10];
        if ((o.piece == pawn) && (o.color == white))
            PVTable[white][bishop][sq+0x20] -= BISHOPBLOCKVALUE;
        o = Board[sq + 0x60];
        if ((o.piece == pawn) && (o.color == black))
            PVTable[black][bishop][sq+0x50] -= BISHOPBLOCKVALUE;
    }
    for (square = 0x77; square >= 0; square--) /*  Calculate RootValue  */
        if (!(square & 0x88))
	{
            o = Board[square]; p = o.piece;
            if (p != empty)
                if (o.color == Player)
                    RootValue +=
                        PiecePosVal(p, Player, square);
                else
                    RootValue -=
                        PiecePosVal(p, Opponent, square);
	}
}

/*
 *  Update pawnbit and calculates value when a pawn is removed from line
 */

function decpawnstrval(color, line)
{
    var o = pawnbit[color][Depth];
    var t = ~filebittab[line];
    o.one = (o.one & t) | o.dob;
    o.dob &= t;
    return (pawnstrval(Depth, color) - pawnstrval(Depth - 1, color));
}

/*
 *  Update pawnbit and calculates value when a pawn moves
 *  from old to nw1 file
 */

function movepawnstrval(color, nw1, old)
{
    var o = pawnbit[color][Depth];
    var t = filebittab[nw1];
    var t2 = ~filebittab[old];
    o.dob |= (o.one & t);
    o.one = ((o.one & t2) | o.dob) | t;
    o.dob &= t2;
    return (pawnstrval(Depth, color) - pawnstrval(Depth - 1, color));
}

/*
 *  Calculate STATIC evaluation of the move
 */

function StatEvalu(move)
{
    var value = 0;
    if (move.spe)
        if (move.movpiece == king)
        {
            var Cast = CASTTYPE();
            GenCastSquare(move.nw1, Cast);
            value = PiecePosVal(rook, Player, Cast.castsquare) -
                    PiecePosVal(rook,Player, Cast.cornersquare);
            if (move.nw1 > move.old)
                value += castvalue[shrt-1];
            else
                value += castvalue[lng-1];
        }
        else if (move.movpiece == pawn)
        {
            var epsquare = move.nw1 - PawnDir[Player];  /*  E.p. capture  */
            value = PiecePosVal(pawn, Opponent, epsquare);
        }
        else            /*  Pawnpromotion  */
            value = PiecePosVal(move.movpiece, Player, move.old) -
                    PiecePosVal(pawn, Player, move.old) +
                    decpawnstrval(Player, move.old & 7);

    if (move.content != empty)  /*  normal moves  */
        {
            value += PiecePosVal(move.content, Opponent, move.nw1);
            /*  Penalty for exchanging pieces when behind in material  */
            if (Math.abs(MainEvalu) >= 0x100)
                if (move.content != pawn)
                    if ((ProgramColor == Opponent) == (MainEvalu >= 0))
                        value -= EXCHANGEVALUE;
        }
	/*  calculate pawnbit  */
    copyPwBt( pawnbit[black][Depth], pawnbit[black][Depth-1] );
    copyPwBt( pawnbit[white][Depth], pawnbit[white][Depth-1] );
    if ((move.movpiece == pawn) && ((move.content != empty) || move.spe))
            value += movepawnstrval(Player, move.nw1 & 7, move.old & 7);
    if ((move.content == pawn) || move.spe && (move.movpiece == pawn))
            value -= decpawnstrval(Opponent, move.nw1 & 7);
        /*  Calculate value of move  */
    return (value + PiecePosVal(move.movpiece, Player, move.nw1)-
                PiecePosVal(move.movpiece, Player, move.old));
}

/* === SEARCH with own MOVEGEN 2 === */

/*
 *  Global Variables for this module
 */

Mo = {};	// pointer to MovTab[mc] - current move
Mpre = {};	// pointer to MovTab[mc-1] - previous move by opponent

Analysis = 1;	// to display
MateSrch = 0;	// set 1 to search mate only

MaxDepth = 0;	// max.ply reached (=Depth-1)
LegalMoves = 0;
SkipSearch = 0;

rank7 = [0x60, 0x10];

timer = { started:0, elapsed:0 };
function timesecs() { return (((new Date()).getTime() / 1000 )|0); }
function InitTime() { timer.started = timesecs(); timer.elapsed = 0; }

Nodes = 0;

killingmove = [[],[]];	// [2][MAXPLY+1]
checktab = [];	//[MAXPLY+3], start from 1, not 0
/*  Square of eventual pawn on 7th rank  */
passedpawn = [];	// [MAXPLY+4], start from 2

alphawindow = 0;  /*  alpha window value  */
repeatevalu = 0;  /*  MainEvalu at ply one  */

function INFTYPE()
{
 return {
        principv:0,	/*  Principal variation search  */
        value:0,	/*  Static incremental evaluation  */
        evaluation:0	/*  Evaluation of position */
        }
}
startinf = INFTYPE();     /*  Inf at first ply  */

mane = 0; specialcap = 1; kill = 2; norml = 3;	/*  move type  */

LOSEVALUE  = 0x7D00;
MATEVALUE  = 0x7C80; 
DEPTHFACTOR  = 0x80;


function LINETYPE() { var a=[],i=0; while((i++)<MAXPLY+2) a.push( MOVETYPE() ); return a; }
function copyMLine(a) { var b=[],i=0; while(i<a.length) b.push(cloneMove(a[i++])); return b; }
function MLINE() { return { a:LINETYPE() } }	// we need object to pass as parameter

MainLine = MLINE();
MainEvalu = 0;

function SEARCHTYPE()
{
 return {
        line:MLINE(),        /*  best line at next ply  */
        capturesearch:0,  /*  indicates capture search  */
        maxval:0,       /*  maximal evaluation returned in search */
        nextply:0,          /*  Depth of search at next ply  */
        next:[],         /* information at Next ply  */
        zerowindow:0,     /*  Zero-width alpha-beta-window  */
        movgentype:0
        }
}

function PARAMTYPE()
{
 return {
        alpha:0, beta:0,
        ply:0,
        inf: INFTYPE(),
        bestline: {},
        S: SEARCHTYPE()
        }
}

preDispMv = { mxdp:0 };

function DisplayMove()
{
   if (Analysis && Depth==1)
      {
	var move = MainLine.a[1];
	if(move.movpiece && (preDispMv.mxdp<MaxDepth || !EqMove(preDispMv,move)))
	{
	preDispMv = cloneMove(move); preDispMv.mxdp = MaxDepth;
	console.log(''+ MaxDepth + " ply " +
	 	timer.elapsed + " sec. " + Nodes + " nodes " +
		sq2str(move.old)+sq2str(move.nw1));
	PrintBestMove();
	}
      }
}

function PrintBestMove()
{
   var s="",dep=1;
   while(1)
   {
	var move = MainLine.a[dep++];
	if(move.movpiece == empty) break;
	s += sq2str(move.old) + sq2str(move.nw1) + " ";
   }
   
   console.log('ev:' + EvValStr() + " " + s);
}

// evalvalue as string
function EvValStr()
{
 var e = (MainEvalu/256);
 if(Player==black) e=-e;
 return e.toFixed(2);
}

/*
 *  Initialize killingmove, checktab and passedpawn
 */
 
function clearkillmove()
{
    var dep,col,sq,i,o;

    for (dep = 0; dep <= MAXPLY; dep++)
        for (i = 0; i < 2; i++)
            killingmove[i][dep] = ZeroMove;
    checktab[0] = 0;
    passedpawn[0] = -1;  /*  No check at first ply  */
    passedpawn[1] = -1;
    /*  Place eventual pawns on 7th rank in passedpawn  */
    for (col = white; col <= black; col++)
        for (sq = rank7[col]; sq <= rank7[col] + 7; sq++)
	{
            o = Board[sq];
            if ((o.piece == pawn) && (o.color == col))
                if (col == Player)
                    passedpawn[0] = sq;
                else
                    passedpawn[1] = sq;
	}
}

/*
 *  Update killingmove using bestmove
 */

function updatekill(bestmove)
{
    if (bestmove.movpiece != empty)
    {
    /*  Update killingmove unless the move is a capture of last
        piece moved  */
        if ((Mpre.movpiece == empty) || (bestmove.nw1 != Mpre.nw1))
            if ((killingmove[0][Depth].movpiece == empty) ||
                (EqMove(bestmove, killingmove[1][Depth])))
            {
                killingmove[1][Depth] = cloneMove( killingmove[0][Depth] );
                killingmove[0][Depth] = cloneMove( bestmove );
            }
            else if (!EqMove(bestmove, killingmove[0][Depth]))
                killingmove[1][Depth] = cloneMove( bestmove );
    }
}  /*  Updatekill  */



/*
 *  Test if move has been generated before
 */

function generatedbefore(/*PARAMTYPE*/ P)
{
    if (P.S.movgentype != mane)
    {
        if( EqMove(Mo, P.bestline.a[Depth]) ) return 1;

        if (!P.S.capturesearch)
            if (P.S.movgentype != kill)
                for (var i = 0; i < 2; i++)
                    if( EqMove( Mo, killingmove[i][Depth]) )
                        return 1;
    }
    return 0;
}


/*
 *  Test cut-off.  Cutval cantains the maximal possible evaluation
 */

function cut(cutval,P)
{
    var ct = 0;
    if (cutval <= P.alpha)
    {
        ct = 1;
        if (P.S.maxval < cutval) P.S.maxval = cutval;
    }
    return ct;
}


/*
 *  Perform move, calculate evaluation, test cut-off, etc
 */
function tkbkmv() { unPerform(); return 1; }

function update(P)
{
    Nodes++;
    P.S.nextply = P.ply - 1;      /*  Calculate next ply  */
    if (MateSrch)  /*  MateSrch  */
    {
        Perform( Mo, 0 );  /*  Perform Move on the board  */
        /*  Check if Move is legal  */
        if (Attacks(Opponent, PieceTab[Player][0].isquare))
		return tkbkmv(); //TAKEBACKMOVE
        if (Depth==1) LegalMoves++;
        checktab[Depth] = 0;
        passedpawn[1+Depth] = -1;
	var d = P.S.next; d.value = 0; d.evaluation = 0;
        if (P.S.nextply <= 0)  /*  Calculate chech and perform evt. cut-off  */
        {
            if (!P.S.nextply)
                checktab[Depth] = Attacks(Player,
                    PieceTab[Opponent][0].isquare);
            if (!checktab[Depth])
                if (cut(P.S.next.value, P))
			return tkbkmv(); //TAKEBACKMOVE
        }

        DisplayMove();
        return 0;	//ACCEPTMOVE
    }
    
    /*  Make special limited capturesearch at first iteration  */
    if (MaxDepth <= 1)
        if (P.S.capturesearch && Depth >= 3)
	{
            if (!((Mo.content < Mo.movpiece)
                || (P.S.movgentype == specialcap) || (Mo.old == MovTab[mc-2].nw1)))
		{
		DisplayMove();
                return 1;	// CUTMOVE
		}
	}
            /*  Calculate nxt static incremental evaluation  */
    P.S.next.value = -P.inf.value + StatEvalu(Mo);
    /*  Calculate checktab (only checks with moved piece are calculated)
        Giving Check does not count as a ply  */
    checktab[Depth] = PieceAttacks(Mo.movpiece, Player, Mo.nw1, PieceTab[Opponent][0].isquare);
    if (checktab[Depth]) P.S.nextply = P.ply;
    /*  Calculate passedpawn.  Moving a pawn to 7th rank does not
        count as a ply  */
    passedpawn[1+Depth] = passedpawn[1+(Depth-2)];
    if (Mo.movpiece == pawn)
        if ((Mo.nw1 < 0x18) || (Mo.nw1 >= 0x60))
        {
            passedpawn[1+Depth] = Mo.nw1;
            P.S.nextply = P.ply;
        }
        /*  Perform selection at last ply and in capture search  */
    var selection = ((P.S.nextply <= 0) && !checktab[Depth] && (Depth > 1));
    if (selection)   /*  check evaluation  */
        if (cut(P.S.next.value + 0, P)) { DisplayMove(); return 1; }	// CUTMOVE
    Perform( Mo, 0 );  /*  perform move on the board  */
    /*  check if move is legal  */
    if (Attacks(Opponent, PieceTab[Player][0].isquare))
		return tkbkmv(); //TAKEBACKMOVE
    var p = passedpawn[1+Depth];
    if (p >= 0)  /*  check passedpawn  */
        {
	var b = Board[p];
        if (b.piece != pawn || b.color != Player)
            passedpawn[1+Depth] = -1;
        }
    if (Depth==1)
    {
        LegalMoves++;
        P.S.next.value += ((Math.random()*4)|0);
    }
    P.S.next.evaluation = P.S.next.value;
//ACCEPTMOVE:
    DisplayMove();
    return 0;
}


/*
 *  Calculate draw bonus/penalty, and set draw if the game is a draw
 */

function drawgame(/*SEARCHTYPE*/ S)
{
    var o = S.next;
    if (Depth == 2)
    {
        var searchfifty = FiftyMoveCnt();
        var searchrepeat = Repetition(0);
        if (searchrepeat >= 3)
        {
            o.evaluation = 0;
            return 1;
        }
        var drawcount = 0;
        if (searchfifty >= 96)  /*  48 moves without pawn moves or captures */
            drawcount = 3;
        else
        {
            if (searchrepeat >= 2)  /*  2nd repetition  */
                drawcount = 2;
            else if (searchfifty >= 20)  /*  10 moves without pawn moves or  */
               drawcount = 1;        /*  captures  */
        }
	var n = ((repeatevalu * drawcount)/4)|0;	// int
        o.value += n;
        o.evaluation += n;	//int
    }
    if (Depth >= 4)
    {
        var searchrepeat = Repetition(1);
        if (searchrepeat >= 2)       /*  Immediate repetition counts as  */
        {                            /*  a draw                          */
            o.evaluation = 0;
            return 1;
        }
    }
    return 0;
}


/*
 *  Update bestline and MainEvalu using line and maxval
 */

function updatebestline(P)
{
    P.bestline.a = copyMLine( P.S.line.a );
    P.bestline.a[Depth] = cloneMove( Mo );	/* copies to new MOVETYPE() */

    if (Depth==1)
    {
        MainEvalu = P.S.maxval;
        if (MateSrch) P.S.maxval = alphawindow;
	DisplayMove();
    }
}


/*
 *  The inner loop of the search procedure.  MovTab[mc] contains the move.
 */

function loopbody(P)
{
    if (generatedbefore(P)) return 0;
    if (Depth < MAXPLY)
    {
        if (P.S.movgentype == mane)
            P.S.line.a = copyMLine( P.bestline.a );
        P.S.line.a[Depth+1] = ZeroMove;
    }
    /*  principv indicates principal variation search  */
    /*  Zerowindow indicates zero - width alpha - beta window  */
    P.S.next.principv = 0;
    P.S.zerowindow = 0;
    if (P.inf.principv)
        if (P.S.movgentype == mane)
            P.S.next.principv = (P.bestline.a[Depth+1].movpiece != empty);
        else
            P.S.zerowindow = (P.S.maxval >= P.alpha);
	    
    while(1)
    {
//REPEATSEARCH:

    if (update(P)) return 0;
    var f=1;
    if (MateSrch)  /*  stop evt. search  */
        if ((P.S.nextply <= 0) && !checktab[Depth]) f=0;
    if (f && drawgame(P.S)) f=0;
    if (f && Depth >= MAXPLY) f=0;
    if(f)
    {
    /*  Analyse nextply using a recursive call to search  */
    var oldplayer = Player;
    Player = Opponent;
    Opponent = oldplayer;
    Depth++;
    if (P.S.zerowindow)
        P.S.next.evaluation = -search(-P.alpha - 1, -P.alpha, P.S.nextply,
                P.S.next, P.S.line );
    else
        P.S.next.evaluation = -search(-P.beta, -P.alpha, P.S.nextply,
                P.S.next, P.S.line );
    Depth--;
    oldplayer = Opponent;
    Opponent = Player;
    Player = oldplayer;
    }
//NOTSEARCH:
    unPerform();  /*  take back move  */
    if (SkipSearch)
        return 1;
    if (Analysis)
    {
     if (MainEvalu > alphawindow) SkipSearch = timeused();
     if (MaxDepth <= 1) SkipSearch = 0;
    }
    P.S.maxval = Math.max(P.S.maxval, P.S.next.evaluation);  /*  Update Maxval  */
    if( EqMove(P.bestline.a[Depth], Mo ))  /*  Update evt. bestline  */
        updatebestline(P);
    if (P.alpha < P.S.maxval)      /*  update alpha and test cutoff */
    {
        updatebestline(P);
        if (P.S.maxval >= P.beta)
            return 1;
        /*  Adjust maxval (tolerance search)  */
        if (P.ply >= 2  && P.inf.principv && !P.S.zerowindow)
            P.S.maxval = Math.min(P.S.maxval + TOLERANCE, P.beta - 1);
        P.alpha = P.S.maxval;
        if (P.S.zerowindow && ! SkipSearch)
        {
            /*  repeat search with full window  */
            P.S.zerowindow = 0;
            continue; //goto REPEATSEARCH;
        }
    }
    break;
    }
    
    return SkipSearch;
}


/*
 *  generate  pawn promotions
 */

function pawnpromotiongen(P)
{
    Mo.spe = 1;
    for (var promote = queen; promote <= knight; promote++)
    {
        Mo.movpiece = promote;
        if (loopbody(P)) return 1;
    }
    Mo.spe = 0;
    return 0;
}


/*
 *  Generate captures of the piece on Newsq
 */

function capmovgen( newsq, P )
{
    var nxtsq,sq,i,p,q,m,b;
    Mo.content = Board[newsq].piece;
    Mo.spe = 0;
    Mo.nw1 = newsq;
    Mo.movpiece = pawn;  /*  pawn captures  */
    nxtsq = Mo.nw1 - PawnDir[Player];
    for (sq = nxtsq - 1; sq <= nxtsq + 1; sq++)
        if (sq != nxtsq)
            if (!(sq & 0x88))
		{
		b = Board[sq];
                if (b.piece == pawn && b.color == Player)
                {
                    Mo.old = sq;
                    if (Mo.nw1 < 8 || Mo.nw1 >= 0x70)
                    {
                        if (pawnpromotiongen(P))
                            return 1;
                    }
                    else if (loopbody(P))
                        return 1;
                }
		}
    for (i = OfficerNo[Player]; i >= 0; i--)  /*  other captures  */
	{
	m = PieceTab[Player][i]; p = m.ipiece; q = m.isquare;

        if (p != empty && p != pawn)
            if (PieceAttacks(p, Player, q, newsq))
            {
                Mo.old = q;
                Mo.movpiece = p;
                if (loopbody(P)) return 1;
            }
	}
    return 0;
}
              

/*
 *  Generates non captures for the piece on oldsq
 */

function noncapmovgen( oldsq, P )
{
    var first, last, dir, direction, newsq;
    Mo.spe = 0;
    Mo.old = oldsq;
    Mo.movpiece = Board[oldsq].piece;
    Mo.content = empty;

    switch (Mo.movpiece)
    {
        case king :
            for (dir = 7; dir >= 0; dir--)
            {
                newsq = Mo.old + DirTab[dir];
                if (!(newsq & 0x88))
                    if (Board[newsq].piece == empty)
                    {
                        Mo.nw1 = newsq;
                        if (loopbody(P))
                             return 1;
                    }
            }
            break;
        case knight :
            for (dir = 7; dir >= 0; dir--)
            {
                newsq = Mo.old + KnightDir[dir];
                if (!(newsq & 0x88))
                    if (Board[newsq].piece == empty)
                    {
                        Mo.nw1 = newsq;
                        if (loopbody(P))
                            return 1;
                    }
            }
            break;
        case queen :
        case rook  :
        case bishop :
            first = 7;
            last = 0;
            if (Mo.movpiece == rook) first = 3;
            else if (Mo.movpiece == bishop) last = 4;
            for (dir = first; dir >= last; dir--)
            {
                direction = DirTab[dir];
                newsq = Mo.old + direction;
                while (!(newsq & 0x88))
                {
                    if (Board[newsq].piece != empty) break;	// goto TEN
                    Mo.nw1 = newsq;
                    if (loopbody(P))
                        return 1;
                    newsq = Mo.nw1 + direction;
                }
//TEN:
                continue;
            }
            break;
        case pawn :
            /*  One square forward  */
            Mo.nw1 = Mo.old + PawnDir[Player];
            if (Board[Mo.nw1].piece == empty)
                if (Mo.nw1 < 8 || Mo.nw1 >= 0x70)
                {
                    if (pawnpromotiongen(P)) return 1;
                }
                else
                {
                    if (loopbody(P))
                        return 1;
                    if (Mo.old < 0x18 || Mo.old >= 0x60)
                    {
                        /*  two squares forward  */
                        Mo.nw1 += (Mo.nw1 - Mo.old);
                        if (Board[Mo.nw1].piece == empty)
                            if (loopbody(P))
                                return 1;
                    }
               }
    } /*  switch  */
    return 0;
}


/*
 *  castling moves
 */

function castlingmovgen(P)
{
    Mo.spe = 1;
    Mo.movpiece = king;
    Mo.content = empty;
    for (var castdir = (lng-1); castdir <= shrt-1; castdir++)
    {
        var m = CastMove[Player][castdir];
        Mo.nw1 = m.castnew;
        Mo.old = m.castold;
        if (KillMovGen(Mo))
            if (loopbody(P))
                return 1;
    }
    return 0;
}


/*
 *  e.p. captures
 */

function epcapmovgen(P)
{
    if (Mpre.movpiece == pawn)
        if (Math.abs(Mpre.nw1 - Mpre.old) >= 0x20)
        {
            Mo.spe = 1;
            Mo.movpiece = pawn;
            Mo.content = empty;
            Mo.nw1 = (Mpre.nw1 + Mpre.old) / 2;
            for (var sq = Mpre.nw1 - 1; sq <= Mpre.nw1 + 1; sq++)
                if (sq != Mpre.nw1)
                    if (!(sq & 0x88))
                    {
                        Mo.old = sq;
                        if (KillMovGen(Mo))
                            if (loopbody(P))
                                return 1;
                    }
        }
    return 0;
}


/*
 *  Generate the next move to be analysed.
 *   Controls the order of the movegeneration.
 *      The moves are generated in the order:
 *      Main variation
 *      Captures of last moved piece
 *      Killing moves
 *      Other captures
 *      Pawnpromotions
 *      Castling
 *      Normal moves
 *      E.p. captures
 */

function searchmovgen(P)
{
    var index, w = P.bestline.a[Depth], p;

    copyMove(Mo,ZeroMove);
    
    /*  generate move from the main variation  */
    if (w.movpiece != empty)
    {
        copyMove( Mo, w );
        P.S.movgentype = mane;
        if (loopbody(P)) return;
    }
    if (Mpre.movpiece != empty)
        if (Mpre.movpiece != king)
        {
            P.S.movgentype = specialcap;
            if (capmovgen(Mpre.nw1, P)) return;
        }
    P.S.movgentype = kill;
    if (!P.S.capturesearch)
        for (var killno = 0; killno <= 1; killno++)
        {
            copyMove( Mo, killingmove[killno][Depth] );
            if (Mpre.movpiece != empty)
                if (KillMovGen(Mo))
                    if (loopbody(P)) return;
        }
    P.S.movgentype = norml;
    for (index = 1; index <= PawnNo[Opponent]; index++)
        {
	w = PieceTab[Opponent][index];
        if (w.ipiece != empty)
            if (Mpre.movpiece == empty || w.isquare != Mpre.nw1)
                if (capmovgen(w.isquare, P))
                    return;
        }
    if (P.S.capturesearch)
    {
        p = passedpawn[1+(Depth-2)];
        if (p >= 0)
            {
	    w = Board[p];
            if (w.piece == pawn && w.color == Player)
                if (noncapmovgen(p, P)) return;
            }
    }
    if (!P.S.capturesearch)                /*  non-captures  */
    {
        if (castlingmovgen(P))
            return;      /*  castling  */
        for (index = PawnNo[Player]; index >= 0; index--)
            {
	    w = PieceTab[Player][index];
            if (w.ipiece != empty)
                if (noncapmovgen(w.isquare, P)) return;
            }
    }
    if (epcapmovgen(P))
        return;  /*  e.p. captures  */
}


/*
 *  Perform the search
 *  On entry :
 *    Player is next to move
 *    MovTab[Depth-1] contains last move
 *    alpha, beta contains the alpha - beta window
 *    ply contains the Depth of the search
 *    inf contains various information
 *
 *  On exit :
 *    Bestline contains the principal variation
 *    search contains the evaluation for Player
 */

function search( alpha, beta, ply, inf, bestline)
{
    var S = SEARCHTYPE(), P = PARAMTYPE();
    /*  Perform capturesearch if ply <= 0 and !check  */
    S.capturesearch = ((ply <= 0) && !checktab[Depth-1]);
    if (S.capturesearch)  /*  initialize maxval  */
    {
        S.maxval = -inf.evaluation;
        if (alpha < S.maxval)
        {
            alpha = S.maxval;
            if (S.maxval >= beta) return S.maxval;	//goto STOP
        }
    }
    else
    {
        S.maxval = -(LOSEVALUE - (Depth-1)*DEPTHFACTOR);
    }
    P.alpha = alpha;
    P.beta = beta;
    P.ply = ply;
    P.inf = inf;
    P.bestline = bestline;
    P.S = S;
    searchmovgen(P);   /*  The search loop  */
    if (SkipSearch) return S.maxval;	// goto STOP
    if (S.maxval == -(LOSEVALUE - (Depth-1) * DEPTHFACTOR))   /*  Test stalemate  */
        if (!Attacks(Opponent, PieceTab[Player][0].isquare))
        {
            S.maxval = 0;
            return S.maxval;	//goto STOP
        }
    updatekill(P.bestline.a[Depth]);
//STOP:
    return S.maxval;
}


/*
 *  Begin the search
 */

function callsearch( alpha, beta )
{
    startinf.principv = (MainLine.a[1].movpiece != empty);
    LegalMoves = 0;
    var maxval = search(alpha, beta, MaxDepth, startinf, MainLine );
    if (!LegalMoves)
        MainEvalu = maxval;
    return maxval;
}


/*
 *  Checks whether the search time is used
 */

function timeused()
{
   if (Analysis)
    {
    timer.elapsed = timesecs() - timer.started;
    return (timer.elapsed >= MAXSECS);
    }
    return 0;
}


/*
 *  setup search (Player = color to play, Opponent = opposite)
 */

function FindMove()
{
    ProgramColor = Player;
    InitTime();
    Nodes = 0;
    SkipSearch = 0;
    clearkillmove();
    pawnbit = [ PwBtList(), PwBtList() ];
    CalcPVTable();
    startinf.value = -RootValue;
    startinf.evaluation = -RootValue;
    MaxDepth = 0;
    MainLine = MLINE();
    MainLine.a = LINETYPE();
    MainEvalu = RootValue;
    alphawindow = MAXINT;

    do
    {
        /*  update various variables  */
        if (MaxDepth <= 1) repeatevalu = MainEvalu;
        alphawindow = Math.min(alphawindow, MainEvalu - 0x80);
        if (MateSrch)
        {
            alphawindow = 0x6000;
            if (MaxDepth > 0) MaxDepth++;
        }
        MaxDepth++;
        var maxval = callsearch(alphawindow, 0x7f00);  /*  perform the search  */
        if (maxval <= alphawindow && !SkipSearch && !MateSrch &&  LegalMoves > 0)
        {
            /*  Repeat the search if the value falls below the
                    alpha-window  */
            MainEvalu = alphawindow;
            maxval = callsearch(-0x7F00, alphawindow - TOLERANCE * 2);
            LegalMoves = 2;
        }
    } while (!SkipSearch && !timeused() && (MaxDepth < MAXPLY) &&
            (LegalMoves > 1) &&
            (Math.abs(MainEvalu) < MATEVALUE - 24 * DEPTHFACTOR));

   DisplayMove();
   PrintBestMove();
   //printboard();
   return retMvStr();
}

function retMvStr()
{
   var ret = "", move = MainLine.a[1], p = move.movpiece;
   if(move.movpiece)
    {
    ret = sq2str(move.old)+sq2str(move.nw1);
    if( move.spe && (p!=pawn && p!=king)) ret+="qrbn".charAt(p-2);
    }
   return ret;
}

/* === STARTING === */


// initiate engine
function initEngine()
{
CalcAttackTab();
ResetGame();
}

/* === Opening book === */

/*decodes*/
function UnBase64(s){var a,i,q=window.atob(s);a=new Uint8Array(q.length);for(i in a)a[i]=q.charCodeAt(i);return a};
function UnDecode(n,s){var i,k,a,w="-_:*{}[]()!@#$%^&~<>,.`?|;";for(k=n;k;k--){a=s.split(w[k-1]);for(i in a)if(i&1)a[i]=(new Array(parseInt(a[i])+1)).join(a[i-1].substr(a[i-1].length-k));s=a.join("").toString();}return UnBase64(s)};

/* Globals */
LibNo = 0;		// [0...32000]
OpCount = 0;		// current move in list
LibMc = 0;
LibMTab = [];
UseLib = 200;
LibFound = 0;

UNPLAYMARK = 0x3f;
 
/*
 *  Sets libno to the previous move in the block
 */

function PreviousLibNo()
{
 var n = 0;
 do { 
        LibNo--;
	var o = Openings[LibNo];
        if (o>= 128) n++;
        if (o & 64) n--;
    } while (n);
}

/*
 *  Set libno to the first move in the block
 */
 
function FirstLibNo()
{
    while (!(Openings[LibNo-1] & 64)) 
        PreviousLibNo();
}

/*
 *  set libno to the next move in the block.  Unplayable
 *  moves are skipped if skip is set
 */

function NextLibNo(skip)
{
    if (Openings[LibNo] >= 128) FirstLibNo();
    else
    {
        var n = 0;
        do
        {
            var o = Openings[LibNo];
            if (o & 64) n++;
            if (o >= 128) n--;
            LibNo++;
        } while (n);
        if (skip && (Openings[LibNo] == UNPLAYMARK))
            FirstLibNo();
    }
}

/*
 *  find the node corresponding to the correct block
 */

function FindNode()
{
    var o;
    LibNo++;
    if (mc >= LibMc)
    {
        LibFound = 1;
        return;
    }
    OpCount = -1;
    InitMovGen();
    for(var i=0;i<BufCount;i++)
    {
        OpCount++;
        MovGen();
        if(EqMove(Next, LibMTab[mc])) break;
    }

    if (Next.movpiece != empty)
    {
        while (1)
        {
        o = Openings[LibNo];
        if (((o & 63) == OpCount) || (o >= 128)) break;
        NextLibNo(0);
        }
	    
	    
        if ((o & 127) == (64+OpCount))
        {
            DoMove( Next );
            FindNode();
            UndoMove();

        }
    }
}



function CalcLibNo()
{
    LibNo = 0;
    if (mc <= UseLib)
    {
        LibMTab = copyMLine(MovTab);
        LibMc = mc;
        ResetGame();
        LibFound = 0;
        FindNode();
        while(mc < LibMc)
            {
            DoMove( LibMTab[mc] );
	    }
        if (!LibFound)
        {
            UseLib = mc-1;
            LibNo = 0;
        }
    }
}

/*
 *  find an opening move from the library,
 *  return move string or "", also sets LibFound
 */

function FindOpeningMove()
{
    Nodes = 0;
    CalcLibNo();
    if (!LibNo) return "";

	
    var weight = [7, 10, 12, 13, 14, 15, 16];	// [7]
    var cnt=0, p=0, countp=1;

    var r = (Math.random()*16)|0;   /*  calculate weighted random number in 0..16  */
    while (r >= weight[p]) p++;
    for (; countp <= p; countp++)  /* find corresponding node */
        NextLibNo(1);
    OpCount = Openings[LibNo] & 63;  /*  generate the move  */

    InitMovGen();
    for(var i=0;cnt<=OpCount && i<BufCount;i++)
    {
        MovGen();
        cnt++;
    }
    
                          /* store the move in mainline  */
    MainLine = MLINE();
    MainLine.a[1] = cloneMove(Next);
    MainEvalu = 0;
    PrintBestMove();
    return retMvStr();
}

/*data OPENING.LIB */
Openings=UnDecode(4,"/0FDT09WR1pRQlRVRdJBREFHTM/EwkHAVlDWz91U2kLLzYU/zpVb2s+GP1XEQ4TBw9CGz4JCysOLTthJm+DY2Jo/RolFiknXwMHSgMDAwMDA19feS4JDgz/ak*1*D/c0dTR39HHiD/Lw83P0tDZhz/D1lXBwcPCjcPMzcPA2s+CP8KDP0KSS4RDw0iCxNZVwcHDwo3DzM3DwNrPgkjBxEzPxMJBwFZQ1s/dVNpCy82FP86VW9rPhj9VxEOEwcPQhs+CQsrDi07YSZvg2NiaP0aJRYpJ18DB0oDAwMDAwNfX3kuCQ4M/2p*1*A/3NHU0d/Rx4g/y8PNz9LQ2YdPwc/LxcDCwMCE08DAytHQwpLCxMFHTM/EwkHAVlDWz91U2kLLzYU/zpVb2s+GP1XEQ4TBw9CGz4JCysOLTthJm+DY2Jo/RolFiknXwMHSgMDAwMDA19feS4JDgz/ak*1*D/c0dTR39HHiD/Lw83P0tDZhz/D1lXBwcPCjcPMzcPA2s+CP8LExJFAwUGPP1OPQMHZz9mOUdPGlcKCP0PAw9DBwMCV1sXUwcXCRJ7C1sDCxMDD0tSVQENB28LC3sHB6eXBwKbIVMRA2Vpd21dWwoU/wcDSmT/ChT9f1uvc6JPX2ttWwoU/wcDSmT/CnT/Kgz/DwMCCP9XYQYHAwcDVw48/QcRUg8DD2NrUwMHAgdbBwMLB2uDA0YY/Q8TVlMTAw9XD0MHAwJU/Q8XAxdbD09TC4MKdRdTNxMGCwMJWydnEWsbTgz/Qw8OD08NG1EHAwMXA3kio2cXM49XLwcDbhsDEwKPAgMDBUoLBwsLQQ5U/wIBVQk3AgcGEP8CBP0PEQcPPwcCCUIHAwc7DweLWikDE05NM1NvAwYHQwsDDWeDUgEHfwoLUwMGAVVZFUkTAQdhaQMHEwMHdz8LA0sDI3Ic/wsDAwMDUw4s/T8LCwUCFwuBa0MDDQsPBlNjAwMtkwNnSwIZTz2nAwNVCws/V5YFkwNrDwJbl40yAwcDYzGGJzJ3AwOHjTIDBwNjMYYnMnWfC5IvBwODKwpM/wIHphMXE2M/BnUjZgz/CwcPCwcXAwNviwYA/18TXw8PI0Z4/RMBFwEDA4+jA1sXA04TCwWik4tTTw8LAweHM0OuOP0PAxNjhwMLowJDO00HAwMDcxcCBQsDAwNzFwIHn3cWDScHF1MTDQcHAwMGS49/ClsPB0tFeydyHXJzCmdFOQsJNWsPCQWHdGD/CwMHNxc/Swd/Y5YE/2sXQwc/gxtjEwcLCxNrUwtLAwOHMhD/CmD/Eys/D4MDAmURCwdbC4MDA08LZgdtKx0nAwMHBwNDAwII/wMPAwMCAP0DB2sXEw8CGwZVLx0bYScDAwcHA0MDAgj/Aw8DAwIA/0cXAhD/AxWDG4cLA4OybWcDAwcChXcLA3+zNzODEzs2fP0HEWN7mxMKi3MLAnsLHz4E/QMPhxMbUwcDWjNXC4dnb0cPF18DAw9aeP0TARNLAwM3jXtnExtnhwIE/wMrPgUPA4MJRwcLawpHDwNHvQdPBhMDS1ITOz9vLxcDAwUKTQcDX5cGCQ8LkxMHyxIM/2oJC1sTBw8LBxcDA2+LBgNLAQMHbwMHfwIBBwMDB1ePDiMPAwcPNxsOEQkBAUlTYwMHRxEDhYcDCwc3Z2d/ijj/owdrC4oE/w8zchj/AwEHX3c3E0M6bUsJCytzXwsZChsqCXpXP2EDhYcDCwc3Z2d/ijj/owdrC4oE/w8zchs7T0cRA4WHAwsHN2dnf4o4/6MHawuKBP8PM3IbXXV5E1c3AwdydP9PAwNbGgj/W1MLX0OfRwMDD1dPChNDYy8fQ09qEP1dXRcBAwOPowNbFwNOEwsFopOLU08PCwMHhzNDrjj9DwMTY4cDC6MCQztNBwMDA3MXAgULAwMDcxcCB593Fg9JE0sDAzeNe2cTG2eHAgT/Ays+BQ8DgwlHBwtrCkcPA0e9B08GEwNLUhM7P28vFwMDBQpNBwNflwYJDwuTEwfLEgz/agsPCQtHdgcPBwNTN39LT18LAwMGEP8KAUVFXVkJCwkDAwVnNwdXfhMaIQtkAP1XPGz/JwEDRzeFFwMDOwZ3CwkCj3c/sgeKDx8DA0NOSP8vAwc3D08LDyoU/Q9DbhczAwcHAwdbAwZM/w8PCwMDBWc3B1d+Exog/VcJDw8DB1pDCwcDC2cTPwVbDysDEiNSDRsDBwMHA38LB1M6By8DAwsDAQ8HYjz/BwcHBzdTX2kCdwt7YzsimP0LAUcxbwMCUw8TRxcDAxKA/wNjAwdHEQOFhwMLBzdnZ3+KOP+jB2sLigT/DzNyG1sBAwdvAwd/AgEHAwMHV48OIw8DBw83Gw4Q/RsLAwNTd0oBAlkPDwsLR21ymw8LA5cXDUZzAg1PW04RWwEHAwMFZ4lGc0sDgi9GRwMHawMDCwMBOwdWLwNvXjtfPUMDAwNvDxcvAwMGBVoRYhNTET8TCytWhwcjWzMbAwJE/w9HCwkLRwJPAxNXJ2dzX2dPCxMPS3c+DUEJAws9Bwu3C0s3wRNjY4dbF6Y0/1trfsj/f2MDOwIE/wdXDwl3CwpbcwtCGQEHQwEHB09zBz9yTP9GAP87AwcHXkT9Vzk5Cwk1aw8JBYd0YP8LAwc3Fz9LB39jlgT/axdDBz+DG2MTBwsLE2tTC0sDA4cyEP8KYP8TKz8PgwMCZRELB1sLgwMDTwtmB20rHScDAwcHA0MDAgj/Aw8DAwIA/QMHaxcTDwIbBlUvHRthJwMDBwcDQwMCCP8DDwMDAgD/RxcCEP8DFYMbhwsDg7JtZwMDBwKFdwsDf7M3M4MTOzZ8/QcRY3ubEwqLcwsCewsfPgT9Aw+HExtTBwNaM1cLh2dvRw8XXwMDD1p4/RMBE0sDAzeNe2cTG2eHAgT/Ays+BQ8DgwlHBwtrCkcPA0e9B08GEwNLUhM7P28vFwMDBQpNBwNflwYJDwuTEwfLEgz/agkLWxMHDwsHFwMDb4sGA0sBAwdvAwd/AgEHAwMHV48OIw8DBw83Gw4TRzldWQkLCQMDBWc3B1d+ExohC2QA/Vc8bP8nAQNHN4UXAwM7BncLCQKPdz+yB4oPHwMDQ05I/y8DBzcPTwsPKhT9D0NuFzMDBwcDB1sDBkz/Dw8LAwMFZzcHV34TGiD9VwkPDwMHWkMLBwMLZxM/BVsPKwMSI1INGwMHAwcDfwsHUzoHLwMDCwMBDwdiPP8HBwcHN1NfaQJ3C3tjOyKY/QsBRzFvAwJTDxNHFwMDEoD/A2MDB0cRA4WHAwsHN2dnf4o4/6MHawuKBP8PM3IbWwEDB28DB38CAQcDAwdXjw4jDwMHDzcbDhD9AwkDAx9TXgVDWUsHAjVWBTMLAgsLD1MHAzdGRP0XAw8zB08+E0cDB5Fbg28TDlMXZ2oRFwVnTwdNnwdWZ4sHBkj/PgMHBwdpf0uDUS4TbhdLTV5DawMHAwJ9DQE5AVktEyEdWREnQxMfWwsDC2M3LwsHMgD/OxteCP8LJzc/PgT9IwsDB0MDAgs3E18bXgj9GzMbSxM3TiMzWx87j3cXSwMDAwcDAwfTBgz9IzdvQ05rL0NLZw5M/yMzIzNLH18TThj9CyEvITNhPzczDysXByMDP05zO2M+Ey8VOwN2bwsBB1NKNwtbf0cuO18pHVkRJ0MTH1sLAwtjNy8LBzIA/zsbXgj/Cyc3Pz4E/SMLAwdDAwILNxNfG14I/RszG0sTN04jM1sfO493F0sDAwMH*1*0wYNByMvIzJjSiz9QwE7B1cGS0tPBw8fAwMDBl0HA0JdT0tTdwIPJy8nN0cXBzcDC2sPioD/U0VDEXsLB2cLW3cLCodnCwMDD18vAxMTAysqJP8GBUUDAzkLAlz/Vgj/Rw0DBW8bEwMCAQsDAwMrOQJnPk87dXMfDl0LAwMBOg8CCw+JgxMKR18VAwcCAwcTWzeLf6sHD2sHBwtvbwcCjQcFBwMDe2dPJz8KE24JDw83NzdbUwMGUP8SdVs/BU9DYzsNXl03C1sDAg0LW3oDZ1WDAwIbOwMCGP8XQwMDCwMCCP8LQQcHQgz/AwNCXVVBBw93RzsHXwlTNxZg/wIA/QsBCgc/AwtLh0tOD0cBi09/Sz87GwM3Jy8vjw8GDQcDAwdnk0pvPwMHFQsDNx8PZw9nfks7IwsDD2cPZ35I/z0HRkM9WRVJEwEHYWkDBxMDB3c/CwNLAyNyHP8LAwMDA1MOLP0/CwsFAhcLgWtDAw0LDwZTYwMDLZMDZ0sCGU89pwMDVQsLP1eWBZMDaw8CW5eNMgMHA2MxhicydwMDh40yAwcDYzGGJzJ1nwuSLwcDgysKTP8CB6YTFxNjPwZ1I2YM/wsHDwsHFwMDb4sGAP9fE18PDyNGeP0TARcBAwOPowNbFwNOEwsFopOLU08PCwMHhzNDrjj9DwMTY4cDC6MCQztNBwMDA3MXAgULAwMDcxcCB593Fg0nBxdTEw0HBwMDBkuPfwpbDwdLRXsnch1ycwpnRTkLCTVrDwkFh3Rg/wsDBzcXP0sHf2OWBP9rF0MHP4MbYxMHCwsTa1MLSwMDhzIQ/wpg/xMrPw+DAwJlEQsHWwuDAwNPC2YHbSsdJwMDBwcDQwMCCP8DDwMDAgD9AwdrFxMPAhsGVS8dG2EnAwMHBwNDAwII/wMPAwMCAP9HFwIQ/wMVgxuHCwODsm1nAwMHAoV3CwN/szczgxM7Nnz9BxFje5sTCotzCwJ7Cx8+BP0DD4cTG1MHA1ozVwuHZ29HDxdfAwMPWnj9EwETSwMDN417ZxMbZ4cCBP8DKz4FDwODCUcHC2sKRw8DR70HTwYTA0tSEzs/by8XAwMFCk0HA1+XBgkPC5MTB8sSDP9qCQtbEwcPCwcXAwNviwYDSwEDB28DB38CAQcDAwdXjw4jDwMHDzcbDhD9R0EPDQMFbxsTAwIBCwMDAys5Amc+Tzt1cx8OXQsDAwE6DwILD4mDEwpHXxUDBwIDBxNbN4t/qwcPawcHC29vBwKNBwUHAwN7Z08nPwoTbgkPDzc3N1tTAwZQ/xJ1NzldWQkLCQMDBWc3B1d+ExohC2QA/Vc8bP8nAQNHN4UXAwM7BncLCQKPdz+yB4oPHwMDQ05I/y8DBzcPTwsPKhT9D0NuFzMDBwcDB1sDBkz/Dw8LAwMFZzcHV34TGiD9VwkPDwMHWkMLBwMLZxM/BVsPKwMSI1INGwMHAwcDfwsHUzoHLwMDCwMBDwdiPP8HBwcHN1NfaQJ3C3tjOyKY/QsBRzFvAwJTDxNHFwMDEoD/A2MDB0cRA4WHAwsHN2dnf4o4/6MHawuKBP8PM3IbWwEDB28DB38CAQcDAwdXjw4jDwMHDzcbDhD/WwGLT39LPzsbAzcnLy+PDwYNBwMDB2eTSm8/AwcVCwM3Hw9nD2d+SzsjCwMPZw9nfksHAQM9gxtHU2tLBwtaB7dJPgtyUVtFCgc/AwtLh0tODT9BAUlTYwMHRxEDhYcDCwc3Z2d/ijj/owdrC4oE/w8zchj/AwEHX3c3E0M6bUsJCytzXwsZChsqCXpXP2EDhYcDCwc3Z2d/ijj/owdrC4oE/w8zchs7T0cRA4WHAwsHN2dnf4o4/6MHawuKBP8PM3IbXXV5E1c3AwdydP9PAwNbGgj/W1MLX0OfRwMDD1dPChNDYy8fQ09qEP1dXRcBAwOPowNbFwNOEwsFopOLU08PCwMHhzNDrjj9DwMTY4cDC6MCQztNBwMDA3MXAgULAwMDcxcCB593Fg9JE0sDAzeNe2cTG2eHAgT/Ays+BQ8DgwlHBwtrCkcPA0e9B08GEwNLUhM7P28vFwMDBQpNBwNflwYJDwuTEwfLEgz/agsPCQtHdgcPBwNTN39LT18LAwMGEP8TA1cDBwcLbT8HAkGnQ3c7CwMCU0cHA0OKDRVBCQkDATkbT1NlAwVLFwMKSP8HLX+bCwcHC4IZapuOmP8OFVEpAwBPC2cOEWOFGwMDQ3IjfyMDBDT/GlT9GwduV3dNYmdmDP9PRR49YyteIwdrC28TNw5BTQFxBWcbP0NbZwMGMP9rGUJDO0M7Dz5o/0JQ/x9FR19PBwYPTxs7X08HBw9fNw4s/UtRHj1jK14jB2sLbxM3DkNecP0eC19PUmT9J2UzAwMHBhtKSVJDP1EiCVcFR20HA3ILAwFCf1N/A28eUTuRAwdvMUMHA1tmcP2HQ4pHVneaBP97AwMDB4sHE1dPZ38GQP03RR49YyteIwdrC28TNw5DBxFSbTJvHmz/CgT9Sz8LAwEpAwBPC2cOEWOFGwMDQ3IjfyMDBDT/GlT9GwduV3dNYmdmDP9PRR49YyteIwdrC28TNw5DDwUCBwc2BUELAQFPUQllDYdfpwsTImUNX38Lcmj/WrD/eiD9WwkfAwc7TnMOsXtPI3uPSw6tEjsfAwIM/RMfbwMCVV8ff2mfCwsDA0enaypXBwkPfZNHRguOR5JDfw0HAwkDhkJfqx8TVxtXRxcSEP0TRR49YyteIwdrC28TNw5DIwEDBytRBrFTe0cLCx9qMz6XBwMDTVp3cwsuEQtXBwMCBQ0bT1NlAwVLFwMKSP8HLX+bCwcHC4IZapuOmP8OFVEpAwBPC2cOEWOFGwMDQ3IjfyMDBDT/GlT9GwduV3dNYmdmDP9PRR49YyteIwdrC28TNw5BTQFxBWcbP0NbZwMGMP9rGUJDO0M7Dz5o/0JQ/x9FR19PBwYPTxs7X08HBw9fNw4s/UtRHj1jK14jB2sLbxM3DkNecP0eC19PUmT9J1EHdUcLa10LrVcNh22PHTObO5Mqj0OZM5MqjzsDAzO3Z2oXQxUvl4+TKoz/l5kzkyqPOwMDM7dnahWPJSaFekcbB3dbKysGC3dvPxUulzcDAxsPE0cnig0nNytHBgMDA0ZrW613eUMJOwMDNQ5rIw8GLP8PFS6XNwMDGw8TRyeKDwsPPxUulzcDAxsPE0cnigz/Cw2HbY8dM5s7kyqPQ5kzkyqPOwMDM7dnahdDFS+Xj5MqjP+XmTOTKo87AwMzt2dqFY8lJoV6RxsHd1srKwYLd28/FS6XNwMDGw8TRyeKD2MLS10LrVcNh22PHTObO5Mqj0OZM5MqjzsDAzO3Z2oXQxUvl4+TKoz/l5kzkyqPOwMDM7dnahWPJSaFekcbB3dbKysGC3dvPxUulzcDAxsPE0cnig0nNytHBgMDA0ZrW613eUMJOwMDNQ5rIw8GLP8PFS6XNwMDGw8TRyeKDwsPPxUulzcDAxsPE0cnigz/Cw2HbY8dM5s7kyqPQ5kzkyqPOwMDM7dnahdDFS+Xj5MqjP+XmTOTKo87AwMzt2dqFY8lJoV6RxsHd1srKwYLd28/FS6XNwMDGw8TRyeKDUsBAwcrUQaxU3tHCwsfajM+lwcDA01ad3MLLhNTN04RW09uGQcXDjMTSQNPGzFTM3ZPEj8fRwMDVwMHNUJhDwcqSy9vFysTAwJc/w8DByNfUYcHD0uvB5JjCwUfT58Lhlj/f0uvB5JhXSMHUxJM/yoA/Q8HBltLCwsDASkDAE8LZw4RY4UbAwNDciN/IwMENP8aVP0bB25Xd01iZ2YM/09FHj1jK14jB2sLbxM3DkEBCQEBQVE1ZQ2HX6cLEyJlDV9/C3Jo/1qw/3og/VsJHwMHO05zDrF7TyN7j0sOrRI7HwMCDP0TH28DAlVfH39pnwsLAwNHp2sqVwcJD32TR0YLjkeSQ38NBwMJA4ZCX6sfE1cbV0cXEhD9CxFSbTJvHm0jZQ41GxMORy8DAwcXD1J5D2dLAwM/Kj8bdWMFB18zrVcNh22PHTObO5Mqj0OZM5MqjzsDAzO3Z2oXQxUvl4+TKoz/l5kzkyqPOwMDM7dnahWPJSaFekcbB3dbKysGC3dvPxUulzcDAxsPE0cnig0nNytHBgMDA0Zo/zddC61XDYdtjx0zmzuTKo9DmTOTKo87AwMzt2dqF0MVL5ePkyqM/5eZM5MqjzsDAzO3Z2oVjyUmhXpHGwd3WysrBgt3bz8VLpc3AwMbDxNHJ4oNJzcrRwYDAwNGa1utd3lDCTsDAzUOayMPBiz/DxUulzcDAxsPE0cnig8LDz8VLpc3AwMbDxNHJ4oM/wsNh22PHTObO5Mqj0OZM5MqjzsDAzO3Z2oXQxUvl4+TKoz/l5kzkyqPOwMDM7dnahWPJSaFekcbB3dbKysGC3dvPxUulzcDAxsPE0cnigz/Pmj/Bw9fM0YE/QtnRwMDRytbRj0GFSYXOxcOMP0PEwIfBkj/S0sDATVlDYdfpwsTImUNX38Lcmj/WrD/eiD9WwkfAwc7TnMOsXtPI3uPSw6tEjsfAwIM/RMfbwMCVV8ff2mfCwsDA0enaypXBwkPfZNHRguOR5JDfw0HAwkDhkJfqx8TVxtXRxcSEP0LEVJtMm8ebSNlDjUbEw5HLwMDBxcPUnkPZ0sDAz8qPxt1YwUHXzOtVw2HbY8dM5s7kyqPQ5kzkyqPOwMDM7dnahdDFS+Xj5MqjP+XmTOTKo87AwMzt2dqFY8lJoV6RxsHd1srKwYLd28/FS6XNwMDGw8TRyeKDSc3K0cGAwMDRmj/N10LrVcNh22PHTObO5Mqj0OZM5MqjzsDAzO3Z2oXQxUvl4+TKoz/l5kzkyqPOwMDM7dnahWPJSaFekcbB3dbKysGC3dvPxUulzcDAxsPE0cnig0nNytHBgMDA0ZrW613eUMJOwMDNQ5rIw8GLP8PFS6XNwMDGw8TRyeKDwsPPxUulzcDAxsPE0cnigz/Cw2HbY8dM5s7kyqPQ5kzkyqPOwMDM7dnahdDFS+Xj5MqjP+XmTOTKo87AwMzt2dqFY8lJoV6RxsHd1srKwYLd28/FS6XNwMDGw8TRyeKDP8+aP1fSwMHFgkjSwMHAhcORP0uE0cDR0UDBwaLCwMDCwdRNg8/AQIHBgj9S0M7CwsDASkDAE8LZw4RY4UbAwNDciN/IwMENP8aVP0bB25Xd01iZ2YM/09FHj1jK14jB2sLbxM3DkETJQoDPkkrIwMDFh0LAT1BAU9RCWUNh1+nCxMiZQ1ffwtyaP9asP96IP1bCR8DBztOcw6xe08je49LDq0SOx8DAgz9Ex9vAwJVXx9/aZ8LCwMDR6drKlcHCQ99k0dGC45HkkN/DQcDCQOGQl+rHxNXG1dHFxIQ/RNFHj1jK14jB2sLbxM3DkMjAQMHK1EGsVN7RwsLH2ozPpcHAwNNWndzCy4RC1cHAwIFDRtPU2UDBUsXAwpI/wctf5sLBwcLghlqm46Y/w4VUSkDAE8LZw4RY4UbAwNDciN/IwMENP8aVP0bB25Xd01iZ2YM/09FHj1jK14jB2sLbxM3DkFNAXEFZxs/Q1tnAwYw/2sZQkM7QzsPPmj/QlD/H0VHX08HBg9PGztfTwcHD183Diz9S1EePWMrXiMHawtvEzcOQ15w/R4LX09SZP0nUQd1RwtrXQutVw2HbY8dM5s7kyqPQ5kzkyqPOwMDM7dnahdDFS+Xj5MqjP+XmTOTKo87AwMzt2dqFY8lJoV6RxsHd1srKwYLd28/FS6XNwMDGw8TRyeKDSc3K0cGAwMDRmtbrXd5Qwk7AwM1DmsjDwYs/w8VLpc3AwMbDxNHJ4oPCw8/FS6XNwMDGw8TRyeKDP8LDYdtjx0zmzuTKo9DmTOTKo87AwMzt2dqF0MVL5ePkyqM/5eZM5MqjzsDAzO3Z2oVjyUmhXpHGwd3WysrBgt3bz8VLpc3AwMbDxNHJ4oPYwtLXQutVw2HbY8dM5s7kyqPQ5kzkyqPOwMDM7dnahdDFS+Xj5MqjP+XmTOTKo87AwMzt2dqFY8lJoV6RxsHd1srKwYLd28/FS6XNwMDGw8TRyeKDSc3K0cGAwMDRmtbrXd5Qwk7AwM1DmsjDwYs/w8VLpc3AwMbDxNHJ4oPCw8/FS6XNwMDGw8TRyeKDP8LDYdtjx0zmzuTKo9DmTOTKo87AwMzt2dqF0MVL5ePkyqM/5eZM5MqjzsDAzO3Z2oVjyUmhXpHGwd3WysrBgt3bz8VLpc3AwMbDxNHJ4oNSwEDBytRBrFTe0cLCx9qMz6XBwMDTVp3cwsuE1M3ThFbT24ZBxcOMxNJA08bMVMzdk8SPx9HAwNXAwc1QmEPBypLL28XKxMDAlz/DwMHI19RhwcPS68HkmMLBR9PnwuGWP9/S68HkmMLATkbT1NlAwVLFwMKSP8HLX+bCwcHC4IZapuOmP8OFVEpAwBPC2cOEWOFGwMDQ3IjfyMDBDT/GlT9GwduV3dNYmdmDP9PRR49YyteIwdrC28TNw5BTQFxBWcbP0NbZwMGMP9rGUJDO0M7Dz5o/0JQ/x9FR19PBwYPTxs7X08HBw9fNw4s/UtRHj1jK14jB2sLbxM3DkNecP0eC19PUmT9J2UzAwMHBhtKSVJDP1EiCVcFR20HA3ILAwFCf1N/A28eUTuRAwdvMUMHA1tmcP2HQ4pHVneaBP97AwMDB4sHE1dPZ38GQP03RR49YyteIwdrC28TNw5DBxFSbTJvHm8TAwc/NyNfUYcHD0uvB5JjCwUfT58Lhlj/f0uvB5JjFwsLCgkLCwVFR3FTD0UlEQEHe3c7Bgj/Aj2Xbzs7pwdLIg9DF18np1+TB3dfa1+LBz4M/ReXIwsGnzNvEo8DAQkHdws3OwsXAxcPBwNTO1NbXnz/Dj8zDwNDVxsGEQMHO0NPAwp/XT8HK28DAwMbRkEGBw8ndwMDNTsDAgtpK0cDAws3M28DD2rE/48rF2t7QwJlYQkJYQc7YwcvP0tTAwItOz8DAwcDAw83OzcDagtLJwMHFxtnLwMHVjklAwFFKVEzCzZrA2c7Y3tPC0NWGWdHPytbByMThwsHD1sDDzYTXTM/C14dL1cfQwceAxsbO0s7PgltdyNfS2qjXwNPBwWfNzMPYwsHA2pTNzMXR1sBAydLWiErDyo7B0M6Ex87G2tHVx4Y/18pFxtTAwNjLx8CN08bPzMDBwMLR1cDAwJg/VcfATcvBwcTX0Mfp4prh0c/UwtWCwsDAzNfi0dDPhj9g0sDV4sLKwcCXwM7M1NSExd/azd3XwdaPSsDAwNvSwNTdRc5LwtTb1dDd3sLAwMvIh9PcTs3Fy9iUzcDV0N3ewsDAy8iHU99FzE7NxcvYlM3A1dDd3sLAwMvIh8bR1MDdy83FwtLFjT/G19Th1I5OwclAwVDU1NvF28Lmw8XAwceAP8bCwMHA6dDA1cPLmtvA0NnS5MTCxc/SwMDO1uWPWMHhwUTJ49vawcHXxNPCw82OP8/jxOTb0cODP9TBwdLUwsrCwsDBwMzC2sXSisDA0M7S2NTBweKEU0PBQs7C1dzC0NHM3ceMwVjR39fCztbC2NjBxsCAzs3W2sLUwcHT297B25RRwdFBwsLO0eCW1cLEzs/AweHTxcHEwdfbwMDA28XBwcCbQMDQztLY1MHB4oQ/z8/SwdLQw5NBwcPNzd5QwcDSztHR29Lj08iISETM1c/Rws/SwsXdkkrP1NXUwsHB0MCA1cXDwNXK0dDkktHBwdTBwcDAy4vAwFNY0MGOzcPZzuXPzOTY0drAwJZPz9vW1cvPycvAwoLD0M/YzsHYwdbAwcvBy9nLypVSQNJCwE/aUkLB20LXwMJOwlXDwsDBj8PDwtFVltiW0MTB0sPY04XAwcHWTsJVw8LAwY/Dw8LRVZbYltDEwdLD2NOFP1DBxMLYgMPM18RCwMCWwcjWlcfAwcDCgMXWQMHN1dHCwNZOwlXDwsDBj8PDwtFVltiW0MTB0sPY04U/T9dSgsfAwY/EwcHOVNVMxU7aVMTAxE7BwMDCzMDAwM7w1OifwMLi4WGjwMLOwcDk75vAzU/dycfCwMDAy8TE1lPYwOBFre2AUdjA4EWt7YDnwMCB4oY/0drPxMDETsHAwMLMwMDAzvDU6J/AwuLhYaPAws7BwOTvm07ES9pUxMDETsHAwMLMwMDAzvDU6J/AwuLhYaPAws7BwOTvm8DNT93Jx8LAwMDLxMTWU9jA4EWt7YBR2MDgRa3tgOfAwIHihj/T2svEwMROwcDAwszAwMDO8NTon8DC4uFho8DCzsHA5O+b0cVM2svEwMROwcDAwszAwMDO8NTon8DC4uFho8DCzsHA5O+by9rPxMDETsHAwMLMwMDAzvDU6J/AwuLhYaPAws7BwOTvmz/M1tTAQYLAxMnU05bG1ULBQMHN1dHCwNZOwlXDwsDBj8PDwtFVltiW0MTB0sPY04U/T9dSgsfAwY/EwcHOVNVMxU7aVMTAxE7BwMDCzMDAwM7w1OifwMLi4WGjwMLOwcDk75vAzU/dycfCwMDAy8TE1lPYwOBFre2AUdjA4EWt7YDnwMCB4oY/0drPxMDETsHAwMLMwMDAzvDU6J/AwuLhYaPAws7BwOTvm07ES9pUxMDETsHAwMLMwMDAzvDU6J/AwuLhYaPAws7BwOTvm8DNT93Jx8LAwMDLxMTWU9jA4EWt7YBR2MDgRa3tgOfAwIHihj/T2svEwMROwcDAwszAwMDO8NTon8DC4uFho8DCzsHA5O+b0cVM2svEwMROwcDAwszAwMDO8NTon8DC4uFho8DCzsHA5O+by9rPxMDETsHAwMLMwMDAzvDU6J/AwuLhYaPAws7BwOTvmz/M1tTAQYLAxMnU05Y/xdBRwEDAQMHAgcHBwIHBwWjBwIHpwcCB1cFRwECBwcJpwMDA4cHZgurAwMDhwdmC3oFWBD/AwMCWP8HC1MDAw83bQIHUw8KNQUBAUWjBUsxZxEHBxNbNgj/bwtXJRonFjj9Y2UXVw+Vcw87NwcBRwUCcwcVGitDCRcpAwkCBwovBi8TBw8vAwsLZzpU/08jBzNrO68HBwMCCP8LDwsBsnOfAQdFKlcnAwZ7C0UyNy8DAnsPVxOVcw87NwcBRwUCcwcVGitDCRcpAwkCBwovBi8TBw8vAwsLZzpU/08jBzNrO68HBwMCCP8LDwsBsnOfAQdFKlcnAwZ7C0UyNy8DAnkTZ1tXD5VzDzs3BwFHBQJzBxUaK0MJFykDCQIHCi8GLxMHDy8DCwtnOlT/TyMHM2s7rwcHAwII/wsPCwGyc58BB0UqVycDBnsLRTI3LwMCezpdEzFXEwcHQ1s2CP1TZUNXD5VzDzs3BwFHBQJzBxUaK0MJFykDCQIHCi8GLxMHDy8DCwtnOlT/TyMHM2s7rwcHAwII/wsPCwGyc58BB0UqVycDBnsLRTI3LwMCeP8PVz+Vcw87NwcBRwUCcwcVGitDCRcpAwkCBwovBi8TBw8vAwsLZzpU/08jBzNrO68HBwMCCP8LDwsBsnOfAQdFKlcnAwZ7C0UyNy8DAntDZ1tXD5VzDzs3BwFHBQJzBxUaK0MJFykDCQIHCi8GLxMHDy8DCwtnOlT/TyMHM2s7rwcHAwII/wsPCwGyc58BB0UqVycDBnsLRTI3LwMCeP0PNQqFDytbZ14Xd2djHQcDAwcCC3tXE51eQ7MHAwMCC1sxD2UTVz+Vcw87NwcBRwUCcwcVGitDCRcpAwkCBwovBi8TBw8vAwsLZzpU/08jBzNrO68HBwMCCP8LDwsBsnOfAQdFKlcnAwZ7C0UyNy8DAnj/R1cTlXMPOzcHAUcFAnMHFRorQwkXKQMJAgcKLwYvEwcPLwMLC2c6VP9PIwczazuvBwcDAgj/Cw8LAbJznwEHRSpXJwMGewtFMjcvAwJ4/UtlF1cPlXMPOzcHAUcFAnMHFRorQwkXKQMJAgcKLwYvEwcPLwMLC2c6VP9PIwczazuvBwcDAgj/Cw8LAbJznwEHRSpXJwMGewtFMjcvAwJ7D1cTlXMPOzcHAUcFAnMHFRorQwkXKQMJAgcKLwYvEwcPLwMLC2c6VP9PIwczazuvBwcDAgj/Cw8LAbJznwEHRSpXJwMGewtFMjcvAwJ7F2VDVw+Vcw87NwcBRwUCcwcVGitDCRcpAwkCBwovBi8TBw8vAwsLZzpU/08jBzNrO68HBwMCCP8LDwsBsnOfAQdFKlcnAwZ7C0UyNy8DAnj/D1c/lXMPOzcHAUcFAnMHFRorQwkXKQMJAgcKLwYvEwcPLwMLC2c6VP9PIwczazuvBwcDAgj/Cw8LAbJznwEHRSpXJwMGewtFMjcvAwJ4/5YE/wcVP48xVxMHB0NbNgj9U2VDVw+Vcw87NwcBRwUCcwcVGitDCRcpAwkCBwovBi8TBw8vAwsLZzpU/08jBzNrO68HBwMCCP8LDwsBsnOfAQdFKlcnAwZ7C0UyNy8DAnj/D1c/lXMPOzcHAUcFAnMHFRorQwkXKQMJAgcKLwYvEwcPLwMLC2c6VP9PIwczazuvBwcDAgj/Cw8LAbJznwEHRSpXJwMGewtFMjcvAwJ7Q2dbVw+Vcw87NwcBRwUCcwcVGitDCRcpAwkCBwovBi8TBw8vAwsLZzpU/08jBzNrO68HBwMCCP8LDwsBsnOfAQdFKlcnAwZ7C0UyNy8DAnsPh1JrSQkBN2sXdw9fKzsDB0sPF1p7D1s2XP8HP6MxZxEHBxNbNgj/bwtXJRonFjj9Y2UXVw+Vcw87NwcBRwUCcwcVGitDCRcpAwkCBwovBi8TBw8vAwsLZzpU/08jBzNrO68HBwMCCP8LDwsBsnOfAQdFKlcnAwZ7C0UyNy8DAnsPVxOVcw87NwcBRwUCcwcVGitDCRcpAwkCBwovBi8TBw8vAwsLZzpU/08jBzNrO68HBwMCCP8LDwsBsnOfAQdFKlcnAwZ7C0UyNy8DAnkTZ1tXD5VzDzs3BwFHBQJzBxUaK0MJFykDCQIHCi8GLxMHDy8DCwtnOlT/TyMHM2s7rwcHAwII/wsPCwGyc58BB0UqVycDBnsLRTI3LwMCezpc/V8/TyFqGwMLXx8KTxoQ/wYM/QMJS0cjU2s3BwE/ZQ8DAwcHAwMDA3+HjTtXEktLVwZjFwMHCwIJOwc3bxJJY10/excDCgNDexIBJ2cfDUcxK0kvk0pLSlcDe0MHAwM2jP8DCwIM/xcDAwMDaxOHTzczAwYTJ0VjU0s3BwE/ZQ8DAwcHAwMDA3+HjTtXEktLVwZjFwMHCwIJOwc3bxJJY10/excDCgNDexIBJ2cfDUcxK0kvk0pLSlcDe0MHAwM2jP8DCwIM/xcDAwMDaxOHTzczAwYQ/0dTazcHAT9lDwMDBwcDAwMDf4eNO1cSS0tXBmMXAwcLAgk7BzdvEkljXT97FwMKA0N7EgEnZx8NRzErSS+TSktKVwN7QwcDAzaM/wMLAgz/FwMDAwNrE4dPNzMDBhERCwdHAwFTV2chLzM3TwMFV1NPCwoPA1tHC0NjlgsyLUMBA0tPeWY/dgMHDVeDB08KFT85CztWISc3Lw8+Oy83Ux8nN28jk0d+cP8KSz85S1cCT0JXSwd1B1dHCwMBBxEGDwufBg8CBUcPRxcGowMHPwtLDwMDQoEjC09VIwMDgyMFOgNfA3cBDzMCJxOPAysSJUt7HhUbF0qHDz8WESsJTw8HO0sBV19Tbw4RT1tfbw4Q/wIE/UYDA0VjU0s3BwE/ZQ8DAwcHAwMDA3+HjTtXEktLVwZjFwMHCwIJOwc3bxJJY10/excDCgNDexIBJ2cfDUcxK0kvk0pLSlcDe0MHAwM2jP8DCwIM/xcDAwMDaxOHTzczAwYQ/0dTazcHAT9lDwMDBwcDAwMDf4eNO1cSS0tXBmMXAwcLAgk7BzdvEkljXT97FwMKA0N7EgEnZx8NRzErSS+TSktKVwN7QwcDAzaM/wMLAgz/FwMDAwNrE4dPNzMDBhNHCRMDAgkLRQsLSjcHBUtrT2MqV0NXT20WPyePXw9rEwMPDo8PPwoFDUkZCUFdAR07WTc3Ah8rAwcNIwtLAwMqKP93C1ITW1c7RXorCikFOwsvawMHMwtXLgz/Wxc7OwsDAgj9GjsOLWkZPwcHOQ4I/z4M/QcJYzE3AwcBfh0DBwMDDhz9Fz8DAzI7Pz8DA0I4/w4FOzUmA18HJwMGO18FPzcLC5ePL4UjBwMBZyuLF3ITji8/DwcDC48DA1dhNwMDVwNnJkMrAwNXA2cmQw83NwuXjy+FIwcDAWcrixdyE44vPw8HAwuPAwNXYTcDA1cDZyZDKwMDVwNnJkMKEP1DPxIvDwUHXycDBy8CBw87H1sCBV8xWxkLBweDbzt2RxMDAxMHCQ9zAwZc/ytXKxs+LP8CCRsZN0FzEwufZwMCC1cDB3NXDwcLAj8PQ08LBwMDAwIFLwMDFQcXO08zPwohVzs3Dw4VMwtfNw4U/RMPBwVbiy9nCkD9AhMzBgMLZwc/P08uNP07GVYJAwMCY3dBCwcCRVcDBwcCQXMPC59jAwIDD49fAwcLBkEPDy8DAxMHBVuLL2cKQP0CEzMGA28bBzsrDy4Y/RMPAwEHExNHWzMHAScXKwdOYRcjJy9LLSIHLisvIxcvSy0iBy4rFxVDSQc3BwEnFysHTmEXIycvSy0iBy4rLyMXL0stIgcuKP8LMwcBJxcrB05hFyMnL0stIgcuKy8jFy9LLSIHLij/B0dbMwcBJxcrB05hFyMnL0stIgcuKy8jFy9LLSIHLisWBT1dT38XQ0cBBwsDBQcPCzcTJx4w/14I/wtLf5VGAz8DBwMCC0sNWzk/VwoDM3MOWwcLI29LFwpXFRNPTWcDBUELOyuHC54nPXcDBh8DAyoI/24Xc1KDA09RZwcFRwuCDxsXYw9CMXFLHwYPQ0V6KworCwpc/1tZWn9KDSFBBV1lDRM/B48DCzMJS4crdyNvAgD/M19TpWcDA1s+RzNvT6OONP8HgQMJDzszCUuHK3cjbwIA/zNfU6VnAwNbPkczb0+jjjU3YxMnZjM7OTdrV6ELBWcDA1s+RzNvT6OON0Z8/wsJS4crdyNvAgD/M19TpWcDA1s+RzNvT6OONQ8/AwszCUuHK3cjbwIA/zNfU6VnAwNbPkczb0+jjjT/Ew8LCzdPM10PBwenPnFTi4sTK093AQIjBiNXAwMrTw8CDP1bAQo3Dj9DBQcDAgGPDxcDCzMJS4crdyNvAgD/M19TpWcDA1s+RzNvT6OONYcTB1NTJwMDDnMPD48DCzMJS4crdyNvAgD/M19TpWcDA1s+RzNvT6OONP0PQV8PB48DCzMJS4crdyNvAgD/M19TpWcDA1s+RzNvT6OONwMFQ2cSBP8XMRc/UjNDCwcDApUCC0tbE2MDBw8PTlUDAQkDARFHVTkPCQMHO11DPTVHHGT/FzNCYP0WNztDPjT/G2kLizqnDhj9VwtHO0tvA4dPF0PHAz8XV24ZH3NbP1+DOh8SGP1DZStrV3Mrf4NDJjtbC1YTPgz9QhEjK18DAicDBT9lJlMPBUM9NUccZP8XM0Jg/RY3O0M+NP8baQuLOqcOGP1XC0c7S28Dh08XQ8cDPxdXbhkfc1s/X4M6HxIbCwc7XUM9NUccZP8XM0Jg/RY3O0M+NP8baQuLOqcOGP1XC0c7S28Dh08XQ8cDPxdXbhkfc1s/X4M6HxIY/TdDbxMfBwd/W5MWT2sNOwMHOwVDPTVHHGT/FzNCYP0WNztDPjT/G2kLizqnDhj9VwtHO0tvA4dPF0PHAz8XV24ZH3NbP1+DOh8SGzc/HwcHf1uTFkz9X1k3R0cTHwcHf1uTFk9HDTsDBzsFQz01Rxxk/xczQmD9Fjc7Qz40/xtpC4s6pw4Y/VcLRztLbwOHTxdDxwM/F1duGR9zWz9fgzofEhs3Px8HB39bkxZNS0MCBzdbR0NvEx8HB39bkxZM/1tBCzcXCQMHO11DPTVHHGT/FzNCYP0WNztDPjT/G2kLizqnDhj9VwtHO0tvA4dPF0PHAz8XV24ZH3NbP1+DOh8SGP1DZStrV3Mrf4NDJjtbC1YTPgz/GTkPCQMHO11DPTVHHGT/FzNCYP0WNztDPjT/G2kLizqnDhj9VwtHO0tvA4dPF0PHAz8XV24ZH3NbP1+DOh8SGP1DZStrV3Mrf4NDJjtbC1YTPgz9QhEjK18DAicDBT9lJlMPBUM9NUccZP8XM0Jg/RY3O0M+NP8baQuLOqcOGP1XC0c7S28Dh08XQ8cDPxdXbhkfc1s/X4M6HxIbCwc7XUM9NUccZP8XM0Jg/RY3O0M+NP8baQuLOqcOGP1XC0c7S28Dh08XQ8cDPxdXbhkfc1s/X4M6HxIY/TdDbxMfBwd/W5MWT2sNOwMHOwVDPTVHHGT/FzNCYP0WNztDPjT/G2kLizqnDhj9VwtHO0tvA4dPF0PHAz8XV24ZH3NbP1+DOh8SGzc/HwcHf1uTFkz/VzdjawsbQ1MKDP87bQcNWwMDFwtve1NWR3cDAhcLAwMbR0NvEx8HB39bkxZM/QtnPwUDAVsDAxcLb3tTVkd3AwIVWxcKYV8RBi8TBwovcwMHOws3XllDZQsFAwFbAwMXC297U1ZHdwMCFVsXCmFfEQYvEwcKL3MDBzsLN15ZAwEHDVsDAxcLb3tTVkd3AwIXCwMDG0dDbxMfBwd/W5MWTWc9DwMCDQsDAw93Fw8LS092g1cDAxsOQXsDBwcPNW8XCjEjURITkw8DCwtLEgUPRw9DB4ePGwYFdyNzFx8PGgmrFQIHE3MDAwcPYjtjFQtLk2kHBwdrqiMTCwsDAgUSAxsPDwMKRX8PCwcTAwMLPxMLAwc7WktjBQcDAgGPDxcDCzMJS4crdyNvAgD/M19TpWcDA1s+RzNvT6OONYcTB1NTJwMDDnMPD48DCzMJS4crdyNvAgD/M19TpWcDA1s+RzNvT6OON2M5XwMDWQcDAoMLCQcDAn8zFz51CwMrSwMDP5sDW1sfmxM/Cw45AwEGE3NbBzlHLwaDTx0TJwsrDnsKAVcDAwEKEwdLTxc/VzcPN0svTx87Ggz/R3UPAwINCwMDD3cXDwtLT3aDVwMDGw5A/VMHCzkHVQ8JO2kDAS9bewdXZxOXKodbXTMHV2cTlyqE/w91L5dHn35fCwcHAwMDAokPTQMDV3Uvl0effl8LBwcDAwMCiUcTewsLKQsLBj0aMzYJVgsjd1sLKy9iFVoI/z8fBwcHA2+nIjk3CzdNQwsXAxIrPxcTczcDCwd3BwKHAwM3gS9bewdXZxOXKodbXTMHV2cTlyqE/w91L5dHn35fCwcHAwMDAok/Aw9pAwEvW3sHV2cTlyqHW10zB1dnE5cqhP8PdS+XR59+XwsHBwMDAwKJD00DA1d1L5dHn35fCwcHAwMDAolHE3sLCykLCwY9GjM2CVYLI3dbCysvYhVaCP8/HwcHBwNvpyI7Fw0fAy8XO1cuAzcDHxc7Vy4BQ1UTCQMDB4EvW3sHV2cTlyqHW10zB1dnE5cqhP8PdS+XR59+XwsHBwMDAwKI/wtpAwEvW3sHV2cTlyqHW10zB1dnE5cqhP8PdS+XR59+XwsHBwMDAwKJD00DA1d1L5dHn35fCwcHAwMDAolHE3sLCykLCwY9GjM2CVYLI3dbCysvYhVaCP8/HwcHBwNvpyI4/wcDD2kDAS9bewdXZxOXKodbXTMHV2cTlyqE/w91L5dHn35fCwcHAwMDAokPTQMDV3Uvl0effl8LBwcDAwMCiUcTewsLKQsLBj0aMzYJVgsjd1sLKy9iFVoI/z8fBwcHA2+nIjsbCwtNHwMvFztXLgM3Ax8XO1cuAwMHazkHVQ8JO2kDAS9bewdXZxOXKodbXTMHV2cTlyqE/w91L5dHn35fCwcHAwMDAokPTQMDV3Uvl0effl8LBwcDAwMCiUcTewsLKQsLBj0aMzYJVgsjd1sLKy9iFVoI/z8fBwcHA2+nIjk3CzdNQwsXAxIrPxcTczcDCwd3BwKHAwM3gS9bewdXZxOXKodbXTMHV2cTlyqE/w91L5dHn35fCwcHAwMDAok/Aw9pAwEvW3sHV2cTlyqHW10zB1dnE5cqhP8PdS+XR59+XwsHBwMDAwKJD00DA1d1L5dHn35fCwcHAwMDAolHE3sLCykLCwY9GjM2CVYLI3dbCysvYhVaCP8/HwcHBwNvpyI7Fw0fAy8XO1cuAzcDHxc7Vy4BQ1UTCQMDB4EvW3sHV2cTlyqHW10zB1dnE5cqhP8PdS+XR59+XwsHBwMDAwKI/wtpAwEvW3sHV2cTlyqHW10zB1dnE5cqhP8PdS+XR59+XwsHBwMDAwKJD00DA1d1L5dHn35fCwcHAwMDAolHE3sLCykLCwY9GjM2CVYLI3dbCysvYhVaCP8/HwcHBwNvpyI4/wcDD2kDAS9bewdXZxOXKodbXTMHV2cTlyqE/w91L5dHn35fCwcHAwMDAokPTQMDV3Uvl0effl8LBwcDAwMCiUcTewsLKQsLBj0aMzYJVgsjd1sLKy9iFVoI/z8fBwcHA2+nIjsbCwtNHwMvFztXLgM3Ax8XO1cuAP1bBUM7Q3UPAwINCwMDD3cXDwtLT3aDVwMDGw5A/0c5XwMDWQcDAoMLCQcDAn8zFz51CwMrSwMDP5sDW1sfmxM/Cw45AwEGE3NbBzlHLwaDTx0TJwsrDnsKAVcDAwEKEwdLTxc/VzcPN0svTx87Ggz/R3UPAwINCwMDD3cXDwtLT3aDVwMDGw5DP1NHCQsFAwFbAwMXC297U1ZHdwMCFVsXCmFfEQYvEwcKL3MDBzsLN15ZAwEHDVsDAxcLb3tTVkd3AwIXCwMDG0dDbxMfBwd/W5MWTWc9DwMCDQsDAw93Fw8LS092g1cDAxsOQXsDBwcPNW8XCjEjURITkw8DCwtLEgUPRw9DB4ePGwYFdyNzFx8PGgmrFQIHE3MDAwcPYjtjFQtLk2kHBwdrqiMTCwsDAgUSAxsPDwMKRX8PCwcTAwMLPxMLAwc7WktjBQcDAgGPDxcDCzMJS4crdyNvAgD/M19TpWcDA1s+RzNvT6OONYcTB1NTJwMDDnMPD48DCzMJS4crdyNvAgD/M19TpWcDA1s+RzNvT6OONQ8DQVdLO29LjyMXBwcLAwIPP2MHUkj9Az0fR2sFB1UPCTtpAwEvW3sHV2cTlyqHW10zB1dnE5cqhP8PdS+XR59+XwsHBwMDAwKJD00DA1d1L5dHn35fCwcHAwMDAolHE3sLCykLCwY9GjM2CVYLI3dbCysvYhVaCP8/HwcHBwNvpyI5Nws3TUMLFwMSKz8XE3M3AwsHdwcChwMDN4EvW3sHV2cTlyqHW10zB1dnE5cqhP8PdS+XR59+XwsHBwMDAwKJPwMPaQMBL1t7B1dnE5cqh1tdMwdXZxOXKoT/D3Uvl0effl8LBwcDAwMCiQ9NAwNXdS+XR59+XwsHBwMDAwKJRxN7CwspCwsGPRozNglWCyN3WwsrL2IVWgj/Px8HBwcDb6ciOxcNHwMvFztXLgM3Ax8XO1cuAUNVEwkDAweBL1t7B1dnE5cqh1tdMwdXZxOXKoT/D3Uvl0effl8LBwcDAwMCiP8LaQMBL1t7B1dnE5cqh1tdMwdXZxOXKoT/D3Uvl0effl8LBwcDAwMCiQ9NAwNXdS+XR59+XwsHBwMDAwKJRxN7CwspCwsGPRozNglWCyN3WwsrL2IVWgj/Px8HBwcDb6ciOP8HAw9pAwEvW3sHV2cTlyqHW10zB1dnE5cqhP8PdS+XR59+XwsHBwMDAwKJD00DA1d1L5dHn35fCwcHAwMDAolHE3sLCykLCwY9GjM2CVYLI3dbCysvYhVaCP8/HwcHBwNvpyI7GwsLTR8DLxc7Vy4DNwMfFztXLgE/WVdrM2cLS055DwVfIwtjU0cGXwsDAwtycx8nVwcDUw9LYwcLZwIFOws/AwM7DgljRz8TCltna3sZaxtbS0JbAwkjG1s/Pg8OFRNDBwEGCwI5Gw0HAwMHTllSBxYPFw0PPwcHWwsLTzcfNh0LQwMBBxMTR1szBwEnFysHTmEXIycvSy0iBy4rLyMXL0stIgcuKxcVQ0kHNwcBJxcrB05hFyMnL0stIgcuKy8jFy9LLSIHLij/CzMHAScXKwdOYRcjJy9LLSIHLisvIxcvSy0iBy4o/wdHWzMHAScXKwdOYRcjJy9LLSIHLisvIxcvSy0iBy4pBzkWEwcXAwMTR1szBwEnFysHTmEXIycvSy0iBy4rLyMXL0stIgcuKRs9RxcHSQczBwcDAScXKwdOYRcjJy9LLSIHLisvIxcvSy0iBy4rCzsDAwcBJxcrB05hFyMnL0stIgcuKy8jFy9LLSIHLij9Cl8GEP8XAx8BBz0LNxcLU00LAydxcl9ve3oTKwEraw8vdmz/C3FyX297ehMbOU8NC1MLAydxcl9ve3oQ/w9NCwMncXJfb3t6EysBK2sPL3Zs/wtxcl9ve3oQ/wsLU00LAydxcl9ve3oTKwEraw8vdmz/C3FyX297ehMCPP1DCWMpAwcHCQJfRysHW1sHSgdbI1M3SwMHPRcJDg8LP09Ln28ODP0OLxotFxtTTwMVa2MXezMLBwcOpxNnLiFfFwdPU2daUQMTWhT/EV1PfxdDRwEHCwMFBw8LNxMnHjD/Xgj/C0t/lUYDPwMHAwILSw1bOT9XCgMzcw5bBwsjb0sXClcVE09NZwMFQQs7K4cLnic9dwMGHwMDKgj/bhdzUoMDT1FnBwVHC4IPGxdjD0IxcUsfBg9DRXorCisLCl1fFQMBDwcaAxtTWg0fW28HAwMHKz4pSwMGMRM5WmdTZ2cnU1MapP0LWwMPUws7DwJLBzdjEyc3D4MfC0cTH38DA1s3Jj1HB1kCDlUXGz9TExMDQX5DXm1bCwYVTw8DQSpVT2MGUz9LRyVSA0oPDwcGCQUVBUFFYVENO0EFdQtdAwlDAwUJAwUBCwMDAw1vO0FlHoz/Roj/IoD9awsWL3cLFzsTDwYJBwMDAw1vO0FlHoz/Roj/IoD9awsWL3cLFzsTDwYLVwMLO09mFP8Tcz9FJlM2OP9fawITT1dXZoz/H30PXxcPBzMPXzsDBhtDdwMDEwcyfP8jgwMFD18XDwczD187AwYbQ3cDAxMHMnz9EwcCVw8FBwcDBwILAxMvAQI7BjkFA38CYwcBBgMDdQqHBmT/XwkKBP+VAwlDAwUJAwUBCwMDAw1vO0FlHoz/Roj/IoD9awsWL3cLFzsTDwYJBwMDAw1vO0FlHoz/Roj/IoD9awsWL3cLFzsTDwYLVwMLO09mFP8Tcz9FJlM2OP9fawITT1dXZoz/H30PXxcPBzMPXzsDBhtDdwMDEwcyfP8jgwMFD18XDwczD187AwYbQ3cDAxMHMnz/FwUHBQcDAjz/Agj/A19DOweFAwNbAwMHgwMDPwNbAjdTAwNXWwMDB3sDAz8DWwI3B0EnZTsDAwcDAwNbaxsHBztLTp8TBwcDBwMCPTV1C10DCUMDBQkDBQELAwMDDW87QWUejP9GiP8igP1rCxYvdwsXOxMPBgkHAwMDDW87QWUejP9GiP8igP1rCxYvdwsXOxMPBgtXAws7T2YU/xNzP0UmUzY4/19rAhNPV1dmjP8ffQ9fFw8HMw9fOwMGG0N3AwMTBzJ8/yODAwUPXxcPBzMPXzsDBhtDdwMDEwcyfP0TBwJXDwUHBwMHAgsDEy8BAjsGOQUDfwJjBwEGAwN1CocGZP9fCQoE/5UDCUMDBQkDBQELAwMDDW87QWUejP9GiP8igP1rCxYvdwsXOxMPBgkHAwMDDW87QWUejP9GiP8igP1rCxYvdwsXOxMPBgtXAws7T2YU/xNzP0UmUzY4/19rAhNPV1dmjP8ffQ9fFw8HMw9fOwMGG0N3AwMTBzJ8/yODAwUPXxcPBzMPXzsDBhtDdwMDEwcyfP0TBQcFBwMCPP8DCwoDAlU7AwMHAwMCWwt3L10DCUMDBQkDBQELAwMDDW87QWUejP9GiP8igP1rCxYvdwsXOxMPBgkHAwMDDW87QWUejP9GiP8igP1rCxYvdwsXOxMPBgtXAws7T2YU/xNzP0UmUzY4/19rAhNPV1dmjP8ffQ9fFw8HMw9fOwMGG0N3AwMTBzJ8/yODAwUPXxcPBzMPXzsDBhtDdwMDEwcyfP9HOw0FdQtdAwlDAwUJAwUBCwMDAw1vO0FlHoz/Roj/IoD9awsWL3cLFzsTDwYJBwMDAw1vO0FlHoz/Roj/IoD9awsWL3cLFzsTDwYLVwMLO09mFP8Tcz9FJlM2OP9fawITT1dXZoz/H30PXxcPBzMPXzsDBhtDdwMDEwcyfP8jgwMFD18XDwczD187AwYbQ3cDAxMHMnz9EwcCVw8FBwcDBwILAxMvAQI7BjkFA38CYwcBBgMDdQqHBmT/XwkKBP+VAwlDAwUJAwUBCwMDAw1vO0FlHoz/Roj/IoD9awsWL3cLFzsTDwYJBwMDAw1vO0FlHoz/Roj/IoD9awsWL3cLFzsTDwYLVwMLO09mFP8Tcz9FJlM2OP9fawITT1dXZoz/H30PXxcPBzMPXzsDBhtDdwMDEwcyfP8jgwMFD18XDwczD187AwYbQ3cDAxMHMnz/FwUHBQcDAjz/Agj/A19DOweFAwNbAwMHgwMDPwNbAjdTAwNXWwMDB3sDAz8DWwI1NQ0LQXEHE5dzgR9hGg8SCxNPExMPCxNzchz/Pgj/VwcKUU9BBXULXQMJQwMFCQMFAQsDAwMNbztBZR6M/0aI/yKA/WsLFi93Cxc7Ew8GCQcDAwMNbztBZR6M/0aI/yKA/WsLFi93Cxc7Ew8GC1cDCztPZhT/E3M/RSZTNjj/X2sCE09XV2aM/x99D18XDwczD187AwYbQ3cDAxMHMnz/I4MDBQ9fFw8HMw9fOwMGG0N3AwMTBzJ8/RMHAlcPBQcHAwcCCwMTLwECOwY5BQN/AmMHAQYDA3UKhwZk/18JCgT/lQMJQwMFCQMFAQsDAwMNbztBZR6M/0aI/yKA/WsLFi93Cxc7Ew8GCQcDAwMNbztBZR6M/0aI/yKA/WsLFi93Cxc7Ew8GC1cDCztPZhT/E3M/RSZTNjj/X2sCE09XV2aM/x99D18XDwczD187AwYbQ3cDAxMHMnz/I4MDBQ9fFw8HMw9fOwMGG0N3AwMTBzJ8/xcFBwUHAwI8/wII/wNfQzsHhQMDWwMDB4MDAz8DWwI3UwMDV1sDAwd7AwM/A1sCNP9bBwc5Di0GKwoo/UULDXEHE5dzgR9hGg8SCxNPExMPCxNzchz/Pgj/VwcKU08NBXULXQMJQwMFCQMFAQsDAwMNbztBZR6M/0aI/yKA/WsLFi93Cxc7Ew8GCQcDAwMNbztBZR6M/0aI/yKA/WsLFi93Cxc7Ew8GC1cDCztPZhT/E3M/RSZTNjj/X2sCE09XV2aM/x99D18XDwczD187AwYbQ3cDAxMHMnz/I4MDBQ9fFw8HMw9fOwMGG0N3AwMTBzJ8/RMHAlcPBQcHAwcCCwMTLwECOwY5BQN/AmMHAQYDA3UKhwZk/18JCgT/lQMJQwMFCQMFAQsDAwMNbztBZR6M/0aI/yKA/WsLFi93Cxc7Ew8GCQcDAwMNbztBZR6M/0aI/yKA/WsLFi93Cxc7Ew8GC1cDCztPZhT/E3M/RSZTNjj/X2sCE09XV2aM/x99D18XDwczD187AwYbQ3cDAxMHMnz/I4MDBQ9fFw8HMw9fOwMGG0N3AwMTBzJ8/xcFBwUHAwI8/wII/wNfQzsHhQMDWwMDB4MDAz8DWwI3UwMDV1sDAwd7AwM/A1sCNwcACP8GAP0LRUsNM3cHXQMJQwMFCQMFAQsDAwMNbztBZR6M/0aI/yKA/WsLFi93Cxc7Ew8GCQcDAwMNbztBZR6M/0aI/yKA/WsLFi93Cxc7Ew8GC1cDCztPZhT/E3M/RSZTNjj/X2sCE09XV2aM/x99D18XDwczD187AwYbQ3cDAxMHMnz/I4MDBQ9fFw8HMw9fOwMGG0N3AwMTBzJ8/wd3L10DCUMDBQkDBQELAwMDDW87QWUejP9GiP8igP1rCxYvdwsXOxMPBgkHAwMDDW87QWUejP9GiP8igP1rCxYvdwsXOxMPBgtXAws7T2YU/xNzP0UmUzY4/19rAhNPV1dmjP8ffQ9fFw8HMw9fOwMGG0N3AwMTBzJ8/yODAwUPXxcPBzMPXzsDBhtDdwMDEwcyfP8vDXEHE5dzgR9hGg8SCxNPExMPCxNzchz/Pgj/VwcKUw8JDwUyA1MbO7cPBwcDBwEHDy97So8PawcDAjsDQ0sDBw8PYUMLjwMCC1II/0NXZzsNBXULXQMJQwMFCQMFAQsDAwMNbztBZR6M/0aI/yKA/WsLFi93Cxc7Ew8GCQcDAwMNbztBZR6M/0aI/yKA/WsLFi93Cxc7Ew8GC1cDCztPZhT/E3M/RSZTNjj/X2sCE09XV2aM/x99D18XDwczD187AwYbQ3cDAxMHMnz/I4MDBQ9fFw8HMw9fOwMGG0N3AwMTBzJ8/RMHAlcPBQcHAwcCCwMTLwECOwY5BQN/AmMHAQYDA3UKhwZk/18JCgT/lQMJQwMFCQMFAQsDAwMNbztBZR6M/0aI/yKA/WsLFi93Cxc7Ew8GCQcDAwMNbztBZR6M/0aI/yKA/WsLFi93Cxc7Ew8GC1cDCztPZhT/E3M/RSZTNjj/X2sCE09XV2aM/x99D18XDwczD187AwYbQ3cDAxMHMnz/I4MDBQ9fFw8HMw9fOwMGG0N3AwMTBzJ8/xcFBwUHAwI8/wII/wNfQzsHhQMDWwMDB4MDAz8DWwI3UwMDV1sDAwd7AwM/A1sCNP0LQz9lSw0zdwddAwlDAwUJAwUBCwMDAw1vO0FlHoz/Roj/IoD9awsWL3cLFzsTDwYJBwMDAw1vO0FlHoz/Roj/IoD9awsWL3cLFzsTDwYLVwMLO09mFP8Tcz9FJlM2OP9fawITT1dXZoz/H30PXxcPBzMPXzsDBhtDdwMDEwcyfP8jgwMFD18XDwczD187AwYbQ3cDAxMHMnz/B3cvXQMJQwMFCQMFAQsDAwMNbztBZR6M/0aI/yKA/WsLFi93Cxc7Ew8GCQcDAwMNbztBZR6M/0aI/yKA/WsLFi93Cxc7Ew8GC1cDCztPZhT/E3M/RSZTNjj/X2sCE09XV2aM/x99D18XDwczD187AwYbQ3cDAxMHMnz/I4MDBQ9fFw8HMw9fOwMGG0N3AwMTBzJ8/y8NcQcTl3OBH2EaDxILE08TEw8LE3NyHP8+CP9XBwpRDwUDPT9jSwMHDw9hQwuPAwILUgs3EVtTSwkKM2sBC0sHIQsHAwNmASMDAkN7Ww8LB38yWwNTh1UKOQM/PwMCOUdPD3N/AwY7P3cKOUtRAwNrRXoPdg1jXQYDAwd7D2p1WwkKM2sBC0sHIQsHAwNmASMDAkN7Ww8LB38yWwNTh1UKOQM/PwMCOUdPD3N/AwY7P3cKO2sLC2UKM1N5OwMHB4dDEwcDfTpTLlMLM3sBAgMGDQpPa1NHCwtlCjNTeTsDBweHQxMHA306Uy5TCzN7AQIDBg8LdYtTOyVWR2tbF0MPG04ZIw8XUSMLBg8CE1cLAw0vQ0Obb1cGAzOXJztvVwYBGwMDbUZvAj8XCws/Tlz/P0VfU0c7DQV1C10DCUMDBQkDBQELAwMDDW87QWUejP9GiP8igP1rCxYvdwsXOxMPBgkHAwMDDW87QWUejP9GiP8igP1rCxYvdwsXOxMPBgtXAws7T2YU/xNzP0UmUzY4/19rAhNPV1dmjP8ffQ9fFw8HMw9fOwMGG0N3AwMTBzJ8/yODAwUPXxcPBzMPXzsDBhtDdwMDEwcyfP0TBwJXDwUHBwMHAgsDEy8BAjsGOQUDfwJjBwEGAwN1CocGZP9fCQoE/5UDCUMDBQkDBQELAwMDDW87QWUejP9GiP8igP1rCxYvdwsXOxMPBgkHAwMDDW87QWUejP9GiP8igP1rCxYvdwsXOxMPBgtXAws7T2YU/xNzP0UmUzY4/19rAhNPV1dmjP8ffQ9fFw8HMw9fOwMGG0N3AwMTBzJ8/yODAwUPXxcPBzMPXzsDBhtDdwMDEwcyfP8XBQcFBwMCPP8CCP8DX0M7B4UDA1sDAweDAwM/A1sCN1MDA1dbAwMHewMDPwNbAjVHV2c7DQV1C10DCUMDBQkDBQELAwMDDW87QWUejP9GiP8igP1rCxYvdwsXOxMPBgkHAwMDDW87QWUejP9GiP8igP1rCxYvdwsXOxMPBgtXAws7T2YU/xNzP0UmUzY4/19rAhNPV1dmjP8ffQ9fFw8HMw9fOwMGG0N3AwMTBzJ8/yODAwUPXxcPBzMPXzsDBhtDdwMDEwcyfP0TBwJXDwUHBwMHAgsDEy8BAjsGOQUDfwJjBwEGAwN1CocGZP9fCQoE/5UDCUMDBQkDBQELAwMDDW87QWUejP9GiP8igP1rCxYvdwsXOxMPBgkHAwMDDW87QWUejP9GiP8igP1rCxYvdwsXOxMPBgtXAws7T2YU/xNzP0UmUzY4/19rAhNPV1dmjP8ffQ9fFw8HMw9fOwMGG0N3AwMTBzJ8/yODAwUPXxcPBzMPXzsDBhtDdwMDEwcyfP8XBQcFBwMCPP8CCP8DX0M7B4UDA1sDAweDAwM/A1sCN1MDA1dbAwMHewMDPwNbAjcTAUsTBzd2X3M/UyNLTw8DChEBOU0BCwEjCw19K28VWxNKKP8GfP8TWytrE0orM2OAKP8GbP8PCx19K28VWxNKKP8GfP8TWytrE0orM2OAKP8GbP8fKxcDAxsCBP9DBwdnB09SBP0zAScrFwMHGwIFCwMPDzl9K28VWxNKKP8GfP8TWytrE0orM2OAKP8Gb1sDC3kiNwMDHj8TAyMrDwMDG2dXAgD9Bg0+CwINQT9fAQMHAwMbJ3NXAgD/Bjj/AwM3F0ODD29OEP1LAQM3Z2c/R1cPagz/BjkPAwM1Pz0vW1sHBzMDBRODBwEDZ1sbQoT/BwcDB5eTkmD9M4UCA4pfVn9TWQMLKzMDBRODBwEDZ1sbQoT/BwcDB5eTkmD9M4UCA4pfVnz/LwcHMwMFE4MHAQNnWxtChP8HBwMHl5OSYP0zhQIDil9WfU89A1kvB0MzAwUTgwcBA2dbG0KE/wcHAweXk5Jg/TOFAgOKX1Z8/zcLKzMDBRODBwEDZ1sbQoT/BwcDB5eTkmD9M4UCA4pfVn0zW0MHBzMDBRODBwEDZ1sbQoT/BwcDB5eTkmD9M4UCA4pfVnz/O1kDCyszAwUTgwcBA2dbG0KE/wcHAweXk5Jg/TOFAgOKX1Z8/y8HBzMDBRODBwEDZ1sbQoT/BwcDB5eTkmD9M4UCA4pfVn03U0c/WwcHMwMFE4MHAQNnWxtChP8HBwMHl5OSYP0zhQIDil9WfP9SYRMFSwMHAwJLAzsLR5MDQj8LDwdBdl9DG2NbnxMOeP0/RQ8FX1NHOw0FdQtdAwlDAwUJAwUBCwMDAw1vO0FlHoz/Roj/IoD9awsWL3cLFzsTDwYJBwMDAw1vO0FlHoz/Roj/IoD9awsWL3cLFzsTDwYLVwMLO09mFP8Tcz9FJlM2OP9fawITT1dXZoz/H30PXxcPBzMPXzsDBhtDdwMDEwcyfP8jgwMFD18XDwczD187AwYbQ3cDAxMHMnz9EwcCVw8FBwcDBwILAxMvAQI7BjkFA38CYwcBBgMDdQqHBmT/XwkKBP+VAwlDAwUJAwUBCwMDAw1vO0FlHoz/Roj/IoD9awsWL3cLFzsTDwYJBwMDAw1vO0FlHoz/Roj/IoD9awsWL3cLFzsTDwYLVwMLO09mFP8Tcz9FJlM2OP9fawITT1dXZoz/H30PXxcPBzMPXzsDBhtDdwMDEwcyfP8jgwMFD18XDwczD187AwYbQ3cDAxMHMnz/FwUHBQcDAjz/Agj/A19DOweFAwNbAwMHgwMDPwNbAjdTAwNXWwMDB3sDAz8DWwI1R1dnOw0FdQtdAwlDAwUJAwUBCwMDAw1vO0FlHoz/Roj/IoD9awsWL3cLFzsTDwYJBwMDAw1vO0FlHoz/Roj/IoD9awsWL3cLFzsTDwYLVwMLO09mFP8Tcz9FJlM2OP9fawITT1dXZoz/H30PXxcPBzMPXzsDBhtDdwMDEwcyfP8jgwMFD18XDwczD187AwYbQ3cDAxMHMnz9EwcCVw8FBwcDBwILAxMvAQI7BjkFA38CYwcBBgMDdQqHBmT/XwkKBP+VAwlDAwUJAwUBCwMDAw1vO0FlHoz/Roj/IoD9awsWL3cLFzsTDwYJBwMDAw1vO0FlHoz/Roj/IoD9awsWL3cLFzsTDwYLVwMLO09mFP8Tcz9FJlM2OP9fawITT1dXZoz/H30PXxcPBzMPXzsDBhtDdwMDEwcyfP8jgwMFD18XDwczD187AwYbQ3cDAxMHMnz/FwUHBQcDAjz/Agj/A19DOweFAwNbAwMHgwMDPwNbAjdTAwNXWwMDB3sDAz8DWwI3EwFLEwc3dl9zP1MjS08PAwoRAVtSBw8LBUc3U0sLBwsHBwMDP1YTTzULS0sHCwcHAwM/VhD/T0sLBwsHBwMDP1YRXwMTU0c7DQV1C10DCUMDBQkDBQELAwMDDW87QWUejP9GiP8igP1rCxYvdwsXOxMPBgkHAwMDDW87QWUejP9GiP8igP1rCxYvdwsXOxMPBgtXAws7T2YU/xNzP0UmUzY4/19rAhNPV1dmjP8ffQ9fFw8HMw9fOwMGG0N3AwMTBzJ8/yODAwUPXxcPBzMPXzsDBhtDdwMDEwcyfP0TBwJXDwUHBwMHAgsDEy8BAjsGOQUDfwJjBwEGAwN1CocGZP9fCQoE/5UDCUMDBQkDBQELAwMDDW87QWUejP9GiP8igP1rCxYvdwsXOxMPBgkHAwMDDW87QWUejP9GiP8igP1rCxYvdwsXOxMPBgtXAws7T2YU/xNzP0UmUzY4/19rAhNPV1dmjP8ffQ9fFw8HMw9fOwMGG0N3AwMTBzJ8/yODAwUPXxcPBzMPXzsDBhtDdwMDEwcyfP8XBQcFBwMCPP8CCP8DX0M7B4UDA1sDAweDAwM/A1sCN1MDA1dbAwMHewMDPwNbAjdbAwdXAgFDRQsDCwMGH1tXEw8KMwcDQ08LAwYdHR0JTU9BQQEDEQNDaxVrXjMrW25jBRcKVP9CCQsNDxcGVwUXClT/QgsHEwNDaxVrXjMrW25g/xcDAQNDaxVrXjMrW25jBRcKVP9CCwMDQxMDQ2sVa14zK1tuYUFBVQEDEQNDaxVrXjMrW25jBRcKVP9CCQsNDxcGVwUXClT/QgsHEwNDaxVrXjMrW25g/xcDAQNDaxVrXjMrW25jBRcKVP9CCQMDXxMDQ2sVa14zK1tuYQcBBxdPDwZXVw0PFwZXBRcKVP9CCP8fPwpo/x9XPwMBA0NrFWteMytbbmMFFwpU/0ILA0FTA0MTA0NrFWteMytbbmNDA18TA0NrFWteMytbbmD9P0NRVQEDEQNDaxVrXjMrW25jBRcKVP9CCQsNDxcGVwUXClT/QgsHEwNDaxVrXjMrW25g/xcDAQNDaxVrXjMrW25jBRcKVP9CCQMDXxMDQ2sVa14zK1tuYQcBBxdPDwZXVw0PFwZXBRcKVP9CCP8fPwprG0NLVz8DAQNDaxVrXjMrW25jBRcKVP9CCQdJQz0DAQcXTw8GV1cNDxcGVwUXClT/Qgj/CwMDF08PBlUDPU8DPw0PFwZXBRcKVP9CCP8/AQcXTw8GV1cNDxcGVwUXClT/QgsLPwMBRxc3DwZU/zcXTw8GVP0DTQdBUwNDEwNDaxVrXjMrW25jQwNfEwNDaxVrXjMrW25g/0dBQgz/BwNfEwNDaxVrXjMrW25hJ01LQ0cBAxkDVQ9LJis7Czo7E1cGRwcbA1UPSyYrOws6OP9DQ1cBAxkDVQ9LJis7Czo7E1cGRwcbA1UPSyYrOws6O0dNCUFVAQMRA0NrFWteMytbbmMFFwpU/0IJCw0PFwZXBRcKVP9CCwcTA0NrFWteMytbbmD/FwMBA0NrFWteMytbbmMFFwpU/0IJAwNfEwNDaxVrXjMrW25hBwEHF08PBldXDQ8XBlcFFwpU/0II/x8/Cmj/H1c/AwEDQ2sVa14zK1tuYwUXClT/QgkDQUIM/wcDXxMDQ2sVa14zK1tuYRNBBwMDF08PBlcLAwcXTw8GVydDVwEDGQNVD0smKzsLOjsTVwZHBxsDVQ9LJis7Czo5BwNDR3ElDwMLBS9bpwIM/1IvAwMDB1JU/wYBXUMCAP8zWStRQgT/GgT/QgD9QwkXQ1FVAQMRA0NrFWteMytbbmMFFwpU/0IJCw0PFwZXBRcKVP9CCwcTA0NrFWteMytbbmD/FwMBA0NrFWteMytbbmMFFwpU/0IJAwNfEwNDaxVrXjMrW25hBwEHF08PBldXDQ8XBlcFFwpU/0II/x8/Cmj/E0MTV08DAQNDaxVrXjMrW25jBRcKVP9CCRsJP0MTV08DAQNDaxVrXjMrW25jBRcKVP9CCP8bQ0tXPwMBA0NrFWteMytbbmMFFwpU/0ILawcjAyYU/RcBC0sDSQsDZx8DCwtTfo1DA2N/Blz/ZktTSUs9PzsKBxdfCwOHVw8LNgsXCws3R1saCw8DP0dvfw8LB38LSwZJRUkVCUkRDwMBBwcFTx8DA0snBoj/Ylz9Mx9+P1Y0/RNJDwMCC1cLAwMXB2tLDyl7DwcDUgUHAQMHAwtCnX9vUwcDAy8TUmNTbQIA/3MHAwMvE1JhKjsDBwMDAn0DSwsFXwEGBwsHCwd6UVsLBgMLCQIDWwuHKyorBzUDRQtTV1NyCVNTA0sCBQMHAwMDBwJTBwFHJQYHgwsKVwMFk0t6WXc/awtHG3s1W0kDAwcDAzNebw8JAwMHCQILXjsbYxYPC0lTCwMBBitedzcDAwEzT54XU097O5MHPw8PAwIPSyM/AwMHZ0NzQwMDX2sTQ28HjwFeBwdTWydDa5ZhCgMGBP8HF1dnOgz9FxEPAwMzW2Vuj08LD3c7AwNjXwVvd2YDLzt/VQJXokVLBQdTGwQA/1MzBwMTPzZ1H1FXBwszBwMTPzZ0/wcEAP9TMwcDEz82dwMDE18HBz8tFz9SdQMBa1UDBwMLQp1/b1MHAwMvE1JjU20CAP9zBwMDLxNSY1KBZ1F7DwcDUgUHAQMHAwtCnX9vUwcDAy8TUmNTbQIA/3MHAwMvE1JhKjsDBwMDAn1fQ1djAwsqbyZU/0NJDwMCC1cLAwMXB2tLDyl7DwcDUgUHAQMHAwtCnX9vUwcDAy8TUmNTbQIA/3MHAwMvE1JhKjsDBwMDAn8DFQtPQnNHTwpw/RdFNw8DAw8ffj0HAwMLT18/BytjIytSTP8JDwMBBwcFTx8DA0snBoj/Ylz9Mx9+P1Y0/RNJDwMCC1cLAwMXB2tLDyl7DwcDUgUHAQMHAwtCnX9vUwcDAy8TUmNTbQIA/3MHAwMvE1JhKjsDBwMDAn0DSwsFXwEGBwsHCwd6UVsLBgMLCQIDWwuHKyorBzUDRQtTV1NyCVNTA0sCBQMHAwMDBwJTBwFHJQYHgwsKVwMFk0t6WXc/awtHG3s1W0kDAwcDAzNebw8JAwMHCQILXjsbYxYPC0lTCwMBBitedzcDAwEzT54XU097O5MHPw8PAwIPSyM/AwMHZ0NzQwMDX2sTQ28HjwFeBwdTWydDa5ZhCgMGBSMNU0UGBwMGBP8DR2oHAw8fR2oE/RsdMx9LUwIPU1MDDwsJEwMCN1NbBxsHR1M7Qi0nCy4ZIyNTUwsLWweODwMHEhMFFQsbS1NbBwcfEAD/UzMHAxM/NnUTI0cbWlkHFxNHOxFDBwYLBws+CP8DSTMDI1daCP1bC0YJDwMCZxoI/RsPU1UnAwY9AkE+TxsDB0diWyMRUxMaBwJZHQ1FRR0NPwtBBQmDRwdLTxdvEgD/Agz/ZgD/Zgj9VwcDA28vBzF3AwYbAweHS0cDAmsPAwMHdlVdUQU9awc/DysPJhj/PwdvDysPJhj9dz8KAzMDAgj/Qz8HB28PKw8mGP87Q1cHB28PKw8mGP07VRcPUjz/Yz8HB28PKw8mGwcDA1NjQz8JcxcHBwcLYqtbD3cnCxsXY2t/KitBOxERMycHAwNxQgz/excHN3NbNwcDI25XPgD/AwEHAwIbY28qaP0DDxsXZ1sCgwo0/xsdQ1c/B1sPFw8KNz9XRwdbDxcPCjT9H0VLD0cHAwNvLwcxdwMGGwMHh0tHAwJo/0ENPwtBBQmDRwdLTxdvEgD/Agz/ZgD/Zgj9VwcDA28vBzF3AwYbAweHS0cDAmsPAwMHdlVdUQU9awc/DysPJhj/PwdvDysPJhj9dz8KAzMDAgj/Qz8HB28PKw8mGP87Q1cHB28PKw8mGP07VRcPUjz/Yz8HB28PKw8mGwcDA1NjQz8JcxcHBwcLYqtbD3cnCxsXY2t/KitDC0NDBwMDZwNLV6E7Nw8TAjcLOzsTAjT9SUEJSREPAwEHBwVPHwMDSycGiP9iXP0zH34/VjT9E0kPAwILVwsDAxcHa0sPKXsPBwNSBQcBAwcDC0Kdf29TBwMDLxNSY1NtAgD/cwcDAy8TUmEqOwMHAwMCfQNLCwVfAQYHCwcLB3pRWwsGAwsJAgNbC4crKisHNQNFC1NXU3IJU1MDSwIFAwcDAwMHAlMHAUclBgeDCwpXAwWTS3pZdz9rC0cbezVbSQMDBwMDM15vDwkDAwcJAgteOxtjFg8LSVMLAwEGK153NwMDATNPnhdTT3s7kwc/Dw8DAg9LIz8DAwdnQ3NDAwNfaxNDbwePAV4HB1NbJ0NrlmEKAwYE/wcXV2c6DP0XEQ8DAzNbZW6PTwsPdzsDA2NfBW93ZgMvO39VAleiRUsFB1MbBAD/UzMHAxM/NnUfUVcHCzMHAxM/NnT/BwQA/1MzBwMTPzZ3AwMTXwcHPy0XP1J1AwFrVQMHAwtCnX9vUwcDAy8TUmNTbQIA/3MHAwMvE1JjUoFnUXsPBwNSBQcBAwcDC0Kdf29TBwMDLxNSY1NtAgD/cwcDAy8TUmEqOwMHAwMCfV9DV2MDCypvJlT/Q0kPAwILVwsDAxcHa0sPKXsPBwNSBQcBAwcDC0Kdf29TBwMDLxNSY1NtAgD/cwcDAy8TUmEqOwMHAwMCfwMVC09Cc0dPCnD9F0U3DwMDDx9+PQcDAwtPXz8HK2MjK1JM/wkPAwEHBwVPHwMDSycGiP9iXP0zH34/VjT9E0kPAwILVwsDAxcHa0sPKXsPBwNSBQcBAwcDC0Kdf29TBwMDLxNSY1NtAgD/cwcDAy8TUmEqOwMHAwMCfQNLCwVfAQYHCwcLB3pRWwsGAwsJAgNbC4crKisHNQNFC1NXU3IJU1MDSwIFAwcDAwMHAlMHAUclBgeDCwpXAwWTS3pZdz9rC0cbezVbSQMDBwMDM15vDwkDAwcJAgteOxtjFg8LSVMLAwEGK153NwMDATNPnhdTT3s7kwc/Dw8DAg9LIz8DAwdnQ3NDAwNfaxNDbwePAV4HB1NbJ0NrlmEKAwYFIw1TRQYHAwYE/wNHagcDDx9HagVFFT03DwMDDx9+PQcDAwtPXz8HK2MjK1JM/wkPAwEHBwVPHwMDSycGiP9iXP0zH34/VjT9E0kPAwILVwsDAxcHa0sPKXsPBwNSBQcBAwcDC0Kdf29TBwMDLxNSY1NtAgD/cwcDAy8TUmEqOwMHAwMCfQNLCwVfAQYHCwcLB3pRWwsGAwsJAgNbC4crKisHNQNFC1NXU3IJU1MDSwIFAwcDAwMHAlMHAUclBgeDCwpXAwWTS3pZdz9rC0cbezVbSQMDBwMDM15vDwkDAwcJAgteOxtjFg8LSVMLAwEGK153NwMDATNPnhdTT3s7kwc/Dw8DAg9LIz8DAwdnQ3NDAwNfaxNDbwePAV4HB1NbJ0NrlmEKAwYE/Qc5Ow0DRQtTV1NyCVNTA0sCBQMHAwMDBwJTBwFHJQYHgwsKVwMFk0t6WXc/awtHG3s1W0kDAwcDAzNebw8JAwMHCQILXjsbYxYPC0lTCwMBBitedzcDAwEzT54XU097O5MHPw8PAwIPSyM/AwMHZ0NzQwMDX2sTQ28HjwFeBwdTWydDa5ZhCgMGBxMFBxszYxoDN2cfAwYbGwlTBwMBFgNDXxcHa0sPKXsPBwNSBQcBAwcDC0Kdf29TBwMDLxNSY1NtAgD/cwcDAy8TUmEqOwMHAwMCfP87SQ8DAgtXCwMDFwdrSw8pew8HA1IFBwEDBwMLQp1/b1MHAwMvE1JjU20CAP9zBwMDLxNSYSo7AwcDAwJ9DR0NPwtBBQmDRwdLTxdvEgD/Agz/ZgD/Zgj9VwcDA28vBzF3AwYbAweHS0cDAmsPAwMHdlVdUQU9awc/DysPJhj/PwdvDysPJhj9dz8KAzMDAgj/Qz8HB28PKw8mGP87Q1cHB28PKw8mGP07VRcPUjz/Yz8HB28PKw8mGwcDA1NjQz8JcxcHBwcLYqtbD3cnCxsXY2t/KitBOxERMycHAwNxQgz/excHN3NbNwcDI25XPgD/AwEHAwIbY28qaP0DDxsXZ1sCgwo0/RMPBgEHAwMLTgkCDQoPIgz/HxU7CQ8DAzNbZW6PTwsPdzsDA2NfBW93ZgMvO39VAleiRUsFB1MbBAD/UzMHAxM/NnUfUVcHCzMHAxM/NnT/BwQA/1MzBwMTPzZ3AwMTXwcHPy0XP1J1AwFrVQMHAwtCnX9vUwcDAy8TUmNTbQIA/3MHAwMvE1JjUoFnUXsPBwNSBQcBAwcDC0Kdf29TBwMDLxNSY1NtAgD/cwcDAy8TUmEqOwMHAwMCfV9DV2MDCypvJlT/Q0kPAwILVwsDAxcHa0sPKXsPBwNSBQcBAwcDC0Kdf29TBwMDLxNSY1NtAgD/cwcDAy8TUmEqOwMHAwMCfUcJQwcDARYDQ18XB2tLDyl7DwcDUgUHAQMHAwtCnX9vUwcDAy8TUmNTbQIA/3MHAwMvE1JhKjsDBwMDAnz/PwUHUxsEAP9TMwcDEz82dR9RVwcLMwcDEz82dP8HBAD/UzMHAxM/NncDAxNfBwc/LRc/UnUDAWtVAwcDC0Kdf29TBwMDLxNSY1NtAgD/cwcDAy8TUmNSgWdRew8HA1IFBwEDBwMLQp1/b1MHAwMvE1JjU20CAP9zBwMDLxNSYSo7AwcDAwJ9X0NXYwMLKm8mVP8/CVMHAwEWA0NfFwdrSw8pew8HA1IFBwEDBwMLQp1/b1MHAwMvE1JjU20CAP9zBwMDLxNSYSo7AwcDAwJ8/ztJDwMCC1cLAwMXB2tLDyl7DwcDUgUHAQMHAwtCnX9vUwcDAy8TUmNTbQIA/3MHAwMvE1JhKjsDBwMDAn0XPT9FCw0DAWY4/08ffj8PBwMDBlT/Tw8DAw8ffj0LRQcDAhD9PQ8DAQcHBU8fAwNLJwaI/2Jc/TMffj9WNP0TSQ8DAgtXCwMDFwdrSw8pew8HA1IFBwEDBwMLQp1/b1MHAwMvE1JjU20CAP9zBwMDLxNSYSo7AwcDAwJ9A0sLBV8BBgcLBwsHelFbCwYDCwkCA1sLhysqKwc1A0ULU1dTcglTUwNLAgUDBwMDAwcCUwcBRyUGB4MLClcDBZNLell3P2sLRxt7NVtJAwMHAwMzXm8PCQMDBwkCC147G2MWDwtJUwsDAQYrXnc3AwMBM0+eF1NPezuTBz8PDwMCD0sjPwMDB2dDc0MDA19rE0NvB48BXgcHU1snQ2uWYQoDBgc7DQMBZjj/Tx9+Pw8HAwMGVP9DRTcPAwMPH349BwMDC09fPwcrYyMrUkz/CQ8DAQcHBU8fAwNLJwaI/2Jc/TMffj9WNP0TSQ8DAgtXCwMDFwdrSw8pew8HA1IFBwEDBwMLQp1/b1MHAwMvE1JjU20CAP9zBwMDLxNSYSo7AwcDAwJ9A0sLBV8BBgcLBwsHelFbCwYDCwkCA1sLhysqKwc1A0ULU1dTcglTUwNLAgUDBwMDAwcCUwcBRyUGB4MLClcDBZNLell3P2sLRxt7NVtJAwMHAwMzXm8PCQMDBwkCC147G2MWDwtJUwsDAQYrXnc3AwMBM0+eF1NPezuTBz8PDwMCD0sjPwMDB2dDc0MDA19rE0NvB48BXgcHU1snQ2uWYQoDBgULQRsVT09DAQIPBgz/Q01XAQIPBg8DA14M/RNFBwMCEP09DwMBBwcFTx8DA0snBoj/Ylz9Mx9+P1Y0/RNJDwMCC1cLAwMXB2tLDyl7DwcDUgUHAQMHAwtCnX9vUwcDAy8TUmNTbQIA/3MHAwMvE1JhKjsDBwMDAn0DSwsFXwEGBwsHCwd6UVsLBgMLCQIDWwuHKyorBzUDRQtTV1NyCVNTA0sCBQMHAwMDBwJTBwFHJQYHgwsKVwMFk0t6WXc/awtHG3s1W0kDAwcDAzNebw8JAwMHCQILXjsbYxYPC0lTCwMBBitedzcDAwEzT54XU097O5MHPw8PAwIPSyM/AwMHZ0NzQwMDX2sTQ28HjwFeBwdTWydDa5ZhCgMGBzsNAwFmOP9PH34/DwcDAwZVRUkRDwMBBwcFTx8DA0snBoj/Ylz9Mx9+P1Y0/RNJDwMCC1cLAwMXB2tLDyl7DwcDUgUHAQMHAwtCnX9vUwcDAy8TUmNTbQIA/3MHAwMvE1JhKjsDBwMDAn0DSwsFXwEGBwsHCwd6UVsLBgMLCQIDWwuHKyorBzUDRQtTV1NyCVNTA0sCBQMHAwMDBwJTBwFHJQYHgwsKVwMFk0t6WXc/awtHG3s1W0kDAwcDAzNebw8JAwMHCQILXjsbYxYPC0lTCwMBBitedzcDAwEzT54XU097O5MHPw8PAwIPSyM/AwMHZ0NzQwMDX2sTQ28HjwFeBwdTWydDa5ZhCgMGBP8HF1dnOgz9FxEPAwMzW2Vuj08LD3c7AwNjXwVvd2YDLzt/VQJXokVLBQdTGwQA/1MzBwMTPzZ1H1FXBwszBwMTPzZ0/wcEAP9TMwcDEz82dwMDE18HBz8tFz9SdQMBa1UDBwMLQp1/b1MHAwMvE1JjU20CAP9zBwMDLxNSY1KBZ1F7DwcDUgUHAQMHAwtCnX9vUwcDAy8TUmNTbQIA/3MHAwMvE1JhKjsDBwMDAn1fQ1djAwsqbyZU/0NJDwMCC1cLAwMXB2tLDyl7DwcDUgUHAQMHAwtCnX9vUwcDAy8TUmNTbQIA/3MHAwMvE1JhKjsDBwMDAn8DFQtPQnNHTwpzBxtLU1sHBx8QAP9TMwcDEz82dSsNRkFPQwY8/wNDYj0fQUcXC01XAQIPBg8DA14PCxVPT0MBAg8GDP9DTVcBAg8GDwMDXg0SDwMPJ0NiPRUFQREDBSNDZVVbJw4xVwcGNP9jNQsHR18WH0oLU2NLPyMDAjT9YzkGI0ojGxdmTScHV0MFVVsnDjFXBwY0/2M1CwdHXxYfSgtTY0s/IwMCNP8fI1dfA0cLAxMzagNJAx8nZ2cCVycfV2cGFQMRIwdfQz1VWycOMVcHBjT/YzULB0dfFh9KC1NjSz8jAwI3PwUjQ2VVWycOMVcHBjT/YzULB0dfFh9KC1NjSz8jAwI0/WM5BiNKIxsXZkz9HxFLI0NfA0cLAxMzagD/PyNXXwNHCwMTM2oDGhFLQSMnU1cCBwUDHydnZwJXJx9XZwYU/Q4LF0FHAwYg/wUDBSNDZVVbJw4xVwcGNP9jNQsHR18WH0oLU2NLPyMDAjT9YzkGI0ojGxdmTScHV0MFVVsnDjFXBwY0/2M1CwdHXxYfSgtTY0s/IwMCNP8fI1dfA0cLAxMzagD9KwVPEQc9QmFfaxsrAwc/NwcbClMXBz5M/0cTGgVDEVMTGgcCWxcTSwVDQwVVWycOMVcHBjT/YzULB0dfFh9KC1NjSz8jAwI3B0M9VVsnDjFXBwY0/2M1CwdHXxYfSgtTY0s/IwMCNSMFTw9HVScDBj0CQT5PGwMHR2JbQw9TVScDBj0CQT5PGwMHR2JZLxNPMz8DAwMDR083UwNTEz8PCwMCAycHTxMzDwpIA-2894-A=");

